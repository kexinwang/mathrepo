#!/usr/bin/env python3
#
# generate AZ.rst file
#

import os, sys
import re

from common import *

#
# main function
#
def main () :
        
    #
    # start with DB of all index files
    #
    DB = build_indexdb()

    #
    # build list of titles and corresponding file name
    #

    titles = [ ( filename, DB[filename]['title'] ) for filename in DB.keys() ]
    
    #
    # generate AZ.rst
    #

    # sort entries according to title
    titles   = sorted( titles, key = lambda entry : canonical_title( entry[1] ).lower() )

    filename = os.path.join( 'source', 'AZ.rst' )
    
    with open( filename, 'w' ) as azfile :
        azfile.write( '####################\n' )
        azfile.write( 'A - Z\n' )
        azfile.write( '####################\n' )
        azfile.write( '\n' )
    
        for entry in titles :
            # print( entry[1] )
            dirname = os.path.dirname( entry[0] )
            # remove everything before and including 'source'
            pos = dirname.find( 'source' )
            dirname = dirname[pos+7:]
            azfile.write( '- :doc:`{}/index`\n'.format( dirname ) )
            azfile.write( '\n' )

        azfile.write( '\n' )
        azfile.write( '..\n' )
        azfile.write( '\n' )
        azfile.write( '.. toctree::\n' )
        azfile.write( '   :maxdepth: 0\n' )
        
if __name__ == '__main__':
    main()
