#!/usr/bin/env python3
#
# generate year files for all index.rst based on git log entries
#

import pdb
import os
import git
import re

from common     import get_indexdb
from termcolors import colors
from datetime   import datetime

monthnames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]

#
# main function
#
def main () :

    #
    # search for index.rst files
    #
    indexdb = get_indexdb()


    #
    # collect datetime of first commit for each file
    #
    
    repo   = git.repo.Repo( "." )
    datedb = {}

    # regular expression for date: dd/mm/yyyy
    date_re = re.compile( '(\d+)[/-](\d+)[/-](\d+)', re.IGNORECASE )

    # things to remove
    rem_re = re.compile( '\.|:raw-html:`[\w<>/ ]+`' )

    today = datetime.now()

    for indexfile in indexdb :

        #
        # first try to read date from file
        #

        found_date = False
        
        with open( indexfile, 'r' ) as index :

            for line in index :
                
                pos = line.find( 'Project page created:' )
                if pos >= 0 :
                    # start after 'Project page created:'
                    date = line[pos+21:]
                    # remove known stuff
                    res = rem_re.search( date )
                    while res != None :
                        date = date.replace( res.group(), '' )
                        res  = rem_re.search( date )
                    # remove whitespaces
                    date = date.lstrip().rstrip()
                    # remove all known things like version, etc.
                    res  = date_re.search( date )
                    if res != None :
                        d = int( res.group( 1 ) )
                        m = int( res.group( 2 ) )
                        y = int( res.group( 3 ) )

                        # some consistency tests
                        if d < 1 or d > 31 :
                            print( '{}error: day in date "{}" in file {} out of range{}'.format( colors['red'], date, indexfile, colors['reset'] ) )
                            break

                        if m < 1 or m > 12 :
                            print( '{}error: month in date "{}" in file {} out of range{}'.format( colors['red'], date, indexfile, colors['reset'] ) )
                            break
                            
                        if y < 2000 or y > today.year :
                            print( '{}error: year in date "{}" in file {} out of range{}'.format( colors['red'], date, indexfile, colors['reset'] ) )
                            break
                            
                        if not y in datedb :
                            datedb[y] = []

                        datedb[y].append( [ indexfile, datetime( year = y, month = m, day = d ) ] )
                        found_date = True
                        break
                    else :
                        print( '{}warning{}: unsupported date "{}" in file "{}"'.format( colors['yellow'], colors['reset'], date, indexfile ) )
                        
                
        #
        # if no date was specified, try to get it from Git directly
        #

        if not found_date :
            w = repo.iter_commits( paths = [indexfile, ] )
            l = list(w)

            if l != None and l != [] :

                # last entry in list is first commit
                c = l[-1]
                d = c.authored_datetime
                y = d.year

                if not y in datedb :
                    datedb[y] = []

                datedb[y].append( [ indexfile, d ] )
                # print( f"{p} first created {c.authored_datetime} by {c.author}" )
            else:
                print( '{}error: file {} not in Git repository{}'.format( colors['red'], indexfile, colors['reset'] ) )

    #
    # write file for each year
    #

    # reverse sort for 'years.rst' below
    years = sorted( list( datedb.keys() ), reverse = True )

    # sort entries in year
    for year in years :
        datedb[year] = sorted( datedb[year], key = lambda entry : entry[1].timestamp(), reverse = True )
    
    # for year in years :

    #     entrylist = datedb[year]
    #     filename  = os.path.join( 'source', '%d.rst' % year )
        
    #     with open( filename, 'w' ) as outfile :
    #         outfile.write( '####################\n' )
    #         outfile.write( '%d\n' % year )
    #         outfile.write( '####################\n' )
    #         outfile.write( '\n' )
    #         outfile.write( '.. toctree::\n' )
    #         outfile.write( '   :maxdepth: 1\n' )
    #         outfile.write( '\n' )
    
    #         for entry in entrylist :
    #             # remove leading 'source/'
    #             indexfile = entry[0]
    #             pos       = indexfile.find( 'source' )
    #             indexfile = indexfile[pos+7:]
    #             outfile.write( '   {}\n'.format( indexfile ) )

    #         outfile.write( '\n' )
    #         outfile.write( '..\n' )
        
    filename = os.path.join( 'source', 'years.rst' )
    with open( filename, 'w' ) as outfile :
        outfile.write( '####################\n' )
        outfile.write( 'By Year\n' )
        outfile.write( '####################\n' )
        outfile.write( '\n' )
        outfile.write( '.. toctree::\n' )
        outfile.write( '   :maxdepth: 1\n' )
        outfile.write( '\n' )

        # for year in years :
        #     for entry in datedb[year] :
        #         indexfile = entry[0]
        #         pos       = indexfile.find( 'source' )
        #         indexfile = indexfile[pos+7:]
        #         outfile.write( '   {}\n'.format( indexfile ) )

        # for year in years :
        #     outfile.write( '  {}\n'.format( year ) )

        outfile.write( '\n' )
        
        for year in years :
            outfile.write( '{}\n'.format( year ) )
            outfile.write( ('{:-<%d}\n' % len( str(year) ) ).format( '' ) )
            
            entrylist = datedb[year]
            
            for entry in entrylist :
                # remove leading 'source/'
                indexfile = os.path.dirname( entry[0] )
                pos       = indexfile.find( 'source' )
                indexfile = indexfile[pos+7:]
                # outfile.write( '  - :doc:`{}/index` ({}. {})\n'.format( indexfile, entry[1].day, monthnames[ entry[1].month-1 ],  ) )
                outfile.write( '  - :doc:`{}/index`\n'.format( indexfile ) )
            
            outfile.write( '\n' )
            
        outfile.write( '..\n' )
        
if __name__ == '__main__':
    main()

