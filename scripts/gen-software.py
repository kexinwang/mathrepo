#!/usr/bin/env python3
#
# generate AZ.rst file
#

import pdb

import os

from common     import *
from termcolors import colors

#
# main function
#
def main () :

    #
    # start with DB of all index files
    #
    DB = build_indexdb()
        
    #
    # build mapping from software to file
    #

    softwaredb = {}
    
    for filename in DB.keys() :

        software = None

        for sw in DB[filename]['software'] : 
            if not sw in softwaredb :
                softwaredb[sw] = []
            softwaredb[sw].append( filename )
                
    #
    # generate software.rst
    #

    # sort entries according to name
    softwarelist = sorted( softwaredb.keys(), key = str.lower )

    filename = os.path.join( 'source', 'software.rst' )
    
    with open( filename, 'w' ) as outfile :
        outfile.write( '####################\n' )
        outfile.write( 'By Software\n' )
        outfile.write( '####################\n' )
        outfile.write( '\n' )

        for sw in softwarelist :
            outfile.write( sw + '\n' )
            outfile.write( ('{:-<%d}\n' % len( sw ) ).format( '' ) )

            files = sorted( softwaredb[sw], key = lambda x : canonical_title( DB[x]['title'] ).lower() )
            
            for filename in files :
                dirname = os.path.dirname( filename )
                
                # remove everything before and including 'source'
                pos     = dirname.find( 'source' )
                dirname = dirname[pos+7:]
                outfile.write( '  - :doc:`{}/index`\n'.format( dirname ) )
            outfile.write( '\n' )

        outfile.write( '\n' )
        outfile.write( '..\n' )
        outfile.write( '\n' )
        outfile.write( '.. toctree::\n' )
        outfile.write( '   :maxdepth: 0\n' )
        
if __name__ == '__main__':
    main()
