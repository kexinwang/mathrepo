#!/usr/bin/env python3
#
# generate AZ.rst file
#

import pdb

import os

from common     import *
from termcolors import colors

#
# return cleaned up surname for sorting
#
def surname ( name, lowercase=True ) :

    surname = ""
    # deal with multiple last names provided in curly brackets
    if '{' in name :
        start = name.find('{')
        end = name.find('}', start)
        if start != -1 and end != -1:
            surname = name[start+1:end]
    
    else:
        surname = name.split( ' ' )[-1]

    surname = surname.replace( 'Ç', 'C' )
    
    return surname.lower() if lowercase else surname


def first_names( name ) :

    first_names = ""
    if '{' in name :
        first_names = name.split( '{' )[0]
    else :
        first_names = ' '.join( name.split( ' ' )[0:-1] )

    return first_names


#
# main function
#
def main () :

    #
    # start with DB of all index files
    #
    DB = build_indexdb()
        
    #
    # extract authors from each index file
    #

    # things to remove
    rem_re = re.compile( '\(\w*\)|:raw-html:`[\w<>/ ]+`' )
    
    authordb = {}

    for filename in DB.keys() :

        for author in DB[filename]['authors'] :
            if not author in authordb :
                authordb[author] = []
            authordb[author].append( filename )
                
    #
    # generate authors.rst
    #

    # sort entries according to surname (which is assumed to be last part of name)
    authorlist = sorted( authordb.keys(), key = surname )

    filename = os.path.join( 'source', 'authors.rst' )
    
    with open( filename, 'w' ) as authfile :
        authfile.write( '####################\n' )
        authfile.write( 'By Author\n' )
        authfile.write( '####################\n' )
        authfile.write( '\n' )
    
        for author in authorlist :
            authfile.write( '.. raw:: html\n' )
            # authfile.write( '\n  <h4>%s, %s</h4>\n\n' % ( author.split( ' ' )[-1], ' '.join( author.split( ' ' )[0:-1] ) ) )
            authfile.write( '\n  <h4>%s, %s</h4>\n\n' % ( surname(author, lowercase=False), first_names(author) ) )
            # authfile.write( author + '\n' )
            # authfile.write( ('{:-<%d}\n' % len( author ) ).format( '' ) )

            files = sorted( authordb[author], key = lambda x : canonical_title( DB[x]['title'] ).lower() )
            
            for filename in files :
                dirname = os.path.dirname( filename )
                # remove everything before and including 'source'
                pos = dirname.find( 'source' )
                dirname = dirname[pos+7:]
                authfile.write( '- :doc:`{}/index`\n'.format( dirname ) )
            authfile.write( '\n' )

        authfile.write( '\n' )
        authfile.write( '..\n' )
        authfile.write( '\n' )
        authfile.write( '.. toctree::\n' )
        authfile.write( '   :maxdepth: 0\n' )
        
if __name__ == '__main__':
    main()
