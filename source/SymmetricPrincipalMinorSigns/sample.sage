#!/usr/bin/env sage

# Sample rational symmetric matrices with bounded numerator and denominator
# and `1`s on the diagonal. Record their sign patterns.

import sys
import argparse

parser = argparse.ArgumentParser(description='Sample for representations of sign patterns.')
parser.add_argument('dim', type=int)
parser.add_argument('-t', '--tries', default=1, type=int)
parser.add_argument('-n', '--numer-bound', default=35, type=int)
parser.add_argument('-d', '--denom-bound', default=35, type=int)
parser.add_argument('-b', '--break-early', action='store_true')
parser.add_argument('-p', '--pattern', default=None, type=str)
args = parser.parse_args()

N = args.dim
TRIES = args.tries
NUMER = args.numer_bound
DENOM = args.denom_bound
break_early = args.break_early
pattern = args.pattern
# Extract the signs for 1x1 and 2x2 minors in order.
if pattern != None:
    p1 = [pattern[i+1] for i in range(N)]
    p2 = [pattern[N + (2*N - i - 1)*i / 2 + (j - i)] for i in range(N) for j in range(i+1, N)]

Mat = MatrixSpace(QQ, N, N)
zero_matrix = Mat.zero_matrix()

# We always put `1`s on the diagonal. It is shown in the paper that every
# hyperoctahedral orbit can be represented by such a matrix, if it can be
# represented at all.
def random_matrix():
    A = copy(zero_matrix)
    for i in range(N):
        for j in range(i+1, N):
            A[i,j] = QQ.random_element(NUMER, DENOM)
            A[j,i] = A[i,j]
        A[i,i] = 1
    return A

# Make a better guess for a fixed pattern s: the diagonal is set to the
# signs indicated by 1x1 minors and the signs of 2x2 minors dictate for
# every off-diagonal entry whether its absolute value should be less
# than or greater than 1.
def random_matrix_for(p1, p2):
    A = copy(zero_matrix)
    k = 0
    for i in range(N):
        for j in range(i+1, N):
            A[i,j] = QQ.random_element(NUMER, DENOM)
            if p1[i] == p1[j] and p2[k] == '+' and abs(A[i,j]) > 1:
                A[i,j] = 1 / A[i,j]
            A[j,i] = A[i,j]
            k += 1
        A[i,i] = 1 if p1[i] == '+' else -1
    return A

def signstr(x):
    if x > 0:
        return '+'
    elif x < 0:
        return '-'
    else:
        return 'Z'

def sign_pattern(A):
    out = []
    for k in range(N+1):
        for J in Subsets(range(N),k):
            submat = A.matrix_from_rows_and_columns(J,J)
            det = submat.determinant()
            sign = signstr(det)
            out.append(sign)
    return "".join(out)

# We want to retain only the smallest matrices representing a given sign
# pattern. This is measured by squaring all numerators and denominators
# in the matrix. We want all of them to be as small as possible.
def complexity(A):
    return sum(flatten([(A[i,j].numer()^2, A[i,j].denom()^2) for i in range(N) for j in range(i+1,N)]))

witnesses = {}
for i in range(TRIES):
    A = random_matrix_for(p1, p2) if pattern != None else random_matrix()
    pat = sign_pattern(A)
    if 'Z' in pat: # A is not principally regular
        continue
    if pattern != None and pat != pattern:
        continue
    a = complexity(A)
    if pat in witnesses:
        witnesses[pat][0] += 1
        b, B = witnesses[pat][1]
        if a < b:
            witnesses[pat][1] = [a, A]
    else:
        witnesses[pat] = [1, [a, A]]
    if pattern != None and pat == pattern and break_early:
        break

# Print a machine-readable summary of the findings
for k, v in sorted(witnesses.items()):
    c = v[0]
    A = v[1][1]
    entries = [A[i,j] for i in range(N) for j in range(i,N)]
    print("{:s}: {:d} [{:s}]".format(k, c, ", ".join([str(q) for q in entries])))
