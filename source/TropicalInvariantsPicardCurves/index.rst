=================================================================================
Tropical invariants for binary forms and reduction types of Picard curves
=================================================================================


| This page contains auxiliary files to the paper:
| Paul Alexander Helminck, Yassine El Maazouz and Enis Kaya: Tropical invariants for binary quintics and reduction types of Picard curves
| In: Glasgow mathematical journal, 66 (2024) 1, p. 65-87
| DOI: `10.1017/S0017089523000344 <https://dx.doi.org/10.1017/S0017089523000344>`_ ARXIV: https://arxiv.org/abs/2206.00420   CODE: https://mathrepo.mis.mpg.de/TropicalInvariantsPicardCurves/index.html

   
Abstract: In this paper, we express the reduction types of Picard curves in terms of tropical invariants associated to binary forms. These invariants are connected to Picard modular forms using recent work by Clery and van der Geer. We furthermore give a general framework for tropical invariants associated to group actions on arbitrary varieties. The previous problem fits in this general framework by mapping the space of binary forms to symmetrized versions of the Deligne--Mumford compactification :math:`\overline{M}_{0,n}`. We conjecture that the techniques introduced here can be used to find tropical invariants for binary forms of any degree.



 .. image:: M_trop_05.jpg
  :width: 200
  :height: 150

.. image:: TypeII.png
  :width: 600
  :height: 200

.. image:: M_trop_041.jpg
  :width: 200
  :height: 150


.. image:: Degenerations.jpg
  :width: 200
  :height: 150


========================================================================================================================================================
This repository includes our code to prove Theorems 1 and 2 by computing the invariants and universal families of binary quintics and Picard curves.
========================================================================================================================================================
This repository includes our code to prove Theorems 1 and 2 by computing the invariants and universal families of binary quintics and Picard curves. Our code is written in `Sage <https://swmath.org/software/825>`_ (version 9.4)





.. toctree::
  :maxdepth: 1
  :glob:
  
  code1  
  code2
  code3


.. toctree::
   :maxdepth: 1

   ModularForms.rst





Project page created: 23/05/2022.

Project contributors: Paul Alexander Helminck, Yassine El Maazouz and Enis Kaya.

Corresponding author of this page: Yassine El Maazouz, yassine.el-maazouz@berkeley.edu

Software used: SageMath (version 9.4).
