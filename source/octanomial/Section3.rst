================================
Section 3: Arrangements of Trees
================================

In Theorem 3.4 we prove that the :math:`27` lines on a tropically smooth octanomial surface have distinct tropicalization.

We use normalized Plücker coordinates
where the first nonzero entry is :math:`1`. Nine of the lines
are identified by their zero coordinates. The other lines come in
six triplets. Each triplet is identified by its zero coordinates.
In the proof we use the following data:

 - the :download:`secondary cones <secondary_cones.txt>` of the 53 unimodular triangulations of the support of the surface;

 - the :download:`minimal polynomials <all_minpols.txt>` of the nonzero Plücker coordinates of the six triplets of lines.
