==============================================
Algebraic Geometry of Quantum Graphical Models
==============================================

| This page contains auxiliary files to the paper:
| Eliana Duarte, Dmitrii Pavlov, and Maximilian Wiesmann: Algebraic Geometry of Quantum Graphical Models
| ARXIV: `2308.11538 <https://arxiv.org/abs/2308.11538>`_

| ABSTRACT: Algebro-geometric methods have proven to be very successful in the study of graphical models in statistics. In this paper we introduce the foundations to carry out a similar study of their quantum counterparts. These quantum graphical models are families of quantum states satisfying certain locality or correlation conditions encoded by a graph. We lay out several ways to associate an algebraic variety to a quantum graphical model. The classical graphical models can be recovered from most of these varieties by restricting to quantum states represented by diagonal matrices. We study fundamental properties of these varieties and provide algorithms to compute their defining equations. Moreover, we study quantum information projections to quantum exponential families defined by graphs and prove a quantum analogue of Birch's Theorem.

The paper features several algorithms and computational examples that we implemented in `Julia  <https://julialang.org>`_ using the `Oscar <https://oscar.computeralgebra.de>`_ computer algebra and the `HomotopyContinuation <https://www.juliahomotopycontinuation.org>`_ numerical algebraic geometry package. 

In Example 3.5, the defining equations of the QCMI variety of the 3-chain graph are computed using Algorithm 1 and have been implemented in :download:`Example_3_5.jl<code/Example_3_5.jl>`. The resulting 735 equations can be found in :download:`QCMI_3_chain.txt<data/QCMI_3_chain.txt>`.

As demonstrated in Example 4.2, the Gibbs variety of a linear system of Hamiltonians associated to a graph is a computationally challenging object. In the case of the 3-chain graph, no linear or quadratic forms vanish on it. The code verifying this is :download:`Example_4_2.jl<code/Example_4_2.jl>`.

In contrast, the Gibbs variety of a unirational variety of decomposable tensors is a more feasible object. In the case of the 3-chain graph (Example 4.5), all its defining equations can be determined using :download:`Example_4_5.jl<code/Example_4_5.jl>`. The linear and quadratic equations can be found in :download:`linear_equations_GV.txt<data/linear_equations_GV.txt>` and :download:`quadratic_equations_GV.txt<data/quadratic_equations_GV.txt>`, respectively.

Algorithm 3 computes defining equations of the Gibbs variety of a family of commuting Hamiltonians. It is implemented in :download:`Algorithm_3.jl<code/Algorithm_3.jl>`. This code also contains the computation from Example 5.7 where the resulting equations can be found in :download:`equations_GV_Comm_Hams.txt<data/equations_GV_Comm_Hams.txt>`, as well as the computation of the quantum information projection from Example 5.12.

Project page created: 15/08/2023.

Project contributors: Eliana Duarte, Dmitrii Pavlov, Maximilian Wiesmann.

Corresponding author of this page: Maximilian Wiesmann, `wiesmann@mis.mpg.de <mailto:wiesmann@mis.mpg.de>`_.
 
Code written by: Dmitrii Pavlov, Maximilian Wiesmann

Software used: Julia (Version 1.9.1), Oscar (Version 0.12.1), HomotopyContinuation.jl (Version 2.8.1).

System setup used: MacBook Pro with macOS Monterey 12.5, Processor Apple M1 Pro, Memory 16 GB LPDDR5.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html).

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/).

Last updated 29/08/2023.
