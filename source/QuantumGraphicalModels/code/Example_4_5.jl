# compute equations for Gibbs variety of unirational family of Hamiltonians for 3-chain graph
# this is code for Example 4.5 in "Algebraic Geometry of Quantum Graphical Models"
# by Eliana Duarte, Dmitrii Pavlov and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1, HomotopyContinuation version 2.8.1

using HomotopyContinuation
using LinearAlgebra
using Oscar
using IterTools

@var a[1:3] b[1:3] c[1:3] d[1:3]

A = [a[1] a[2];a[2] a[3]]
B = [b[1] b[2];b[2] b[3]]
C = [c[1] c[2];c[2] c[3]]
D = [d[1] d[2];d[2] d[3]]
id = [1 0; 0 1]

E1 = kron(kron(A,B),id)
E2 = kron(id,kron(C,D))

# define unirational variety of Hamiltonians
X = E1 + E2

pars = [a;b;c;d]

# sample from GM(X)
samples = []
nsamples = 50
for i = 1:nsamples
    sample_matrix = exp(Float64.(HomotopyContinuation.evaluate.(X,pars=>randn(length(pars)))))
    sample_vector = [sample_matrix[i,j] for i = 1:8 for j = i:8]    # turns the sample_matrix into a vector
    samples = push!(samples,sample_vector)
end

samples_matrix = hcat(samples...)

# create Motzkin matrix to get linear equations
function MotzkinRow(b)
    n = length(b)

    ip = findfirst(i->abs(b[i])>1e-5,length(b):-1:1)
    if isnothing(ip)
        W = fill(zero(typeof(b[1])),n,n)
        for j = 1:n
            W[j,j] = 1
        end
    else
        W = fill(zero(typeof(b[1])),n,n-1)
        ip = n-ip+1
        for j = 1:ip-1
            W[j,j] = b[ip]
            W[ip,j] = -b[j]
        end
        for j = ip:n-1
            W[j+1,j] = 1
        end
    end
    return W/norm(W)
end

function MotzkinMatrix(M)
    global W
    m = size(M,1)
    for i = 1:m
        if i == 1
            W = MotzkinRow(M[1,:])
        else
            a = M[i,:]
            b = transpose(a)*W
            W = W*MotzkinRow(b)
            W = W*diagm([1/norm(W[:,k]) for k = 1:size(W,2)])
        end
    end
    return W
end

# the Motzkin matrix gives a nice basis for linear equations
W = MotzkinMatrix(transpose(samples_matrix))

# Define a polynomial ring for the equations of the Gibbs variety
x_strings = ["x$i$j" for i = 1:8 for j = i:8]
R, x = PolynomialRing(QQ,x_strings)

# look for linear equations
d=1
# Make a Vandermonde matrix by evaluating all monomials of degree d at all sample vectors
exp_vecs = collect(exponents(sum(x)^d))
vdm_matrix = transpose(hcat([[prod(sv.^ev) for ev in exp_vecs] for sv in samples]...))

# rationalize the coloumns of the Motzkin matrix 
vectors = [W[:,i] for i in range(1, length=size(W)[2])]
rationalized_vectors = []
for v in vectors
    w = v/v[findfirst(k->abs(k)>1e-8, v)]
    w[findall(abs.(v) .<1e-10)] .= 0
    w = rationalize.(v,tol=1e-7)
    common_denominator = lcm([denominator(frac) for frac in w])
    common_numerator = gcd([numerator(frac) for frac in w])
    w = w*common_denominator/common_numerator
    push!(rationalized_vectors, w)
end

# We check that the rationalized vectors are still in the kernel
norms = [norm(vdm_matrix*v) for v in rationalized_vectors]

# write linear equations into file
mons = [prod(x.^ev) for ev in exp_vecs]
linear_equations = [transpose(v)*mons for v in rationalized_vectors]
file = open("data/linear_equations_GV.txt", "w")
for eq in linear_equations
    println(file, eq)
end
close(file)


# look for quadratic equations
d = 2

# Define a polynomial ring for the equations of the Gibbs variety
x_strings = ["x$i$j" for i = 1:8 for j = i:8]
R, x = PolynomialRing(QQ,x_strings)
x_vars = Dict([t for t in tuple.(["x$i$j" for i = 1:8 for j = i:8], x)])

samples = []
nsamples = binomial(36 - 1 + 2, 2) + 200
for i = 1:nsamples
    sample_matrix = exp(Float64.(HomotopyContinuation.evaluate.(X,pars=>randn(length(pars)))))
    sample_vector = [sample_matrix[i,j] for i = 1:8 for j = i:8]    # turns the sample_matrix into a vector
    samples = push!(samples,sample_vector)
end

samples_matrix = hcat(samples...)
# Make a Vandermonde matrix by evaluating all monomials of degree d at all sample vectors
exp_vecs = collect(exponents(sum(x)^d))


# rationalize a basis for the kernel of the Vandermonde matrix
function rationalize_vectors(vectors_with_floats)
    rationalized_vectors = []
    for v in vectors_with_floats
        w = v/v[findfirst(k->abs(k)>1e-8, v)]
        w[findall(abs.(v) .<1e-10)] .= 0
        w = rationalize.(v,tol=1e-7)
        push!(rationalized_vectors, w)
    end
    return rationalized_vectors
end


# eliminate terms from linear relations in the Vandermonde matrix
vars_to_eliminate = [
    get(x_vars, "x23", 0),
    get(x_vars, "x25", 0),
    get(x_vars, "x35", 0),
    get(x_vars, "x45", 0),
    get(x_vars, "x27", 0),
    get(x_vars, "x36", 0),
    get(x_vars, "x46", 0),
    get(x_vars, "x47", 0),
    get(x_vars, "x67", 0),
]


function to_be_eliminated(ev)
    support = [i for (i,n) in enumerate(ev) if n > 0]
    return any(i -> x[i] in vars_to_eliminate, support)
end


vdm_matrix = transpose(hcat([[prod(sv.^ev) for ev in exp_vecs if !to_be_eliminated(ev)] for sv in samples]...))

# make the matrix numerically stable
for i = 1:size(vdm_matrix,1)
    vdm_matrix[i,:] = vdm_matrix[i,:]/norm(vdm_matrix[i,:])
end


# it is borrowed from estimate_equations.jl from the LearningAlgebraicVarieties package
function kernel_qr(R::Array{T,2}, tol::Float64) where {T <: Number}
    n,m = size(R)

    if n>=m
        index = findall(x -> abs(x) < tol, [R[i,i] for i in 1:m])
        index2 = setdiff([i for i in 1:m], index)
        R_small = R[:,index2]

        kernel = zeros(eltype(R), length(index), m)

        for i = 1:length(index)
            kernel[i,index[i]] = one(eltype(R))
            kernel[i,index2] =  (-1) .* R_small\R[:,index[i]]
        end
        return kernel
    else
        index = findall(x -> abs(x) < tol, [R[i,i] for i in 1:n])
        index2 = setdiff([i for i in 1:m], index)
        R_small = R[:,index2]

        kernel = zeros(eltype(R), length(index), m)

        for i = 1:length(index)
            kernel[i,index[i]] = one(eltype(R))
            kernel[i,index2] =  (-1) .* R_small\R[:,index[i]]
        end
        R_new = [R; LinearAlgebra.pinv(kernel) * kernel]
        s = LinearAlgebra.svd(R_new, full = true)
        rk = sum(s.S .> tol)
        return  [kernel; s.Vt[rk + 1:end,:]]
    end
end

N = transpose(kernel_qr(LinearAlgebra.qr(vdm_matrix).R, 1e-7))

# rationalize a basis for the kernel of the Vandermonde matrix
vectors = [N[:,i] for i in range(1, length=size(N)[2])]
rationalized_vectors = rationalize_vectors(vectors)

# We check that the rationalized vectors are still in the kernel
norms = [norm(vdm_matrix*v) for v in rationalized_vectors]

mons = [prod(x.^ev) for ev in exp_vecs if !to_be_eliminated(ev)]
quadratic_equations = [transpose(v)*mons for v in rationalized_vectors]
file = open("data/quadratic_equations_GV.txt", "w")
for eq in quadratic_equations
    println(file, eq)
end
close(file)

J = ideal(vcat(linear_equations, quadratic_equations))
is_prime(J)
Oscar.dim(J)