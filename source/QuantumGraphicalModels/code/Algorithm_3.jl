# Algorithm to compute equations defining the Gibbs variety GV(H_G) for commuting family of Hamiltonians H_G
# this implements Algorithm 3, Example 5.7 and Example 5.12 in "Algebraic Geometry of Quantum Graphical Models"
# by Eliana Duarte, Dmitrii Pavlov and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1, HomotopyContinuation version 2.8.1

using HomotopyContinuation
using Oscar
using Graphs
using Base.Iterators
using IterTools
using StatsBase
import Base.kron

id = [1 0; 0 1]
X = [0 1; 1 0]
Z = [1 0; 0 -1]

# auxiliary function for kron(matrices::Vector)
function kron(matrices::Vector, helpmatrix::Matrix)
    if length(matrices) == 1
        return Base.kron(matrices[1], helpmatrix)
    else
       return kron(matrices[1:length(matrices)-1], Base.kron(last(matrices), helpmatrix))
    end
end


# compute Kronecker product of multiple matrices
function kron(matrices::Vector)
    if length(matrices) == 1
        return matrices[1]
    else
        return kron(matrices[1:length(matrices)-1], last(matrices))
    end
end


# get an undirected graph from list of edges
function graph_from_edges(edgelist::Vector)
    numvertices = maximum([flatten(edgelist)...])
    g = SimpleGraph(numvertices)
    for e in edgelist
        Graphs.add_edge!(g, e[1], e[2])
    end
    return g
end


# compute Hamiltonians associated to graph as in Section 5.1
function hamiltonians_from_graph(g)
    L = []
    for v in Graphs.vertices(g)
        d = Dict(v => X)
        for n in Graphs.neighbors(g, v)
            merge!(d, Dict(n => Z))
        end
        push!(L, kron([get(d, i, id) for i in Graphs.vertices(g)]))
    end
    return L
end


# compute LSSM given generating matrices
function lssm(matrices, vars)
    function multiply(x,y)
        return x*y
    end
    return sum(z -> multiply.(z[1], z[2]), zip(vars, matrices))
end


# convert matrix to element in Oscar MatrixSpace
function matrixToOscar(m, MS)
    new_m = MS()
    for (i,j) in Iterators.product(1:(size(m)[1]), 1:(size(m)[2]))
        new_m[i,j] = m[i,j]
    end
    return new_m
end


# simultaneously diagonalise a set of commuting matrices
# assumption: all matrices are of the same size and have rational eigenvectors
function simultaneous_diagonalization(L)
    s = size(L[1])[1]
    varstring = ["x$i" for i in range(1, length(L)-1)]
    R, x = PolynomialRing(ZZ, varstring)
    S = FractionField(R)
    MS = MatrixSpace(S, s, s)

    diagonalizing_matrix = MS()
    LOscar = [(matrixToOscar(l, MS), i) for (l,i) in zip(L, 1:length(L))]
    coeffs = vcat([1], x)
    matrixmix = sum(j -> coeffs[j[2]]*j[1], LOscar)
    Id = identity_matrix(S, s)

    for val in zip(Iterators.product(ntuple(i -> [-1,1], length(L))...), 1:s)
        idcoeff = sum(j -> j[1]*coeffs[j[2]], zip(val[1], 1:length(val[1])))
        commoneigenvec = kernel(matrixmix - idcoeff*Id)[2]
        for i in 1:s
            diagonalizing_matrix[val[2], i] = commoneigenvec[i]
        end
    end

    output_matrix = zeros(Int, s, s)
    for (i,j) in Iterators.product(1:s, 1:s)
        output_matrix[i,j] = Int(Oscar.evaluate(diagonalizing_matrix[j,i], repeat([0], length(x))))
    end
    return output_matrix
end


# compute scaled toric ideal from integer matrix A with scaling given by weights
function toricIdeal(A::Matrix, weights::Vector)
    n = size(A)[1]
    m = size(A)[2]
    R, vars = PolynomialRing(QQ, vcat(["x$i" for i in 1:n], ["y$i" for i in 1:m]))
    S = FractionField(R)
    x = vars[1:n]
    y = vars[(n+1):(n+m)]
    f = (i,j) -> (j >= 0) ? S(i^j) : S(1)//(i^(-j))
    equ = prod(x).*[y[i] - weights[i]*prod(f.(x, A[:,i])) for i in 1:m]
    println(equ)
    equ = numerator.(equ)
    I = ideal(equ)
    K = ideal([prod(x)])
    J = saturation(I, K)
    J = eliminate(J, x)
    return J
end


function toricIdeal(A::Matrix)
    weights = vec(Int.(ones(1, size(A)[2])))
    return toricIdeal(A, weights)
end


# gives ideal from "independence model", see Theorem 5.5 and Remark 5.6
function equationsIndependenceModel(n::Integer)
    A = Matrix(transpose(hcat([repeat(vcat((-1)*vec(Int.(ones(1, 2^(n-i)))), (1)*vec(Int.(ones(1, 2^(n-i))))), outer=2^(i-1)) for i in 1:n]...)))
    return toricIdeal(A)
end


# compute equations of GV(H_G) by coordinate change of equations of independence model
function equationsAfterBaseChange(g)
    L = hamiltonians_from_graph(g)
    U = simultaneous_diagonalization(L)
    equInd = equationsIndependenceModel(Graphs.nv(g))
    R = base_ring(equInd)
    N = 2^(Graphs.nv(g))
    S, z = PolynomialRing(QQ, ["z[$i,$j]" for i in 1:N for j in 1:N])
    zDict = Dict([t for t in tuple.(["z[$i,$j]" for i in 1:N for j in 1:N], z)]) 
    MS = MatrixSpace(S, N, N)
    Z = MS()
    for (i,j) in Iterators.product(1:N, 1:N)
        Z[i,j] = get(zDict, "z[$i,$j]", 0)
    end
    linear_change = [(matrixToOscar(U, MS)^(-1)*Z*matrixToOscar(U, MS))[i,i] for i in 1:N]
    ϕ = hom(R, S, vcat(vec(Int.(zeros(1, size(L)[1]))), linear_change))
    equNewVars = gens(ϕ(equInd))
    addEqus = [(matrixToOscar(U, MS)^(-1)*Z*matrixToOscar(U, MS))[i,j] for i in 1:N for j in 1:N if !(i==j)]
    return ideal(vcat(equNewVars, addEqus))
end


# convert Oscar polynomial to HomotopyContinuation expression
function polyOscarToHomCon(p, vars)
    homconp = 0
    for t in terms(p)
        expo = [e for e in exponents(t)][1]
        coef = [c for c in Oscar.coefficients(t)][1]
        homconp += Rational(coef) * prod(imap(^, vars, expo))
    end
    return homconp
end


# convert Oscar ideal to HomotopyContinuation system
function idealOscarToHomConSystem(I)
    vars = gens(base_ring(I))
    @var a[1:(size(vars)[1])]
    return System([polyOscarToHomCon(p, a) for p in gens(I)], a)
end


# auxiliary function for getAffineM
function getBValues(X, L)
    return [tr(A*X) for A in L]
end


# compute affine linear space M as in Theorem 5.10
function getAffineM(X, L)
    s = size(X)[1]
    R, m = PolynomialRing(QQ, ["m[$i,$j]" for i in 1:s for j in 1:s])
    mDict = Dict([t for t in tuple.(["m[$i,$j]" for i in 1:s for j in 1:s], m)])
    b = getBValues(X, L)
    MS = MatrixSpace(R, s, s)
    M = MS()
    for (i,j) in Iterators.product(1:s, 1:s)
        if i <= j
            M[i,j] = get(mDict, "m[$i,$j]", 0)
        else
            M[i,j] = get(mDict, "m$j$i", 0)
        end
    end
    equs = [Oscar.trace(matrixToOscar(L[i], MS)*M) - b[i] for i in 1:length(L)]
    return ideal(equs)
end


# compute ideal of intersection between M and GV(H_G)
function calculateIntersectionIdeal(g, X=nothing)
    equ = equationsAfterBaseChange(g)
    L = hamiltonians_from_graph(g)
    R = base_ring(equ)
    matSize = Int(sqrt(length(gens(R))))
    if X===nothing
        X = rand(-5:5, matSize, matSize)
        X = Int64.(X)
        X = X*transpose(X)
    end
    M = getAffineM(X, L)
    varDict = Dict([t for t in tuple.(["z[$i,$j]" for i in 1:matSize for j in 1:matSize], gens(R))]) 
    varsToEliminate = [get(varDict, "z[$i,$j]", 0) for (i,j) in Iterators.product(1:matSize, 1:matSize) if i > j]
    equ = eliminate(equ, varsToEliminate)
    S = base_ring(M)
    ϕ = hom(S, R, gens(R))
    M = ϕ(M)
    I = M + equ
    symR, x = PolynomialRing(QQ, ["x[$i,$j]" for (i,j) in Iterators.product(1:8, 1:8) if i <= j])
    xVarDict = Dict([t for t in tuple.(["x[$i,$j]" for (i,j) in Iterators.product(1:matSize, 1:matSize) if i<= j], x)]) 
    f = hom(R, symR, [get(xVarDict, "x[$i,$j]", symR(0)) for i in 1:matSize for j in 1:matSize])
    I = f(I)
    return (I, X)
end


# Example 5.7
g = graph_from_edges([(1,2), (2,3), (3,1), (2,4)])
I = equationsAfterBaseChange(g);
file = open("data/equations_GV_Comm_Hams.txt", "w")
for g in gens(I)
    println(file, g)
end
close(file)
Oscar.dim(I)
length(gens(I))
mean([length(terms(p)) for p in gens(I)])


# Example 5.12
g = graph_from_edges([(1,2), (2,3)])
A = [84 -22 11 -51 -15 -8 -26 4; -22 51 -5 -7 23 -13 17 40; 11 -5 51 25 -16 -3 9 28; -51 -7 25 70 -19 17 18 -26; -15 23 -16 -19 92 32 23 24; -8 -13 -3 17 32 62 2 -36; -26 17 9 18 23 2 94 10; 4 40 28 -26 24 -36 10 109]
I, A = calculateIntersectionIdeal(g, A)
sys = idealOscarToHomConSystem(I)
S = HomotopyContinuation.solve(sys)
sols = HomotopyContinuation.real_solutions(S)
Mats = [zeros(8,8) for i in 1:length(sols)]
indexDict = Dict((i,j) => Int((j^2-j)/2 + i) for i in 1:8 for j in 1:8 if i <= j)
for (k,i,j) in Iterators.product(1:length(Mats),1:8,1:8)
    if i <= j
        Mats[k][i,j] = sols[k][get(indexDict, (i,j), 0)]
    else
        Mats[k][i,j] = sols[k][get(indexDict, (j,i), 0)]
    end
end

isPSD(M) = all([i > 0 for i in eigvals(M)])
M = [M for M in Mats if isPSD(M)][1]