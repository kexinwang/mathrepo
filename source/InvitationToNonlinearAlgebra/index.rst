===============================
Invitation to Nonlinear Algebra
===============================

Here you find the codes presented during the block course `Invitation to Nonlinear Algebra <https://www.mis.mpg.de/calendar/students/summer-semester-2019/michalek-sturmfels-invitation-to-nonlinear-algebra.html>`_, June 4-14 2019, at the MPI MiS Leipzig.


| Micha\lek, Mateusz and Bernd Sturmfels : Invitation to nonlinear algebra
| Providence, R.I. : American Mathematical Society, 2021 . - 226 p.
| (Graduate studies in mathematics ; 211)
| ISBN 978-1-4704-5367-1 ISBN 978-1-4704-6308-3
| CODE: https://mathrepo.mis.mpg.de/InvitationToNonlinearAlgebra


References
----------

.. role:: raw-html(raw)
    :format: html

[MS21] Michałek, Mateusz, and Bernd Sturmfels. Invitation to nonlinear algebra. Vol. 211. American Mathematical Soc., 2021. :raw-html:`<br />`



.. toctree::
   :maxdepth: 2

   Chapter1.rst
   Chapter3.rst
   Chapter7.rst
   Chapter9.rst



Project page created: 01/06/2019 :raw-html:`<br />`
Code contributors: Zachary Adams, Madeline Brandt, Rodica Dinu, Yassine El Maazouz, Paul Goerlach, Yuhan Jiang, Mahsa Sayyary Namin, and Tim Seynnaeve :raw-html:`<br />`
Project contributors: Zachary Adams, Madeline Brandt, Rodica Dinu, Yassine El Maazouz, Paul Goerlach, Yuhan Jiang, Mahsa Sayyary Namin, and Tim Seynnaeve

Software used: Macaulay2, Polymake, Maple :raw-html:`<br />`
