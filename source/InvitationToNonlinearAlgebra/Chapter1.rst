###########################
Chapter 1: Polynomial Rings
###########################

This chapter was presented by Tim Seynnaeve and Zachary Adams. All their codes are in Macaulay2.

Example 1.18: elimination of variables (see also chapter 4)

.. code-block:: macaulay2

                restart
                R=QQ[x,y,z,MonomialOrder=>Lex]
                I=ideal(y-x^3+4*x,z-x^3-x+1)
                gens gb I
                J=ideal(x^3-7,y^4-5,z-x-y)
                flatten entries gens gb J

Example 1.18: finitely many points

.. code-block:: macaulay2

                restart
                R=QQ[x,y,z]
                I=ideal(x*y-z,x*z-y,y*z-x)
                I==radical(I)
                G=gens gb I
                dim I
                degree I
                --This suggests that V(I) consists of 5 points
                --We can find the 5 points using primary decomposition
                primaryDecomposition I

Example 1.19 with degree reverse lexicographic order

.. code-block:: macaulay2

                restart
                R=QQ[x_1..x_4]
                --If no monomial order is specified, Macaulay2 chooses DegRevLex by default
                f=random(4,R)
                g=random(4,R)
                I=ideal(f,g)
                G=gens gb I
                degrees G

                --Just out of curiosity, we also compute dimension and degree
                dim I
                degree I
                --Geometric interpretation: Our 2 quartics cut out a 2-dimensional surface of degree 16(=4x4) in C^4

Example 1.19 with degree lexicographic order

.. code-block:: macaulay2

                restart
                R=ZZ/32003[x_1..x_4,MonomialOrder=>GLex]
                --Over QQ the computation takes a long time, so instead we took a finite field
                f=random(4,R)
                g=random(4,R)
                I=ideal(f,g)
                G=gens gb I;
                degrees G
