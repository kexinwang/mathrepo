=====================================================================================
Complete quadrics: Schubert calculus for Gaussian models and semidefinite programming
=====================================================================================

| This page contains auxiliary files to the paper
| Laurent Manivel, Mateusz Michalek, Leonid Monin, Tim Seynnaeve, and Martin Vodicka:
| Complete quadrics: Schubert calculus for Gaussian models and semidefinite programming 
| In: Journal of the European Mathematical Society, 26 (2024) 8, p. 3091-3135
| DOI: `10.4171/JEMS/1330 <https://dx.doi.org/10.4171/JEMS/1330>`_ ARXIV: https://arxiv.org/abs/2011.08791 CODE: https://mathrepo.mis.mpg.de/MLdegreeCompleteQuadrics

The maximum likelihood degree :math:`\phi(n,d)` of the generic linear concentration model is a quantity that is of interest for algbraic statistics, but also admits natural geometric definition: it the the degree of the variety :math:`\phi(n,d)` obtained by inverting all matrices in a general :math:`d` -dimensional linear space of symmetric :math:`n \times n` matrices.
Using the geometry of complete quadrics, we prove a conjecture of Sturmfels and Uhler, stating that for fixed :math:`d`, :math:`\phi(n,d)` is a polynomial in :math:`n`, of degree :math:`d-1`. 
Our proof method yields an explicit algorithm for computing these polynomials.
  
To view the code, click the link below.

.. toctree::
	:maxdepth: 1
	:glob:

	MLdegree

To run the code yourself, download the following Jupyter notebook in Sage :download:`MLdegree.ipynb <MLdegree.ipynb>`.

Alternatively, you can run the code online through Binder without installing any software:

.. image:: ../binder_badge.svg
 :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mis.mpg.de%2Frok%2Fmathrepo/develop?filepath=source%2FMLdegreeCompleteQuadrics%2FMLdegree.ipynb

Project page created: 12/11/2020

Project contributors: Laurent Manivel, Mateusz Michałek, Leonid Monin, Tim Seynnaeve, Martin Vodicka
 
Code written by: Tim Seynnaeve

Jupyter notebook written by: Tim Seynnaeve

Software used: Sage (Version 9.0)

Corresponding author of this page: Tim Seynnaeve, tim.seynnaeve@kuleuven.be
