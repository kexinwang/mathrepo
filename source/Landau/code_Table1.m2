getLandauDisc = (I,U,E) -> (
        J = I;
	for i from 1 to E do (
	    J = saturate(J,a_i);
	    );
	alist = apply(E,i->a_(i+1));
	J = saturate(J,U);
	return eliminate(J,alist)
	)

--- npltrb
R = QQ[a_1..a_6,M,m,s];
U_npltrb = a_1*a_2 + a_1*a_3 + a_1*a_4 + a_2*a_4 + a_3*a_4 + a_2*a_5 + a_3*a_5 + a_4*a_5 + a_1*a_6 + a_2*a_6 + a_3*a_6 + a_5*a_6;
F0 = M*a_1*a_2*a_3 + M*a_1*a_2*a_4 + s*a_1*a_3*a_4 + M*a_2*a_3*a_4 + s*a_1*a_2*a_5 + s*a_1*a_3*a_5 + M*a_2*a_3*a_5 + s*a_1*a_4*a_5 + M*a_2*a_4*a_5 + M*a_1*a_3*a_6 + M*a_2*a_3*a_6 + M*a_1*a_4*a_6 + M*a_2*a_4*a_6 + M*a_3*a_4*a_6 + s*a_1*a_5*a_6 + s*a_2*a_5*a_6 + M*a_3*a_5*a_6 + M*a_4*a_5*a_6;
F = F0 - m*U_npltrb*(a_1+a_2+a_3+a_4+a_5+a_6);
I_npltrb = ideal apply(6, i->diff(a_(i+1), F));
timing getLandauDisc(I_npltrb,U_npltrb,6)

--- pltrb
R = QQ[a_1..a_6,M,m,s];
U_pltrb = a_1*a_2 + a_1*a_3 + a_1*a_4 + a_2*a_5 + a_3*a_5 + a_4*a_5 + a_1*a_6 + a_2*a_6 + a_3*a_6 + a_4*a_6 + a_5*a_6;
F0 = M*a_1*a_2*a_3 + s*a_1*a_2*a_4 + M*a_1*a_3*a_4 + s*a_1*a_2*a_5 + s*a_1*a_3*a_5 + M*a_2*a_3*a_5 + s*a_1*a_4*a_5 + s*a_2*a_4*a_5 + M*a_3*a_4*a_5 + M*a_1*a_3*a_6 + M*a_2*a_3*a_6 + s*a_1*a_4*a_6 + s*a_2*a_4*a_6 + M*a_3*a_4*a_6 + s*a_1*a_5*a_6 + s*a_2*a_5*a_6 + M*a_3*a_5*a_6;
F = F0 - m*U_pltrb*(a_1+a_2+a_3+a_4+a_5+a_6);
I_pltrb = ideal apply(6, i->diff(a_(i+1), F));
timing getLandauDisc(I_pltrb,U_pltrb,6)

--- dbox
R = QQ[a_1..a_7,M,m,s,t];
U_dbox = a_1*a_3 + a_2*a_3 + a_1*a_4 + a_2*a_4 + a_1*a_5 + a_2*a_5 + a_3*a_6 + a_4*a_6 + a_5*a_6 + a_1*a_7 + a_2*a_7 + a_3*a_7 + a_4*a_7 + a_5*a_7 + a_6*a_7;
F0 = M*a_1*a_2*a_3 + M*a_1*a_2*a_4 + M*a_1*a_3*a_4 + M*a_2*a_3*a_4 + M*a_1*a_2*a_5 + s*a_1*a_3*a_5 + s*a_2*a_3*a_5 + M*a_1*a_4*a_5 + M*a_2*a_4*a_5 + M*a_1*a_3*a_6 + s*a_2*a_3*a_6 + M*a_1*a_4*a_6 + s*a_2*a_4*a_6 + M*a_3*a_4*a_6 + M*a_1*a_5*a_6 + s*a_2*a_5*a_6 + s*a_3*a_5*a_6 + M*a_4*a_5*a_6 + M*a_1*a_2*a_7 + M*a_1*a_3*a_7 + t*a_1*a_4*a_7 + M*a_2*a_4*a_7 + M*a_3*a_4*a_7 + M*a_1*a_5*a_7 + s*a_2*a_5*a_7 + s*a_3*a_5*a_7 + M*a_4*a_5*a_7 + M*a_1*a_6*a_7 + s*a_2*a_6*a_7 + s*a_3*a_6*a_7 + M*a_4*a_6*a_7;
F = F0 - m*U_dbox*(a_1+a_2+a_3+a_4+a_5+a_6+a_7);
I_dbox = ideal apply(7, i->diff(a_(i+1), F));
timing getLandauDisc(I_dbox,U_dbox,7)

--- tdetri
R = QQ[a_1..a_5,M,m,s];
U_tdetri = a_1*a_2*a_3 + a_1*a_2*a_4 + a_1*a_3*a_4 + a_2*a_3*a_4 + a_1*a_3*a_5 + a_2*a_3*a_5 + a_1*a_4*a_5 + a_2*a_4*a_5;
F0 = s*a_1*a_2*a_3*a_4 + M*a_1*a_2*a_3*a_5 + M*a_1*a_2*a_4*a_5 + M*a_1*a_3*a_4*a_5 + M*a_2*a_3*a_4*a_5;
F = F0 - m*U_tdetri*(a_1+a_2+a_3+a_4+a_5);
I_tdetri = ideal apply(5, i->diff(a_(i+1), F));
timing getLandauDisc(I_tdetri,U_tdetri,5)

--- debox
R = QQ[a_1..a_5,M,m,s,t];
U_debox = a_1*a_4 + a_2*a_4 + a_3*a_4 + a_1*a_5 + a_2*a_5 + a_3*a_5 + a_4*a_5;
F0 = M*a_1*a_2*a_4 + s*a_1*a_3*a_4 + M*a_2*a_3*a_4 + M*a_1*a_2*a_5 + s*a_1*a_3*a_5 + M*a_2*a_3*a_5 + M*a_1*a_4*a_5 + t*a_2*a_4*a_5 + M*a_3*a_4*a_5;
F = F0 - m*U_debox*(a_1+a_2+a_3+a_4+a_5);
I_debox = ideal apply(5, i->diff(a_(i+1), F));
timing getLandauDisc(I_debox,U_debox,5)

--- tdebox
R = QQ[a_1..a_6,M,m,s,t];
U_tdebox = a_1*a_2*a_5 + a_1*a_3*a_5 + a_2*a_3*a_5 + a_2*a_4*a_5 + a_3*a_4*a_5 + a_1*a_2*a_6 + a_1*a_3*a_6 + a_2*a_3*a_6 + a_2*a_4*a_6 + a_3*a_4*a_6 + a_2*a_5*a_6 + a_3*a_5*a_6;
F0 = M*a_1*a_2*a_3*a_5 + s*a_1*a_2*a_4*a_5 + s*a_1*a_3*a_4*a_5 + M*a_2*a_3*a_4*a_5 + M*a_1*a_2*a_3*a_6 + s*a_1*a_2*a_4*a_6 + s*a_1*a_3*a_4*a_6 + M*a_2*a_3*a_4*a_6 + M*a_1*a_2*a_5*a_6 + M*a_1*a_3*a_5*a_6 + t*a_2*a_3*a_5*a_6 + M*a_2*a_4*a_5*a_6 + M*a_3*a_4*a_5*a_6;
F = F0 - m*U_tdebox*(a_1+a_2+a_3+a_4+a_5+a_6);
I_tdebox = ideal apply(6, i->diff(a_(i+1), F));
timing getLandauDisc(I_tdebox,U_tdebox,6)

--acn
R = QQ[a_1..a_5,M,m,s,t]
U_acn = a_5*(a_1+a_2+a_3+a_4) + a_1*(a_2+a_3)+a_4*(a_2+a_3)
F1 = a_1*a_2*a_5
F2 = a_2*a_3*(a_1+a_4+a_5)
F3 = a_3*a_4*a_5
F4 = a_1*a_4*(a_2+a_3+a_5)
F12 = a_1*a_3*a_5
F23 = a_2*a_4*a_5
F13 = 0 
F = F12*s + F23*t + M*(F1+F2+F3+F4) - U_acn*m*(a_1+a_2+a_3+a_4+a_5)
I_acn = ideal apply(5,i->diff(a_(i+1),F))
timing getLandauDisc(I_acn,U_acn,5)


-- par
R = QQ[a_1..a_4,M,m,s]
U_par = (a_1+a_2)*(a_3+a_4) + a_3*a_4
F3 = a_2*a_3*a_4
F4 = a_1*a_3*a_4
F12 = a_1*a_2*(a_3+a_4)
F = s*F12 + M*F3 + M*F4 - U_par*(m*a_1 + m*a_2 + m*a_3 + m*a_4)
I_par = ideal apply(4,i->diff(a_(i+1),F))
timing getLandauDisc(I_par,U_par,4)




