################################
# Diagram information
################################

name = "B4"

edges = [[1, 2], [1, 2], [1, 2], [1, 2]]
nodes = [1, 1, 2, 2]
internal_masses = [0, 0, 0, 0]
external_masses = [M[1], M[2], M[3], M[4]]

U = x[1]*x[2]*x[3] + x[1]*x[2]*x[4] + x[1]*x[3]*x[4] + x[2]*x[3]*x[4]
F = s*x[1]*x[2]*x[3]*x[4]
parameters = [s]
variables = [x[1], x[2], x[3], x[4]]

χ_generic = 1
f_vector = [5, 10, 10, 5]

################################
# Component 1
################################

D[1] = s
χ[1] = 0
weights[1] = [[-1, -1, -1, -1]]
computed_with[1] = ["PLD_sym"]

