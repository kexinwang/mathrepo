using MultivariatePolynomials
using Polymake
using SmithNormalForm  # to add: pkg> add https://github.com/wildart/SmithNormalForm.jl.git
using LinearAlgebra
using HomotopyContinuation
include("PolynomialSystems_v01.jl")
using .PolynomialSystems_v01
include("Polytopes.jl")
include("CoxRings.jl")

## Cox homotopy

function computeStartSystem(f̂)
    # returns start system ĝ with monodromy seed p, i.e. ĝ(p)=0
    vars = variables(f̂)
    n = length(vars)
    p = randn(ComplexF64,n) #seed for monodromy
    ĝ = map(1:n) do j
        termsg = terms(f̂[j])
        exponentMatrix = zeros(Int64,length(termsg),n+1)
        vrsj = variables(f̂[j])
        ptr = map(i->1+findfirst(isequal(vrsj[i]), variables(f̂)),1:length(vrsj))
        for i = 1:length(termsg)
            exponentMatrix[i,hcat([1],ptr')] = [1 transpose(exponents(termsg[i]))];
        end
        exponentMatrix = transpose(exponentMatrix[:,2:end]) #support f̂[j]
        linearForm = map( i -> prod(p.^exponentMatrix[:,i]), [1:size(exponentMatrix,2);]);
        linearForm = reshape(linearForm,size(exponentMatrix,2),1);
        kernel = nullspace(transpose(linearForm));
        coeffg = kernel*randn(ComplexF64, size(kernel,2),1);
        monomials = map( i -> prod(vars.^exponentMatrix[:,i]), [1:size(exponentMatrix,2);]);
        h = transpose(coeffg)*monomials;
        h[1]
    end
    return ĝ, p
end

function solveGeneric(ĝ, pt, Fᵀ, monodromy = false, nsols=nothing)
    # Returns mixed volume many homogeneous solutions of ĝ = 0, where ĝ is assumed generic,
    # by solving over the torus first and then homogenizing via QR.
    # In the output, homStartSols contains the n homogeneous coordinates of the solutions
    # which are not put to 1 by the QR selection, which is given by p. These are the coordinates
    # x[j] for j ∈ p[1:n]. fullhomStartSols contains all k coordinates of the solutions.
    k,n = size(Fᵀ)
    if monodromy
        println("(using monodromy)")
        f̂,q = computeStartSystem(ĝ)
        @polyvar τ
        γ = randn() + im*randn()
        Ĥ = (1.0-τ).*ĝ + γ*τ.*f̂
        R = monodromy_solve(Ĥ, pt, [0.0]; parameters=[τ], target_solutions_count = nsols, compile = false)
    else
        R = solve(ĝ)
    end
    startsols = solutions(R)
    delta = length(startsols)
    # Homogenize the starting solutions
    F = transpose(Fᵀ)
    T = zeros(Complex{Float64},n,delta) # We arrange the start solutions in a matrix T
    for i = 1:delta
        T[:,i] = transpose(startsols[i])
    end
    logT = log.(T) # Take the logarithm, The system we need to solve is F logX = logT
    Z = qr(F,Val(true))
    Q = Z.Q; R = Z.R; p = Z.p;
    logX̃ = R[:,1:n]\(transpose(Q)*logT)
    logX = zeros(Complex{Float64},k,delta)
    logX[p[1:n],:] = logX̃
    X = exp.(logX)
    homStartSols = [X[p[1:n],i] for i in 1:delta]
    fullhomStartSols = [X[:,i] for i in 1:delta]
    return homStartSols, p, fullhomStartSols
end

function moveSlice(g,homStartSols,p,x̂₁,K₁,y,τ)
    (k,n) = size(K₁)
    L₁ = x̂₁ + K₁*y
    x̂₀ = fill(zero(ComplexF64),k)
    x̂₀[p[n+1:k]] = ones(k-n)
    K₀ = fill(zero(ComplexF64),k,n)
    K₀[p[1:n],:] = Matrix{ComplexF64}(I,n,n)
    L₀ = x̂₀ + K₀*y
    γ = randn(ComplexF64)
    L = System((1-τ)*L₀ + τ*L₁;parameters = [τ])
    G = System(g;parameters = [τ])
    H = compose(G,L,compile = false)
    R = solve(H, homStartSols; p₁ =[0.0], p₀ =[1.0])
    return solutions(R)
end

function solveTarget(f, g, homStartSols, p, x,y,τ; target = 0.9, compile = false)
    # This tracks from the homogeneous system g to the homogeneous system f
    # The linear slice at the start is of the form x[j] - 1.0 = 0, j ∈ p[n+1:end]
    # The linear slice at the target is random
    # homStartSols contains the coordinates x[j], j ∈ p[1:n] of the starting solutions.
    k = length(p)
    n = length(homStartSols[1])
    Lₜ = fill(sum((1.0+1.0*im)*y),k);
    for i = 1:n
        Lₜ[p[i]] = y[i]
    end
    for i = n+1:k
        Lₜ[p[i]] = 1.0 -τ + τ*(sum(randn(ComplexF64,n).*y[1:n]) + randn(ComplexF64));
    end
    Lₜ_sys = System(Lₜ; parameters = [τ])
    γ = randn(ComplexF64)
    H = (1.0-τ).*(g) + γ*τ.*(f)
    H_sys = System(H; parameters = [τ])
    Hy = compose(H_sys,Lₜ_sys; compile=false)
    homResult = solve(Hy, homStartSols; start_parameters =[0.0], target_parameters=[[complex(target)],[complex(1.0)]])
    R = homResult[1][1] # tracking to t=0.9
    ySols = solutions(R)
    singSols = solutions(singular(R));
    homogeneousSols = [Lₜ_sys(s,[target]) for s in ySols]
    homSingSols = [Lₜ_sys(s,[target]) for s in singSols]
    h = subs(H,τ=>target)
    ℓ = x[p[n+1:end]] - subs(Lₜ[p[n+1:end]],y=>x[p[1:n]],τ=>target)
    solvedSys = vcat(h,ℓ)
    homogeneousSols, homSingSols, R, homResult[2][1], solvedSys, H_sys, Lₜ_sys, H, Lₜ
end

function getSomeRepresentatives(p,L,Fᵀ,x,targetTime,λsols,reps)
    # only runs monodromy, returns the solutions
    k,n = size(Fᵀ)
    @polyvar z λ[1:k-n]
    Lhom = L + subs(L,x=>zeros(size(x)))*(z - 1)
    A, param = computeOrbitParametrization(Fᵀ, λ)
    pullback = subs(Lhom, x=>p.*param[1:k], z=>param[k+1])
    f̂,q = computeStartSystem(pullback)
    @polyvar τ
    γ = randn(ComplexF64)
    Ĥ = (1.0-τ).*pullback + γ*τ.*f̂
    R = monodromy_solve(Ĥ, λsols, [0.0], parameters=[τ], timeout=targetTime, compile = false)
    λsols = solutions(R)
    return λsols, map(ℓ -> (p.*param[1:k])(λ=>ℓ)/(param[k+1])(λ =>ℓ),λsols)
end

function getAllRepresentatives(p,L,Fᵀ,x, monodromy = false)
    # for a point p in the total coordinate space of X, contained in the n-dimensional
    # linear space defined by L, this computes the coordinates of all other points in L on
    # the same G-orbit.
    k,n = size(Fᵀ)
    @polyvar z λ[1:k-n]
    Lhom = L + subs(L,x=>zeros(size(x)))*(z - 1)
    A, param = computeOrbitParametrization(Fᵀ, λ)
    pullback = subs(Lhom, x=>p.*param[1:k], z=>param[k+1])
    if monodromy
        println("(using monodromy)")
        f̂,q = computeStartSystem(pullback)
        @polyvar τ
        γ = randn(ComplexF64)
        Ĥ = (1.0-τ).*pullback + γ*τ.*f̂
        R = monodromy_solve(Ĥ, ones(k-n), [0.0]; parameters=[τ],compile = false)
    else
        R = solve(pullback)
    end
    λsols = solutions(R)
    return map(ℓ -> (p.*param[1:k])(λ=>ℓ)/(param[k+1])(λ =>ℓ),λsols)
end

function coxHomotopy(f̂; endgameStart = 0.9, details = false, fixSlice = true, emergencyEndpoint = .999, get_all_reps=true, print_progress = false)
    # In the output, `homogeneousSols` should be points that end up on the target orbit
    if print_progress println("======================== COX HOMOTOPY - OFFLINE PHASE ========================") end
    if print_progress println("Computing polytope information...") end
    P, Fᵀ, a = computePolytope(f̂)
    if fixSlice
        (k,n) = size(Fᵀ)
        x̂ = randn(ComplexF64,k)
        K = randn(ComplexF64,k,n) # x̂ and K define the random slice x = x̂ + Ky
        return coxHomotopy2(f̂, P, Fᵀ, a, x̂, K, endgameStart, details, emergencyEndpoint, print_progress)
    else
        return coxHomotopy(f̂,P, Fᵀ, a,endgameStart, details, get_all_reps)
    end
end
function coxHomotopy(f̂, P, Fᵀ, a; endgameStart = 0.9, details = false, get_all_reps=false, use_monodromy=true, monodromy_initial_timeout=0.5,print_progress = false)
    if print_progress println("======================== COX HOMOTOPY - OFFLINE PHASE ========================") end
    ĝ, pt = computeStartSystem(f̂)
    if print_progress println("Solving start system...") end
    homStartSols, p, fullhomStartSols = solveGeneric(ĝ, pt, Fᵀ)
    k,n = size(Fᵀ)
    @polyvar x[1:k] y[1:n] τ λ[1:k-n]
    B = baseEquations(P,Fᵀ,a,x)
    f, αf = homogenize(f̂, Fᵀ,x)
    g, αg = homogenize(ĝ, Fᵀ,x)
    if print_progress println("======================== COX HOMOTOPY - ONLINE PHASE ========================") end
    if print_progress println("Cox homotopy: pre-endgame") end
    homogeneousSols, homSingSols, homMidResult, homTargetResult, solvedSys, H_sys, Lₜ_sys, H, Lₜ = solveTarget(f, g, homStartSols, p, x, y, τ; target=endgameStart)
    finalSols = copy(homogeneousSols) ## intialized: sols @ t=0.9
    if print_progress println("Entering end game...") end
    orbDeg = convert(Int,orbitDegree(Fᵀ)[1])
    pathResults = getfield(homTargetResult, :path_results)
    successfulPathIndices = filter(i -> getfield(pathResults[i],:return_code) == :success, 1:length(pathResults))
    # filter out base solutions
    homSol = map(i->Lₜ_sys(solution(pathResults[i]),[1.0]),successfulPathIndices)
    resR = get_residual(B,homSol,x)
    successfulPathIndices = successfulPathIndices[findall(r -> r > 1e-3, resR)]
#    badPathIndices = filter(i -> getfield(pathResults[i],:return_code) != :success, 1:length(pathResults))
    if print_progress println(length(homStartSols)-length(successfulPathIndices)," unsuccessful paths") end
    if print_progress println("Entering end game...") end
    if (endgameStart >= 1)
        error("engameStart expected a values < 1")
    end
    L = solvedSys[n+1:k]
    Hy = compose(H_sys,Lₜ_sys; compile = false)
    # loop that edits finalSols
    for i=1:length(finalSols)
        if i in successfulPathIndices
            if print_progress println(i," is successful from the beginning") end
            finalSols[i] = Lₜ_sys(solution(pathResults[i]),[1.0])
        else
            if print_progress println("finalizing path " * string(i) * "...") end
            if print_progress println("   trying some other representatives for path " * string(i)) end
            nsols = 0
            reps = [homogeneousSols[i]]
            λsols = [ones(k-n)]
            targetTime = monodromy_initial_timeout # seconds
            while nsols == 0 && length(reps) < orbDeg
                λsols, reps = getSomeRepresentatives(homogeneousSols[i], L, Fᵀ, x, targetTime, λsols, reps)
                targetTime = 2*targetTime
                if print_progress println("targetTime ", targetTime) end
                R = solve(Hy, map(k->k[p[1:n]], reps); p₁=[endgameStart], p₀=[1.0], compile = false)
                homSolR = map(solR -> Lₜ_sys(solR,[1.0]), solutions(R))
                ## check if empty because of get_residual
                if length(homSolR) == 0
                    nonBaseLocusSolutions = homSolR
                    nsols = 0
                else
                    resR = get_residual(B,homSolR,x)
                    nonBaseLocusIndices = findall(r -> r > 1e-3, resR)
                    nonBaseLocusSolutions = homSolR[nonBaseLocusIndices]
                    nsols = length(nonBaseLocusSolutions)
                end
                if nsols != 0
                    if print_progress println("   success!") end
                    finalSols[i] = nonBaseLocusSolutions[1]
                elseif length(singular(R))>0 #&& length(findall(result -> get_residual(B,[result],x)[1] > 1e-6,solutions(singular(R))))
                    if print_progress println("  (singular) success!") end
                    homSolRSing = map(ℓ -> Lₜ_sys(singular(R)[ℓ].solution,[1.0]), 1:length(singular(R)))
                    resR = get_residual(B,homSolRSing,x)
                    nonBaseLocusIndices = findall(r -> r > 1e-3, resR)
                    nonBaseLocusSolutions = homSolRSing[nonBaseLocusIndices]
                    nsols = length(nonBaseLocusSolutions)
                    if nsols != 0
                        finalSols[i] = nonBaseLocusSolutions[1]
                    end
                end
            end
            if nsols == 0
                if print_progress println("========== !!! FOUND NO GOOD REPRESENTATIVE") end
            end
        end
    end
    toricSols = dehomogenizeSolutions(finalSols, Fᵀ); #or do we want to return just the nonsingular solutions?
    if details
         return finalSols, f, toricSols, Fᵀ, R, homogeneousSols
     end
    return finalSols, f, toricSols, homTargetResult
end

function coxHomotopy2(f̂, P, Fᵀ, a, x̂, K, endgameStart = 0.9,details = false, emergencyEndpoint = 0.999, print_progress = false)
    # Solve f̂ = 0 using the Cox Homotopy in the toric variety X coming from the data P, Fᵀ, a.
    # The linear slice is fixed to be x = x̂ + Ky.
    # =================== Output:
    # finalSols: homogeneous solutions (Cox variables ordered according to the rows of Fᵀ)
    # f: homogeneous equations vanishing at finalSols
    # toricSols: image of finalSols under the Cox quotient (will contain 'large' doubles for approximate solutions near infinity)
    # B: equations for the base locus of the toric variety X
    # data: vector encoding the behaviour of each solution at the end of the homotopy, see coxEndgame(...)
    ĝ,pt = computeStartSystem(f̂)
    if print_progress println("Solving start system...") end
    homStartSols, p, fullhomStartSols = solveGeneric(ĝ, pt, Fᵀ)
    k,n = size(Fᵀ)
    @polyvar x[1:k] y[1:n] τ λ[1:k-n]
    B = baseEquations(P,Fᵀ,a,x)
    f, αf = homogenize(f̂, Fᵀ,x)
    g, αg = homogenize(ĝ, Fᵀ,x)
    if print_progress println("======================== COX HOMOTOPY - ONLINE PHASE ========================") end
    if print_progress println("Moving the linear slice to the input slice") end
    startsols = moveSlice(g,homStartSols,p,x̂,K,y,τ)
    if print_progress println("Cox homotopy: pre-endgame") end
    L = System(x̂ + K*y; parameters = [τ])
    γ = randn(ComplexF64)
    GF = System(γ*(1-τ)*g + τ*f; parameters = [τ])
    H = compose(GF, L, compile = false)
    R = solve(H, startsols; p₁ = [0.0], p₀ = [endgameStart])
    ysols = solutions(R)
    if print_progress println("number of solutions at τ = endgameStart: " * string(length(ysols))) end
    homogeneousSols = [x̂ + K*ysol for ysol in ysols]
    if endgameStart < 1
        if print_progress println("Entering end game...") end
        finalSols, data = coxEndgame(H,homogeneousSols,ysols,x̂,K,endgameStart,B,Fᵀ,x, emergencyEndpoint,print_progress)
    else
        finalSols = copy(homogeneousSols)
        data = nothing
    end
    toricSols = dehomogenizeSolutions(finalSols, Fᵀ); #or do we want to return just the nonsingular solutions?
    if details
        return finalSols, f, toricSols, Fᵀ, x̂, K, B, data
    else
        return finalSols, f, toricSols
    end
end


function coxEndgame(H,homogeneousSols,ysols,x̂,K,endgameStart,B,Fᵀ,x,emergencyEndpoint,print_progress)
    # End game for Cox Homotopy H with fixed slice x = x̂ + K y
    # B has monomial equations for the base locus, homogeneousSols and ysols have x- and y-solutions for H at t = endgameStart.
    # The output vector data contains an integer between 0 and 3 for each solution in homogeneousSols:
    # 0: path failure
    # 1: solution landed directly on an orbit
    # 2: we had to try different representatives and found a regular solution
    # 3: we had to try different representatives and found a singular solution
    k = length(x)
    finalSols = copy(homogeneousSols)
    data = fill(0,length(homogeneousSols))
    for i = 1:length(homogeneousSols)
        if print_progress println("finalizing path " * string(i) * "...") end
        result = fill(zero(ComplexF64),k)
        R = solve(H, [ysols[i]]; p₁ =[endgameStart], p₀ =[1.0])
        nsols = length(solutions(R))
        if nsols != 0
            result = x̂ + K*solutions(R)[1]
        elseif length(singular(R)) > 0
            result = x̂ + K*(singular(R)[1].solution)
            nsols = 1
        end
        if nsols == 0 || get_residual(B,[result],x)[1] < 1e-6
            if print_progress println("   trying other representatives for path " * string(i)) end
            reps = getAllRepresentatives(homogeneousSols[i],nullspace(K')'*(x-x̂),Fᵀ,x)
            j = 1
            while (nsols == 0 || get_residual(B,[result],x)[1] < 1e-6 || norm(result)>1e4) && j <= length(reps)
                if print_progress println("   trying representative " * string(j)) end
                R = solve(H, [K\(reps[j]-x̂)]; p₁=[endgameStart], p₀=[1.0])
                nsols = length(solutions(R))
                if nsols != 0
                    result = x̂ + K*solutions(R)[1]
                    if (get_residual(B,[result],x)[1] > 1e-6)
                        if print_progress println("   success!") end
                        data[i] = 2
                    end
                elseif length(singular(R))>0
                    nsols = length(singular(R))
                    result = x̂ + K*(singular(R)[1].solution)
                    if (get_residual(B,[result],x)[1] > 1e-6)
                        if print_progress println("   success! Landed on singular point...") end
                        data[i] = 3
                    end
                end
                j = j+1
            end
        else
            if print_progress println("path " * string(i) * " landed on orbit.") end
            data[i] = 1
        end
        if data[i] == 0
            if print_progress println("Could not finalize this path, I'll try to track until τ = " *string(emergencyEndpoint)) end
            R = solve(H, [ysols[i]]; p₁ =[endgameStart], p₀ =[emergencyEndpoint])
            nsols = length(solutions(R))
            if nsols != 0
                result = x̂ + K*solutions(R)[1]
            elseif length(singular(R)) > 0
                result = x̂ + K*(singular(R)[1].solution)
                nsols = 1
            else
                if print_progress println("Sorry, failed...") end
                result = []
            end
        end
        finalSols[i] = result
    end
    return finalSols, data
end

function trackPathOrthogonal(f, g, x, homStartsol, P, n, k, Δt, γ)
    # tracks a path in the total coordinate space using orthogonal patches
    z = homStartsol;
    @polyvar τ
    H = τ*g + γ*(1.0-τ)*f;
    t = 1.0;
    conditionNumbers = Real[];
    while t > 0.0
        #println(t)
        L = conj.(P*diagm(z))*(x-z);
        Ĥt = vcat(subs(H, τ => t - Δt),L);
        J = differentiate(Ĥt,x); #Jacobian
        z = Newton_Raphson(Ĥt, J, x, z, 10);
        κ = cond(J(x=>z))
        push!(conditionNumbers,κ);
        #println(κ)
        t = t - Δt;
    end
    return z, conditionNumbers
end

function trackPathRandom(f, g, x, homStartsol, n, k, Δt, γ)
    # tracks a path in the total coordinate space using random patches
    z = homStartsol;
    Pₛ = randn(ComplexF64,k-n,k);
    Lₛ = Pₛ*x - Pₛ*z;
    #Lₜ = Pₜ*x - randn(ComplexF64,k-n);
    g = vcat(g,Lₛ);
    f = vcat(f,Lₛ);
    @polyvar τ
    H = τ*g + γ*(1.0-τ)*f;
    t = 1.0;
    conditionNumbers = Real[];
    while t > 0.0
        Ht = subs(H, τ => t - Δt);
        J = differentiate(Ht,x); #Jacobian
        z = Newton_Raphson(Ht, J, x, z, 10);
        push!(conditionNumbers,cond(J(x=>z)));
        t = t - Δt;
        #println(t)
    end
    return z, conditionNumbers, Lₛ
end
