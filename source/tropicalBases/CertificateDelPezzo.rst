#####################
Certificate Del Pezzo
#####################


The :download:`Certificate for del Pezzo surfaces of degree 3 <CertificateDelPezzo.sing>` contains a certificate which invalidates Conjecture 5.3 of

[RSS16] Q. Ren, K. Shaw, B. Sturmfels: *Tropicalization of Del Pezzo Surfaces*, Adv. Math. 300 (2016), 156--189. arXiv:1402.5651_

.. _arXiv:1402.5651: https://arxiv.org/abs/1402.5651


It works in the polynomial ring

.. math::
          \mathbb Q(d_1,\dots,d_6)[E_1,\dots,E_6,F_{12},F_{13},\dots,F_{56},G_1,\dots,G_6]

in which :math:`d_i` represent generic complex numbers determining generic points :math:`p_i:=(1:d_i:d_i^3)\in\mathbb P_{\mathbb C}^2` and for the smooth cubic surface :math:`X:=\text{Bl}_{p_1,\ldots,p_6}(\mathbb P_{\mathbb C}^2)`:

* :math:`E_i` represents the exceptional divisor over the point :math:`p_i`,
* :math:`F_{ij}` represents the strict transform of the line through :math:`p_i` and :math:`p_j`,
* :math:`G_i` represents the strict transform of the conic through :math:`\{p_1,\ldots,p_6\} \setminus \{p_i\}`.

The script contains a generating set :math:`F` for the universal Cox Ideal :math:`I_X` up to saturation as specified in Proposition 2.2 of [RSS16]. For a specific weight vector :math:`w_{\text{max}}\in\mathbb R^{\{E_i, F_{ij}, G_i\}}`, the script shows

* :math:`w_{\text{max}}\in\text{Trop}(F)` by verifying that all initial forms :math:`\text{in}_{w_{\text{max}}}(f)` for :math:`f\in F` have at least 2 terms
* :math:`w_{\text{max}}\notin\text{Trop}(I_X)` by verifying that the initial ideal :math:`\text{in}_{w_{\text{max}}}(I_X)` contains the monomial :math:`E_6F_{56}G_6`.


.. code-block:: none

                                    SINGULAR                              /  Development
                A Computer Algebra System for Polynomial Computations    /   version 4.1.1
                                                                       0<
                by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann  \   Feb 2018
                FB Mathematik der Universitaet, D-67653 Kaiserslautern    \
                > LIB "tropicalBasis.lib"; // initializes necessary libraries and helper functions
                > intvec wMin = 2,1,0,1,1,1,0,2,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0;
                                                 // wMin is in min-convention
                > intvec wMax = -wMin;           // Singular uses max-convention
                > intvec allOnes = onesVector(size(wMax));
                > ring r = (0,d1,d2,d3,d4,d5,d6),(E1,E2,E3,E4,E5,E6,
                .   F12,F13,F14,F15,F16,F23,F24,F25,F26,F34,F35,F36,F45,F46,F56,
                .   G1,G2,G3,G4,G5,G6),(a(allOnes),a(wMax),lp);
                                          // prepending allOnes makes no difference mathematically
                                          // as the ideal is homogeneous,
                                          // but it helps computationally
                > ideal F =               // Singular ideals are list of polynomials
                .  (d3-d4)*(d1+d3+d4)*E2*F12+(d2-d4)*(d1+d2+d4)*E3*F13-(d2-d3)*(d1+d2+d3)*E4*F14,
                   [...]                  // see uploaded file
                .  -(d5-d6)*(d1+d3+d4)*F24*G4+(d4-d6)*(d1+d3+d5)*F25*G5-(d4-d5)*(d1+d3+d6)*F26*G6;
                > ideal inF = initial(F,wMax); // initial forms of the elements in F
                                               // all are at least binomial, hence wMax lies in Trop(F)
                > ideal IX = groebner(F);
                > ideal inIX = initial(IX,wMax);// initial forms of Groebner basis elements
                                                // this is a Groebner basis of in_wMax(IX)
                > reduce(E6*F56*G6,inIX);       // normal form is 0 hence E6*F56*G6 lies in in_wMax(IX)
                0
