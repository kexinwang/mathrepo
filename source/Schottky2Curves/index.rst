=================================================================================
Crossing the transcendental divide: from Schottky groups to algebraic curves
=================================================================================

| This page contains auxiliary files to the paper:
| Samantha  Fairchild, Ángel David Ríos Ortiz
| Crossing the transcendental divide: from Schottky groups to algebraic curves
| ARXIV: `https://arxiv.org/abs/2401.10801 <https://arxiv.org/abs/2401.10801>`_

**Abstract of paper.**
Though the uniformization theorem guarantees an equivalence of Riemann surfaces and smooth algebraic curves, 
moving between analytic and algebraic representations is inherently transcendental. Our analytic curves identify pairs 
of circles in the complex plane via free groups of Möbius transformations called Schottky groups. 
We construct a family of non-hyperelliptic surfaces of genus :math:`g\geq 3` where we know the Riemann surface as well as properties of the canonical embedding, 
including a nontrivial symmetry group and a real structure with the maximal number of connected components (an :math:`M`-curve). We then numerically 
approximate the algebraic curve and Riemann matrices underlying our family of Riemann surfaces.

 
**Jupyter Notebook** 
All code used in the numerical experiments is contained in the following notebook, either by clicking the link below or downloading directly: :download:`Schottky2Curves.ipynb`.

.. toctree::
    :maxdepth: 1
    :glob:

    Schottky2Curves.ipynb

| Project page created: 19/1/2024
| Project contributors: Samantha Fairchild, Ángel David Ríos Ortiz


| Software used: Julia 1.8.2
| System used: MacBook Air with chip Apple M1 202 and 8 GB memory

| Corresponding author of this page: Samantha Fairchild, samantha.fairchild@mis.mpg.de
| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)
| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
| Last updated 23/1/24.
