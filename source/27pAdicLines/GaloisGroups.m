powers := [[0, 0, 0, 3],
		   [0, 0, 1, 2],
		   [0, 0, 2, 1],
		   [0, 0, 3, 0],
	       [0, 1, 0, 2],
	       [0, 1, 1, 1],
		   [0, 1, 2, 0],
	       [0, 2, 0, 1],
	       [0, 2, 1, 0],
	       [0, 3, 0, 0],
	       [1, 0, 0, 2],
		   [1, 0, 1, 1],
		   [1, 0, 2, 0],
		   [1, 1, 0, 1],
		   [1, 1, 1, 0],
		   [1, 2, 0, 0],
		   [2, 0, 0, 1],
		   [2, 0, 1, 0],
		   [2, 1, 0, 0],
		   [3, 0, 0, 0]];

function randomSmoothCubicSurface(N,p)
	
	_<x,y,z,w> := PolynomialRing(Rationals(),4);
	P3<x,y,z,w> := ProjectiveSpace(Rationals(),3);
	
	listCoef := [0..p^(N+1)-1];
	
	bool := true; 
	while bool do
	
		randSurface := 0*x;		
		for part in powers do
			coef := Random(listCoef);
			monomial := x^part[1] * y^part[2] * z^part[3] * w^part[4];
			randSurface := randSurface + coef * monomial;
		end for;

		S := Surface(P3,randSurface);	
		bool := IsSingular(S);

	end while;

	return randSurface;

end function;



function galoisGroup(surface,p)

	f := surface;
		
	QQabcd<a,b,c,d> := PolynomialRing(Rationals(),4);
	_<s,t> := PolynomialRing(QQabcd,2);

	param := [a*s+b*t,c*s+d*t,s,t];

	pt0 := [1,0];
	pt1 := [1,1];
	ptmin1 := [1,-1]; 
	ptinf := [0,1];

	g0 := Evaluate(f,[Evaluate(param[i],pt0) : i in [1..4]]);
	g1 := Evaluate(f,[Evaluate(param[i],pt1) : i in [1..4]]);
	gmin1 := Evaluate(f,[Evaluate(param[i],ptmin1) : i in [1..4]]);
	ginf := Evaluate(f,[Evaluate(param[i],ptinf) : i in [1..4]]);

	I := Ideal([g0,g1,gmin1,ginf]);	 
	GB := GroebnerBasis(I);
	g := GB[#GB];

	K := pAdicField(p,300);

	newcoeff := [];
	for i in [0..27] do
		Append(~newcoeff,K!Rationals()!Coefficient(g,4,i));
	end for;

	_<x> := PolynomialRing(K);
	gK := 0*x;	
	for i in [1..28] do
		gK := gK + newcoeff[i]*x^(i-1);
	end for;

	try G := GaloisGroup(gK);
	catch e return -1;
	end try;

	return G;
	
end function;



function galoisGroups(N,p,M)
	abelian_groups := [];
	nonabelian_groups := [];
	
	for i in [1..M] do
		print i;
		
		f := randomSmoothCubicSurface(N,p);
		group := galoisGroup(f,p);

		if Type(group) eq GrpPerm then
			if IsAbelian(group) then
				Append(~abelian_groups,group);
			else
				Append(~nonabelian_groups,group);
			end if;
		end if;

	end for;	

	return [abelian_groups,nonabelian_groups];

end function;

///////////////////////////////////////////
////////////////// TESTS //////////////////
///////////////////////////////////////////

p := 7; // prime
M := 5000; // number of trials
N := 7; // precision

groups := galoisGroups(N,p,M);
abelian_groups := groups[1];
nonabelian_groups := groups[2];
max_order := Maximum([#G : G in abelian_groups cat nonabelian_groups]);


print("--------------------------------------------------------------------------------------------");
print "Measure: Haar  |  Prime: ",p," |   Number of trials: ",M," |  Range of coeffs: [0,p^",N+1,")";
print("--------------------------------------------------------------------------------------------");


print "The maximum order is", max_order;


if #abelian_groups eq 0 then
	print "None of the groups are abelian";
else
	print "The number of abelian groups is", #abelian_groups;
	group_names := Sort([GroupName(G) : G in abelian_groups]);
	set_of_groups := Set(group_names);
	for group in set_of_groups do
		mult := Multiplicity(group_names,group);
		print "The abelian group", group ,"appears", mult ,"times";
	end for;
end if;


if #nonabelian_groups eq 0 then
	print "None of the groups are nonabelian";
else
	print "The number of nonabelian groups is", #nonabelian_groups;
	group_names := Sort([GroupName(G) : G in nonabelian_groups]);
	set_of_groups := Set(group_names);
	for group in set_of_groups do
		mult := Multiplicity(group_names,group);
		print "The nonabelian group", group ,"appears", mult ,"times";
	end for;
end if;


number_of_fails := M - (#abelian_groups + #nonabelian_groups);
print "For", number_of_fails, "tries, the precision was not big enough to compute the Galois group";