=======================================
Jordan Algebras of Symmetric Matrices
=======================================

This page contains supplementary code to the following paper:

| Arthur Bik, Henrik Eisenmann, and Bernd Sturmfels: Jordan algebras of symmetric matrices
| In: Le Matematiche, 76 (2021) 2, p. 337-353
| DOI: `10.4418/2021.76.2.3 <https://dx.doi.org/10.4418/2021.76.2.3>`_ ARXIV: https://arxiv.org/abs/2010.00277 CODE: https://mathrepo.mis.mpg.de/JordanAlgebrasSymmetricMatrices

In Theorem 2.7, we classify the unital Jordan algebras of dimension 3. We used SAGE to translate the Jordan axiom into conditions on the structure constants defining the Jordan algebra in certain cases.

.. toctree::
   :maxdepth: 1
   :glob:
   
   EquationsJordanAlgebra

In Example 6.10, we find the quadrics in dual Pluecker coordinates that vanish on the congruence orbits of the nets :math:`\mathcal{L}_1`, :math:`\mathcal{L}_2` and :math:`\mathcal{L}_3` from Example 6.9. 

.. toctree::
	:maxdepth: 1
	:glob:

	QuadricsPluecker

The output files of the computations can be downloaded here:

:download:`L1res.txt <L1res.txt>`

:download:`L2res.txt <L2res.txt>`

:download:`L3res.txt <L3res.txt>`

Project page created: 27/10/2020

Code contributors: Arthur Bik

Code written by: Arthur Bik, 27/10/2020

Corresponding author of this page: Arthur Bik, arthur.bik@mis.mpg.de

Project contributors: Arthur Bik

Software used: SageMath




