loadPackage "Jets";
-- This package can be downloaded from https://github.com/galettof/Jets

-- ----------------
-- Input
-- * k - nonnegative integer
-- * I - zero-dimensional ideal
--
-- Output: the multiplicity of the ideal obtained by intersecting the ideal of
-- of the arc space of I with the ring of differential polynomials of order at most k
-- For example, for k = 0, one will get the multiplicity of I
-- ----------------
truncatedArcMultiplicity = (k, I) -> (
    n = k + 1;
    ringOfI = ring(I);
    varsOfI = vars(ringOfI);
    numbVariables = numgens(source(varsOfI));
    RProlong = QQ[ t_0 .. t_( numbVariables * (k + 1) - 1 ) ];
    varsProlong = toList ( t_0 .. t_( numbVariables * (k + 1) - 1 ) );	
    truncatedIdeal = ideal 0_RProlong;

    dimCur = dim truncatedIdeal;
    lenCur = infinity;
    lenPrev = infinity;

    while (dimCur > 0) or (lenPrev =!= lenCur) do (
        lenPrev = lenCur;
        jetI = jets( n , I );
        jetRing = QQ[ r_0 .. r_( numbVariables * (n + 1) - 1  ) ];
        jetVars = toList ( r_0 .. r_( numbVariables * (n + 1) - 1  ));	
        jetISub = sub( jetI, matrix{ reverse( jetVars ) } );
		jetIElim = eliminate( jetVars_{ ( numbVariables * (k + 1) ) .. ( numbVariables * (n + 1) - 1 ) }, jetISub );
        zeroes := apply(toList(1..(numbVariables * (n - k))), i -> 0);
		truncatedIdeal = sub( jetIElim, matrix{ join( varsProlong, zeroes ) } );
        n = n + 1;

        dimCur = dim truncatedIdeal;
        if dimCur > 0 then
            lenCur = infinity
        else (
            initialI = gin truncatedIdeal;
            monI = monomialIdeal initialI;
            stdPairs = standardPairs monI;
            lenCur = length stdPairs;
        );
        n = n + 1;
    );

	return lenCur;
)



               
