==============================================================
KP Solitons from Tropical Limits
==============================================================

| Here you may find the supplementary codes for the paper: 
| Claudia Fevola, Daniele Agostini, Yelena Mandelshtam, and Bernd Sturmfels: KP solitons from tropical limits
| In: Journal of symbolic computation, 114 (2023), p. 282-301
| DOI: `10.1016/j.jsc.2022.04.009 <https://dx.doi.org/10.1016/j.jsc.2022.04.009>`_ ARXIV: https://arxiv.org/abs/2101.10392 CODE: https://mathrepo.mis.mpg.de/KPSolitonsFromTropicalLimits

*Abstract of the paper*: We study solutions to the Kadomtsev-Petviashvili equation whose underlying algebraic curves undergo tropical degenerations. Riemann's theta function becomes a finite exponential sum that is supported on a Delaunay polytope. We introduce the Hirota variety which parametrizes all tau functions arising from such a sum. We compute tau functions from points on the Sato Grassmannian that represent Riemann-Roch spaces and we present an algorithm that finds a soliton solution from a rational nodal curve.

In this repository you will find the implementation of an algorithm in `MAPLE <https://swmath.org/software/545>`_ for computing the truncated tau function arising from a hyperelliptic curve defined over the field :math:`\mathbb{Q}(\epsilon)`.   

The *Maple* code can be viewed here:

.. toctree::
   :maxdepth: 1

   truncatedTau.rst  

In particular, a similar method can be used to compute the truncated tau function for a `(k,n)` soliton solution.
Here is the *Maple* code for computing the truncated tau function for the `(3,6)` soliton soution we worked out in Example 8:

.. toctree::
   :maxdepth: 1

   (3,6)-soliton.rst 

In Section 2 we list the seventeen types of Delaunay polytopes arising from metric graph of genus 4. As an example you may download the *Maple* code we used to calculate the Delaunay polytopes associated to the vertices of the Voronoi polytope associated to the metric graph appearing third in https://arxiv.org/abs/1707.08520, table 1: 

.. toctree::
   :maxdepth: 1

   DelaunayPolytope.rst 

Project page created: 5/02/2021.


Project contributors: Claudia Fevola, Daniele Agostini, Yelena Mandelshtam, and Bernd Sturmfels.

Software used: MAPLE 2019

Corresponding author of this page: Claudia Fevola, fevola@mis.mpg.de.
