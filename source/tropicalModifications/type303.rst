##########
Type (303)
##########

A tropical curve of type (303) is of the form

.. image:: type303.png
    :align: center
    :width: 10cm

Due to symmetry, we may assume :math:`a\geq c\geq e`. It is never embeddable as a tropical quartic curve in :math:`\mathbb{R}^2`.

To illustrate how a curve is embeddable in a tropical quartic curve as a tropical plane, consider the following configuration of lengths:

.. math:: a=1, \quad b=1, \quad c=1, \quad d=2, \quad e=1, \quad f=3.

For that, consider the following quartic polynomial with its highly degenerate tropical quartic curve:

.. code-block:: none

                poly g = (t19)*x4+x2y2+(t13)*y4+(2t6+2t5+2t4+2t3+2t2+2t+2)*x2y+(2t4+2t3+2t2+2t+2)*xy2+(t12+2t11+3t10+4t9+5t8+6t7+6t6+6t5+5t4+4t3+3t2+2t+1)*x2+(-2t2-4t-2)*xy+(t8+2t7+3t6+4t5+4t4+4t3+3t2+2t+1)*y2+(t7);
                drawTropicalCurve(g,"max");

.. math:: g &= t^{19}x^4+x^2y^2+t^{13}y^4+(2t^6+2t^5+2t^4+2t^3+2t^2+2t+2)x^2y\\
          &\quad +(2t^4+2t^3+2t^2+2t+2)xy^2\\
          &\quad +(t^{12}+2t^{11}+3t^{10}+4t^9+5t^8+6t^7+6t^6+6t^5+5t^4+4t^3+3t^2+2t+1)x^2\\
          &\quad +(-2t^2-4t-2)xy+(t^8+2t^7+3t^6+4t^5+4t^4+4t^3+3t^2+2t+1)y^2+t^7

.. image:: type303a.png
           :align: center
           :width: 10cm

The modification :math:`z_1=x+1+t+t^3+t^4`, which goes along :math:`w_x=0`, yields the following tropical curve after projecting onto the :math:`z_1y` plane (:math:`y` remaining the vertical direction):

.. code-block:: none

                poly g1=substitute(g,x,x-1-t-t3-t4);
                drawTropicalCurve(g1,"max");

.. image:: type303b.png
           :align: center
           :width: 10cm

The entirety of :math:`w_x<0` is mapped onto :math:`w_z=0`, while the edge on the line :math:`w_x=0` with multiplicity :math:`2` reveals a lolli with edge :math:`e` of length :math:`1` and cycle :math:`b` of length :math:`1`.

Similarly, the modifications :math:`z_2=y+1+t++t^2+t^4+t^5+t^6` and :math:`z_0=y-x-2itx`, where :math:`i\in\mathbb C` denotes the imaginary number, will reveal lollis of desired edge and cycle lengths:

.. code-block:: none

                poly g2=substitute(g,y,y-1-t-t2-t4-t5-t6);
                drawTropicalCurve(g2,"max");

.. image:: type303c.png
           :align: center
           :width: 10cm

.. code-block:: none

                ring s = (0,i),(t,x,y),dp;
                minpoly = i2+1;
                poly g = imap(r,g);
                poly g0 = substitute(g,y,y+x+2it*x);
                g0 = subst(g0,i,15);
                setring r;
                poly g0 = imap(s,g0);
                drawTropicalCurve(g0,"max");

(Note that we are cheating slightly in the final modification, as we are setting :math:`i=15` when it comes to drawing the picture)

.. image:: type303d.png
           :align: center
           :width: 10cm
