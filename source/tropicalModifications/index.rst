=========================================================================
Tropical Quartics and Canonical Embeddings for Tropical Curves of Genus 3
=========================================================================

.. image:: tropicalModification.png

In `[BJMS15] <https://dx.doi.org/10.1186/s40687-014-0018-1>`_ the authors study the moduli space of metric graphs that arise from tropical curves in :math:`\mathbb R^2`. In particular, they show that not all abstract tropical curves of genus 3 can be embedded as a tropical quartic in :math:`\mathbb R^2` and offer a complete characterization of all curves which admit such an embedding.


| In:
| Marvin Anas Hahn, Hannah Markwig, Yue Ren, and Ilya Tyomkin: Tropicalized quartics and canonical embeddings for tropical curves of genus 3
| In: International mathematics research notices, rnz084
| MIS-Preprint: `20/2018 <https://www.mis.mpg.de/publications/preprints/2018/prepr2018-20.html>`_ DOI: `10.1093/imrn/rnz084 <https://dx.doi.org/10.1093/imrn/rnz084>`_ ARXIV: https://arxiv.org/abs/1802.02440 CODE: https://mathrepo.mis.mpg.de/tropicalModifications


we show that all tropical curves of genus 3, which are not "realizably hyperellipcitc", can be embedded as tropical quartics in a tropical plane. This is done by explicit constructions for all curves which are not realizably hyperelliptic and obstructions for curves which are.

Here, we present examples for all our constructions, using the computer algebra system Singular_ and its library tropical.lib. All examples can be found bundled in the following :download:`Singular script <examples.sing>`.

We will exclusively work in the following ring and all code blocks on this website will require it to be the currently active base ring:

.. code-block:: none

                LIB "tropical.lib";
                ring r = (0,t),(x,y),dp;


.. _Singular: https://www.singular.uni-kl.de/


.. toctree::
   :maxdepth: 2

   type020.rst
   type000.rst
   type303.rst
   type212.rst
   type111.rst


| [BJMS15] Brodsky, Sarah; Joswig, Michael; Morrison, Ralph; Sturmfels, Bernd: Moduli of tropical plane curves. In: Research in the mathematical sciences, 2 (2015) 1, 4.
| DOI: `10.1186/s40687-014-0018-1 <https://dx.doi.org/10.1186/s40687-014-0018-1>`_ ARXIV: https://arxiv.org/abs/1409.4395

Project contributors: Marvin Anas Hahn, Hannah Markwig, Yue Ren, and Ilya Tyomkin

Software used: Singular
