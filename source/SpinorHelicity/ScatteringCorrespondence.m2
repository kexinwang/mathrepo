
--- This function generates a julia script to compute solutions of scattering equations for general k,n,r.

--- It still lacks a way to compute the gradient of the scattering potential, but one can do that by taking 
--- the expression for phi from this script and plugging in Mathematica to get the gradient and put it back in to julia.



loadPackage "SpechtModule";

signOfList = (L) -> (
    Lindexed = {};
    for i from 0 to #L -1 do (Lindexed = append(Lindexed, {L_i, i}));
    Lindexed = sort(Lindexed);
    permL = {};
    for i from 0 to #Lindexed -1 do (permL = append(permL, (Lindexed_i)_1) );
    return permutationSign(permL);
);

scatteringPotential = (k,n) -> (
    -- dim(X(k,n)) = ( k(n-k) -  (n-1))

    d = k*(n-k) -  (n-1);
    xVars = {};
    for i from 1 to d do(
        xVars = append(xVars, concatenate("x",toString(i)));
    );

    R = QQ[xVars];
    use R;

    varsR = vars  R;

    I = id_(R^k);
    vOnes = transpose(matrix( {toList(k:1)} ));
    hOnes = matrix({toList((n-k-1):1)});
    varsR = reshape(R^(k-1), R^(n-k-1), varsR);

    X = I|(hOnes||varsR)|vOnes;


    L = subsets((0..n-1),k);

    M = flatten entries gens minors(k, X);

    s := symbol x;

    phi = "";

    for i from 0 to #L-1 do(

        if i == 0 then(
            I = L_i;
            phi = concatenate(phi, "s",concatenate(apply(I,x->toString(x+1)))," * log((1+0*im)*",toString(M_i),") ");
        );
        if i > 0 then(
            I = L_i;
            phi = concatenate(phi, " + ", "s",concatenate(apply(I,x->toString(x+1)))," * log((1+0*im)*",toString(M_i),") ");
        );

        
    );

    return phi;
);

juliaScript = (k,n,r) ->(

            d = k*(n-k) -  (n-1); --- dimension of X(k,n)

            na = (k-r)*(n-k+r); --- dimension of Gr(k-r,n)
            nb = (n-k)*r; --- dimension of Gr(r, C^n / (V \cap W^\perp));
            nc = (n-2*k+r)*k; --- dimension of Gr(n-2k+r, C^n / (V \cap W^\perp));

            aVars = {}; -- list of a variables
            bVars = {}; -- list of b variables
            cVars = {}; -- list of c variables

            xVars = {}; -- list of x variables


            -- filling these lists

            for i from 1 to na do(
                aVars = append(aVars, concatenate("a",toString(i)));
            );
            for i from 1 to nb do(
                bVars = append(bVars, concatenate("b",toString(i)));
            );
            for i from 1 to nc do(
                cVars = append(cVars, concatenate("c",toString(i)));
            ); 

            for i from 1 to d do(
                xVars = append(xVars, concatenate("x",toString(i)));
            );


            -- making a ring R with all these variables

            R = QQ[aVars, bVars, cVars, xVars];

            use(R);



            --- Making the matrix whose columns represent X(k,n).


            varsR = (vars R)_(toList (na+nb+nc..na+nb+nc+d-1));

            I = id_(R^k);
            vOnes = transpose(matrix( {toList(k:1)} ));
            hOnes = matrix({toList((n-k-1):1)});
            varsR = reshape(R^(k-1), R^(n-k-1), varsR);

            X = I|(hOnes||varsR)|vOnes;



            --- Computing the scattering potential. This is to be plugged into mathematica later on to get its gradient.

            L = subsets((0..n-1),k);
            M = flatten entries gens minors(k, X);

            s := symbol x;

            phi = "";

            for i from 0 to #L-1 do(

                if i == 0 then(
                    I = L_i;
                    phi = concatenate(phi, "s",concatenate(apply(I,x->toString(x+1)))," * log( (1+0*im)*(",toString(M_i),")) ");
                );
                if i > 0 then(
                    I = L_i;
                    phi = concatenate(phi, " + ", "s",concatenate(apply(I,x->toString(x+1)))," * log( (1+0*im)*(",toString(M_i),"))");
                );

                
            );






            ---- Now we are making the matrix whose minors parametrize SH(k,n,r);

            Ia = id_(R^(k-r));
            Ib = id_(R^(r));
            Ic = id_(R^(n-2*k+r));

            Zb = substitute(0*random(ZZ^(r),ZZ^(k-r)),R);
            Zc = substitute(0*random(ZZ^(n-2*k+r),ZZ^(k-r)),R);



            varsR = (vars R)_(toList (0..na+nb+nc-1));



            a = reshape(R^(k-r), R^(n-k+r), varsR_(toList(0..na-1)));
            b = reshape(R^r, R^(n-k), varsR_(toList(na..na+nb-1)));
            c = reshape(R^(n-2*k+r), R^k, varsR_(toList(na+nb..na+nb+nc-1)));

            X = (Zb|Ib|b) || (Ia|a) || (Zc|Ic|c);






            --- Stroring the expressions of p_I, q_I, s_I in terms of the a,b,c variables.

            L = subsets((0..n-1), k);


            pRel = {};
            qRel = {};
            sRel = {};


            pVars = {};
            qVars = {};
            sVars = {};



            for i from 0 to #L- 1 do (

                I = L_i;
                Ib = toList(set(0..n-1) - set(I));
                e = signOfList(join(I,Ib));

                pI = determinant(X^(toList (0..k-1))_I);
                qI =  e * determinant(X^(toList (r..n-k+r-1))_Ib);
                sI = pI*qI;
                
                pVars = append(pVars, concatenate {"p", concatenate( apply(I, x->toString(x+1)) ) } );
                pRel = append(pRel, concatenate {"p", concatenate( apply(I, x->toString(x+1))) , " = ", toString(pI)});    
                
                qVars = append(qVars, concatenate {"q", concatenate( apply(I, x->toString(x+1)) ) } );
                qRel = append(qRel, concatenate {"q", concatenate( apply(I, x->toString(x+1))) , " = ", toString(qI)});    
                
                sVars = append(sVars, concatenate {"s", concatenate( apply(I, x->toString(x+1)) ) } );
                sRel = append(sRel, concatenate {"s", concatenate( apply(I, x->toString(x+1))) , " = ", toString(sI)});      
            );



            ----- Now we write a julia script incorporating everything

                
            julia = "using HomotopyContinuation; \n \n ";

            julia = concatenate(julia,"@var ",concatenate(apply(join(pVars, qVars), x-> concatenate(toString(x), " ")  )), "\n \n" );

            julia = concatenate(julia,"@var ", concatenate(apply(flatten entries (vars R)_(toList (0..na+nb+nc-1)) , x->concatenate(toString(x)," ") ) ), "\n\n\n");

            julia = concatenate(julia,"@var ", concatenate(apply(flatten entries (vars R)_(toList (na+nb+nc..na+nb+nc+d-1)) , x->concatenate(toString(x)," ") ) ), "\n\n\n");

            julia = concatenate(julia, concatenate( toList ( apply(0..#sVars-1, i -> concatenate( toString(sVars_i), " =  ", toString(pVars_i), " * ", toString(qVars_i), "\n")  ) ) ));

            julia = concatenate(julia, "\n\n");   


            julia = concatenate(julia, "f = ", phi);   

            julia = concatenate(julia, "\n\n");   





            julia = concatenate(julia, "F = System([\n ", concatenate(apply(xVars, x-> concatenate("differentiate(f, ", x,"),\n")))    , "\n], \n\n");


            julia = concatenate(julia, "variables = [ ", concatenate(apply( flatten entries (vars R)_(toList (na+nb+nc..na+nb+nc+d-1))  , x-> concatenate(toString(x), ", ")  )), "], \n\n" );

            julia = concatenate(julia, "parameters = [ ", concatenate(apply(join(pVars, qVars), x-> concatenate(toString(x), ",")  )), "],)\n\n" );

            julia = concatenate(julia, "monres = monodromy_solve(F) \n \n \n ");




            spqRels = "";

            for i from 0 to #sRel-1 do(
                spqRels = concatenate( spqRels, sRel_i, "\n");
            );

            julia = concatenate(julia, "\n\n\n");



            for i from 0 to #pRel-1 do(
                spqRels = concatenate( spqRels, pRel_i, "\n");
            );

            julia = concatenate(julia, "\n\n\n");


            for i from 0 to #qRel-1 do(
                spqRels = concatenate( spqRels, qRel_i, "\n");
            );

            julia = concatenate(julia, spqRels, "\n\n\n");


            julia = concatenate(julia, "\n\n\n");


            julia = concatenate(julia, "f = ", phi,";\n\n");   

            julia = concatenate(julia, "G = System([\n ", concatenate(apply(xVars, x-> concatenate("differentiate(f, ", x,"),\n")))    , "\n], \n\n");
            julia = concatenate(julia, "variables = [ ", concatenate(apply( flatten entries (vars R)_(toList (na+nb+nc..na+nb+nc+d-1))  , x-> concatenate(toString(x), ", ")  )), "], \n\n" );
            julia = concatenate(julia, "parameters = [ ", concatenate(  apply(flatten entries (vars R)_(toList (0..na+nb+nc-1)) , x->concatenate(toString(x),",") ), "])\n") );




            julia = concatenate(julia, concatenate(apply(flatten entries (vars R)_(toList (0..na+nb+nc-1)) , x->concatenate("z", toString(x),", ") ) ), " = 10*randn(ComplexF64, ",toString(na+nb+nc),");\n\n\n");


            zspqRels = replace("a","za",spqRels);
            zspqRels = replace("b","zb",zspqRels);
            zspqRels = replace("c","zc",zspqRels);

            zspqRels = replace("p","zp",zspqRels);
            zspqRels = replace("q","zq",zspqRels);



            julia = concatenate(julia, zspqRels,"\n\n");

            targets1 = concatenate("target1 = [", apply(join(pVars, qVars), x-> concatenate(toString(x), ",")  ), "]");
            targets1 = replace("p","zp", targets1);
            targets1 = replace("q","zq", targets1);


            julia = concatenate(julia, targets1);

            julia = concatenate(julia, "
            start_params = parameters(monres);

            target_params = target1;

            R = HomotopyContinuation.solve(F, solutions(monres),   
                                            start_parameters = start_params,    
                                            target_parameters = target_params)

            target_params_G = [",

            concatenate(apply(flatten entries (vars R)_(toList (0..na+nb+nc-1)) , x->concatenate("z", toString(x),", ") ) )

            ," ];






            solsR = solutions(R); \n

            N = size(solsR)[1];  \n

            remaining  = Vector{Int64}(1:N); \n



            covered = []; \n


            while size(remaining)[1] > 0 \n

                i = remaining[1]; \n

                Sol = monodromy_solve(G, solsR[i], target_params_G);\n
                
                stat = Sol.statistics;\n
                sol = stat.solutions;\n
                d = size(sol)[1];\n
                Nsol = sol[d];\n

                for i in 1:Nsol\n
                    sol_i = Sol.results[i].solution;\n
                    \n
                    for j in 1:N \n
                    
                        err = sum(abs.(solsR[j] - sol_i)); \n
                        if err<1e-10 \n
                            append!(covered, j); \n
                        end \n
                    end\n

                end\n

                println(\"cluster size = \", size(covered)[1]); \n
                
                remaining = filter(e-> !(e in covered), remaining); \n
                covered = []; \n
                #println(\"remaining elements =  \", size(remaining)[1]); \n
                
                println(\"\\n\\n\") \n

            end
            \n\n
            ");


           


            return julia;

);

k = 3;
n = 7;
r = 1;


---- Run the following code for a julia consol:

print(juliaScript(k,n,r))