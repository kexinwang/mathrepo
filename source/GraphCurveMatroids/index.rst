=====================
Graph Curve Matroids
=====================

| This page contains auxiliary files to the paper:
| Alheydis Geiger, Kevin Kühn, and Raluca Vlad: Graph Curve Matroids
| arXiv: `https://arxiv.org/abs/2311.08332 <https://arxiv.org/abs/2311.08332>`_

| **Abstract**: We introduce a new class of matroids, called  *graph curve matroids*.  A graph curve matroid is associated to a graph and defined on the vertices of the graph as a ground set. We prove that these matroids provide a combinatorial description of hyperplane sections of degenerate canonical curves in algebraic geometry. Our focus lies on graphs that are 2-connected and trivalent, which define *identically self-dual* graph curve matroids, but we also develop extensions. Finally, we provide an algorithm that computes the graph curve matroid associated to a given graph, as well as an implementation and data of examples that can be used in :code:`Macaulay2`.

.. One step is to build the cycle matroid of the graph, which can be done by hand (loops correspond to additional 1-circuits, for each edge :math:`e'` parallel to :math:`e` take the set of all bases containing :math:`e` and substitute :math:`e'`). Another modification to the code above needs to be applied to the computation of the set of incident edges, as this function currently only works for simple graphs. 
.. Subsequently, you can briefly describe the purpose and structure of this mathrepo side in a couple of sentences.


Constructing the graph curve matroid
------------------------------------
We have implemented the algorithm for computing the graph curve matroid of any given graph in :code:`Macaulay2`. The code can be downloaded here :download:`graphCurveMatroid.m2 <graphCurveMatroid.m2>`.

Please note that this code only works for simple graphs, as :math:`\texttt{Macaulay2}` does not work with parallel edges in graphs.
However, one can still construct the graph curve matroid of a non-simple graph, but the code needs some adaptation. The main functionalities that need modification are the computation of the cycle matroid, which works for graphs with loops but without parallel edges, and the computation of the set of incident edges :math:`\delta(A)` for a vertex set :math:`A`, which in the above code is implemented only for simple graphs.


* Here is an example how to use the code in :code:`Macaulay2` for a simple graph:

.. code-block:: macaulay2

		value get "graphCurveMatroid.m2"; 

		G = graph({{1, 2}, {2, 3}, {3, 1}, {3, 5}, {4, 5}, {4, 2}, {5, 7}, {6, 7}, {6, 4}, {7, 8}, {8, 6}, {1, 8}}, EntryMode => "edges");

		M = graphCurveMatroid G;

		M == dual(M)
		true

		rank M
		4

		#bases M
		54

		#circuits M
		26

.. image:: doublehouse.png
           
The figure displays the double house graph from Figure 2 in the paper with the labeling as used to make the graph :math:`\texttt{G}` in the code example. So the corresponding graph curve matroid for the double house graph is identically self-dual of rank 4, has 54 bases and 26 circuits.


**Runtime of the algorithm**
 
.. image:: runtime1.png

The runtime of the algorithm to compute the graph curve matroid grows exponentially with the number of vertices of the graph. For example, the computation of a graph curve matroid of a trivalent graph with 14 vertices takes already 1673.56 seconds. 



Graphs and their graph curve matroids
-------------------------------------

* The data on trivalent (also called cubic) simple graphs originated here `https://houseofgraphs.org/meta-directory/cubic <https://houseofgraphs.org/meta-directory/cubic>`_.

Below, we provide the edge sets for trivalent simple graphs with up to 12 vertices and files with the bases of the associated graph curve matroids in a :code:`Macaulay2` readable format.

**Graphs**

| :download:`graphs_04vertices.txt <graphs_04vertices.txt>` 
| :download:`graphs_06vertices.txt <graphs_06vertices.txt>`
| :download:`graphs_08vertices.txt <graphs_08vertices.txt>`
| :download:`graphs_10vertices.txt <graphs_10vertices.txt>`
| :download:`graphs_12vertices.txt <graphs_12vertices.txt>`

**Graph Curve Matroids**

| :download:`matroids_graphs_04vertices.txt <matroids_graphs_04vertices.txt>`
| :download:`matroids_graphs_06vertices.txt <matroids_graphs_06vertices.txt>`
| :download:`matroids_graphs_08vertices.txt <matroids_graphs_08vertices.txt>`
| :download:`matroids_graphs_10vertices.txt <matroids_graphs_10vertices.txt>`
| :download:`matroids_graphs_12vertices.txt <matroids_graphs_12vertices.txt>`

**How to use the files:**
The graph arising from the edges in the ith entry of the list in graphs_04vertices.txt gives rise to the graph curve matroid given by the set of bases stored in the ith entry of the list in matroids_graphs_04vertices.txt.

.. code-block:: macaulay2

		value get "graphCurveMatroid.m2"; 

		GraphsList = value get "graphs_06vertices.txt";

		#GraphsList 
		2

		G = graph(GraphsList#0, EntryMode =>"edges"); --build a graph from the first list of edges
		
		MatroidsList = value get "matroids_graphs_06vertices.txt";

		M = matroid({0,1,2,3,4,5}, MatroidsList#0, EntryMode => "bases") --build a matroid from the list of bases

		M == graphCurveMatroid G; 
		true


.. If applicable:
.. The code is presented as a Jupyter notebook in Julia which can be downloaded here: download:`my_notebook.ipynb <my_notebook.ipynb>`.


Constructing a basis
--------------------
Given a trivalent, 2-connected graph :math:`G` and a vertex :math:`v`, the proof of Proposition 4.3 provides a constructive way to directly create a basis of :math:`M_G` containing :math:`v` without computing the whole matroid :math:`M_G` first. Correctness follows from Lemmas 4.1 and 4.2.


.. image:: Algorithm2.png
 

We implemented this algorithm in :code:`Macaulay2`. Our code can be downloaded here: :download:`basis-construction.m2 <basis-construction.m2>`. The code asks the user to input a graph (in a Macaulay2-readable format) and a vertex, and outputs to the user a basis of the associated graph curve matroid containing the given vertex.


.. code-block:: macaulay2

		value get "basis-construction.m2"
		Graph: graph({{1, 2}, {2, 3}, {3, 1}, {3, 5}, {4, 5}, {4, 2}, {5, 7}, {6, 7}, {6, 4}, {7, 8}, {8, 6}, {1, 8}}, EntryMode => "edges")
		vertex: 3
		set {1, 3, 5, 7}
 
------------------------------------------------------------------------------------------------------------------------------

Project page created: 08/11/2023

Project contributors: Alheydis Geiger, Kevin Kühn, and Raluca Vlad

Corresponding author of this page: Alheydis Geiger, geiger@mis.mpg.de

.. Responsible person for project page on https://mathrepo.mis.mpg.de (optionally with persistent mail address):
 
Code written by: Alheydis Geiger and Raluca Vlad

.. Jupyter notebook written by: Jane Doe, dd/mm/yy

Software used: Macaulay2 (v1.19.1)

System setup used: MacBook Pro with macOS 12.7, Processor 2,6 GHz Quad-Core Intel Core i7, Memory 16 GB, Graphics Intel HD Graphics 530.

.. If applicable, please include all relevant information regarding the code: Version and system setup you used for computing your results in the corresponding paper.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated 09/07/2024.
