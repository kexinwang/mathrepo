needsPackage "Matroids"


G = value read "Graph: ";     -- read a trivalent 2-connected graph G
v = value read "vertex: ";    -- read a vertex of G


g = (#edges(G))//3 + 1;    -- the genus of G
N = dual(matroid(G));      -- the bond matroid of G


-- we construct a basis B containing v
B = set{v};
A = B;


-- construct set of edges of G that are incident to A
AIncidentEdges = set{};
for i from 0 to 3*g-4 do
{
  e = toList((edges(G))#i);
  if e#0 == v or e#1 == v then AIncidentEdges = AIncidentEdges + set{i};
};


while #B < g-1 do
{

  -- find a vertex w of G that is not in A, but is incident to A
  w = 0;
  for i in vertices(G) do
  {
    if member(i,A) == false and neighbors(G,i) * A =!= set{} then
    {
      w = i; break;
    };
  };


  -- construct set of edges of G that are incident to w
  wIncidentEdges = set{};
  for i from 0 to 3*g-4 do
  {
    e = toList((edges(G))#i);
    if e#0 == w or e#1 == w then wIncidentEdges = wIncidentEdges + set{i};
  };


  -- update B if condition holds
  if rank(N, AIncidentEdges + wIncidentEdges) > rank(N, AIncidentEdges) then B = B + set{w};


  -- update A and its incident edges
  A = A + set{w};
  AIncidentEdges = AIncidentEdges + wIncidentEdges;

};


print(B);

