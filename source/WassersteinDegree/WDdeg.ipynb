{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3c0db412-4458-413a-b302-8dcd0e377d6e",
   "metadata": {},
   "source": [
    "# Minimal polynomial for the Wasserstein distance degree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f73116c1-c9bd-43a7-a3fa-d16d7a0b1e8e",
   "metadata": {},
   "source": [
    "This notebook contains the code explained in Seciton 4.2 of the paper \n",
    "**\"The algebraic degree of the Wasserstein distance\"**\n",
    "by Chiara Meroni, Bernhard Reinke, and Kexin Wang."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "6987436c-f281-4709-9699-e2cd4cfcaebc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Welcome to Nemo version 0.43.3\n",
      "\n",
      "Nemo comes with absolutely no warranty whatsoever\n",
      "  ___   ____   ____    _    ____\n",
      " / _ \\ / ___| / ___|  / \\  |  _ \\   |  Combining ANTIC, GAP, Polymake, Singular\n",
      "| | | |\\___ \\| |     / _ \\ | |_) |  |  Type \"?Oscar\" for more information\n",
      "| |_| | ___) | |___ / ___ \\|  _ <   |  Manual: https://docs.oscar-system.org\n",
      " \\___/ |____/ \\____/_/   \\_\\_| \\_\\  |  Version 1.0.2\n"
     ]
    }
   ],
   "source": [
    "using Oscar\n",
    "using HomotopyContinuation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "917b1a78-0864-4e51-965f-d6ce14150160",
   "metadata": {},
   "source": [
    "We implement here the algorithm for cubic polynomials. An analogous procedure can be used for polynomials of other degrees. For linear or quadratic polynomials, finding the minimal polynomial is quite straightforward. For degree $4$ or higher, the elimination step might not terminate in reasonable time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a404d8a-8149-4e2a-a15a-44f79428e9d2",
   "metadata": {},
   "source": [
    "## Cubic polynomials"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6e6ae89f-26b0-4aa6-9a99-87e1b400cc93",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "d = 3;"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0024a986-b34f-42a8-b88e-552ae92a22c7",
   "metadata": {},
   "source": [
    "The Wasserstein distance degree of two univariate polynomials $p, q$ is the algebraic degree of the Wasserstein distance between $p$ and $q$.\n",
    "In degree three, the Wasserstein distance degree depends only on the number of their real and complex roots."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65bbb5a0-e877-4894-b08b-92faee525242",
   "metadata": {},
   "source": [
    "We define the functions that provide the minimal polynomial of WDdeg($p,q$). Each of them constructs an ideal encoding the coefficients of the polynomials, and it eliminates an auxiliary variable. The output is a multiple of the minimal polynomial of the Wasserstein distance of $p, q$. For sufficiently generic $p, q$, the output is irreducible, hence it is the actual minimal polynomial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ffb38cbb-f2e0-4627-80b0-44ba75881c97",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "minimal_poly_33 (generic function with 1 method)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function minimal_poly_33(coeff_p, coeff_q)\n",
    "    ## Input:\n",
    "        ## coeff_p = list of coefficients of p, polynomial with 3 real roots\n",
    "        ## coeff_q = list of coefficients of q, polynomial with 3 real roots\n",
    "    ## Output: \n",
    "        ## a list of primary ideals among which there is the minimal polynomial of the Wasserstein distance of p,q\n",
    "    R, a, b, z = polynomial_ring(QQ, \"a\" => 1:d, \"b\" => 1:d, \"z\" =>1:1)\n",
    "    # a[1], a[2], a[3] denote the real roots of p\n",
    "    # b[1], b[2], b[3] denote the real roots of q\n",
    "    S, t = polynomial_ring(R, \"t\")\n",
    "    pp = prod(t-a[i] for i=1:d)\n",
    "    coeff_pp = collect(Oscar.coefficients(pp))\n",
    "    qq = prod(t-b[i] for i=1:d)\n",
    "    coeff_qq = collect(Oscar.coefficients(qq))\n",
    "    I = ideal(R, [[coeff_p[i]-coeff_pp[i] for i=1:d+1];\n",
    "                  [coeff_q[i]-coeff_qq[i] for i=1:d+1];\n",
    "                   z[1] - sum([(a[i]-b[i])^2 for i=1:d])])\n",
    "    J = eliminate(I, [a;b])\n",
    "    return primary_decomposition(J)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "63d06a60-c9fa-4010-960f-fd52844c60de",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "minimal_poly_13 (generic function with 1 method)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function minimal_poly_13(coeff_p, coeff_q)\n",
    "    ## Input:\n",
    "        ## coeff_p = list of coefficients of p, polynomial with 1 real root\n",
    "        ## coeff_q = list of coefficients of q, polynomial with 3 real roots\n",
    "    ## Output: \n",
    "        ## a list of primary ideals among which there is the minimal polynomial of the Wasserstein distance of p,q\n",
    "    R, a, b, z = polynomial_ring(QQ, \"a\" => 1:d, \"b\" => 1:d, \"z\" =>1:1)\n",
    "    # a[1], a[2]+ia[3], a[2]-ia[3] denote the roots of p\n",
    "    # b[1], b[2], b[3] denote the real roots of q\n",
    "    I = ideal(R, [a[1]+2*a[2]+coeff_p[3]//coeff_p[4], 2*a[1]*a[2]+a[2]^2+a[3]^2-coeff_p[2]//coeff_p[4], a[1]*(a[2]^2+a[3]^2)+coeff_p[1]//coeff_p[4],\n",
    "                  b[1]+b[2]+b[3]+coeff_q[3]//coeff_q[4], b[1]*b[2]+b[2]*b[3]+b[3]*b[1]-coeff_q[2]//coeff_q[4], b[1]*b[2]*b[3]+coeff_q[1]//coeff_q[4],\n",
    "                  z[1] - ((a[1]-b[1])^2 + (a[2]-b[2])^2 + (a[2]-b[3])^2 + 2*a[3]^2)])\n",
    "    J = eliminate(I, [a;b])\n",
    "    return primary_decomposition(J)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3c06a6ba-eb54-4e79-940d-f42fa91bfcf7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "minimal_poly_11 (generic function with 1 method)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function minimal_poly_11(coeff_p, coeff_q)\n",
    "    ## Input:\n",
    "        ## coeff_p = list of coefficients of p, polynomial with 1 real root\n",
    "        ## coeff_q = list of coefficients of q, polynomial with 1 real root\n",
    "    ## Output: \n",
    "        ## a list of primary ideals among which there is the minimal polynomial of the Wasserstein distance of p,q\n",
    "    R, a, b, z = polynomial_ring(QQ, \"a\" => 1:d, \"b\" => 1:d, \"z\" =>1:1)\n",
    "    # a[1], a[2]+ia[3], a[2]-ia[3] denote the roots of p\n",
    "    # b[1], b[2]+ib[3], b[2]-ib[3] denote the roots of q\n",
    "    # here the associated graphs Gamma have different cycle structures, hence we have multiple options for the minimal polynomial\n",
    "    I1 = ideal(R, [a[1]+2*a[2]+coeff_p[3]//coeff_p[4], 2*a[1]*a[2]+a[2]^2+a[3]^2-coeff_p[2]//coeff_p[4], a[1]*(a[2]^2+a[3]^2)+coeff_p[1]//coeff_p[4],\n",
    "                  b[1]+2*b[2]+coeff_q[3]//coeff_q[4], 2*b[1]*b[2]+b[2]^2+b[3]^2-coeff_q[2]//coeff_q[4], b[1]*(b[2]^2+b[3]^2)+coeff_q[1]//coeff_q[4],\n",
    "                  z[1] - ((a[1]-b[1])^2 + 2*(a[2]-b[2])^2 + 2*(a[3]-b[3])^2)])\n",
    "    J1 = eliminate(I1, [a;b])\n",
    "    I2 = ideal(R, [a[1]+2*a[2]+coeff_p[3]//coeff_p[4], 2*a[1]*a[2]+a[2]^2+a[3]^2-coeff_p[2]//coeff_p[4], a[1]*(a[2]^2+a[3]^2)+coeff_p[1]//coeff_p[4],\n",
    "                  b[1]+2*b[2]+coeff_q[3]//coeff_q[4], 2*b[1]*b[2]+b[2]^2+b[3]^2-coeff_q[2]//coeff_q[4], b[1]*(b[2]^2+b[3]^2)+coeff_q[1]//coeff_q[4],\n",
    "                  3*z[1] - ((a[1]-b[2])^2 + a[3]^2 + (a[2]-b[2])^2 + (a[3]+b[3])^2 + (b[1]-a[2])^2 + b[3]^2)])\n",
    "    J2 = eliminate(I2, [a;b])\n",
    "    return [primary_decomposition(J1) , primary_decomposition(J2)] \n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd6e3779-af8f-47cf-a063-3f6f5883651b",
   "metadata": {},
   "source": [
    "We put the three functions together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "5bd8a2b0-c2b8-409f-b03b-156ec12da924",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "minimal_poly (generic function with 1 method)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function minimal_poly(coeff_p, coeff_q)\n",
    "    ## Input:\n",
    "        ## coeff_p = list of coefficients of p\n",
    "        ## coeff_q = list of coefficients of q\n",
    "    ## Output: \n",
    "        ## [(n,m), L] where\n",
    "        ## n is the number of real roots of p\n",
    "        ## m is the number of real roots of q\n",
    "        ## L is a list of primary ideals among which there is the minimal polynomial of the Wasserstein distance of p,q\n",
    "    @var t\n",
    "    # we compute the number of real and complex roots of p and q\n",
    "    p = t^3*coeff_p[1]+t^2*coeff_p[2]+t*coeff_p[3]+coeff_p[4]\n",
    "    real_p = nreal(HomotopyContinuation.solve(System([p])))\n",
    "    q = t^3*coeff_q[1]+t^2*coeff_q[2]+t*coeff_q[3]+coeff_q[4]\n",
    "    real_q = nreal(HomotopyContinuation.solve(System([q])))\n",
    "    # based on the number of real roots, we apply the appropriate function\n",
    "    if real_p == 3\n",
    "        if real_q == 3\n",
    "            return [(real_p, real_q), minimal_poly_33(coeff_p, coeff_q)]\n",
    "        elseif real_q == 1\n",
    "            return [(real_p, real_q), minimal_poly_13(coeff_q, coeff_p)]\n",
    "        end\n",
    "    elseif real_p == 1\n",
    "        if real_q == 3\n",
    "            return [(real_p, real_q), minimal_poly_13(coeff_p, coeff_q)]\n",
    "        elseif real_q == 1\n",
    "            return [(real_p, real_q), minimal_poly_11(coeff_p, coeff_q)]\n",
    "        end\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7055bf8-4e12-4574-a859-9aef48908324",
   "metadata": {},
   "source": [
    "## Tests"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1371c8a-6ab7-4aeb-b587-455bd9f749ce",
   "metadata": {},
   "source": [
    "We test our algorithm on various coefficient lists. We start with two polynomials that have all real roots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c59728cc-2253-4706-949f-4ee444f171a0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×4 Matrix{Int64}:\n",
       " -951  311  -32  1"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "coeff_p = [-32 47 -15 1] # this represents the polynomial p = -32 + 47*x - 15*x^2 + x^3\n",
    "coeff_q = [-951 311 -32 1] # this represents the polynomial q = -951 + 311*x - 32*x^2 + x^3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "39929bc1-f5a2-4ab8-8352-0b938187f205",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32mTracking 3 paths... 100%|███████████████████████████████| Time: 0:00:14\u001b[39m\n",
      "\u001b[34m  # paths tracked:                  3\u001b[39m\n",
      "\u001b[34m  # non-singular solutions (real):  3 (3)\u001b[39m\n",
      "\u001b[34m  # singular endpoints (real):      0 (0)\u001b[39m\n",
      "\u001b[34m  # total solutions (real):         3 (3)\u001b[39m\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "2-element Vector{Any}:\n",
       " (3, 3)\n",
       " Tuple{MPolyIdeal{QQMPolyRingElem}, MPolyIdeal{QQMPolyRingElem}}[(Ideal with 1 generator, Ideal with 1 generator)]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mp = minimal_poly(coeff_p, coeff_q)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abfceae5-0fe5-45e1-9e57-47990116e533",
   "metadata": {},
   "source": [
    "The output confirms that both $p$ and $q$ have $3$ real roots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "cdad3269-cb30-4163-9986-1395c8653eec",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Ideal generated by\n",
       "  z[1]^6 - 1278*z[1]^5 + 660151*z[1]^4 - 175286252*z[1]^3 + 25035024823*z[1]^2 - 1809005947030*z[1] + 51596679643601"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mp[2][1][1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee6221c2-e129-46b9-b6f2-bf1b3599cf67",
   "metadata": {},
   "source": [
    "As predicted in Table 2 of [arXiv:2401.12735], the degree of the minimal polynomial is $6$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db4cce83-f64e-4a9a-b671-9877390ebbb9",
   "metadata": {},
   "source": [
    "For a polynomial with one real root, and one with three real roots we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "53667b9c-73ff-48f5-8a2d-7a2a2858e2b9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×4 Matrix{Int64}:\n",
       " 3  4  -5  1"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "coeff_p = [-56 87 -94 1] \n",
    "coeff_q = [3 4 -5 1] "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "1191bf2a-19c5-4a7c-aa67-138635459ea6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Vector{Any}:\n",
       " (1, 3)\n",
       " Tuple{MPolyIdeal{QQMPolyRingElem}, MPolyIdeal{QQMPolyRingElem}}[(Ideal with 1 generator, Ideal with 1 generator)]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mp = minimal_poly(coeff_p, coeff_q)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "30c801d6-505d-4df9-a1a7-266e38406ada",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Oscar.degree(mp[2][1][1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28468d1d-bbcc-45fb-a744-378b2c868203",
   "metadata": {},
   "source": [
    "Whereas for two polynomials with one real root each, we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "1f4db786-1414-4b7f-8114-93a64b053465",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×4 Matrix{Int64}:\n",
       " 4  -5  -1  3"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "coeff_p = [-7 -3 1 2]\n",
    "coeff_q = [4 -5 -1 3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "fb454e0d-eeb7-4930-9f09-16a9c13ec5b3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Vector{Any}:\n",
       " (1, 1)\n",
       " Vector{Tuple{MPolyIdeal{QQMPolyRingElem}, MPolyIdeal{QQMPolyRingElem}}}[[(Ideal with 1 generator, Ideal with 1 generator)], [(Ideal with 1 generator, Ideal with 1 generator)]]"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mp = minimal_poly(coeff_p, coeff_q)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81b7ad4b-7071-4b4b-bb9c-441701892121",
   "metadata": {},
   "source": [
    "In this case we get $2$ candidates for the minimal polynomial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "8f5cc911-e63d-4327-bc72-dedade2abaa4",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Vector{Vector{Tuple{MPolyIdeal{QQMPolyRingElem}, MPolyIdeal{QQMPolyRingElem}}}}:\n",
       " [(Ideal with 1 generator, Ideal with 1 generator)]\n",
       " [(Ideal with 1 generator, Ideal with 1 generator)]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mp[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d1bb124-3c6e-48c2-8560-cd0d0ff84aad",
   "metadata": {},
   "source": [
    "However, the two candidates have the same degree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "b415b298-c420-4166-8b89-f65ef6ac063e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(18, 18)"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(Oscar.degree(mp[2][1][1][1]), Oscar.degree(mp[2][2][1][1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab9dbbb7-b2d3-49a6-b466-b16df019ae03",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.10 1.10.2",
   "language": "julia",
   "name": "julia-1.10-1.10"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
