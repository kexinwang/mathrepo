#######################################
Theta Divisor of a Rational Nodal Curve
#######################################

This page contains the code computing the theta divisor of an irreducible rational nodal curve. The computation of the elimination ideal terminates fast for :math:`g\leq 6`. If you want to run the code yourself, you may download the following file :download:`theta_divisor.m2 <theta_divisor.m2>`.

.. code-block:: macaulay2

                 restart
                 C = ZZ/32999
                 --choose a value for the genus g
 		g = 5
		R = C[x_1..x_(g-1),k_1..k_(2*g),q_1..q_g]


		--first addend in the definition of the image
		imAbel1 = mutableMatrix(R,1,g);

		for i from 0 to 0 do {
 		   for j from 0 to g-1 do {
			imAbel1_(i,j)=1
		    }	
		}  

		for i from 0 to 0 do {
		    for j from 0 to g-1 do {
			    for l from 1 to g-1 do {
				imAbel1_(i,j)=imAbel1_(i,j)*(1-k_(2*j+2)*x_(l))
			    }
		    }	
		} 
		imAbel1 = matrix(imAbel1);
		--second addend in the definition of the image
		imAbel2 = mutableMatrix(R,1,g);

		for i from 0 to 0 do {
		    for j from 0 to g-1 do {
			imAbel2_(i,j)=1
		    }	
		}  

		for i from 0 to 0 do {
		    for j from 0 to g-1 do {
			    for l from 1 to g-1 do {
				imAbel2_(i,j)=imAbel2_(i,j)*(1-k_(2*j+1)*x_(l))
			    }
		    }	
		} 
		imAbel2 = matrix(imAbel2);

		imAbel = {};

		for i from 0 to 0 do {
		    for j from 0 to g-1 do 
		    imAbel = append(imAbel, q_(j+1)*imAbel1_(i,j)-imAbel2_(i,j))  
		}
		--Define the ideal 
		I = ideal(imAbel);

		--List of x variables
		X = {};
		for i from 0 to g-2 do
		    X = append(X,x_(i+1))
		gbTrace = 3
		I = eliminate(I,X);
		mingens I; 

		--List of q variables 
		Q = {};
		for i from 0 to g-1 do
		    Q = append(Q,q_(i+1)) 
		f = (mingens I)_(0,0);
		c = flatten entries((coefficients(f,Variables=>Q))_1);

		-- now we compute the coefficients of the theta divisor and notice 
		-- that they coincide with the parametrization given by the map \phi 
		-- described in section 2     
		(coefficients(f,Variables=>Q))_0
		aParam = apply(c,i->factor i)
 
		