wavePairs = method(Options => { ModPlucker => true, Patch => null })
wavePairs(Matrix, ZZ) := o -> (A, r) -> (
  if r == 0 then return waveIncidenceRZero(A);
  R := ring A;
  KK := coefficientRing R;
  n := numgens R;
  if r < 0 or r >= n then error("expected argument between 0 and " | toString(n-1) | ".");

  s := symbol s;
  w := symbol w;
  R' := local R';
  S := local S;
  if o.Patch === null then (
    R' = KK[s_(1,1)..s_(n-r, n), w_1..w_(n-r)];
    S = transpose genericMatrix(R',n,n-r);
  )
  else (
    R' = KK[s_(1,1)..s_(n-r, r), w_1..w_(n-r)];
    S = transpose genericMatrix(R',r,n-r);
    Id := map(R'^(n-r), R'^(n-r), 1);
    patch := null;
    if instance(o.Patch, List) then (
      if #o.Patch != n-r then error("option Patch expects a list with " | toString(n-r) | " entries");
      patch = o.Patch;
    ) else if instance(o.Patch, ZZ) and o.Patch != 0 then (
      patch = (subsets(n,n-r))#(o.Patch);
    );

    if patch =!= null then (
      S = transpose entries S;
      Id = transpose entries Id;
      ids := apply(Id, sort patch, (i,j) -> {i,j});
      scan(ids, i -> S = insert(last i, first i, S));
      S = matrix transpose S;
    ) else S = Id | S;
  );

  phi := map(R', R, matrix{{w_1..w_(n-r)}} * S);

  k := numColumns A;
  z := symbol z;
  T := R'[z_1..z_k];
  M := sub(phi(A), T) * transpose vars T;

  wVars := take(gens R', -(n-r)) / (wi -> sub(wi, T));
  J' := ideal last coefficients(M, Variables => wVars);

  -- remove wVars
  SS := KK[s_(1,1)..s_(n-r,n), z_1..z_k, MonomialOrder => {(n-r)*n, k}];
  J := sub(J', SS);

  SSbar := SS/J;

  G := Grassmannian(n-r-1, n-1, CoefficientRing => KK);
  RG := ring G; -- ring of Grassmannian

  T = RG ** KK[z_1..z_k];
  idxs := gens RG / last @@ baseName / myToList;
  pMappings := gens RG /
                (p -> det submatrix(S, myToList last baseName p)) /
                (f -> sub(f, SSbar));
  zMappings := take(gens SSbar, -k);

  Tbar := if o.ModPlucker then T/sub(G,T) else T;

  psi := map(SSbar, Tbar, pMappings | zMappings);
  I := ker psi;

  zIrrelevant := ideal take(gens Tbar, -k);
  pIrrelevant := ideal take(gens Tbar, #gens Tbar - k);

  saturate(saturate(I, pIrrelevant), zIrrelevant)
)

waveIncidenceRZero = A -> (
  R := ring A;
  KK := coefficientRing R;
  k := numColumns A;

  z := symbol z;
  S := R[z_1..z_k];

  M := vars S * transpose sub(A, S);
  MM := last coefficients(M, Variables => gens R / (x -> sub(x, S)));

  T := KK[z_1..z_k];
  saturate(ideal sub(MM, T), ideal gens T)
)

myToList = L -> (if instance(L,ZZ) then {L} else toList L)