===============================
Combinatorics of m=1 Grasstopes
===============================

| This page contains auxiliary files to the paper:
| Yelena Mandelshtam, Dmitrii Pavlov, and Elizabeth Pratt: Combinatorics of :math:`m=1` Grasstopes
| ARXIV: https://arxiv.org/abs/2307.09603 CODE: https://mathrepo.mis.mpg.de/Grasstopes

| ABSTRACT: A Grasstope is the image of the totally nonnegative Grassmannian :math:`\mathrm{Gr}_{\geq 0}(k,n)` under a linear map :math:`\mathrm{Gr}(k,n)\dashrightarrow \mathrm{Gr}(k,k+m)`. This is a generalization of the amplituhedron, a geometric object of great importance to calculating scattering amplitudes in physics. The amplituhedron is a Grasstope arising from a totally positive linear map. While amplituhedra are relatively well-studied, much less is known about general Grasstopes. We study Grasstopes in the :math:`m=1` case and show that they can be characterized as unions of cells of a hyperplane arrangement satisfying a certain sign variation condition, extending the work of Karp and Williams. Inspired by this characterization, we also suggest a notion of a Grasstope arising from an arbitrary oriented matroid.

Table 2 in Section 6 contains extremal counts for the number of regions in a Grasstope arising from a fixed arrangement of :math:`k` hyperplanes in an :math:`n`-dimensional real projective space. The Python code used to obtain these counts can be downloaded here: :download:`matroidtograsstope.py <matroidtograsstope.py>`. 

Each hyperplane arrangement gives rise to a realizable oriented matroid, with regions of the arrangement being labeled by maximal covectors of this matroid. The catalog of oriented matroids is available at https://finschi.com/math/om/?p=catom. We consider the entries in this catalog for small :math:`k` and :math:`n` and for each entry iterate over all reorientations and permutations of the ground set. We then count the number of maximal covectors with at least :math:`k` sign changes to obtain the number of regions in the corresponding Grasstope. 

Note that the database https://finschi.com/math/om/?p=catom indexes matroids by vectors of signs of their bases (i.e. by their chirotopes). In order to obtain maximal covectors from the chirotope, we perform several operations, implemented as Python functions. First, we convert the chirotope of a matroid to the set of its circuits. Then, we obtain the set of all vectors of the matroid. Finally, the set of all vectors is converted into the set of maximal covectors. More explanations can be found inside the Python code file. 

The data obtained by running the code for small values of :math:`k` and :math:`n` is available here: :download:`RegionCounts.zip <RegionCounts.zip>`. Each file contains data for all matroid isomorphism classed for a fixed value of :math:`k` and :math:`n`. Matroids are indexed by their chirotope.

Project page created: 12/07/2023.

Project contributors: Yelena Mandelshtam, Dmitrii Pavlov, and Elizabeth Pratt.

Corresponding author of this page: Dmitrii Pavlov, `pavlov@mis.mpg.de <mailto:pavlov@mis.mpg.de>`_.
 
Software used: Python (Version 3.10.5).

System setup used: MacBook Pro with macOS Monterey 12.6, Processor 2,8 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html).

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/).

Last updated 14/07/23.

