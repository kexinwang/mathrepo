using HomotopyContinuation
using LinearAlgebra
using Combinatorics

function projection_grassmannian_degree(n, d)
    #in the trivial case where the projection Grassmannian is a point, we give the answer directly
    if d == 0 || d == n
        return 1
    end
    
    #create a generic symmetric matrix
    @var P[1:n, 1:n]
    P = Symmetric(P)
    p = [P[i,j] for i in 1:n for j in i:n]

    #equations for the projection Grassmannian
    Ind_generators = [[(P^2 - P)[i,j] for i in 1:n for j in i:n]; sum(diag(P)) - d];

    #variables describing an affine space of correct codimension
    @var a[1:d*(n-d), 1:Int(n*(n+1)/2)]
    @var b[1:d*(n-d)]
    L = sum(a * p + b, dims = 2)

    F = System([Ind_generators; vec(L)]; variables = p, parameters = [[(a...)...]; b])
    solns = monodromy_solve(F, threading=true)
    return nresults(solns)
end

n = parse(Int64, ARGS[1])
d = parse(Int64, ARGS[2])
println("The degree of the projection Grassmannian for n = ", n, " and d = ", d, " is:")
print(projection_grassmannian_degree(n, d))
