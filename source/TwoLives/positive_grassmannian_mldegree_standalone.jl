using HomotopyContinuation
using LinearAlgebra
using Combinatorics

function positive_grassmannian_mldegree(n, d)
    @var a[1:d, 1:n - d]
    @var u[1:binomial(n, d)]

    A = [UniformScaling(1) a];
    a = vec(a);

    minors = [det(A[:,s]) for s in combinations(1:n, d)]

    denominator = sum(minors)

    likelihood = sum(u[i] * log(minors[i]) for i in 1:binomial(n,d)) - sum(u[i] for i in 1:binomial(n,d))*log(denominator)

    F = System(differentiate(likelihood, a); parameters = u)

    start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12)

    solns = monodromy_solve(F, start_pair[1], start_pair[2], check_startsolutions = false, show_progress=false, threading=true)
    return nresults(solns)
end

n = parse(Int64, ARGS[1])
d = parse(Int64, ARGS[2])

println("The ML degree of the positive Grassmannian for n = ", n, " and d = ", d, " is:")
println(positive_grassmannian_mldegree(n, d))