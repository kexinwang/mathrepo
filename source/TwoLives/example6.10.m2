n = 7
R = QQ[q_1..q_(binomial(n+1,2))];

-- Generate a generic symmetric n x n matrix with 0 diagonal
Q = genericSymmetricMatrix(R,q_1,n)
zeroDiag = for i from 0 to n-1 list (Q_(i,i) => 0)
Q = sub(Q, zeroDiag)

-- Create the ideal for the squared Grassmannian with the 2 x 2 minors of Q included
sGRIdeal = minors(4, Q);
M = matrix{for i from 0 to n - 1 list sum(entries(Q_i))} || matrix{for i from 0 to n - 1 list random(1, 20)}
I = sGRIdeal + minors(2, M);

-- Saturate
-- Two colons is enough to check saturation for n = 4, 5, 6, 7
J = ideal entries (transpose M)_0

Itemp = I:J;
I = Itemp;

Itemp = I:J;
I == Itemp

-- The dimension in the paper is obtained by subtracting n+1 to account
-- for the unused diagonal variables and projective dimension
dim I - (n+1)

-- This gives the number of cubics
numgens source (mingens I) - (n - 1)

degree I


