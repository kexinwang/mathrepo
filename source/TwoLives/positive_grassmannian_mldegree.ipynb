{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "440d982e",
   "metadata": {},
   "source": [
    "# Computing the ML degree of the positive Grassmannian\n",
    "\n",
    "In this file, we compute the ML degree of the positive Grassmannian. We use a birational parametrization of the Grassmannian where a point in the Grassmannian $\\textrm{Gr}(d, n)$ is represented by a $d \\times n$ matrix $A$ where the first $d \\times d$ block is the identity matrix and the remaining entries are filled with variables $a_{ij}$. The matrix $A$ parametrizes the point $(\\det(A_I))_{I \\in \\binom{[n]}{d}}$ in the Grassmannian.\n",
    "For example, if $n = 5, d = 2$, we have the matrix \n",
    "$$ A = \\begin{pmatrix} 1 & 0 & a_{11} & a_{12} & a_{13} \\\\\n",
    "                   0 & 1 & a_{21} & a_{22} & a_{23} \\end{pmatrix}.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "246bd433",
   "metadata": {},
   "outputs": [],
   "source": [
    "using HomotopyContinuation\n",
    "using LinearAlgebra\n",
    "using Combinatorics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33731363",
   "metadata": {},
   "source": [
    "When $n$ and $d$ are specified below, the remainder of the code computes the ML degree of the positive Grassmannian $\\textrm{Gr}_{\\geq 0}(d,n)$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "74c703f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 6;\n",
    "d = 2;"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92f78198",
   "metadata": {},
   "source": [
    "## Setting up the system\n",
    "\n",
    "Here we define our variable matrix $A$ as described above and our data vector $u$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "4934b789",
   "metadata": {},
   "outputs": [],
   "source": [
    "@var a[1:d, 1:n - d]\n",
    "@var u[1:binomial(n, d)]\n",
    "\n",
    "A = [UniformScaling(1) a];\n",
    "a = vec(a);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "ff04d492",
   "metadata": {},
   "outputs": [],
   "source": [
    "minors = [det(A[:,s]) for s in combinations(1:n, d)];\n",
    "denominator = sum(minors);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f286cb8",
   "metadata": {},
   "source": [
    "Here we define the log-likelihood equation $$L_u = \\left (\\sum_{I \\in {[n] \\choose d}} u_I \\log(\\det(A_I)) \\right ) -  \\left( \\sum_{I \\in {[n] \\choose d}} u_I \\right) \\log\\left (\\sum_{I \\in {[n] \\choose d}} \\det(A_I) \\right ),$$\n",
    "where $A_I$ is the submatrix of $A$ given by selecting only the columns indexed by $I$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "9972fe22",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ u₂*log(a₂₋₁) + u₃*log(a₂₋₂) + u₄*log(a₂₋₃) + u₅*log(a₂₋₄) + u₆*log(-a₁₋₁) + u₇*log(-a₁₋₂) + u₈*log(-a₁₋₃) + u₉*log(-a₁₋₄) + log(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁)*u₁₁ + log(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*u₁₀ + log(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂)*u₁₃ + log(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂)*u₁₄ + log(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁)*u₁₂ + log(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃)*u₁₅ - log(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)*(u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉) $$"
      ],
      "text/plain": [
       "u₂*log(a₂₋₁) + u₃*log(a₂₋₂) + u₄*log(a₂₋₃) + u₅*log(a₂₋₄) + u₆*log(-a₁₋₁) + u₇*log(-a₁₋₂) + u₈*log(-a₁₋₃) + u₉*log(-a₁₋₄) + log(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁)*u₁₁ + log(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*u₁₀ + log(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂)*u₁₃ + log(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂)*u₁₄ + log(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁)*u₁₂ + log(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃)*u₁₅ - log(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)*(u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "likelihood = sum(u[i] * log(minors[i]) for i in 1:binomial(n,d)) - sum(u[i] for i in 1:binomial(n,d))*log(denominator)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d743b89c",
   "metadata": {},
   "source": [
    "We want to find critical points of this function, i.e. points where all the derivatives vanish. Therefore we take the derivatives and put them in a $\\verb|HomotopyContinuation.jl|$ system with variables $a$ and parameters $u$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "2586a9f0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "System of length 8\n",
       " 8 variables: a₁₋₁, a₂₋₁, a₁₋₂, a₂₋₂, a₁₋₃, a₂₋₃, a₁₋₄, a₂₋₄\n",
       " 15 parameters: u₁, u₂, u₃, u₄, u₅, u₆, u₇, u₈, u₉, u₁₀, u₁₁, u₁₂, u₁₃, u₁₄, u₁₅\n",
       "\n",
       " u₆/a₁₋₁ + a₂₋₃*u₁₁/(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁) + a₂₋₂*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) + a₂₋₄*u₁₂/(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(-1 + a₂₋₂ + a₂₋₃ + a₂₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₂/a₂₋₁ - a₁₋₃*u₁₁/(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁) - a₁₋₂*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) - a₁₋₄*u₁₂/(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(1 - a₁₋₂ - a₁₋₃ - a₁₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₇/a₁₋₂ - a₂₋₁*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) + a₂₋₃*u₁₃/(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂) + a₂₋₄*u₁₄/(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(-1 - a₂₋₁ + a₂₋₃ + a₂₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₃/a₂₋₂ + a₁₋₁*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) - a₁₋₃*u₁₃/(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂) - a₁₋₄*u₁₄/(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(1 + a₁₋₁ - a₁₋₃ - a₁₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₈/a₁₋₃ - a₂₋₁*u₁₁/(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁) - a₂₋₂*u₁₃/(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂) + a₂₋₄*u₁₅/(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(-1 - a₂₋₁ - a₂₋₂ + a₂₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₄/a₂₋₃ + a₁₋₁*u₁₁/(a₁₋₁*a₂₋₃ - a₁₋₃*a₂₋₁) + a₁₋₂*u₁₃/(a₁₋₂*a₂₋₃ - a₁₋₃*a₂₋₂) - a₁₋₄*u₁₅/(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(1 + a₁₋₁ + a₁₋₂ - a₁₋₄)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₉/a₁₋₄ - a₂₋₂*u₁₄/(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂) - a₂₋₁*u₁₂/(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁) - a₂₋₃*u₁₅/(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(-1 - a₂₋₁ - a₂₋₂ - a₂₋₃)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)\n",
       " u₅/a₂₋₄ + a₁₋₂*u₁₄/(a₁₋₂*a₂₋₄ - a₁₋₄*a₂₋₂) + a₁₋₁*u₁₂/(a₁₋₁*a₂₋₄ - a₁₋₄*a₂₋₁) + a₁₋₃*u₁₅/(a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₃) - (u₁ + u₁₀ + u₁₁ + u₁₂ + u₁₃ + u₁₄ + u₁₅ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)*(1 + a₁₋₁ + a₁₋₂ + a₁₋₃)/(1 - a₁₋₁ - a₁₋₂ - a₁₋₃ - a₁₋₄ + a₂₋₁ + a₂₋₂ + a₂₋₃ + a₂₋₄ + a₁₋₁*a₂₋₂ + a₁₋₁*a₂₋₃ + a₁₋₁*a₂₋₄ - a₁₋₂*a₂₋₁ + a₁₋₂*a₂₋₃ + a₁₋₂*a₂₋₄ - a₁₋₃*a₂₋₁ - a₁₋₃*a₂₋₂ + a₁₋₃*a₂₋₄ - a₁₋₄*a₂₋₁ - a₁₋₄*a₂₋₂ - a₁₋₄*a₂₋₃)"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F = System(differentiate(likelihood, a); parameters = u)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f844d304",
   "metadata": {},
   "source": [
    "## Solving the system \n",
    "\n",
    "We then compute a start system and then solve the system using monodromy. \n",
    "Depending on the system, it may take more tries than the default to compute a start pair, so we find a start pair manually. \n",
    "We can use threading here to speed up the monodromy which can be very time consuming. \n",
    "We then certify the solutions to check that we got distinct solutions. \n",
    "We now have a lower bound on the ML degree of this partiticular positive Grassmannian. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "39292cce",
   "metadata": {},
   "outputs": [],
   "source": [
    "start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "df1db4cf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "MonodromyResult\n",
       "===============\n",
       "• return_code → :heuristic_stop\n",
       "• 156 solutions\n",
       "• 1092 tracked loops\n",
       "• random_seed → 0xa039df43"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "solns = monodromy_solve(F, start_pair[1], start_pair[2], threading=true)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "10169dcf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "CertificationResult\n",
       "===================\n",
       "• 156 solution candidates given\n",
       "• 156 certified solution intervals (0 real, 156 complex)\n",
       "• 156 distinct certified solution intervals (0 real, 156 complex)"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "certify(F, solns)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.8.5",
   "language": "julia",
   "name": "julia-1.8"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
