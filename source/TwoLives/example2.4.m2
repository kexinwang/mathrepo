n = 6
d = 3

R = QQ[subsets(1..n,d) / (i -> x_i)]
use R
entry = (i,j) -> (
    K := (sort(subsets(1..n,d-1)))_j;
    if any(K,l -> l == i + 1)
    then 0
    else (iK := insert(0, i + 1, K);
        sign := (-1)^(number(K, l -> l < i + 1));
        iKsorted := sort(iK);
        sign * x_iKsorted)
)

X = map(R^n,binomial(n,d-1),entry)