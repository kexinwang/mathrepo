{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1de0f9df",
   "metadata": {},
   "source": [
    "# Computing the ML degree of the squared Grassmannian\n",
    "In this file, we compute the ML degree of the squared Grassmannian. We do this computation using the birational parametrization of the (regular) Grassmannian, together with the $2^{n-1}:1$ parametrization of the squared Grassmannian via the Grassmannian.\n",
    "A point in the Grassmannian $\\textrm{Gr}(d, n)$ is represented by a $d \\times n$ matrix $A$ where the first $d \\times d$ block is the identity matrix and the remaining entries are filled with variables $a_{ij}$. The matrix $A$ parametrizes the point $(\\det(A_I)^2)_{I \\in \\binom{[n]}{d}}$ in the squared Grassmannian.\n",
    "For example, if $n = 5, d = 2$, we have the matrix \n",
    "$$ A = \\begin{pmatrix} 1 & 0 & a_{11} & a_{12} & a_{13} \\\\\n",
    "                   0 & 1 & a_{21} & a_{22} & a_{23} \\end{pmatrix}.$$\n",
    "                "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "246bd433",
   "metadata": {},
   "outputs": [],
   "source": [
    "using HomotopyContinuation\n",
    "using LinearAlgebra\n",
    "using Combinatorics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b6c0d55",
   "metadata": {},
   "source": [
    "When $n$ and $d$ are specified below, the remainder of the code computes the ML degree of the squared Grassmannian $\\textrm{sGr}(d,n)$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "74c703f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 5;\n",
    "d = 3;"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f54f9102",
   "metadata": {},
   "source": [
    "## Setting up the system\n",
    "\n",
    "Here we define our variable matrix $A$ as described above and our data vector $u$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4934b789",
   "metadata": {},
   "outputs": [],
   "source": [
    "@var a[1:d, 1:n - d]\n",
    "@var u[1:binomial(n, d)]\n",
    "\n",
    "A = [UniformScaling(1) a];\n",
    "a = vec(a);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7f3a201",
   "metadata": {},
   "source": [
    "The next cell defines the log-likelihood equation:\n",
    "$$ L_u = \\left (\\sum_{I \\in \\binom{[n]}{d}} (u_I \\log(\\det(A_I)^2)) \\right ) - \\left (\\sum_{I \\in \\binom{[n]}{d}} u_I \\right) \\log \\left ( \\sum_{I \\in \\binom{[n]}{d}} \\det(A_I)^2 \\right ),$$\n",
    "where $A_I$ is the submatrix of $A$ given by selecting only the columns indexed by $I$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "ff04d492",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$$ u₂*log(a₃₋₁^2) + u₃*log(a₃₋₂^2) + u₄*log(a₂₋₁^2) + u₅*log(a₂₋₂^2) + u₇*log(a₁₋₁^2) + u₈*log(a₁₋₂^2) - log(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉) + log((a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2)*u₁₀ + log((-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2)*u₉ + log((-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)*u₆ $$"
      ],
      "text/plain": [
       "u₂*log(a₃₋₁^2) + u₃*log(a₃₋₂^2) + u₄*log(a₂₋₁^2) + u₅*log(a₂₋₂^2) + u₇*log(a₁₋₁^2) + u₈*log(a₁₋₂^2) - log(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉) + log((a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2)*u₁₀ + log((-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2)*u₉ + log((-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)*u₆"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "minors = [det(A[:,s]) for s in combinations(1:n, d)]\n",
    "denominator = sum([minor^2 for minor in minors])\n",
    "likelihood = sum(u[i] * log(minors[i]^2) for i in 1:binomial(n,d)) - sum(u[i] for i in 1:binomial(n,d))*log(denominator)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3199232",
   "metadata": {},
   "source": [
    "We want to find critical points of this function, i.e. points where all the derivatives vanish. Therefore we take the derivatives and put them in a $\\verb|HomotopyContinuation.jl|$ system with variables $a$ and parameters $u$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "2586a9f0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "System of length 6\n",
       " 6 variables: a₁₋₁, a₂₋₁, a₃₋₁, a₁₋₂, a₂₋₂, a₃₋₂\n",
       " 10 parameters: u₁, u₂, u₃, u₄, u₅, u₆, u₇, u₈, u₉, u₁₀\n",
       "\n",
       " 2*u₇/a₁₋₁ + 2*a₂₋₂*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) + 2*u₉*a₃₋₂/(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁) - (2*a₁₋₁ + 2*(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*a₂₋₂ + 2*(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)*a₃₋₂)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)\n",
       " 2*u₄/a₂₋₁ - 2*a₁₋₂*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) + 2*u₆*a₃₋₂/(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁) - (2*a₂₋₁ - 2*(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*a₁₋₂ + 2*(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)*a₃₋₂)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)\n",
       " 2*u₂/a₃₋₁ - 2*u₉*a₁₋₂/(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁) - 2*u₆*a₂₋₂/(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁) - (2*a₃₋₁ - 2*(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)*a₁₋₂ - 2*(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)*a₂₋₂)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)\n",
       " 2*u₈/a₁₋₂ - 2*a₂₋₁*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) - 2*u₉*a₃₋₁/(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁) - (2*a₁₋₂ - 2*(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*a₂₋₁ - 2*(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)*a₃₋₁)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)\n",
       " 2*u₅/a₂₋₂ + 2*a₁₋₁*u₁₀/(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁) - 2*u₆*a₃₋₁/(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁) - (2*a₂₋₂ + 2*(a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)*a₁₋₁ - 2*(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)*a₃₋₁)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)\n",
       " 2*u₃/a₃₋₂ + 2*u₉*a₁₋₁/(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁) + 2*u₆*a₂₋₁/(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁) - (2*a₃₋₂ + 2*(-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)*a₁₋₁ + 2*(-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)*a₂₋₁)*(u₁ + u₁₀ + u₂ + u₃ + u₄ + u₅ + u₆ + u₇ + u₈ + u₉)/(1 + a₁₋₁^2 + a₁₋₂^2 + a₂₋₁^2 + a₂₋₂^2 + a₃₋₁^2 + a₃₋₂^2 + (a₁₋₁*a₂₋₂ - a₁₋₂*a₂₋₁)^2 + (-a₃₋₁*a₁₋₂ + a₃₋₂*a₁₋₁)^2 + (-a₃₋₁*a₂₋₂ + a₃₋₂*a₂₋₁)^2)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F = System(differentiate(likelihood, a); parameters = u)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "428d22a9",
   "metadata": {},
   "source": [
    "## Solving the system\n",
    "\n",
    "We then compute a start system and then solve the system using monodromy. \n",
    "Depending on the system. it may take more tries than the default to compute a start pair, so manually compute a start pair.\n",
    "We can use threading here to speed up the monodromy which can be very time consuming. \n",
    "We then certify the solutions to check that we got distinct solutions. \n",
    "We now have a lower bound on the ML degree of this partiticular squared Grassmannian. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "39292cce",
   "metadata": {},
   "outputs": [],
   "source": [
    "start_pair = find_start_pair(F; max_tries = 10000, atol = 0.0, rtol = 1e-12);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63a361b3",
   "metadata": {},
   "source": [
    "Recall that because of the parameterization, the number of solutions is $2^{n-1}$ times the ML degree of the squared Grassmannian.\n",
    "Thus the compute the ML degree of the squared Grassmannian, we must divide the output by $2^{n-1}$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "df1db4cf",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32mSolutions found: 192    Time: 0:00:07\u001b[39m\n",
      "\u001b[34m  tracked loops (queued):            1344 (0)\u001b[39m\n",
      "\u001b[34m  solutions in current (last) loop:  0 (0)\u001b[39m\n",
      "\u001b[34m  generated loops (no change):       7 (5)\u001b[39m\n"
     ]
    }
   ],
   "source": [
    "solns = monodromy_solve(F, start_pair[1], start_pair[2], threading=true);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "d403fac6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12.0"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nresults(solns) / 2^(n-1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.8.5",
   "language": "julia",
   "name": "julia-1.8"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
