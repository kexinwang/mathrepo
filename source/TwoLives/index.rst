=============================
Two Lives of the Grassmannian
=============================

| This page contains auxiliary files to the paper:
| Karel Devriendt, Hannah Friedman, Bernhard Reinke and Bernd Sturmfels: The Two Lives of the Grassmannian
| ARXIV:  https://arxiv.org/abs/2401.03684

**Abstract.** The real Grassmannian is both a projective variety (via Plücker coordinates) and an affine variety (via orthogonal projections). We connect these two representations, and we develop the commutative algebra of the latter variety. We introduce the squared Grassmannian, and we study applications to determinantal point processes in statistics. 

Symbolic computations
---------------------


Example 2.3
^^^^^^^^^^^
We compare two formulas for the projection matrix: :math:`P = A^T ( A A^T)^{-1} A` (given in Example 1.1) and
:math:`P = \frac{d \cdot XX^T}{{\rm trace}(X^TX)}` (given in Corollary 2.5). We do this for :math:`n=5` and :math:`d=2` and

.. math::
   A = \begin{pmatrix} 1 & 0 & a_{11} & a_{12} & a_{13} \\
                       0 & 1 & a_{21} & a_{22} & a_{23} \end{pmatrix}

in the following Macaulay2 code snippet: 
:download:`example2.3.m2 <example2.3.m2>`

.. literalinclude:: example2.3.m2
   :language: macaulay2
   :linenos:

Example 2.4
^^^^^^^^^^^
The following Macaulay2 code produces the :math:`n \times \binom{n}{d-1}` cocircuit matrix :math:`(x_{iK})` of Example 2.4.

:download:`example2.4.m2 <example2.4.m2>`

.. literalinclude:: example2.4.m2
   :language: macaulay2
   :linenos:

Conjecture 3.6
^^^^^^^^^^^^^^
Our computational evidence for Conjecture 3.6 can be downloaded here: :download:`conjecture3.6.m2 <conjecture3.6.m2>`.
The Macaulay2 file lists 285 linearly independent quartics that are verified to lie in the ideal of :math:`\mathrm{sGr}(3,6)`.
Additional computations (such as matching dimension and degree) suggest that they generate the ideal.
 


Degree of the projection Grassmannian
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The following Macaulay2 code computes the degree of the projection Grassmannian :math:`\mathrm{pGr}(d,n)` as given in Proposition 5.5.

.. code-block:: macaulay2
  :linenos:
  
  n = 7
  d = 3
  R = QQ[p_1..p_(binomial(n+1,2))];
  P = genericSymmetricMatrix(R,p_1,n)
  I = minors(1,P^2-P)+ideal(trace(P)-d);
  toString mingens I
  dim I, degree I

Example 6.10
^^^^^^^^^^^^
The following Macaulay2 code produces the ideal of the generic fiber :math:`\mathrm{sGr}(2,n)_z` for :math:`4 \leq n \leq 7` of the projective moment map for the squared Grassmannian.

:download:`example6.10.m2 <example6.10.m2>`

.. literalinclude:: example6.10.m2
   :language: macaulay2
   :linenos:

Numerical computations
----------------------

The following Jupyter notebooks show how to do our numerical degree computations using  the `julia <https://julialang.org/>`_ package
`HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_.

.. toctree::
	:maxdepth: 1
	:glob:

	projection_grassmannian_degree
	positive_grassmannian_mldegree
	squared_grassmannian_mldegree

The Jupyter notebooks can be downloaded here:

:download:`projection_grassmannian_degree.ipynb <projection_grassmannian_degree.ipynb>`

:download:`positive_grassmannian_mldegree.ipynb <positive_grassmannian_mldegree.ipynb>`

:download:`squared_grassmannian_mldegree.ipynb <squared_grassmannian_mldegree.ipynb>`

We also provide the code of these Jupyter notebooks as standalone Julia files:

:download:`projection_grassmannian_degree_standalone.jl <projection_grassmannian_degree_standalone.jl>`

:download:`positive_grassmannian_mldegree_standalone.jl <positive_grassmannian_mldegree_standalone.jl>`

:download:`squared_grassmannian_mldegree_standalone.jl <squared_grassmannian_mldegree_standalone.jl>`

They can be used in the following way:

.. code-block:: bash

    julia -t [NUM_THREADS] projection_grassmannian_degree_standalone.jl [N] [D]

where NUM_THREADS is the number of threads to be used by ``julia``. For example,

.. code-block:: bash

    julia -t 256 projection_grassmannian_degree_standalone.jl 9 2

computes the degree of :math:`\mathrm{pGr}(2,9)` using 256 threads.

We used the standalone files together with the `batch system <https://docs.mis.mpg.de/compute/batch.html>`_ of the MPI MiS to compute the degrees
in the tables of Theorem 4.1 and Proposition 5.5.


Project page created: 22/05/2024

Project contributors: Karel Devriendt, Hannah Friedman, Bernhard Reinke and Bernd Sturmfels

Corresponding author of this page: Bernhard Reinke, bernhard.reinke@mis.mpg.de

Software used: Julia (Version 1.10.2), Macaulay2 (v1.21)

System setup used: 2x 32-Core AMD Epyc 7601 at 2.2 GHz (max. 3.2 GHz), 1024 GB RAM

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated 22/05/2024.
