using HomotopyContinuation

@var x1 x2 x3 x4 x5 u[1:10] 

## ML degree 11 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [1,1,1,1,1,1,1,1,1,1]
## ML degree 10 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [7/8,1,1,1,1,1,1,32/7,1,1]
## ML degree 9 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [1,1,1,1,1,1,1,4,4,1]
## ML degree 8 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [1,1,1,1,1,1,1,4,4,4]
## ML degree 7 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [1,1/4,1,1,1,1,1,1/4,1/4,4]
## ML degree 6 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [-3,7/2,21/4*(1-sqrt(3)im),3/4*(1+sqrt(3)im),-2,-3-3*sqrt(3)im,-3/7+3/7*sqrt(3)im,-7,-1,3]
## ML degree 6 scaling
w12, w13, w14, w15, w23, w24, w25, w34, w35, w45 = [-4,1,4,-19/4-2*sqrt(3),-1,-4,19/4-2*sqrt(3),4,-4,-3]
##
F = System(
    [
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])*(w12*x1*x2+w13*x1*x3+w14*x1*x4+w15*x1*x5+w23*x1*x2*x3+w24*x1*x2*x4+w25*x1*x2*x5+w34*x1*x3*x4+w35*x1*x3*x5+w45*x1*x4*x5)-(u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])*(w12*x1*x2+w23*x1*x2*x3+w24*x1*x2*x4+w25*x1*x2*x5)-(u[1]+u[5]+u[6]+u[7])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])*(w13*x1*x3+w23*x1*x2*x3+w34*x1*x3*x4+w35*x1*x3*x5)-(u[2]+u[5]+u[8]+u[9])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])*(w14*x1*x4+w24*x1*x2*x4+w34*x1*x3*x4+w45*x1*x4*x5)-(u[3]+u[6]+u[8]+u[9])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6]+u[7]+u[8]+u[9]+u[10])*(w15*x1*x5+w25*x1*x2*x5+w35*x1*x3*x5+w45*x1*x4*x5)-(u[4]+u[7]+u[9]+u[10])
    ];
    parameters = u
)
## Solve generically:
u0 = rand(ComplexF64,10)
result_u0 = solve(F, target_parameters = u0)
## Solve for random parameter values
data = [rand((0:100000),10)]
data_points = solve(
      F,
      solutions(result_u0),
      start_parameters =  u0,
      target_parameters = data,
)
##


