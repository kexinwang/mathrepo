------------------------------------------------------------------
------------------------------------------------------------------
-- Non-isomorphic simple complete graphs on 4 nodes             --
------------------------------------------------------------------
------------------------------------------------------------------

restart 
loadPackage "LyapunovModel"
L={{{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}}, {{1, 2}, {1,
   3}, {1, 4}, {2, 3}, {3, 4}, {4, 2}}, {{1, 2}, {1, 3}, {2, 3}, {2,
   4}, {3, 4}, {4, 1}}, {{1, 2}, {1, 3}, {2, 3}, {2, 4}, {4, 1}, {4,
   3}}}

----------------------
-- Graph 0: DAG     --
----------------------
G=digraph L_0
(R,M,S)=lyapunovData G
AS=ASigmaGraph (G)
F=extractFactors(factor det AS) --factors det AS
netList F
areAllFactorsPpalMinors G

-------------------------------------------------------------
-- Graph 1: 3-cycle + extra node with only outgoing edges  --
-------------------------------------------------------------
restart 
G=digraph L_1
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=extractFactors(factor det AS) --factors det AS
netList F

--SOS decomposition of relevant factor of the determinant
F_4
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_4
s=sosPoly sol
netList s#generators
--l_(3,3)l_(4,4)>0

------------------------------------
-- Graph 2: completion of 4-cycle --
------------------------------------
restart 
G=digraph L_2
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=extractFactors(factor det AS) --factors det AS
netList F

--SOS decomposition of relevant factor of the determinant
F_4
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_4
s=sosPoly sol
netList s#generators
--l_(2,2)l_(3,3)l_(4,4)>0

-------------------------------------------------------------
-- Graph 3: 3-cycle + extra node with only ingoing edges   --
-------------------------------------------------------------
restart 
G=digraph L_3
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=extractFactors(factor det AS) --factors det AS
netList F
--2 factors besides the determinant
F_4 -- non-vanishing SOS
--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#generators
--l_(2,2)l_(4,4)>0




------------------------------------------------------------------
------------------------------------------------------------------
-- Non-isomorphic simple complete graphs on 5 nodes             --
------------------------------------------------------------------
------------------------------------------------------------------

restart 
loadPackage "LyapunovModel"
L={{{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {3,
   5}, {4, 5}}, {{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {2,
   5}, {3, 4}, {4, 5}, {5, 3}}, {{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2,
   3}, {2, 4}, {3, 4}, {3, 5}, {4, 5}, {5, 2}}, {{1, 2}, {1, 3}, {1,
   4}, {1, 5}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {5, 2}, {5, 4}}, {{1,
   2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {3, 5}, {4,
   5}, {5, 1}}, {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3,
   4}, {3, 5}, {5, 1}, {5, 4}}, {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2,
   4}, {2, 5}, {3, 4}, {4, 5}, {5, 1}, {5, 3}}, {{1, 2}, {1, 3}, {1,
   4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {5, 1}, {5, 3}, {5, 4}}, {{1,
   2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {4, 5}, {5,
   1}, {5, 2}}, 
{{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {4, 2}, {4, 5}, {5, 1}}, 
{{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {4, 2}, {5, 1}, {5, 4}}, 
{{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {4, 1}, {4, 5}, {5, 1}, {5, 2}}}
#L

----------------------
-- Graph 0: DAG     --
----------------------
G=digraph L_0
(R,M,S)=lyapunovData G
AS=ASigmaGraph (G)
F=extractFactors(factor det AS) --seems to take forever....
netList F
areAllFactorsPpalMinors G

-------------------------------------------------------------
-- Graph 1:  
-------------------------------------------------------------
restart 
G=digraph L_1
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=extractFactors(factor det AS); 
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#generators
--l_(4,4)l_(5,5)>0

F_5==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 2: 
-------------------------------------------------------------
restart 
G=digraph L_2
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#generators
--l_(3,3)l_(4,4)l_(5,5)>0
netList s#coefficients
F_5==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)


toString F_5

l_(2,5)^2*l_(3,3)^2*l_(4,4)^2-l_(2,3)*l_(2,5)*l_(3,3)*l_(3,5)*l_(4,4)^2+l_(2,3)^2*l_(3,5)^2*l_(4,4)^2+l_(3,3)^2*l_(3,5)^2*l_(4,4)^2-l_(2,4)*l_(2,5)*l_(3,3)^2*l_(4,4)*l_(4,5)+l_(2,3)*
       l_(2,5)*l_(3,3)*l_(3,4)*l_(4,4)*l_(4,5)+l_(2,3)*l_(2,4)*l_(3,3)*l_(3,5)*l_(4,4)*l_(4,5)-l_(2,3)^2*l_(3,4)*l_(3,5)*l_(4,4)*l_(4,5)+l_(2,4)^2*l_(3,3)^2*l_(4,5)^2-2*l_(2,3)*l_(2,4)*l_(3
       ,3)*l_(3,4)*l_(4,5)^2+l_(2,3)^2*l_(3,4)^2*l_(4,5)^2+l_(2,3)^2*l_(4,4)^2*l_(4,5)^2+l_(3,3)^2*l_(4,4)^2*l_(4,5)^2+l_(2,4)^2*l_(3,3)^2*l_(5,5)^2-2*l_(2,3)*l_(2,4)*l_(3,3)*l_(3,4)*l_(5,5
       )^2+l_(2,3)^2*l_(3,4)^2*l_(5,5)^2+l_(2,3)^2*l_(4,4)^2*l_(5,5)^2+l_(3,3)^2*l_(4,4)^2*l_(5,5)^2

toString gens ring(F_5)

{l_(1,1), l_(1,2), l_(1,3), l_(1,4), l_(1,5), l_(2,2), l_(2,3), l_(2,4), l_(2,5), l_(3,3), l_(3,4), l_(3,5), l_(4,4), l_(4,5), l_(5,5)}

sub(F_5,l_(1,1)=>l[1,1])

ring AS
R2=QQ[l11,l12,l13,l14,l15,l22,l23,l24,l25,l33,l34,l35,l44,l45,l55]
f=map(R2,ring AS,gens R2)
F5=f(F_5)

toString F5 

p=l25^2*l33^2*l44^2-l23*l25*l33*l35*l44^2+l23^2*l35^2*l44^2+l33^2*l35^2*l44^2-l24*l25*l33^2*l44*l45+l23*l25*l33*l34*l44*l45+l23*l24*l33*l35*l44*l45-l23^2*l34*l35*l44*l45+l24^2*l33^2*
       l45^2-2*l23*l24*l33*l34*l45^2+l23^2*l34^2*l45^2+l23^2*l44^2*l45^2+l33^2*l44^2*l45^2+l24^2*l33^2*l55^2-2*l23*l24*l33*l34*l55^2+l23^2*l34^2*l55^2+l23^2*l44^2*l55^2+l33^2*l44^2*l55^2


toString gens R2

F_5==sum apply(s#generators,s#coefficients,(i,j)->i*j)

-------------------------------------------------------------
-- Graph 3: 
-------------------------------------------------------------
restart 
G=digraph L_3
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 24.2187 seconds
netList F

--non-vanishing SOS:
F_5
--SOS decomposition of relevant factor of the determinant
F_6
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_6
s=sosPoly sol
netList s#generators
--l_(3,3)l_(5,5)>0


-------------------------------------------------------------
-- Graph 4: 
-------------------------------------------------------------
restart 
G=digraph L_4
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 127.078 seconds
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#generators
--l_(2,2)l_(3,3)l_(4,4)l_(5,5)>0


-------------------------------------------------------------
-- Graph 5: 
-------------------------------------------------------------
restart 
G=digraph L_5
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 115.797 seconds
netList F

--non-vanishing SOS:
F_5
--SOS decomposition of relevant factor of the determinant
F_6
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_6
s=sosPoly sol
netList s#generators
--l_(2,2)l_(3,3)l_(5,5)>0


-------------------------------------------------------------
-- Graph 6: 
-------------------------------------------------------------
restart 
G=digraph L_6
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
 -- used 138.563 seconds
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#coefficients
netList s#generators
--If l_(2,2)l_(3,5)^2l_(4,4)=0, then l_(3,5)=0 and hence
-- -76/295 l_(2,2)l_(3,5)l_(4,4)+l_(2,2)l_(4,4)l_(5,5)^2=l_(2,2)l_(4,4)l_(5,5)^2>0


-------------------------------------------------------------
-- Graph 7: 
-------------------------------------------------------------
restart 
G=digraph L_7
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 93.1875 seconds
netList F

--non-vanishing SOS:
F_5
F_6
--SOS decomposition of relevant factor of the determinant
F_7
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_7
s=sosPoly sol
netList s#generators
--l_(2,2)l_(5,5)>0

-------------------------------------------------------------
-- Graph 8: 
-------------------------------------------------------------
restart 
G=digraph L_8
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 412.828 seconds
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
netList s#coefficients
netList s#generators
-- if l_(2,5)^2l_(3,3)l_(4,4)=0 then l_(2,5)=0 and hence
-- -(130804499585327632/504957363401894371)*l_(2,5)^2*l_(3,3)*l_(4,4)+
-- (26675944610657300/504957363401894371)*l_(2,3)*l_(2,5)*l_(3,5)*l_(4,4)+
-- l_(3,3)*l_(4,4)*l_(5,5)^2 =l_(3,3)*l_(4,4)*l_(5,5)^2 >0


-------------------------------------------------------------
-- Graph 9: 
-------------------------------------------------------------
restart 
G=digraph L_9
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 2085.44 seconds
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5
s=sosPoly sol
--Too many heap sections: Increase MAXHINCR or MAX_HEAP_SECTS
netList s#generators


-------------------------------------------------------------
-- Graph 10: 
-------------------------------------------------------------
restart 
G=digraph L_10
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
-- used 3093.45 seconds
netList F

--SOS decomposition of relevant factor of the determinant
F_5
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_5;
s=sosPoly sol
netList s#generators

-------------------------------------------------------------
-- Graph 11: 
-------------------------------------------------------------
restart 
G=digraph L_11
(R,M,SL,S)=lyapunovDataPD G
AS=ASigmaGraphPD(G)
F=time extractFactors(factor det AS); 
netList F

--non-vanishing SOS:
F_5
--SOS decomposition of relevant factor of the determinant
F_6
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"
sol=solveSOS F_6
s=sosPoly sol
netList s#generators
