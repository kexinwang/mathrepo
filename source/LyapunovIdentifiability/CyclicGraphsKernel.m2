------------------------------------------------------------------
------------------------------------------------------------------
-- Non-isomorphic simple cyclic complete graphs on 4 nodes      --
------------------------------------------------------------------
------------------------------------------------------------------

restart 
loadPackage "LyapunovModel"
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"

G=digraph{{1,2},{2,3},{3,4},{4,1}}
--compute matrix A(\Sigma) with Cholesky decomposition
A=ASigmaPD G
(R,M,SL,S)=lyapunovDataPD G
--compute kernel
kern=syz A

pattern G
kernelPattern G
-------------------------------------------------------------
-- Graph 1: 3-cycle + extra node with only outgoing edges  --
-------------------------------------------------------------

G1=digraph{{1, 2}, {1, 3}, {1, 4}, {2, 3}, {3, 4}, {4, 2}}
pattern G1
kernelPattern G1
kern^(kernelPattern G1)

f=det kern^(kernelPattern G1);
F=extractFactors factor f;
netList F
F_2

sol=solveSOS F_2
s=sosPoly sol
peek s
netList s#generators
--l_(3,3)l_(4,4)>0

--Check that it is a proper SOS decomposition
F_2==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

------------------------------------
-- Graph 2: completion of 4-cycle --
------------------------------------

G=digraph{{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4}, {4, 1}}
kernelPattern G

f=det kern^(kernelPattern G);
F=extractFactors factor f;
netList F
F_2

sol=solveSOS F_2
s=sosPoly sol
peek s
netList s#generators
--l_(2,2)l_(3,3)l_(4,4)>0


--Check that it is a proper SOS decomposition
F_2==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 3: 3-cycle + extra node with only ingoing edges   --
-------------------------------------------------------------

G=digraph{{1, 2}, {1, 3}, {2, 3}, {2, 4}, {4, 1}, {4, 3}}
kernelPattern G

f=det kern^(kernelPattern G);
F=extractFactors factor f;
netList F
F_2 -- strictly positive SOS since l_(4,4)>0
F_3

sol=solveSOS F_3
s=sosPoly sol
peek s
netList s#generators
--l_(2,2)l_(4,4)>0

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)


------------------------------------------------------------------
------------------------------------------------------------------
-- Non-isomorphic simple cyclic complete graphs on 5 nodes      --
------------------------------------------------------------------
------------------------------------------------------------------

restart 
loadPackage "LyapunovModel"
loadPackage "SemidefiniteProgramming"
loadPackage "SumsOfSquares"

G=digraph{{1,2},{2,3},{3,4},{4,5},{5,1}}
--compute matrix A(\Sigma) with Cholesky decomposition
A=ASigmaPD G
(R,M,SL,S)=lyapunovDataPD G
--compute kernel
kern=syz A

-------------------------------------------------------------
-- Graph 1:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {4, 5}, {5, 3}}
kernelPattern G

f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3

sol=time solveSOS F_3
s=sosPoly sol
peek s
netList s#generators
--l_(4,4)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 2:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {4, 5}, {5, 2}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3

sol=time solveSOS F_3
s=sosPoly sol
peek s
netList s#generators
--l_(3,3)l_(4,4)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 3:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {1, 5}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {5, 2}, {5, 4}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3
-- strictly positive SOS since l_(5,5)>0
F_4

sol=time solveSOS F_4
s=sosPoly sol
peek s
netList s#generators
--l_(3,3)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_4==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 4:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {3, 5}, {4, 5}, {5, 1}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3

sol=time solveSOS F_3
-- used 27.6094 seconds
s=sosPoly sol
peek s
netList s#generators
--l_(2,2)l_(3,3)l_(4,4)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 5:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {3, 5}, {5, 1}, {5, 4}}
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3
-- strictly positive SOS since l_(5,5)>0
F_4

sol=time solveSOS F_4
-- used 1.6875 seconds
s=sosPoly sol
peek s
netList s#generators
--l_(2,2)l_(3,3)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_4==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 6:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {4, 5}, {5, 1}, {5, 3}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3

sol=time solveSOS F_3
-- used 37.3906 seconds
s=sosPoly sol
peek s
netList s#generators
--If l_(3,5) not 0, then s#generators_38=l_(2,2)l_(3,5)^2l_(5,5) not 0.
--If l_(3,5)=0, then s#generators_20 >0
s#generators_38
s#generators_20

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)


-------------------------------------------------------------
-- Graph 7:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {2, 5}, {3, 4}, {5, 1}, {5, 3}, {5, 4}}  
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3
F_4
-- strictly positive SOS's since l_(5,5)>0
F_5

sol=time solveSOS F_5
-- used 6.3125 seconds
s=sosPoly sol
peek s
netList s#generators
-- l_(2,2)l_(5,5)>0

--Check that it is a proper SOS decomposition
F_5==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)


-------------------------------------------------------------
-- Graph 8:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {4, 5}, {5, 1}, {5, 2}}  
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_3

sol=time solveSOS F_3
-- used 147.516 seconds
s=sosPoly sol
peek s
netList s#generators
-- l_(2,2)l_(5,5)>0

s#generators_53
s#generators_37

-- if l_(2,5)^2l_(3,3)l_(4,4)=0 then l_(2,5)=0 and hence s#generators_37>0

--Check that it is a proper SOS decomposition
F_3==sum apply(s#generators,s#coefficients,(i,j)->i^2*j)

-------------------------------------------------------------
-- Graph 9:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {4, 2}, {4, 5}, {5, 1}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_2

sol=time solveSOS F_2
--Too many heap sections: Increase MAXHINCR or MAX_HEAP_SECTS
--Aborted (core dumped)

--Process M2 exited abnormally with code 134

"Simple5Nodes_Graph9.txt" << toString F_2 << endl << close

-- IN THE SERVER:
-- more than 10 min and used 250 GB of memory 
-- l_(2,2)l_(3,3)^2l_(4,4)^2*l_(5,5)>0

-------------------------------------------------------------
-- Graph 10:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {4, 2}, {5, 1}, {5, 4}} 
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_2

sol=time solveSOS F_2
--Too many heap sections: Increase MAXHINCR or MAX_HEAP_SECTS
--Aborted (core dumped)

--Process M2 exited abnormally with code 134

"Simple5Nodes_Graph10.txt" << toString F_2 << endl << close

-- IN THE SERVER:
-- half an hour and used 430 GB memory
-- Returns output but really nasty, not clear that it is a true SOS since
-- there are many
-- not easy to check whether it doesn't vanish
-------------------------------------------------------------
-- Graph 11:  
-------------------------------------------------------------

G=digraph {{1, 2}, {1, 3}, {2, 3}, {2, 4}, {3, 4}, {3, 5}, {4, 1}, {4, 5}, {5, 1}, {5, 2}}  
f=det kern^(kernelPattern G);
F=time extractFactors factor f;
netList F
F_2

sol=time solveSOS F_2
--Too many heap sections: Increase MAXHINCR or MAX_HEAP_SECTS
--Aborted (core dumped)

--Process M2 exited abnormally with code 134

"Simple5Nodes_Graph11.txt" << toString F_2 << endl << close
