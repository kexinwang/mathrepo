##################################
Introduction
##################################

We present the matrix :math:`A(\Sigma)` and its kernel :math:`H(\Sigma)`, as both play an important role in our computations.

In our paper we study parameter identifiability of Graphical Continuous Lyapunov Models. Recall that every graph induces a statistical model (Definition 1.3).
Identifiability then corresponds to the existence of a unique solution to the Lyapunov equation :math:`M \Sigma + \Sigma M^{\top}=-C` for a support restricted matrix :math:`M \in \mathrm{Stab}(E)`,
where :math:`E` is the edge set of the graph :math:`G=(V,E)` under consideration. For more details consider Definition 2.1. In general, the matrix :math:`C` is assumed to be positive-definite. However, we obtain our
strongest results when focusing on :math:`C` diagonal. We give more details in our paper.
Vectorizing the Lyapunov equation, we obtain the linear equation system
:math:`A(\Sigma) \mathrm{vec}(M)=-\mathrm{vech}(C)`. We display the matrix :math:`A(\Sigma)` for :math:`p=3` nodes.

.. image:: ASigma3.jpg
    :width: 700px
    :align: center
    :height: 160px
    :alt: alternate text



The matrix has only :math:`(p+1)p/2=(3+1)3/2=6` rows due to the symmetry of the Lyapunov equation.
The column indices correspond to possible edges in a graph on 3 nodes. In Lemma 3.3. we present a necessary and sufficient criterion
for identifiability based on the (full) column rank of :math:`A(\Sigma)_{\cdot,E}`. By :math:`A(\Sigma)_{\cdot,E}` we denote the submatrix of :math:`A(\Sigma)`
consisting of the columns with indices that are elements of the edge set :math:`E`. For illustration, we
consider the 3-cycle

.. image:: 3cycle.jpg
    :width: 300px
    :align: center
    :height: 160px
    :alt: alternate text


Then we obtain

.. image:: ASigma3cycle.jpg
    :width: 600px
    :align: center
    :height: 160px
    :alt: alternate text

In *Macaulay2* these two matrices can be obtained with the following lines of code. It requires downloading :download:`LyapunovModel <LyapunovModel.m2>`.


.. code-block:: Macaulay2

	restart
	loadPackage "LyapunovModel"
	G=digraph {{1,2},{2,3},{3,1}}
	A=ASigma G
	AS=ASigmaGraph G


Calculating the determinant we obtain :math:`\det(A(\Sigma)_{\cdot,E})=2^3 \det(\Sigma) (\Sigma_{11}\Sigma_{22}\Sigma_{33}-\Sigma_{12}\Sigma_{13}\Sigma_{23})`.
Of course, :math:`\det(\Sigma)>0` and, as we show in Example 1.5, the same applies to the second factor. Using the duality of :math:`A(\Sigma)` and
its kernel :math:`H(\Sigma)`, the rank conditions in Lemma 3.3 can also be established in terms of the kernel, as we present in Lemma 5.4.
In case of a complete graph (:math:`(p+1)p/2` edges), this leads to a convenient reduction of the problem size. For :math:`p=3` nodes we obtain

.. image:: Kernel3.jpg
    :width: 350px
    :align: center
    :height: 200px
    :alt: alternate text

Contrary to :math:`A(\Sigma)`, we have to select the non-edges of the graph to obtain the relevant matrix :math:`H(\Sigma)_{E^c,\cdot}`.
Returning to the 3-cycle example and calculating the determinant, we obtain the same critical factor as before

.. image:: Kernel3cycle.jpg
    :width: 700px
    :align: center
    :height: 90px
    :alt: alternate text

We can compute the restricted kernel and its determinant using this *Macaulay2* code.

.. code-block:: Macaulay2

 --compute kernel
 kern=syz A
 --compute restricted kernel corresponding to non-edges
 kernE=kern^(kernelPattern G)
 f=det E
