.. _table_binary_octic:

=====================================
Euler Stratification of Binary Octics
=====================================

Each stratum is labeled by a decorated partition as follows: a label is a tuple :math:`(\lambda, m_0, m_{\infty})`. Here, :math:`\lambda` is a partition of :math:`8-m_0-m_{\infty}`. The corresponding stratum consists of binary forms with a root of multiplicity :math:`m_0` at zero, a root of multiplicity :math:`m_{\infty}` at infinity, and :math:`|\lambda|` roots in the torus. The first root in the torus has multiplicity :math:`\lambda_1`, the second has multiplicity :math:`\lambda_2`, and so on. The equations for strata of binary forms in :math:`\mathbb{P}^1` correspond to the decorated partitions with :math:`m_0=m_{\infty}=0`.

The .json files can be imported into Oscar at version 0.14 with the following command:

.. code-block:: Julia

   Oscar.load("stratum.json")


.. csv-table:: Euler Stratification of Binary Octics
   :file: strati.csv
   :widths: 20, 40, 40
   :header-rows: 1