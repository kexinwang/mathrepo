Euler strata of the GKZ family corresponding to: 
A = [1 0 1 2 1 0; 0 1 1 1 2 2] 
 
 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 4 
string: (4,1,4) 
 ideal generators: 
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 3 
string: (4,1,3) 
 ideal generators: 
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 8 
euler characteristic: 4 
string: (4,8,4) 
 ideal generators: 
16*z1^3*z5^3*z6^2 + 16*z1^2*z2^2*z5^4 - 16*z1^2*z2*z3*z5^3*z6 - 24*z1^2*z2*z4*z5^2*z6^2 - 8*z1^2*z3^2*z5^2*z6^2 + 36*z1^2*z3*z4*z5*z6^3 - 27*z1^2*z4^2*z6^4 - 32*z1*z2^3*z4*z5^3 - 8*z1*z2^2*z3^2*z5^3 + 64*z1*z2^2*z3*z4*z5^2*z6 - 24*z1*z2^2*z4^2*z5*z6^2 + 8*z1*z2*z3^3*z5^2*z6 - 46*z1*z2*z3^2*z4*z5*z6^2 + 36*z1*z2*z3*z4^2*z6^3 + z1*z3^4*z5*z6^2 - z1*z3^3*z4*z6^3 + 16*z2^4*z4^2*z5^2 - 8*z2^3*z3^2*z4*z5^2 - 16*z2^3*z3*z4^2*z5*z6 + 16*z2^3*z4^3*z6^2 + z2^2*z3^4*z5^2 + 8*z2^2*z3^3*z4*z5*z6 - 8*z2^2*z3^2*z4^2*z6^2 - z2*z3^5*z5*z6 + z2*z3^4*z4*z6^2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 3 
string: (4,1,3) 
 ideal generators: 
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 4 
string: (4,1,4) 
 ideal generators: 
z6
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 4 
string: (4,1,4) 
 ideal generators: 
z5
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z6
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 5 
euler characteristic: 3 
string: (3,5,3) 
 ideal generators: 
16*z1^2*z5^3 - 8*z1*z3^2*z5^2 + 36*z1*z3*z4*z5*z6 - 27*z1*z4^2*z6^2 + z3^4*z5 - z3^3*z4*z6
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 3 
string: (3,1,3) 
 ideal generators: 
z5
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 2 
string: (3,2,2) 
 ideal generators: 
4*z2*z4 - z3^2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 2 
string: (3,3,2) 
 ideal generators: 
z2*z5^2 - z3*z5*z6 + z4*z6^2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z4
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z6
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z5
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 12 
euler characteristic: 3 
string: (3,12,3) 
 ideal generators: 
64*z2^3*z5^4 - 96*z2^2*z3*z5^3*z6 + 144*z2^2*z4*z5^2*z6^2 + 48*z2*z3^2*z5^2*z6^2 - 144*z2*z3*z4*z5*z6^3 + 108*z2*z4^2*z6^4 - 8*z3^3*z5*z6^3 + 9*z3^2*z4*z6^4
12*z1*z5*z6^2 + 16*z2^2*z5^2 - 16*z2*z3*z5*z6 + 12*z2*z4*z6^2 + z3^2*z6^2
27*z1*z4*z6^4 - 16*z2^3*z5^3 + 24*z2^2*z3*z5^2*z6 - 12*z2*z3^2*z5*z6^2 + 2*z3^3*z6^3
8*z1*z2*z5^2 - 4*z1*z3*z5*z6 + 18*z1*z4*z6^2 + 8*z2^2*z4*z5 - 2*z2*z3^2*z5 - 4*z2*z3*z4*z6 + z3^3*z6
16*z1^2*z5^2 - 16*z1*z2*z4*z5 - 8*z1*z3^2*z5 + 24*z1*z3*z4*z6 + 16*z2^2*z4^2 - 8*z2*z3^2*z4 + z3^4
108*z1^2*z4*z6^3 + 32*z1*z2^2*z3*z5^2 + 96*z1*z2^2*z4*z5*z6 - 20*z1*z2*z3^2*z5*z6 - 72*z1*z2*z3*z4*z6^2 + 8*z1*z3^3*z6^2 - 48*z2^3*z4^2*z6 + 24*z2^2*z3^2*z4*z6 - 3*z2*z3^4*z6
8*z1^2*z3*z5*z6 - 36*z1^2*z4*z6^2 - 32*z1*z2^2*z4*z5 - 4*z1*z2*z3^2*z5 + 32*z1*z2*z3*z4*z6 - 2*z1*z3^3*z6 + 16*z2^3*z4^2 - 8*z2^2*z3^2*z4 + z2*z3^4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 3 
string: (3,3,3) 
 ideal generators: 
z3*z5 - z4*z6
z1*z6 - z2*z3
z1*z5 - z2*z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 2 
string: (3,2,2) 
 ideal generators: 
4*z1*z5 - z3^2
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 2 
string: (3,3,2) 
 ideal generators: 
z1*z6^2 + z2^2*z5 - z2*z3*z6
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z5
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z6
z5
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 4 
euler characteristic: 3 
string: (3,4,3) 
 ideal generators: 
16*z1^2*z5^2 - 32*z1*z2*z4*z5 - 8*z1*z3^2*z5 + 16*z2^2*z4^2 - 8*z2*z3^2*z4 + z3^4
z6
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 5 
euler characteristic: 3 
string: (3,5,3) 
 ideal generators: 
27*z1^2*z4*z6^2 - 36*z1*z2*z3*z4*z6 + z1*z3^3*z6 - 16*z2^3*z4^2 + 8*z2^2*z3^2*z4 - z2*z3^4
z5
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 2 
string: (3,1,2) 
 ideal generators: 
z6
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z3
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z6
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
z3*z5 - z4*z6
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z4
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z1*z5 - z3^2
z6
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (2,1,0) 
 ideal generators: 
z6
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z6
z5
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 4 
euler characteristic: 2 
string: (2,4,2) 
 ideal generators: 
8*z3*z5 - 9*z4*z6
12*z1*z5 + z3^2
27*z1*z4*z6 + 2*z3^3
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z1*z5 - z3^2
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 2 
string: (2,3,2) 
 ideal generators: 
27*z1*z4*z6 + z3^3
z5
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 1 
string: (2,3,1) 
 ideal generators: 
z3*z5 - 2*z4*z6
2*z2*z5 - z3*z6
4*z2*z4 - z3^2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z4
z3
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z2*z4 - z3^2
z6
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z2*z4 - z3^2
z5
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
z2*z5 - z3*z6
z4
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z4
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (2,1,0) 
 ideal generators: 
z6
z5
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z6
z4
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 5 
euler characteristic: 2 
string: (2,5,2) 
 ideal generators: 
z3*z5 - z4*z6
4*z2*z5 + z3*z6
4*z2*z4 + z3^2
z1*z6 - z2*z3
4*z1*z5 + z3^2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 3 
euler characteristic: 1 
string: (2,3,1) 
 ideal generators: 
2*z2*z5 - z3*z6
2*z1*z6 - z2*z3
4*z1*z5 - z3^2
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z5
z4
z3
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z2*z4 - z3^2
z6
z5
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 4 
euler characteristic: 2 
string: (2,4,2) 
 ideal generators: 
12*z2*z4 + z3^2
9*z1*z6 - 8*z2*z3
z5
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
z1*z6 - z2*z3
z5
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 2 
string: (2,2,2) 
 ideal generators: 
z1*z5 - z2*z4
z6
z3
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 1 
string: (2,2,1) 
 ideal generators: 
4*z1*z5 - z3^2
z6
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (2,1,1) 
 ideal generators: 
z6
z5
z4
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z3
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z4
z3
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z3
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z4
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z5
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z4
z2
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 0 
string: (1,2,0) 
 ideal generators: 
4*z1*z5 - z3^2
z6
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z5
z3
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z5
z4
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z5
z4
z3
z2
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 1 
string: (1,1,0) 
 ideal generators: 
z5
z4
z3
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 2 
euler characteristic: 0 
string: (1,2,0) 
 ideal generators: 
4*z2*z4 - z3^2
z6
z5
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z4
z3
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z5
z4
z1
------------------------------------------------- 
dimension stratum: 3 
degree stratum: 1 
euler characteristic: 0 
string: (1,1,0) 
 ideal generators: 
z6
z5
z4
z3
------------------------------------------------- 
