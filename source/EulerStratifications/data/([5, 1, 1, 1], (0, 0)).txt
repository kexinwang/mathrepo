280*z0*z8 - 35*z1*z7 + 10*z2*z6 - 5*z3*z5 + 2*z4^2
640*z0*z6*z8 - 245*z0*z7^2 - 140*z1*z5*z8 + 25*z1*z6*z7 + 64*z2*z4*z8 - 5*z2*z5*z7 - 25*z3^2*z8 + z3*z4*z7
400*z0*z5*z8 - 70*z0*z6*z7 - 56*z1*z4*z8 - 50*z1*z5*z7 + 25*z1*z6^2 + 10*z2*z3*z8 + 25*z2*z4*z7 - 5*z2*z5*z6 - 10*z3^2*z7 + z3*z4*z6
800*z0*z4*z8 - 350*z0*z5*z7 + 135*z0*z6^2 - 350*z1*z3*z8 + 54*z1*z4*z7 - 10*z1*z5*z6 + 135*z2^2*z8 - 10*z2*z3*z7 + z2*z4*z6
-9216*z0*z4*z8 + 4130*z0*z5*z7 - 1600*z0*z6^2 + 4130*z1*z3*z8 - 675*z1*z4*z7 + 125*z1*z5*z6 - 1600*z2^2*z8 + 125*z2*z3*z7 - 5*z2*z5^2 - 5*z3^2*z6 + z3*z4*z5
400*z0*z3*z8 - 56*z0*z4*z7 + 10*z0*z5*z6 - 70*z1*z2*z8 - 50*z1*z3*z7 + 25*z1*z4*z6 - 10*z1*z5^2 + 25*z2^2*z7 - 5*z2*z3*z6 + z2*z4*z5
640*z0*z2*z8 - 140*z0*z3*z7 + 64*z0*z4*z6 - 25*z0*z5^2 - 245*z1^2*z8 + 25*z1*z2*z7 - 5*z1*z3*z6 + z1*z4*z5
-17920*z0*z2*z8^2 + 3800*z0*z3*z7*z8 - 1280*z0*z4*z6*z8 - 140*z0*z4*z7^2 + 500*z0*z5^2*z8 + 25*z0*z5*z6*z7 + 6860*z1^2*z8^2 - 679*z1*z2*z7*z8 - 35*z1*z3*z6*z8 + 64*z1*z3*z7^2 - 5*z1*z4*z6*z7 + 64*z2^2*z6*z8 - 25*z2^2*z7^2 - 5*z2*z3*z5*z8 + z2*z3*z6*z7
-7840*z0*z1*z8^2 + 2476*z0*z2*z7*z8 + 540*z0*z3*z6*z8 - 756*z0*z3*z7^2 - 80*z0*z4*z5*z8 + 270*z0*z4*z6*z7 - 100*z0*z5^2*z7 - 350*z1*z2*z6*z8 + 135*z1*z2*z7^2 + 54*z2^2*z5*z8 - 10*z2^2*z6*z7 - 10*z2*z3*z4*z8 + z2*z3*z5*z7
-20480*z0^2*z8^2 + 2460*z0*z1*z7*z8 + 256*z0*z2*z6*z8 - 350*z0*z2*z7^2 - 20*z0*z3*z5*z8 + 54*z0*z3*z6*z7 - 10*z0*z4*z5*z7 - 350*z1^2*z6*z8 + 135*z1^2*z7^2 + 54*z1*z2*z5*z8 - 10*z1*z2*z6*z7 - 10*z1*z3*z4*z8 + z1*z3*z5*z7
249600*z0^2*z8^2 - 29920*z0*z1*z7*z8 - 3720*z0*z2*z6*z8 + 4480*z0*z2*z7^2 + 400*z0*z3*z5*z8 - 680*z0*z3*z6*z7 + 100*z0*z4*z5*z7 + 10*z0*z4*z6^2 + 4480*z1^2*z6*z8 - 1728*z1^2*z7^2 - 680*z1*z2*z5*z8 + 126*z1*z2*z6*z7 + 100*z1*z3*z4*z8 - 5*z1*z3*z6^2 + 10*z2^2*z4*z8 - 5*z2^2*z5*z7 + 2*z2^2*z6^2
-7840*z0^2*z7*z8 + 2476*z0*z1*z6*z8 + 540*z0*z2*z5*z8 - 350*z0*z2*z6*z7 - 80*z0*z3*z4*z8 + 54*z0*z3*z6^2 - 10*z0*z4*z5*z6 - 756*z1^2*z5*z8 + 135*z1^2*z6*z7 + 270*z1*z2*z4*z8 - 10*z1*z2*z6^2 - 100*z1*z3^2*z8 + z1*z3*z5*z6
-17920*z0^2*z6*z8 + 6860*z0^2*z7^2 + 3800*z0*z1*z5*z8 - 679*z0*z1*z6*z7 - 1280*z0*z2*z4*z8 - 35*z0*z2*z5*z7 + 64*z0*z2*z6^2 + 500*z0*z3^2*z8 - 5*z0*z3*z5*z6 - 140*z1^2*z4*z8 + 64*z1^2*z5*z7 - 25*z1^2*z6^2 + 25*z1*z2*z3*z8 - 5*z1*z2*z4*z7 + z1*z2*z5*z6
