========================================
Example computation
========================================

We replicate here Example 3.2 of the paper using step by step the code described in :doc:`implementation`.

.. code-block:: macaulay2

    i1 : load "main_algorithm.m2"

    i2 : R = QQ[x_(1,1)..x_(1,3)]**QQ[x_(2,1),x_(2,2)]**QQ[x_(3,1),x_(3,2)]**QQ[x_(4,1),x_(4,2)];

    i3 : T = 12*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,1)+8*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,2)+
         6*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,1)+
         4*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,2)+30*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,1)+20*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,2)+
         15*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,1)+10*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,2)+8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,1)+
         8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,2)+5*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,1)+6*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,2)+
         35*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,1)+38*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,2)+23*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,1)+
         30*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,2)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,1)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,2)+
         10*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,1)+12*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,2)+52*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,1)+
         64*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,2)+37*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,1)+54*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,2);

    i4 : Rten = spaceReshape((1,2),T)--reshape of the first two factors of T

    o4 = 12x        x   x    + 8x        x   x    + 6x        x   x    + 4x        x   x    + 30x        x   x    + 20x        x   x    + 15x        x   x    + 10x        x   x    + 8x        x   x    + 8x        x   x    + 5x        x   x    + 6x        x   x    + 35x        x   x    + 38x        x   x    + 23x        x   x    + 30x        x   x    + 16x        x   x    + 16x        x   x    + 10x        x   x    + 12x        x   x    + 52x        x   x    + 64x        x   x    + 37x        x   x    + 54x        x   x
            (1, 2),0 3,1 4,1     (1, 2),0 3,1 4,2     (1, 2),0 3,2 4,1     (1, 2),0 3,2 4,2      (1, 2),1 3,1 4,1      (1, 2),1 3,1 4,2      (1, 2),1 3,2 4,1      (1, 2),1 3,2 4,2     (1, 2),2 3,1 4,1     (1, 2),2 3,1 4,2     (1, 2),2 3,2 4,1     (1, 2),2 3,2 4,2      (1, 2),3 3,1 4,1      (1, 2),3 3,1 4,2      (1, 2),3 3,2 4,1      (1, 2),3 3,2 4,2      (1, 2),4 3,1 4,1      (1, 2),4 3,1 4,2      (1, 2),4 3,2 4,1      (1, 2),4 3,2 4,2      (1, 2),5 3,1 4,1      (1, 2),5 3,1 4,2      (1, 2),5 3,2 4,1      (1, 2),5 3,2 4,2

    o4 : QQ[x        ..x        , x   ..x   ]
             (1, 2),0   (1, 2),5   3,1   4,2

    i5 : checkfun(secondSec(Rten)) --verify that \theta_{1,2}(T) belongs to the second secant variety

    o5 = true

    i6 : (A,tconc,cindList) = concisionFirst(1,Rten) --A is the matrix of the first flattening of \theta_{1,2}(T)

    o6 = (| 12 8  6  4  |, | 1 0 1/4 -1/2 |, {0, 1})
          | 30 20 15 10 |  | 0 1 3/8 5/4  |
          | 8  8  5  6  |
          | 35 38 23 30 |
          | 16 16 10 12 |
          | 52 64 37 54 |

    o6 : Sequence

    i7 : tenCONC = reconstructFromMatrixConc(A,tconc,cindList) --concise tensor T' of \theta_{1,2}(T)

                      1             1                          3             5
    o7 = y x   x    + -y x   x    - -y x   x    + y x   x    + -y x   x    + -y x   x
          0 3,1 4,1   4 0 3,2 4,1   2 0 3,2 4,2    1 3,1 4,2   8 1 3,2 4,1   4 1 3,2 4,2

    o7 : newfirst

    i8 : Mat = flattening(tenCONC,1)

    o8 = {{-1, 0, 0} | 1    0   0 0 0 0 0 0 |, {0, -1, 0} | 1 1/4  |, {0, 0, -1} | 1   0    |}
          {-1, 0, 0} | 0    1   0 0 0 0 0 0 |  {0, -1, 0} | 0 -1/2 |  {0, 0, -1} | 1/4 -1/2 |
          {-1, 0, 0} | 1/4  3/8 0 0 0 0 0 0 |  {0, -1, 0} | 0 3/8  |  {0, 0, -1} | 0   1    |
          {-1, 0, 0} | -1/2 5/4 0 0 0 0 0 0 |  {0, -1, 0} | 1 5/4  |  {0, 0, -1} | 3/8 5/4  |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |
                                               {0, -1, 0} | 0 0    |  {0, 0, -1} | 0   0    |

    o8 : List

    i9 : Mat = Mat_0

    o9 = {-1, 0, 0} | 1    0   0 0 0 0 0 0 |
         {-1, 0, 0} | 0    1   0 0 0 0 0 0 |
         {-1, 0, 0} | 1/4  3/8 0 0 0 0 0 0 |
         {-1, 0, 0} | -1/2 5/4 0 0 0 0 0 0 |

                        4              8
    o9 : Matrix newfirst  <--- newfirst

    i10 : Mat = transpose(Mat)

    o10 = {1, 0, 0} | 1 0 1/4 -1/2 |
          {1, 0, 0} | 0 1 3/8 5/4  |
          {1, 0, 0} | 0 0 0   0    |
          {1, 0, 0} | 0 0 0   0    |
          {1, 0, 0} | 0 0 0   0    |
          {1, 0, 0} | 0 0 0   0    |
          {1, 0, 0} | 0 0 0   0    |
          {1, 0, 0} | 0 0 0   0    |

                         8              4
    o10 : Matrix newfirst  <--- newfirst

    i11 : Cc_1 = Mat_{0..(numcols(Mat)//2-1)}^{0,1} ---first face of the pencil

    o11 = {1, 0, 0} | 1 0 |
          {1, 0, 0} | 0 1 |

                         2              2
    o11 : Matrix newfirst  <--- newfirst

    i12 : Cc_2 = Mat_{(numcols(Mat)//2)..numcols(Mat)-1}^{0,1} --second face of the pencil

    o12 = {1, 0, 0} | 1/4 -1/2 |
          {1, 0, 0} | 3/8 5/4  |

                         2              2
    o12 : Matrix newfirst  <--- newfirst

    i13 : rightIn = transpose(Cc_2)*(inverse(Cc_2*transpose(Cc_2))) --right inverse of C2

    o13 = {-1, 0, 0} | 5/2  1   |
          {-1, 0, 0} | -3/4 1/2 |

                     2              2
    o13 : Matrix newfirst  <--- newfirst

    i14 : D = Cc_1*rightIn --C1*C2^{-1}

    o14 = {1, 0, 0} | 5/2  1   |
          {1, 0, 0} | -3/4 1/2 |

                     2              2
    o14 : Matrix newfirst  <--- newfirst

    i15 : D = sub(D,QQ);

                   2        2
    o15 : Matrix QQ  <--- QQ

    i16 : (e,v) = eigenvectors(D) --we need the eigenvalues of C1*C2^{-1}

    o16 = ({2}, | .894427  -.5547 |)
           {1}  | -.447214 .83205 |

    o16 : Sequence

    i17 : for i to 1 do(
              w_i = mingens(ker(D-(realPart(e_i))*id_(RR^2))); --these are the eigenvectors
          );--we do not use the eigenvectors given by the M2 builted function because of the numerical inaccuracy
    -- warning: experimental computation over inexact field begun
    --          results not reliable (one warning given per session)

    i18 : firstindcol = sub(A_{cindList_0},RR)--first indep column of the flattening matrix A

    o18 = | 12 |
          | 30 |
          | 8  |
          | 35 |
          | 16 |
          | 52 |

                     6          1
    o18 : Matrix RR    <--- RR
                   53         53

    i19 : secondindcol = sub(A_{cindList_1},RR)--second indep column of the flattening matrix A

    o19 = | 8  |
          | 20 |
          | 8  |
          | 38 |
          | 16 |
          | 64 |

                     6          1
    o19 : Matrix RR    <--- RR
                   53         53

    i20 : for i to 1 do(
              rmat_i = firstindcol*w_i_(0,0)+secondindcol*w_i_(1,0);--matrix of the eigenvector
              rmat_i = transpose reshape(RR^2,RR^3,rmat_i);--matrix of the eigenvector
              print minors(2,rmat_i);--we do not compute the rank usinf the builted funtion "rank" to avoid problems due to the fact that we are in an inexact field
          );
    ideal (192, -192)
    ideal ()

    i21 : --Equivalently one can simply check the result of the following
    i22: restart

    Macaulay2, version 1.19.1
    with packages: ConwayPolynomials, Elimination, IntegralClosure, InverseSystems, LLLBases, MinimalPrimes, PrimaryDecomposition, ReesAlgebra, Saturation, TangentCone

    i1 : load "eqnTangential.m2"

    i2 : load "main_algorithm.m2"

    i3 : R = QQ[x_(1,1)..x_(1,3)]**QQ[x_(2,1),x_(2,2)]**QQ[x_(3,1),x_(3,2)]**QQ[x_(4,1),x_(4,2)];

    i4 : T = 12*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,1)+8*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,2)+6*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,1)+
             4*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,2)+30*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,1)+20*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,2)+
             15*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,1)+10*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,2)+8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,1)+
             8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,2)+5*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,1)+6*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,2)+
             35*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,1)+38*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,2)+23*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,1)+
             30*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,2)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,1)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,2)+
             10*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,1)+12*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,2)+52*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,1)+
             64*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,2)+37*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,1)+54*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,2);

    i5 : moreThanThreeF(T)
    T corresp. to case f
       


