=======================================================
An algorithm for the identifiability of rank-3 tensors 
=======================================================

| This page contains the algorithm described in the paper:
| Pierpaola Santarsiero: An algorithm for the non-identifiability of rank-3 tensors
| In: Bollettino dell'Unione Matematica Italiana, 16 (2023) 3, p. 595-624
| DOI: `10.1007/s40574-023-00352-0 <https://dx.doi.org/10.1007/s40574-023-00352-0>`_ ARXIV: https://arxiv.org/abs/2211.06384 CODE: https://mathrepo.mis.mpg.de/identifiabilityRank3tensors

.. |code| replace:: :download:`main algorithm <main_algorithm.m2>`
 

The |code| implements different functions in Macaulay2, aimed to understand if a concise tensor is a non-identifiable rank-3 tensor. To run the main algorithm one has to download also the file :download:`eqnTangential.m2 <eqnTangential.m2>` that implements equations for the tangential variety of a Segre variety given by the products of just projective lines (for which we refer to [Oe11]_ ).

**Notation:**

- We denote by :math:`X_{n_1,\dots,n_k}=\nu(Y_{n_1,\dots,n_k})` the Segre variety of the multiprojective space :math:`Y_{n_1,\dots,n_k}=\mathbb{P}^{n_1}\times \cdots \times \mathbb{P}^{n_k}`. 

- The tensor space :math:`\mathbb{C}^{n_1}\otimes \cdots \otimes \mathbb{C}^{n_k}` is denoted by :math:`\mathcal{T}_{n_1,\dots,n_k}` .

The algorithm is based on the classification Theorem 7.1 of [BBS]_, we briefly recall it here in the revised version of the Theorem (cf. Theorem 4.1).


A concise rank-3 tensor :math:`T\in \mathbb{C}^{n_1}\otimes \cdots \otimes \mathbb{C}^{n_k}` is identifiable except if :math:`T` is in one of the following families.
   
**1. Matrix case**
The first trivial example of non-identifiable rank-3 tensors are :math:`3 \times 3` matrices, which is a very classical case.

**2. Tangential case**
The *tangential variety* of a variety is the tangent developable of the variety itself. A point :math:`q` essentially lying on the tangential variety of the Segre :math:`X_{1,1,1}` is actually a point of the tangent space :math:`T_{[p]}X_{1,1,1}` for some :math:`p=u\otimes v \otimes w\in (\mathbb{C}^2)^{\otimes 3}`. Therefore there exists some :math:`a,b,c\in \mathbb{C}^2` such that :math:`T` can be written as 
  .. math::
    T=a\otimes v\otimes w+u\otimes b \otimes w+u\otimes v \otimes c

and hence :math:`q` is actually non-identifiable.

**3. Defective case** 
We recall that the third secant variety of a Segre variety :math:`X_{n_1,\dots,n_k}` is defective if and only if :math:`(n_1,\dots ,n_k)=(1,1,1,1),(1,1,a)` with :math:`a\geq 3` (cf. Theorem 4.5 of [AOP]_). The latter case do not play a role in the discussion and hence we can focus on the case :math:`k=4`. 
By defectivity, the dimension of :math:`\sigma_3(X_{1,1,1,1})` is strictly smaller than the expected dimension and this proves that the generic element of :math:`\sigma_3(X_{1,1,1,1})` has an infinite number of rank-3 decompositions and therefore all the rank-3 tensor of this variety have an infinite number of decompositions.

**4.5. Conic cases**
In this case one works with the Segre variety :math:`X_{2,1,1}` given by the image of a projective plane and two projective lines. 
Let :math:`Y_{2,1,1}=\mathbb{P}^2\times \mathbb{P}^1\times \mathbb{P}^1`. Consider the Segre variety :math:`X_{1,1} \subset \mathbb{P}^3` given by the last two factors of :math:`Y_{2,1,1}` and take a hyperplane section which intersects :math:`X_{1,1}` in a conic :math:`\mathcal{C }`. Let :math:`L_{\mathcal{C}}` be the Segre given by the product of the first factor :math:`\mathbb{P}^2` of :math:`Y_{2,1,1}` and the conic :math:`\mathcal{C}`, therefore :math:`L_{\mathcal{C}}\subset X_{2,1,1}`. The family of non-identifiable rank-3 tensors are points lying in the span of :math:`L_{\mathcal{C}}`.
In this case, the non-identifiability comes from the fact that the points on :math:`\langle \mathcal{C} \rangle` are not identifiable and the distinction between the two cases reflects the fact that the conic :math:`\mathcal{C}` can be either irreducible or reducible. The distinction between the two cases can be expressed as follows working in coordinates: 

4. The non-identifiable tensor :math:`T\in\mathbb{C}^3\otimes \mathbb{C}^2\otimes \mathbb{C}^2` and there exists a basis :math:`\{u_1,u_2,u_3\}\subset\mathbb{C}^3` and a basis :math:`\{ v_1,v_2 \} \subset\mathbb{C}^2` such that :math:`T` can be written as 
     
  .. math::
    T= u_1 \otimes v_1^2+u_2\otimes v_2^2 + u_3 \otimes (\alpha v_1+\beta v_2)^2,
      
for some :math:`\alpha,\beta\neq 0`;

5. The non-identifiable tensor :math:`T\in\mathbb{C}^3\otimes \mathbb{C}^2\otimes \mathbb{C}^2` and there exists a basis  :math:`\{u_1,u_2,u_3\}\subset\mathbb{C}^3` and a basis :math:`\{ v_1,v_2 \} \subset \mathbb{C}^2` such that :math:`T` can be written as  
      
  .. math::
    T= u_1 \otimes v_1\otimes \tilde{p}+u_2\otimes v_2 \otimes \tilde{p}+ u_3\otimes \tilde{q} \otimes w,  
      
  for some :math:`\tilde{q}\in \langle v_1,v_2 \rangle`, where :math:`\tilde{p},w \in \mathbb{C}^2` must be linearly independent;
      
**6. General case**
The last family of non-identifiable rank-3 tensors relates the Segre variety :math:`X_{n_1,n_2,1^{k-2}}` that is the image of the multiprojective space :math:`Y_{n_1,n_2,1^{k-2}}=\mathbb{P}^{n_1}\times \mathbb{P}^{n_2}\times (\mathbb{P}^1)^{(k-2)}`, where either :math:`k\geq 4` and :math:`n_1,n_2\in \{1,2\}` or :math:`k=3` and :math:`(n_1,n_2,n_3)\neq (2,1,1)`.
The non-identifiable rank-3 tensors of this case are as follows.
Let :math:`Y':=\mathbb{P}^1\times \mathbb{P}^1\times \{ u_3\} \times \cdots \times \{ u_k\}` be a proper subset of :math:`Y_{n_1,n_2,1^{k-2}}`, take :math:`q'` in the span of the Segre image of :math:`Y'` with the constrain that :math:`q'` is not an elementary tensor. Therefore :math:`q'` is a non-identifiable tensor of rank-2 since it can be seen as a :math:`2\times 2` matrix of rank-2. Let :math:`p\in X_{n_1,n_2,1^{k-2}}` be a rank-1 tensor taken outside the Segre image of :math:`Y'`. Now any point :math:`q \in \langle  \{q',p \}  \rangle \setminus \{q' , p\}` is a rank-3 tensor and it is not identifiable since :math:`q'` has an infinite number of decompositions and each of these decompositions can be taken by considering :math:`p` together with a decomposition of :math:`q'`.
   
For a coordinate description of this case, we take :math:`T \in \mathbb{C}^{m_1}\otimes \mathbb{C}^{m_2}\otimes (\mathbb{C}^2)^{k-2}`, where :math:`k\geq 3`, :math:`m_1,m_2\in \{2,3 \}` such that :math:`m_1+m_2+ (k-2)\geq 4`. Moreover there exist distinct :math:`a_1,a_2\in \mathbb{C}^{m_1}`, distinct :math:`b_1,b_2\in \mathbb{C}^{m_2}` and for all :math:`i\geq 3` there exists a basis :math:`\{u_i,\tilde{u}_i\}` of the :math:`i`-th factor such that :math:`T` can be written as
    
  .. math::
   T= (a_1\otimes b_1+a_2\otimes b_2)\otimes u_3 \otimes \cdots \otimes u_k + a_3 \otimes b_3\otimes \tilde{u}_3 \otimes \cdots \otimes \tilde{u}_k,
   
where if :math:`m_1=2` then :math:`a_3\in \langle  a_1,a_2\rangle` otherwise :math:`a_1,a_2,a_3` are linearly independent. Similarly, if :math:`m_2=2` then :math:`b_3 \in \langle b_1,b_2\rangle`, otherwise :math:`b_1,b_2,b_3` form a basis of the second factor.
    

**References:**

.. [AOP] H. Abo, G. Ottaviani, C. Peterson, "Induction for secant varieties of Segre varieties", Trans. Am. Math. Soc., 361:767-792, 2009.
.. [BBS] E. Ballico, A. Bernardi, P. Santarsiero, "Identifiability of rank-3 tensors", Mediterr. J. Math., 18:1-26, 2020.
.. [Oe11] L. Oeding, "Set-theoretic defining equations of the tangential variety of the Segre variety." J. Pure Appl. Algebra, 215.6:1516-1527 2011.

.. toctree::
  :maxdepth: 2

  implementation.rst
  example.rst

Project page created: 25/11/2021

Project contributors: Pierpaola Santarsiero

Software used: Macaulay2 (v1.19.1)

System setup used: MacBook Pro with macOS Ventura 13.0.1, Processor 2,6 GHz 6-Core Intel Core i7, Memory 16 GB 2667 MHz DDR4, Graphics Intel UHD Graphics 630 1536 MB.

Corresponding author of this page: Pierpaola Santarsiero, pierpaola.santarsiero@mis.mpg.de
