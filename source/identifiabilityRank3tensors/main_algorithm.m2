--Algorithm 2 (More than 3 factors)

--This function recovers the dimensions of all factors of a given tensor
--INPUT: tensor T in C^n1\otimes...\otimes C^nk
--OUTPUT: h=(n1,...,nk)
infoT=(T)->(
    g := degrees ring T;
    for i to length g_0-1 do(
    	n_i=0;
    	);
    for i in g do(
    	for j to length i-1 do(
	    if i_j != 0 then n_j=n_j+1;
	    );
    	); 
    h:=toSequence({n_0});
    for i from 1 to length g_0-1 do(
    	h=append(h,n_i);
    	);
    return h;
)


--This function computes all possible i-flattening of a tensor T
--INPUT: (tensor T,integer i)
--OUTPUT: list of all possible i-flattening of T
--functions used: infoT
flattening=(T,e)-> (
    t:=infoT(T);
    nfact:=length t; --numbers of factors
    l:={1};
    for i from 1 to nfact-1 do(
	l=append(l,1);
    	);
    K:=subsets(nfact,e);
    slist:=mutableMatrix( ZZ,nfact,length K); --every column corresponds a possible choice of e factors
    for i to length K-1 do(
	for j in K_i do(
	    slist_(j,i)=1;
	    );
	);
    slist=matrix slist;
    FL:={};
    for i to numcols slist -1 do(
	vect1:=entries slist_i; --factors of the domain
	vect2:=l-vect1; --factors of the codomain
	FL=append(FL,contract(basis(vect1,ring T), transpose contract(basis(vect2,ring T), T))); --flattening 
	);
    return FL;
)


--this function computes the d-minors of all possible i-flattenings
--INPUT: (order of minors d,list of all possible i-flatteings FL )
--OUTPUT: list of all computed minors
minflattening=(d,FL)->(
    m:={};
    for i in FL do(
	if (numrows i>=d and numcols i>=d) then(
	     m=append(m,minors(d,i));
	     );
	 );
     return m;
     )


--this function evaluates a tensor T in the equations of the second secant variety
--INPUT: tensor T
--OUTPUT evalutations of T in the 3-minors of all possible i-flattenings of T
--functions used: minflattening, flattening
secondSec=(T)->(
    allmin:={};
    for i from 1 to floor((length (degrees ring T)_0)/2) do(
        allmin=append(allmin, minflattening(3,flattening(T,i)));
	   );
    return allmin;
    )


--this function gives the position of the new variable p_ris in the reshaped ring with respect to the variables in the initial ring
--INPUT: t=(dim factor f1,dim factor f2), tplace={position variable in t_0, position variable in t_1} 
--OUTPUT: ris=position of the new variable with respect to i and j 
--(i,j)---> ris where x_i\tensor x_j--->p_ris
--ex. t=(n1,n2,n3,n4), tplace={i,j,k,l}. Then ris=l+n4*k+n4*n3*j+n4*n3*n2*i
posit=(tpos,tplace)->(
    ris:=0;
    mult:=1; --it is the number 
    for i to length tpos-1 do(
	   if i==0 then(
	       ris=tplace_(length tpos-1);
	   )else(
	       mult=mult*(tpos_(length tpos-i)+1);
	       ris=ris+tplace_(length tpos-1-i)*mult;
	   );
    );
return ris;
)


--this function makes the reshape of a tensor with respect to some factors indexed by a sequence f
--it also works if you want to do a reshape of an already reshaped tensor 
--INPUT: (sequence f, tensor T)
--OUTPUT: the reshaped tensor
--functions used: infoT, posit
spaceReshape=(f,T)->( 
    k:=infoT(T);
    R:=ring T;
    place:=0;
    R1varslist:={};
    listOfVars:=flatten entries vars R;
    for i to length k-1 do(
        for j to k_i-1 do(
            if (any(f,x->x==(i+1))) then(
                R1varslist=append(R1varslist,(listOfVars)_place);
                );
            place=place+1;
            );
        );
    R1varslist=matrix{R1varslist};
    ww:=flatten entries vars R;
    vv:=flatten entries R1varslist;
    for i in vv do(
        ww=delete(i,ww);
        );
    tot:=0;
    for i to length k-2 do(
        if i==0 then(
            lov={};
            for i to k_i-1 do(
                lov=append(lov,listOfVars_i);
                );
            S_i=QQ[lov];
            )else(
            tot=tot+k_(i-1);
            S_i=QQ[(listOfVars)_(tot)..(listOfVars)_(k_i-1+tot)];
            );
    );--getting every component of R
    totvars:=1;
    lista:={};
    for i in f do(
        totvars=totvars*k_(i-1);
        lista=append(lista,k_(i-1)-1);
        );
    lista=toSequence(lista);--proj dimension of the factors f
    totvars= totvars-1;
    S12:=QQ[x_(f,0)..x_(f,totvars)];
    R1:=QQ[flatten entries vars S12,flatten entries vars R];--ring with both p_i and the variables of T
    zeros:={0};
    for i to length f-2 do (
        zeros=append(zeros, 0);
    );
    zeros=toSequence(zeros);        
    allindex:=zeros..lista;
    R1varslist=sub(R1varslist,R1);
    a:={};
    for i in allindex do(
        polvars:=1;
        for j to length i-1 do (
            if j==0 then (
                totale=0;
                )else(
                totale=k_(f_(j-1)-1)+totale;
                );
            polvars=polvars*R1varslist_(0,totale+i_j);
            );
        pol:=x_(f,posit(lista,i))-polvars;--every pol gives the relation between p_ris and the corresponding variables
        a=append(a,pol);
        );
    an:=R1/(ideal a);   
ten:=sub(T,an);
newring:=QQ[x_(f,0)..x_(f,totvars)];
k1=toList(k);
ccont:=0;
for i to length f-1 do(
    k1=drop(k1,{f_i-1-ccont,f_i-1-ccont});
    ccont=ccont+1;
    );
if length k1!=0 then(
    ccont=0;
    for i to length k1-1 do(
        if i==0 then(
            lov2={};
            for i to k1_i-1 do(
                lov2=append(lov2,ww_i);
                );
            newring=newring**QQ[lov2];
            )else( 
            ccont=ccont+k1_(i-1);
            newring=newring**QQ[ww_(ccont)..ww_(ccont+k1_i-1)];
            );
        );
    );
ten = sub(ten,newring)
 )


--this function evaluates T in the equations of the third secant variety of P1xP1xP1xP1
--INPUT: tensor T
--OUTPUT evalutations of T in the 4-minors of all possible i-flattenings of T
--functions used: minflattening, flattening
 defectiveThird=(T)->(
    allmin:={};
     for i from 1 to floor((length (degrees ring T)_0)/2) do(
        allmin=append(allmin,minflattening(4,flattening(T,i)));
        );
    return allmin;
    )


--this function verifies if a list of ideals are all zeros or not
--INPUT: liss=list of lists of ideals
--OUTPUT: boolean value true if all ideals are zero, false otherwise
checkfun=(liss)->(
    flag:=true;
    for i to length liss -1 do(
        for j in liss_i do(
            if mingens j!=0 then
            flag=false;
            );
        );
    return flag;
    )


--this function computes the concision process on a tensor with respect to a given factor
--INPUT: factor f, tensor T
--OUTPUT: concise tensor 
--functions used: flattening
--packages needed: EliminationMatrices
needsPackage "EliminationMatrices"
concisionFirst=(f,T)->(
    MM:=sub(transpose(flattening(T,1))_(f-1),QQ);
    r:= rank MM;
    cdip := for g to (numcols MM -1) list(g);
    cind := maxCol(MM);
    for i to r-1 do(cdip = delete(cind_1_i,cdip));
    tpro := mutableMatrix(ring T,r,numcols MM);
    for i to r-1 do(tpro_(i,cind_1_i)=1);
    for i to length cdip-1 do(
        S := QQ[x_0..x_(r-1)];
        use S;
        ci:=cind_0;
        ci=sub(ci,S);
        aa := -matrix MM_(cdip_i);
        for j to r-1 do(
            aa =aa+ x_j*(matrix ci_j);
            );
        aa = mingens gb ideal (flatten entries aa);
        box := flatten entries aa;
        for jj in box do(
            coeff := coefficients(jj,Monomials=>{x_0..x_(r-1),1});
            coeff = flatten entries coeff_1;
            cont := 0;
            Posit := 0;
            for jjj to r-1 do(
                if coeff_jjj!=0 then (
                cont= cont+1;
                Posit = jjj;
                );
            );
            if cont<2 then (
                h1 := coeff_r;
                h1 = sub(h1,ring T);
                h2 := coeff_Posit;
                h2 = sub(h2,ring T);
                alfa_Posit = -h1*(h2)^(-1);
                );
            tpro_(Posit,cdip_i)=alfa_Posit;
            );      
        );      
    tconc = matrix tpro;
    return (MM,tconc,cind_1);
)


--this function reconstruct the tensor from one of its 1-flattening
--INPUT: (matrix MM, matrix tconc, list cindList ) where MM=matrix of the flattening, tconc=matrix of the relations in the concise space, cindList contains the independent columns of MM
--OUTPUT: concise tensor
--functions used: infoT
reconstructFromMatrixConc=(MM,tconc,cindList)->(
    Ri:=ring tconc;
    k:=infoT(tconc_(0,0));
    ccont:=0;
    for i to length k-1 do(
        if i==0 then(
            newfirst=QQ[y_0,y_1,(flatten entries vars Ri)_0..(flatten entries vars Ri)_(k_i-1)];
            )else(
            ccont=ccont+k_(i-1);
            newfirst=newfirst**QQ[(flatten entries vars Ri)_(ccont)..(flatten entries vars Ri)_(ccont+k_i-1)];
            );
        );
    for i to length k-2 do(
        dividedVars_i=submatrix(vars newfirst,{(k_0+2+2*i)..(k_0+2-1+2*(i+1))} );--is a row with the (i+2)-th set of variables in newfirst, i.e. is the set of variables relative to the i-th column of tconc
        );
    newTen=0;--initialize the new tensor
    tconc=sub(tconc,newfirst);
    for i to numrows tconc -1 do(--rows of tconc
        for j to numcols tconc -1 do(--columns of tconc
            elem=tconc_(i,j)*y_i;--elem is multiplied either by y_0 or by y_1 depending on which row of tconc we are in and by the relative coeff in position (i,j) of tconc 
            for iii to length k-2 do(--iii runs in all factors except the first one
                if iii==0 then(
                    indexElement=j // 2^(length k-2-iii);
                    jj=j%2^(length k-2-iii);
                )else(
                indexElement=jj // 2^(length k-2-iii);--gives the iii-th coefficient in binary expansion of j
                jj=jj%2^(length k-2-iii);
                );
                elem=elem*dividedVars_iii_(0,indexElement);
                );
            newTen=newTen+elem;
            );
        );
    return newTen;
    );


-----
--this function evaluates the tensor in the equations of the tangential variety
--INPUT: (ideal tangential Itan, tensor T, #factors of T)
--OUTPUT: a list containing the ideal of the tangential after the evaluation
evTangential=(Itan,T,k)->(--k=numero fattori 
    use ring Itan;
    varT := flatten entries vars ring T;
    var := flatten entries vars (ring Itan);
    termsT := terms T;
    listIndeces := apply(var,x->(last baseName x));
    listSub :={};
    for ter in termsT do(
        fac := factor ter;
        lis := {};
        for i to k-1 do(--numero fattori-1
            lis = append(lis, fac#i);
        );
        goodl := {};
        for j in lis do(
            for jj in varT do(
                if value j==jj then(
                    goodl = append(goodl,jj);
                );
            );
        );
        goodl = apply(goodl,x->last baseName x);
        goodl = reverse goodl;
        goodIndec := {};
        for i in goodl do(
           try( goodIndec = append(goodIndec,i_1)) else(goodIndec = append(goodIndec,i+1));
        );
        goodIndec = apply(goodIndec,x->x-1);------
        li := delete(goodIndec,listIndeces);
        listSub = append(listSub,z_goodIndec => (coefficients ter)_1_(0,0));
        listIndeces = delete(goodIndec,listIndeces);
    );
for i in listIndeces do(
    listSub = append(listSub,z_i=>0);
    );
return {{sub(Itan,listSub)}};
    )

--checkfun(evTangential(Itan,T,5))


--this function compute the eigenvectors of a 2x2 matrix
--REMARK: to avoid numerical errors we decided to compute symbolically the eigenvectors of
--a 2x2 matrix when the characteristic polynomial does not factor over QQ
--INPUT: 2x2 matrix 
--OUTPUT: it's eigenvectors
evec=(D)->(
    Fi := QQ[b];
    DF := sub(D,Fi);
    Id := sub(id_(QQ^2),Fi);
    fac := factor det(DF-b*Id);
    lisfac := for i in fac list i;
    for i to length lisfac -1 do(
        l := value lisfac_i;
        coe := coefficients l;
        if coe_0_(0,0)==b then(
            eval = sub(-coe_1_(1,0)/coe_1_(0,0),QQ);
            evect_i=mingens ker(D-(eval)*id_(QQ^2));
        )else if coe_0_(0,0)==b^2 then(
            pol := det(DF-b*Id);
            Ca := Fi/ideal(pol);
            cco := sub(coe_1_(1,0),Ca);
            t := -b -cco;
            Dc := sub(DF,Ca);
            evect_0 = matrix(mingens ker(Dc-b*id_(Ca^2)))_0;
            evect_1 = matrix(mingens ker(Dc-t*id_(Ca^2)))_0;
        );  
    );
    return(evect_0,evect_1);  
)    



--this function represents the core of the algorithm and it computes the following steps:
--computes the reshape of a tensor w.r.t. 2 factors indexed by f and verify that the reshaped tensor T lies in the second secant variety
--if so it computes the concise tensor w.r.t. the first factor
--and it verifies that the concise tensor does not belong to the tangential variety
--if so we compute another reshape of the tensor in order to look at it as a matrix pencil C1*s+C2*t w.r.t. its second factor
--Then it computes the eigenvectors x,y of C1*C2^{-1}
-- we look at x and y as matrices and we verify that one of them has rank 2 and the other one has rank 1
--INPUT: (tensor T, sequence f of factors for the reshape)
--OUTPUT: a boolean value
--functions used: infoT, spaceReshape, checkfun, concisionFirst, reconstructFromMatrixConc, 
--                idealTangential, evTangential, evec 
control=(T,f)->(
    k := infoT(T);
    Rten := spaceReshape(f,T);
    FLAG := false;
    if checkfun(secondSec(Rten))==true then(
        (MM,tconc,cindList) := concisionFirst(1,Rten);
        tenCONC = reconstructFromMatrixConc(MM,tconc,cindList);
        Kk = infoT(tenCONC);
        Itan = idealTangential( length Kk);
        listId = evTangential(Itan,tenCONC,length Kk);
        flag2 = checkfun(listId);      ----check per la tangeziale flag2
        if not flag2 then(
            flagFlat=false;
            if length Kk>3 then(
                reList := (3..length Kk);
                tenCONC = spaceReshape(reList,tenCONC);
                flagFlat=true;
            );
            Mat := flattening(tenCONC,1);
            if flagFlat==true then(
                Matr=Mat_1;
            )else(
            Matr = Mat_0;
                );
            Matr = transpose(Matr);
            Cc_1 = Matr_{0..(numcols(Matr)//2-1)}^{0,1};
            Cc_2 = Matr_{(numcols(Matr)//2)..numcols(Matr)-1}^{0,1};
            rightIn := transpose(Cc_2)*(inverse(Cc_2*transpose(Cc_2)));--right inverse
            D := Cc_1*rightIn;
            D = sub(D,QQ);
            evects := evec(D); 
            Riev := ring evects_0;     
            firstindcol := sub(MM_{cindList_0},Riev);
            secondindcol := sub(MM_{cindList_1},Riev);
            for i to 1 do(
                rmat_i = firstindcol*evects_i_(0,0)+secondindcol*evects_i_(1,0);----sono dei w
                rmat_i = transpose reshape(Riev^(k_(f_1-1)),Riev^(k_(f_0-1)),rmat_i);
                mino_i = minors(2,rmat_i);
                if mingens mino_i == 0 then(
                    ran_i = 1;
                )else(ran_i = 2
                );
            );
            if ran_0 != ran_1 then(
                FLAG = true;
            );
        );
    );
    return FLAG;
);



--------------
------MAIN----
--INPUT: tensor T given as a polynomial of QQ[]**....**QQ[]
--OUTPUT: a statement on whether T belongs to one of the families of the classification theorem or not
moreThanThreeF=(T)->(
   k:=infoT(T);
   if k_0==2 and k_1==2 then(
        if length k==4 then(
            <<endl << "containement in 3-sec == " << checkfun(defectiveThird(T))  << endl << "containement in tangential ==" << 0 <<endl;
        )else(
            i:=1;
            j:=2;
            fl:=false;
            while (fl==false) and (i< length k ) do(
                fl=control(T,(i,j));
                if fl==true then(
                    << "T is not id. rank 3 tensor corresp. to case f"<<endl;
                )else if j< length k then(
                    j=j+1;
                )else(
                    i=i+1;
                    j=i+1;
                );
            );
            if fl==false then(
               <<"T is not on the list of non id. rank 3 tensors."<<endl;
            );
        );-------FIN QUI OK
    )else if k_0==3 and k_1==2 then(
        i=1;
        j=2;
        fl=false;
        while (fl==false) and j< length k+1  do(
            fl=control(T,(i,j));
            if fl==true then(
                << "T corresp. to case f"<<endl;
                j=j+1;
            );
        );
        if fl==false then(
           <<"T is not on the list of non id. rank 3 tensors."<<endl;
           );
    )else(
        i=1;
        j=2;
        fl=false;
        fl=control(T,(i,j));
        if fl==true then(
            << "T corresp. to case f"<<endl;
        );
        if fl==false then(
            <<"T is not on the list of non id. rank 3 tensors."<<endl;
        );
    );
);


end


load "eqnTangential.m2"
load "main_algorithm.m2"


--EXAMPLE 3.2:
R = QQ[x_(1,1)..x_(1,3)]**QQ[x_(2,1),x_(2,2)]**QQ[x_(3,1),x_(3,2)]**QQ[x_(4,1),x_(4,2)];
T = 12*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,1)+8*x_(1,1)*x_(2,1)*x_(3,1)*x_(4,2)+6*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,1)+
    4*x_(1,1)*x_(2,1)*x_(3,2)*x_(4,2)+30*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,1)+20*x_(1,1)*x_(2,2)*x_(3,1)*x_(4,2)+
    15*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,1)+10*x_(1,1)*x_(2,2)*x_(3,2)*x_(4,2)+8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,1)+    
    8*x_(1,2)*x_(2,1)*x_(3,1)*x_(4,2)+5*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,1)+6*x_(1,2)*x_(2,1)*x_(3,2)*x_(4,2)+
    35*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,1)+38*x_(1,2)*x_(2,2)*x_(3,1)*x_(4,2)+23*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,1)+
    30*x_(1,2)*x_(2,2)*x_(3,2)*x_(4,2)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,1)+16*x_(1,3)*x_(2,1)*x_(3,1)*x_(4,2)+
    10*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,1)+12*x_(1,3)*x_(2,1)*x_(3,2)*x_(4,2)+52*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,1)+
    64*x_(1,3)*x_(2,2)*x_(3,1)*x_(4,2)+37*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,1)+54*x_(1,3)*x_(2,2)*x_(3,2)*x_(4,2);

--We follow the same passages done in the example

Rten = spaceReshape((1,2),T)--reshape of the first two factors of T
checkfun(secondSec(Rten)) --verify that \theta_{1,2}(T) belongs to the second secant variety
(A,tconc,cindList) = concisionFirst(1,Rten) --A is the matrix of the first flattening of \theta_{1,2}(T)
tenCONC = reconstructFromMatrixConc(A,tconc,cindList) --concise tensor T' of \theta_{1,2}(T)
Mat = flattening(tenCONC,1)
Mat = Mat_0
Mat = transpose(Mat)
Cc_1 = Mat_{0..(numcols(Mat)//2-1)}^{0,1} ---first face of the pencil
Cc_2 = Mat_{(numcols(Mat)//2)..numcols(Mat)-1}^{0,1} --second face of the pencil
rightIn = transpose(Cc_2)*(inverse(Cc_2*transpose(Cc_2))) --right inverse of C2
D = Cc_1*rightIn --C1*C2^{-}
D = sub(D,QQ);
(e,v) = eigenvectors(D) --we need the eigenvalues of C1*C2^{-1}
for i to 1 do(
    w_i = mingens(ker(D-(realPart(e_i))*id_(RR^2))); --these are the eigenvectors
);--we do not use the eigenvectors given by the M2 builted function because of the numerical inaccuracy
firstindcol = sub(A_{cindList_0},RR)--first indep column of the flattening matrix A
secondindcol = sub(A_{cindList_1},RR)--second indep column of the flattening matrix A
for i to 1 do(
    rmat_i = firstindcol*w_i_(0,0)+secondindcol*w_i_(1,0);--matrix of the eigenvector
    rmat_i = transpose reshape(RR^2,RR^3,rmat_i);--matrix of the eigenvector
    print minors(2,rmat_i);--we do not compute the rank usinf the builted funtion "rank" to avoid problems due to the fact that we are in an inexact field
);
--we see that the first matrix has rank 2 while the second has rank 1, hence T is as in case 6.

--Equivalently one can simply check the result of the following
restart
load "main_algorithm.m2"
moreThanThreeF(T)



