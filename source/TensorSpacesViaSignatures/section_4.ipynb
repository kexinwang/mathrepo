{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "91948125",
   "metadata": {},
   "source": [
    "## 4. Decomposition of the Thrall modules ##\n",
    "Our goal to compute the decomposition of the Thrall module $W_{\\lambda}(V)$ into irreducible $\\operatorname{GL}(V)$-representations. We can do this without specifying $\\dim V$ by computing in the <a href=\"https://doc.sagemath.org/html/en/reference/combinat/sage/combinat/sf/sfa.html\"><em>ring of symmetric functions</em></a>:\n",
    "<ul>\n",
    "    <li>$\\texttt{s=SymmetricFunctions(QQ).schur()}$ defines the ring of symmetric functions, expressed in the Schur basis. </li>\n",
    "    <li>$\\texttt{s.gessel_reutenauer(P)}$ computes the character of the Thrall module associated to a partition $\\texttt{s.gessel_reutenauer(P)}$.</li>\n",
    "</ul>\n",
    "For example, the following code computes the decomposition of the Thrall module $W_{(4,1)}(V)$. The summand $\\texttt{2*s[3, 1, 1]}$ indicates that the Schur module $\\mathbb{S}_{(3,1,1)}(V)$ occurs in this decomposition with multiplicity 2. In other words, the Thrall coefficient $a^{(4,1)}_{(3,1,1)}$ is equal to $2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "8de29c37",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "s[2, 1, 1, 1] + s[2, 2, 1] + 2*s[3, 1, 1] + s[3, 2] + s[4, 1]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s=SymmetricFunctions(QQ).schur()\n",
    "s.gessel_reutenauer([4,1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6a00b78",
   "metadata": {},
   "source": [
    "In section 6 we will use the following function. It computes just a single Thrall coefficient $a^{\\lambda}_{\\mu}$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "31f5fc58",
   "metadata": {},
   "outputs": [],
   "source": [
    "def thrall_coefficient(la,mu):\n",
    "    return s.gessel_reutenauer(la).coefficient(mu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2bf46536",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "thrall_coefficient([4,1],[3,1,1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c5e747e",
   "metadata": {},
   "source": [
    "For a given $k$, the following code decomposes $W_{\\lambda}(V)$, for all $\\lambda \\vdash k$. For $k=3$, we read off that $W_{(3)}(V) \\cong \\mathbb{S}^{(2,1)}(V)$, $W_{(2,1)}(V) \\cong \\mathbb{S}^{(1,1,1)}(V) \\oplus \\mathbb{S}^{(2,1)}(V)$, and $W_{(1,1,1)}(V) \\cong \\mathbb{S}^{(3)}(V)$, consistent with example 4.6."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "fe1b6c6a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[3]\n",
      "s[2, 1]\n",
      "----------------------\n",
      "[2, 1]\n",
      "s[1, 1, 1] + s[2, 1]\n",
      "----------------------\n",
      "[1, 1, 1]\n",
      "s[3]\n",
      "----------------------\n"
     ]
    }
   ],
   "source": [
    "k=3\n",
    "s=SymmetricFunctions(QQ).schur()\n",
    "for P in Partitions(k):\n",
    "    print(P)\n",
    "    print(s.gessel_reutenauer(P))\n",
    "    print('----------------------')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a911e86c",
   "metadata": {},
   "source": [
    "A couple of words on how the algorithm works (see also <a href=\"https://doc.sagemath.org/html/en/reference/combinat/sage/combinat/sf/sfa.html#sage.combinat.sf.sfa.SymmetricFunctionsBases.ParentMethods.gessel_reutenauer\">here</a>): writing $\\lambda=(k^{a_k},\\ldots,1^{a_1})$, we have the formula\n",
    "$$\n",
    "W_{\\lambda}(V) \\cong S^{a_1(\\lambda)}(\\operatorname{Lie}^1 V) \\otimes S^{a_2(\\lambda)}(\\operatorname{Lie}^2 V) \\otimes \\cdots \\otimes S^{a_{k}(\\lambda)}(\\operatorname{Lie}^k V).\n",
    "$$\n",
    "Thus we can compute the character if we know how to compute\n",
    "<ol>\n",
    "    <li>the character of the free Lie algebra $\\operatorname{Lie}^m V$. There is an explicit formula for this, see [Reu93, Theorem 8.3]:\n",
    "        $$\n",
    " \\chi(\\operatorname{Lie}^m(V)) = \\frac{1}{m}\\sum_{t \\vert m}{\\mu(t)p_t^{a/m}},\n",
    "$$\n",
    "    where $p_t$ is the power sum symmetric function.\n",
    "        </li>\n",
    "    <li>the character of a symmetric power $S^a(V)$. This is given by the so-called <a href=\"https://doc.sagemath.org/html/en/reference/combinat/sage/combinat/sf/sfa.html#sage.combinat.sf.sfa.SymmetricFunctionAlgebra_generic_Element.plethysm\"><em>plethysm</em></a> $h_a[\\chi(V)]$, where $h_a$ is the complete homogeneous symmetric function. </li>\n",
    "    <li>the character of a tensor product. This is just the tensor product of the characters: $\\chi(V\\otimes W) = \\chi(V)\\cdot\\chi(W)$.</li>\n",
    "</ol>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60819353",
   "metadata": {},
   "source": [
    "<h2>Bibliography</h2>\n",
    "<ul>\n",
    "<li> [Reu93] C. Reutenauer. <cite>Free Lie algebras</cite>, volume 7 of London Mathematical Society Monographs.\n",
    "New Series. The Clarendon Press, Oxford University Press, New York, 1993. Oxford\n",
    "Science Publications. </li>\n",
    "</ul>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 9.5",
   "language": "sage",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
