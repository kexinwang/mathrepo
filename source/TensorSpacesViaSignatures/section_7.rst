========================================
7. The universal variety and tensor rank
========================================

We compute the hyperdeterminant restricted to the universal variety. This is example 7.7.

.. code-block:: macaulay2

           --Example 7.7
           needsPackage "SparseResultants"
           dis = sparseDiscriminant(genericMultihomogeneousPolynomial((2,2,2),(1,1,1)));
	   dis = sub(dis, QQ[flatten entries vars ring dis])
	   
           S = ring dis
	   Ihdet = ideal dis --ideal of the tangential variety of the Segre P1xP1xP1
	   Z=QQ[z_1..z_8] 
	   AN=QQ[s1,s2,t12,u112,u122,Degrees=>{1,1,2,3,3}]--ring with weights
	   
           DIS=map(S,Z,{6*a_(0,0,0),2*(a_(0,0,1)+a_(0,1,0)+a_(1,0,0)),
    		2*(a_(0,1,1)+a_(1,0,1)+a_(1,1,0)),6*a_(1,1,1),a_(0,0,1)-a_(1,0,0),
    		a_(0,1,1)-a_(1,1,0),1/6*a_(0,0,1)-1/3*a_(0,1,0)+1/6*a_(1,0,0),
    		1/6*a_(0,1,1)-1/3*a_(1,0,1)+1/6*a_(1,1,0)})
	   --this is the map from the weighted proj space P(1,1,2,3,3) to P^7
	   gg=map(AN,Z,{s1^3,s1^2*s2,s1*s2^2,s2^3,s1*t12,s2*t12,u112,u122})
	   --preimage of the intersection of hyperdet and U_2,3 in the weighted proj. space
 	   HypT=gg(preimage(DIS, Ihdet))
	   factor HypT_0