========================================
Exercises
========================================

Macaulay2 general
-----------------

Rewriting polynomials
++++++++++++++++++++++

Let :math:`I = (x^2+y^2,x^2y^2,x^3y-xy^3) \subset \mathbb{Q}[x,y]`.

a. Check that :math:`y^4 \in I`.
#. Write :math:`y^4` as a polynomial combination of the above generators


Minimal polynomials
++++++++++++++++++++++

Set :math:`\alpha = 3^{\frac{1}{5}}` and :math:`\beta = 5^{\frac{1}{3}}`.

a. Compute the minimal polynomial of :math:`\alpha+\beta` over :math:`\mathbb{Q}`.
#. Represent :math:`\frac{1}{\alpha + \beta}` as a polynomial of :math:`\alpha, \beta` over :math:`\mathbb{Q}`.

Geometry
+++++++++++
Let :math:`V` be Veronese surface in :math:`\mathbb{P}^5`. It is the image of :math:`\mathbb{P}^2 \to \mathbb{P}^5` given by :math:`(x : y : z) \mapsto (x^2 : y^2 : z^2 : xy : xz : yz)`.

a. Compute the ideal of :math:`V` in :math:`\mathbb{Q}[x_0,\dotsc,x_5]`.
#. Check that :math:`V` is smooth
#. Does the point :math:`(0 : 0 : 0 : 1 : 1 : 1)` lie on the variety? How about :math:`(1 : 1 : 1 : 1 : 1 : 1)`?
#. Does the line :math:`L = V(x_0 + x_1 + x_2, x_3, x_4, x_5)` meet :math:`V`?


Ring maps
++++++++++

Consider the endomorphism :math:`\phi \colon \mathbb{Q}[x,y,z] \to \mathbb{Q}[x,y,z]` defined by

.. math::
   x \mapsto yz, \quad y \mapsto xz, \quad z \mapsto xy

Show that :math:`\phi` induces an isomorphism on :math:`\mathbb{Q}[x,y,z]/(xyz-1)`

Symmetric polynomials
+++++++++++++++++++++++
Let :math:`R = k[x_1,\dotsc,x_n]`. The elementary symmetric polynomials are

.. math::
   \begin{align*}
      e_1(x) &= \sum_i x_i \\
      e_2(x) &= \sum_{i<j} x_i x_j \\
      &\vdots\\
      e_n(x) &= x_1\dotsb x_n
   \end{align*}

a. Given :math:`n`, generates all elementary symmetric polynomials. Try to write in a functional style avoid loops. The command ``subsets`` may help.
#. Define the ring map :math:`\phi \colon k[e_1,\dotsc,e_n] \to k[x_1,\dotsc,x_n]` that sends the symbol :math:`e_i` to the :math:`i` th elementary symmetric polynomial.
#. The image of :math:`\phi` is precisely the set of symmetric polynomials. Set :math:`n=4` and write the following polynomial in terms of elementary symmetric polynomials: 
   ``-x_1^2*x_2^2*x_3-x_1^2*x_2*x_3^2-x_1*x_2^2*x_3^2-x_1^2*x_2^2*x_4-3*x_1^2*x_2*x_3*x_4-3*x_1*x_2^2*x_3*x_4-x_1^2*x_3^2*x_4-3*x_1*x_2*x_3^2*x_4-x_2^2*x_3^2*x_4-x_1^2*x_2*x_4^2-x_1*x_2^2*x_4^2-x_1^2*x_3*x_4^2-3*x_1*x_2*x_3*x_4^2-x_2^2*x_3*x_4^2-x_1*x_3^2*x_4^2-x_2*x_3^2*x_4^2+x_1*x_2*x_3*x_4+x_1^2+2*x_1*x_2+x_2^2+2*x_1*x_3+2*x_2*x_3+x_3^2+2*x_1*x_4+2*x_2*x_4+2*x_3*x_4+x_4^2``


Regular sequences
+++++++++++++++++

Let :math:`M` be an :math:`R`-module. A sequence :math:`f_1,\dotsc,f_r \in R` is a **regular sequence** if :math:`M \neq \langle f_1,\dotsc,f_r \rangle M`, and :math:`f_i` is a nonzerodivisor on :math:`M/\langle f_1,\dotsc,f_{i-1} \rangle M` for all :math:`i`.

a. Program a routine that, checks whether a given sequence of polynomials :math:`f_1,\dotsc,f_r` is a regular sequence on :math:`R`.
#. A permutation of a regular sequence may not be a regular sequence. Write a functions that finds all permutations of :math:`f_1,\dotsc,f_r` that are regular sequences.
#. Run the above function for :math:`R = \mathbb{Q}[x,y,z]`, :math:`f_1 = (x-1)z`, :math:`f_2 = (x-1)y`, :math:`f_3 = x`.

Decomposing
++++++++++++
Let :math:`S, C \subseteq \mathbb{C}[x,y,z]` be ideals corresponding to a surface and a curve.
We define ``S = ideal(x^5+y^5+z^5-1)``, and

.. code-block:: macaulay2

   C = ideal(x*y^5*z-2*x^2*y^3*z^2+x^3*y*z^3-2*y^4+4*x*y^2*z-2*x^2*z^2,y^7-3*x^2*y^3*z^2+2*x^3*y*z^3+x^2*y^4-2*x^3*y^2*z+x^4*z^2-4*y^4+8*x*y^2*z-4*x^2*z^2,x^2*y^5-x^3*y^3*z+x^4*y^2-x^5*z-y^5*z+2*x*y^3*z^2-x^2*y*z^3+2*y^5-x^2*y^2*z-2*x*y^3*z+x^3*z^2+2*x^2*y^2-2*x^3*z-2*y^2*z+2*x*z^2,y^5*z^3-2*x*y^3*z^4+x^2*y*z^5-x*y^4*z^2-2*y^5*z^2+2*x^2*y^2*z^3+2*x*y^3*z^3-x^3*z^4-2*y^6+2*x*y^4*z-2*x^2*y^2*z^2+2*x^3*z^3-2*x^2*y^3+2*x^3*y*z+2*y^2*z^3-2*x*z^4+2*y^3*z-2*x*y*z^2,x^3*y^2*z^3-2*x^2*y^3*z^3-x^4*z^4+2*x^3*y*z^4-2*x*y^6+x^3*y^3*z+2*x^2*y^4*z-x^4*y*z^2-2*x^3*y^2*z^2+2*x^4*z^3-2*x^3*y^3+2*x^4*y*z-x*y^3*z^2+2*y^4*z^2+x^2*y*z^3-2*x*y^2*z^3-4*y^4*z+8*x*y^2*z^2-4*x^2*z^3-2*x^2*y^2+2*x^3*z+2*y^2*z-2*x*z^2,y^6*z^2-2*x*y^4*z^3+x^2*y^2*z^4-2*y^6*z+2*x*y^4*z^2-2*x*y^5+2*x^3*y*z^2-2*x^3*y^2+2*x^4*z+2*y^3*z^2-2*x*y*z^3+2*x*y^2*z-2*x^2*z^2,x^2*y^4*z^2-2*x^2*y^3*z^3-x^4*z^4+2*x^3*y*z^4-2*x*y^6+2*x^3*y^3*z+2*x^2*y^4*z-2*x^4*y*z^2-2*x^3*y^2*z^2+2*x^4*z^3-2*x^3*y^3+2*x^4*y*z-2*x*y^3*z^2+2*y^4*z^2+2*x^2*y*z^3-2*x*y^2*z^3-2*x*y^3*z-4*y^4*z+2*x^2*y*z^2+8*x*y^2*z^2-4*x^2*z^3-4*x^2*y^2+4*x^3*z+4*y^2*z-4*x*z^2,2*x^4*y^2*z^2-x^5*z^3+x^4*y^3+x^5*y*z-x*y^4*z^2+x^2*y^2*z^3+x^6-2*y^6-2*x^2*y^3*z+6*x*y^4*z-2*x^2*y^2*z^2+2*x^2*y^3-2*x^4*z+2*x^3*y*z+y^3*z^2-x*y*z^3+2*x^4-2*y^3*z+x^2*z^2-2*x*y*z^2-4*x^2*z+2*z^2,x^3*y^4*z+x^4*y^3+x^5*y*z-x*y^4*z^2+x^2*y^2*z^3+x^6-2*x^2*y^3*z+2*x*y^4*z+2*x^2*y^3-2*x^4*z+2*x^3*y*z+y^3*z^2-x*y*z^3+2*x^4-2*y^3*z+x^2*z^2-2*x*y*z^2-4*x^2*z+2*z^2,2*x^4*z^5+6*x^4*y*z^3+24*x^2*y^3*z^3-16*x^3*y*z^4+2*x^3*y^4+8*x*y^6+8*x^2*y^4*z+4*x^5*z^2+16*x^3*y^2*z^2+x*y^4*z^2+2*y^5*z^2-8*x^4*z^3-2*x^2*y^2*z^3-4*x*y^3*z^3+x^3*z^4-2*x^2*y*z^4+2*x^5*y+16*x^3*y^3+2*y^6+8*x^4*y*z-6*x*y^4*z-4*y^5*z+2*x^2*y^2*z^2+12*x*y^3*z^2-16*y^4*z^2-6*x^3*z^3-8*x^2*y*z^3+8*x^5+2*x^2*y^3+8*y^5-6*x^3*y*z+4*x^2*y^2*z-24*x*y^3*z+32*y^4*z-4*x^3*z^2-16*x^2*y*z^2-64*x*y^2*z^2+32*x^2*z^3+2*y^2*z^3+8*x^2*y^2-24*x^3*z-2*y^3*z+4*x*y*z^2-4*y^2*z^2+4*x*z^3+8*y*z^3-8*y^2*z+16*x*z^2,x*y^4*z^4-x^2*y^2*z^5-2*x*y^4*z^3-x^3*y^2*z^2-6*x^2*y^3*z^2+x^4*z^3-y^3*z^4+x*y*z^5-2*x^2*y^4-2*x^3*y^2*z-2*x^4*z^2+x*y^2*z^3+2*y^3*z^3-x^2*z^4+2*x*y*z^4-2*x^4*y+2*y^4*z+2*x*y^2*z^2+4*x^2*z^3-4*y^4+4*x^2*y*z+8*x*y^2*z-4*x^2*z^2-2*z^4-2*y*z^2,x^2*y^3*z^4-x^3*y*z^5-4*x^2*y^3*z^3+2*x^3*y*z^4-x^3*y^3*z-4*x^2*y^4*z+x^4*y*z^2-2*x^3*y^2*z^2-y^4*z^3+x*y^2*z^4-2*x^3*y^3-4*x^4*y*z+x*y^3*z^2+4*y^4*z^2-x^2*y*z^3-2*x*y^2*z^3+2*x^2*z^4-2*x^5+4*x*y^3*z-4*y^4*z+4*x^2*y*z^2+8*x*y^2*z^2-4*x^2*z^3+2*x^2*y^2+2*x^3*z-2*y*z^3-2*y^2*z,2*x^4*y*z^4+3*x^5*z^3-x^4*y^3+x^5*y*z+3*x*y^4*z^2-7*x^2*y^2*z^3-x^6+6*y^6+2*x^2*y^3*z-14*x*y^4*z-4*x^3*y*z^2+6*x^2*y^2*z^2-4*x^3*z^3+2*x^2*y^3+2*x^4*z-14*x^3*y*z-y^3*z^2+3*x*y*z^3-6*x^4-2*y^3*z-x^2*z^2+14*x*y*z^2+12*x^2*z-6*z^2)

a. How many irreducible components do :math:`S,C` have?
#. What does the intersection of the surface and the curve look like? How many distinct points are there on the intersection? What are their multiplicities?
#. Let :math:`I = C+S`, but now in the ring :math:`\mathbb{Z}/11\mathbb{Z}`. How many points (over the algebraic closure) does this ideal correspond to?
#. How many of these points are in :math:`\mathbb{Z}/11\mathbb{Z}`? Find all of them.

Working with files
+++++++++++++++++++

a. Let :math:`R = QQ[x,y,z]` with a lexicographic monomial order, where :math:`x > y > z`. Let :math:`I` be the ideal

   .. math::
      I = (x^3 + y^3 + z^2 - 1, x^7 + y^5 + z^3 - 1)

   Compute a Grobner basis of :math:`I`. (If it's too slow, change the first :math:`x^3` to :math:`x^2`)
#. Save this into a file. You can use for example ``f = "gb.txt" << toExternalString gens gb I;``.
#. We now close the file handle: ``close f``.
#. You can now admire the file ``gb.txt``, which contains a long Grobner basis.
#. You can read and evaluate the output by using ``value get "gb.txt``.
#. How would you avoid future recomputations of the Grobner basis? (*hint*: ``viewHelp forceGB``).
#. Consider the same ideal above, but we vary the exponents on :math:`x`. In other words, let

   .. math::
      I_{i,j} = (x^i + y^3 + z^2 - 1, x^j + y^5 + z^3 - 1)
   
   Create a function that takes a pair :math:`(i,j)`, and writes the number :math:`k` of total terms in the Lex Grobner basis of :math:`I_{i,j}` into a file.
   Each result should be on a single line, with the format ``(i,j) has k terms``, where ``i,j,k`` are replaced by actual values.
   Run the function for successively larger ``i,j`` until it starts taking too long.
   See for example ``viewHelp openOutAppend`` and ``viewHelp "<<"`` for inspiration.



Exact eigenvalues and vectors
++++++++++++++++++++++++++++++++++++
A matrix with entries in :math:`\mathbb{Q}` has eigenvalues and vectors
in a finite field extension of :math:`\mathbb{Q}`, namely :math:`\mathbb{Q}[i]/(i^2+1)`. Write a function that does the following

a. Constructs this field.
#. Computes the characteristic polynomial :math:`p(\lambda) = \det(M - \lambda I)` and factors it.
#. Outputs the eigenvalues and eigenvectors.

For example, the matrix :math:`\begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}` has eigenvalues :math:`\pm i`, and vectors :math:`[\pm 1, i]^T`.


Solving by elimination
++++++++++++++++++++++++++++++
In this exercise we will solve zero-dimensional polynomial systems.
Let :math:`I \subseteq R := k[x_1,\dotsc,x_n]` be a zero-dimensional ideal.

a. Suppose :math:`h` is a linear polynomial in :math:`R`. We get a map :math:`\phi : k[y] \to R/I`. Let :math:`J = (g(y)) = \ker \phi`.
   The polynomial :math:`g(y)` is called the *eliminant* of :math:`h`. Write the function ``eliminant(h)``.
   If you want, you can add an optional parameter ``Ring => ...``.
#. Choose :math:`h = x_1`, and define a lexicographic monomial order :math:`x_1 < x_2 < \dotsb < x_n`. The Shape lemma says that
   the Grobner basis has the form :math:`g(x_1), x_2 - g(x_1), \dotsc, x_n - g_n(x_1)`. Using this information, find all
   **exact** solutions to the system :math:`I = (x+z+2,z^2+1,y^2-4z-4)`.
#. (bonus) make the function ``eliminant(h)`` faster by using linear algebra instead of a naive kernel computation.
#. Why is this method not numerically stable? Consider for example :math:`I = (x(x-10000), y-x^{1000}, z-yx)`

Solving by eigenvalues
++++++++++++++++++++++++
This is a continuation of the previous exercise.
As before, let :math:`I \subseteq R := k[x_1,\dotsc,x_n]` be a zero-dimensional ideal.

a. The ring :math:`R/I` is a finite-dimensional vector space. The :math:`R`-module action is a linear mapping on this space.
   If :math:`R = \mathbb{Q}[x,y,z]` and :math:`I = (x+z+2,z^2+1,y^2-4z-4)`, compute the three matrices :math:`M_x, M_y, M_z` corresponding
   to multiplication by :math:`x,y,z`.
#. For any polynomial :math:`h \in R`, we have a corresponding multiplication matrix. Write a function that computes
   this matrix, given the input :math:`h`.
#. Choose your favorite polynomial :math:`h \in R`. Compute the eigenvalues and eigenvectors of :math:`M_h`. How many are there?
#. Choose another polynomial :math:`h \in R`. What happened to the eigenvectors?
#. Choose your favorite eigenvector. Let :math:`h_1 = x`, :math:`h_2 = y`, :math:`h_3 = z`. What are the three eigenvalues?
#. Is this numerically better than the method in the previous exercise? 
#. Using this method, implement a function that find all roots (approximately/exactly) for any zero-dimensional ideal :math:`I`.

Fano varieties
+++++++++++++++
Consider the ideal :math:`I = (x^3+yz^2-w^2z+w^3) \in \mathbb{C}[x,y,z,w]` and its corresponding
variety :math:`V(I) \in \mathbb{P}^3`.

a. What are the dimension and degree of :math:`I`?
#. Without computing, can you guess how many lines lie on :math:`V(I)`?
#.  We will compute all lines on :math:`V(I)`.

    i. We can represent a line in :math:`\mathbb{P}^3` in parametric form as

        .. math::
          \begin{bmatrix} s & t \end{bmatrix} \begin{bmatrix} p_0 & p_1 & p_2 & p_3 \\ q_0 & q_1 & q_2 & q_3 \end{bmatrix}

        Construct a ring :math:`S` with the 10 parameters above
    #. If we view :math:`p_i, q_i` as fixed, the above describes a map from :math:`\mathbb{P}^1 \to \mathbb{P}^3`.
       This is a parametrization of a line.
       Construct the corresponding ring map :math:`\phi` from :math:`R` to :math:`S`.
    #. Compute the conditions on the :math:`p_i, q_i` so that the parametrized line lies on :math:`V(I)`.
       This will be an ideal :math:`J \in S' := \mathbb{Q}[p_0,\dotsc,p_3,q_0,\dotsc,q_3]`.
    #. We can also describe lines as points on a Grassmannian.
       Construct a ring :math:`T` with variables :math:`p_{0,1}, p_{0,2}, p_{0,3}, p_{1,2}, p_{1,3}, p_{2,3}`.
    #. Construct the map from :math:`T` to :math:`S'/J` that sends :math:`p_{i,j}` to the :math:`2 \times 2` minor of the matrix
       
       .. math::
        \begin{bmatrix} p_0 & p_1 & p_2 & p_3 \\ q_0 & q_1 & q_2 & q_3 \end{bmatrix}
       
       using columns :math:`i,j`. 
    #. The kernel of this map is an ideal. What does it represent?  
#. Counting with multiplicity, how many lines lie on :math:`V(I)`?
#. Counting without muliplicities, how many lines are there?
#. How many of these lines are real?
#. Write down equations for one of these lines.


Plucker relations
++++++++++++++++++

a. Compute all polynomial relations between :math:`2 \times 2` minors of a generic :math:`2 \times 4` matrix.
   Avoid manual labor, this should be done purely programmatically. *hint*: Create a map from a polynomial ring :math:`\mathbb{Q}[p_{0,1}, p_{0,2}, p_{0,3}, p_{0,3}, p_{1,2}, p_{1,3}, p_{2,3}]` to a ring with 8 variables. You want the kernel of this map.
#. Make the matrices bigger, i.e. find relations between :math:`n \times n` minors of :math:`n \times m` matrices, where :math:`n \leq m`.
   How far can you go before the computations take too long?
#. The same output can be obtained using the command ``Grassmannian(n,m)``.
   Take a pair :math:`(n,m)` which didn't terminate in the previous steps, and try it git ``Grassmannian``.
   Why was this so much faster?


Lie Algebra Structures
++++++++++++++++++++++++++

Let :math:`e_1, e_2, e_3` be a basis of :math:`\mathbb{C}^3`. A Lie algebra struture on :math:`\mathbb{C}^3` is given by a bracket

.. math::
   [e_i, e_j] = a_{ij1}e_1 + a_{ij2}e_2 + a_{ij3} e_3

with the constraints

.. math:: [e_i, e_j] = -[e_j, e_i]

.. math:: [e_i, [e_j, e_k]] + [e_j, [e_k, e_i]] + [e_k, [e_i,e_j]] = 0

a. Find all polynomial relations among the unknowns :math:`a_{ijk}`. Avoid manual labor, write a program that will generate them for you. *Hint:* look at :math:`a_{iik}` first, then compare :math:`a_{ijk}` to :math:`a_{jik}`.
#. What is the dimension of this variety of Lie algebras?
#. How many irreducible components does this have, and what are they?
#. What about Lie algebras on :math:`\mathbb{C}^4`? See also https://arxiv.org/abs/2208.14631


Coins, math, and computer science
+++++++++++++++++++++++++++++++++

We will be studying two approaches to solve an integer programming problem.
The US dollar contains four common coins: pennies, nickels, dimes, and quarters. Their values in cents are 1,5,10, and 25 respectively.

I. How many distinct ways are there to make :math:`n` dollars using :math:`m` coins?

   a. We start with the mathematical approach. Create a polynomial ring with variables :math:`p,n,d,q`, with the degree 
      ``{1,1}`` for :math:`p`, ``{1,5}`` for :math:`n`, ``{1,10}`` for :math:`d`, and ``{1,25}`` for :math:`q`. (see ``viewHelp Degrees``).
   #. Take the monomial :math:`pdq^3`. What is its degree? What do the numbers mean?
   #. How many ways are there to make 10 dollars using 100 coins? (*hint* ``viewHelp basis``).
   #. How many unique values can be made using 5 coins?
   #. How many ways are there to make 2.5 dollars using any number of coins? (*hint* there is a one-line solution if you change your ring)
   #. A computer scientist would approach the above question recursively. Let's order the denomination as follows: ``p,n,d,q``.
      Let :math:`a_{i,j}` be the number of ways to create :math:`j` dollars using the first :math:`i` denominations. E.g. :math:`a_{2,7}` is the number of ways
      to create :math:`7` dollars using pennies and nickels (the answer is 2: seven pennies, or one nickel + 2 pennies).
      Define the base cases :math:`a_{0,j}` and :math:`a_{i,0}`, and find a recursion. Program this in Macaulay2.
   #. Which approach is faster? (``viewHelp elapsedTime``).
   


II. What is the smallest number of coins needed to make :math:`n` dollars?

   a. Adapt the previous, degree-based method to solve the question: what is the smallest number of coins needed to make 2.47 dollars?
   #. We will write another CS style solver. Let :math:`b_n` be smallest number of coins needed to make :math:`n` dollars. Set up initial conditions, and a recursion.
   #. Here is another approach: Set up a polynomial ring with variables :math:`x,y` and :math:`p,d,n,q`. Define a monomial order that refines the order which first looks at the :math:`x,y` degrees,
      and breaks ties using the :math:`p,d,n,q` degrees (see ``viewHelp Weights``). Define the ideal :math:`I = (xy - p, xy^5 - d, xy^{10} - n, xy^{25} - q)`. What does this ideal represent?
   #. We want to find the smallest :math:`k` such that :math:`x^k y^{247}` can be written in normal form without :math:`x` or :math:`y` variables. What does this normal form represent?
   #. Which method is faster?



Algebraic optimization, ED-degree
++++++++++++++++++++++++++++++++++

a. The *twisted cubic* :math:`C` is the image of the map :math:`\phi \colon x \mapsto (x,x^2,x^3)`. Find the ideal :math:`I_C` corresponding to the image.
#. What is its dimension and degree? Is it a complete intersection?
#. The *critical ideal* of a variety :math:`C` is

   .. math::
      \left( I_C + \left\langle c+1 \times c+1 \text{ minors of } \begin{pmatrix} u - x \\ J(I_C) \end{pmatrix} \right \rangle \right) \colon I_\text{sing}^\infty
   
   where :math:`I_{\text{sing}}` is the ideal corresponding to the singular locus, :math:`c` is the codimension of :math:`C`, and :math:`J(I_C)` is the Jacobian of the generators of :math:`I`.
   The singular locus is given by

   .. math::
      I_\text{sing} = I_C + \langle c \times c \text{ minors of } J(I_C) \rangle.

#. Let :math:`u = (-7,4,1) \in \mathbb{C}^3` be a point. The distance between :math:`C` and :math:`u` is the minimizer of the
   Euclidean distance :math:`(x_1 + 7)^2 + (x_2 - 4)^2 + (x_3 - 1)^2`, where :math:`x \in C`. The set of critical points is the set of :math:`x \in C`
   such that :math:`u-x \perp T_x(C)`, where :math:`T_x(C)` is the tangent space of :math:`C` at :math:`x`. This is the set of points corresponding to the critical ideal.
   Compute this ideal in this case. What is the closest point on :math:`C` to :math:`u = (-7,4,1)`?
#. What is the dimension and degree? What happens to these when you change :math:`u`? This degree is called the *Euclidean distance degree*.
#. Consider the *cardioid* in :math:`\mathbb{R}^2`. It is the variety :math:`X` corresponding to the equation :math:`(x^2 + y^2 + x)^2 - x^2 - y^2`. What is its singular locus?
#. Choose your favorite point :math:`u \in \mathbb{R}^2`, and compute the critical ideal. What is its degree?
#. Compute the set of points :math:`u \in \mathbb{R}^2` for which the degree of the critical ideal is not the expected one.


Algebraic optimization: Lagrange multipliers
+++++++++++++++++++++++++++++++++++++++++++++
Suppose :math:`V \in \mathbb{Q}^n` is a variety given by :math:`I = \langle g_1,\dotsc,g_k \rangle \subseteq \mathbb{Q}[x_1,\dotsc,x_n]`.
If :math:`f` is a polynomial, we want the local extrema of :math:`f|_V \colon V \to Q`.

a. If :math:`V` is smooth, we can use Lagrange multipliers. Write a function that tests whether a variety is smooth.
#. The *Lagrange polynomial* is

   .. math::
      F(x,\lambda) := f(x) + \sum_{i=1}^k \lambda_i g_i(x)

   Write a function that that computes this polynomial.
#. If :math:`p \in V` is a local extremum of :math:`f |_V`, then there exists :math:`\lambda^*` such that the system :math:`\nabla_x F(x,\lambda) = \nabla_\lambda F(x,\lambda) = 0`.
   Write a function that creates and solves the system (the command ``diff`` might help).
#. Compute the critical points of :math:`f(x,y) = x^2 + y^2` under the condition :math:`g(x,y) = y-2x+1 = 0`.
#. Check that the variety :math:`V = V(x^2 + y^2 + z^2 - 16, -x - 2y + z)` is smooth, and compute the critical points of :math:`f(x,y,z) = 2x_4y-5z` on the variety.

Ideals of Points and Hilbert functions
++++++++++++++++++++++++++++++++++++++
a. Write a Macaulay2 script (without using the command ``hilbertFunction``) which takes a set of points in :math:`\mathbb{P}^2` and a degreee :math:`i`, and computes the Hilbert function. 
Hint: determine the rank of the matrix obtained by evaluating the monomials of degree :math:`i` at the points. To get the monomials of degree
:math:`i` in the ring ``R`` use the command ``basis(i,R)``. To evaluate at a point :math:`(b_0:b_1:b_2)`
you might find the command ``map(R,R,{b0,b1,b2})`` useful.

Modules, syzygies, free resolutions
++++++++++++++++++++++++++++++++++++
a. Let :math:`R = \mathbb{Q}[x,y,z]`. Find the set of all :math:`(f_1,f_2,f_3,f_4) \in R^3` such that

   .. math::
      x^2f_1 + xy f_2 + xz f_3 + y^2 f_4 = 0
   
   *hint*: you should get a module
#. Let :math:`a_1,\dotsc,a_r` be the generators of the above module. Find the set of :math:`(f_1,\dotsc,f_r) \in R^r` such that

   .. math::
      f_1 a_1 + f_2 a_2 + \dotsb + f_r a_r = 0
   
   Note that this set is again a module.

#. Let :math:`b_1, \dotsc, b_s` be the generators of the above module. Find the set :math:`(f_1,\dotsc,f_s) \in R^r` such that

   .. math::
      f_1 b_1 + f_2 b_2 + \dotsb + f_r b_s = 0
   
#. By repeatedly solving, we have obtained a **free resolution**. Try the command ``res`` and see if you can replicate what we did here.
   The command ``res`` returns and instance of ``ChainComplex``. The sequence of maps can be accessed by using the key ``dd``, e.g. ``complex = res coker M; complex.dd``.
#. Compute a free resolution of the module ``coker matrix {{x^3, x*y, y^5}}``.
#. What does the command ``betti`` return when the input is the above resolution? Can you decipher the meaning of the numbers?
#. Check that the command ``res`` in fact computes an exact complex by computting the homology of this complex e.d. ``HH(complex)``.
#. Create a complex that has nontrivial homology. Check your result using ``HH``.


Primary decomposition of modules
++++++++++++++++++++++++++++++++++

Suppose :math:`R` is a polynomial ring, and fix

.. math::
   A = \begin{pmatrix} x^2 & y^2 & x z-y z+2 \\ x+y-z & x^2 y & y z \end{pmatrix}

Let :math:`M` be the :math:`R`-submodule of :math:`R^2` generated by the columns of the above matrix.

a. Compute a primary decomposition of :math:`M`, i.e. write :math:`M = M_1 \cap \dotsb \cap M_k`, where each :math:`M_i` is a primary :math:`R`-submodule of :math:`R^2`.
   **note** when we say :math:`M_i` is a primary submodule of :math:`R^2`, we mean :math:`R^2/M_i` is primary.
   *hint:* Macaulay2 cannot directly compute primary decompositions of submodules, but one can start with a primary decomposition of the :math:`R`-module :math:`R^2/M`.
#. What are the associated primes?
#. Check that the following varieties are equal
   
   1. The union of the varieties corresponding to associated primes
   #. The set of points where the rank of :math:`A` drops.
   #. The annihilator of the comodule ``R^2/M``.


Truth/liar riddle 1
++++++++++++++++++++

There are three people: A, B, and C. They say the following

A. B is lying
B. C is not lying
C. either A or B is not lying

Who is lying and who is telling the truth? Solve the riddle using Grobner bases. *Hint* Create a polynomial ring with 3 variables over the field :math:`\mathbb{Z}/2\mathbb{Z}`. Write the above three statements as three equations.

Truth/liar riddle 2
++++++++++++++++++++

There are now four people: A, B, C, and D. They say the following

A. B is telling the truth
B. C or D are telling the truth
C. D is lying
D. There is only one liar

1. How would one formulate a statement about the number of liars?
#. Program a :math:`4 \times 5` table, where each row corresponds to an elementary symmetric polynomial :math:`e_i`, each column is a number :math:`0 \leq j \leq 4`,
   and the entry :math:`(i,j)` is the value of :math:`e_i` when there are :math:`j` liars. You can use ``netList`` to make your table look nice in the terminal.
#. Make a function taking a number :math:`n` as an input, and returning the :math:`n \times (n+1)` table as above.
#. Formulate the four statements as a polynomial system, and solve it.

Truth/liar riddle 3
++++++++++++++++++++

Suppose there are :math:`n` people, with the following statements:

- 1 says 2 is lying
- 2 says 3 is lying
- 3 says 4 is lying
- etc..
- n-1 says n is lying
- n says: I am not lying

Solve the riddle for several values of ``n``. How far can you go? Is there a pattern?

Try the following

- 1 says that n is lying
- 2 says that 1 is lying
- 3 says that 1 and 2 are lying
- 4 says that 1, 2, and 3 are lying
- etc...
- n-1 says that 1,2,3, ..., n-2 are lying

Solve it for several ``n``. How far can you go? What happens to the degree of the Grobner basis?


Graph colorings
++++++++++++++++

We describe a graph on :math:`n` nodes by a list of pairs :math:`\{i,j\}`, which indicate an edge between :math:`i` and :math:`j`. We will assign three colors, -1, 1, and 1, to each node so that no neighboring nodes have the same color.
Thus a coloring is a point :math:`(x_1,\dotsc,x_n) \in \mathbb{F}_3^n`, such that :math:`x_i \neq x_j` when :math:`\{i,j\}` is an edge.

a. Define a polynomial ring :math:`R = \mathbb{F}_3[x_1,\dotsc,x_n]` over the finite field with 3 elements. Show that the set of all possible color assigments are described by the ideal :math:`I = (x_1^3 - x_1, \dotsc, x_n^3 - x_n)`.
#. Prove that :math:`i,j` have different colors if and only if :math:`x_i^2 + x_i x_j + x_j^2 - 1 = 0`.
#. Write a Macaulay2 function that takes a list of edges, and returns the ideal corresponding to graph colorings.
#. We get a symmetry by permuting the colors. How would you mitigate that?
#. Compute all colorings (up to color permutations) of the graph ``{{1,2},{1,4},{1,5},{2,3},{2,4},{2,5},{2,6},{3,4},{3,5},{4,6},{4,7},{5,6},{5,7}}``.
   Draw one of the colorings.
#. Take the regular 7-gon, add a vertex in the middle, and connect the middle vertex to all other vertices.
   You should have 8 vertices and 14 edges. Show that this graph is not 3-colorable.
#. *Bonus* turn the function into a methods, and allow instances of ``Graph`` as input (you will ``needsPackage "Graphs"``).





.. Ideas
.. ++++++++++++

.. a. Compute the algebraic boundary of the image of the polynomial map ...
.. #. Compute a secant/tangent/bitangent variety



Types and packages
--------------------

Multiset
++++++++++++
A *multiset* is a set, where each element can occur more than once

a. Implement the type ``Multiset`` (*hint* inherit from ``HashTable``). One should be able to instantiate a multiset from a list, e.g. ``multiset {1,2,2,3}`` should return a new instance of ``Multiset``.
#. Make it print nicely (implement at least ``expression Multiset``).
#. Implement the comparison operators. For example ``A <= B`` if every element of ``A`` is in ``B``.
#. Implement ``#Multiset``, which returns the number of elements in the multiset.
#. Implement ``unique Multiset``, which return a ``Set``.
#. Implement any number of other functions and method you deem necessary. For inspiration, check ``viewHelp Set``.
#. Create a package called ``Multiset`` containing all previously made methods and functions. Write a minimal documentation and a couple of tests. Make sure the package can be installed (``installPackage``), documentation works (``viewHelp``), and tests pass (``check``).
#. Ideally, you can create a repository on GitHub, and collaborate and divide tasks among teammates. Make sensible commits with descriptive messages.

Hypergraphs
++++++++++++
A *hypergraph* is a pair :math:`G = (V, E)`, where :math:`V` is a finite set, and :math:`E` is a set of disjoint subsets of :math:`V`.

.. image:: figs/Hypergraph-wikipedia.svg

1. Define a type ``Hypergraph``, along with useful basic functionality (e.g. ``expression Hypergraph``, ``AfterPrint``...).
#. If we assign to every :math:`v_i \in V` a variable :math:`x_i`, we can turn a hypergraph into a monomial ideal by setting

   .. math::
      I_G = \left( \prod_{v_i \in e} x_i \middle| e \in E \right)
   
   This is called the **edge ideal**. Write the Macaulay2 method ``edgeIdeal(Hypergraph)``.
#. We say :math:`I = J + K` is a *Betti splitting* if for all :math:`i,j \geq 0`,

   .. math::
      \beta_{i,j}(I) = \beta_{i,j}(J) + \beta_{i,j}(K) + \beta_{i-1,j}(J \cap K)
   
   where :math:`\beta_{i,j}` is the :math:`{i,j}` Betti number. Check that

   .. math::
      J = (x_1x_2, x_5x_1) \qquad \text{ and } K = (x_2x_3, x_3x_4, x_4x_5)
   
   is a Betti splitting.

#. Write the methods ``isBettiSplitting(Ideal,Ideal)``, ``isBettiSplitting(Hypergraph, Hypergraph)``. They should return either ``true`` or ``false``.
#. We say :math:`e \in E` is a *splitting edge* if :math:`I_G = I_{G \setminus \{e\}} + I_{\{e\}}` is a Betti splitting.
   Write a method ``splittingEdges(Hypergraph)``, which returns a list of all splitting edges.
#. What are the splitting edges of the hypergraph in the image above?



Git and Github
--------------

Although everything can be done in a terminal, it is recommended to use a GUI for some of the more complex operations (e.g. 3-way merging).
VScode has a nice built-in nice interface, which can be extended with the GitLens extension. It is also recommended to enable the "Git merge editor" setting in VScode settings.
Another nice git interface is Sublime Merge.


Merge conflicts
++++++++++++++++

By following the instructions below, we will be triggering several issues.

1. Open a terminal, create a directory ``mkdir git_test``, enter the directory ``cd git_test``
#. Initialize a git repository ``git init``
#. Create a file ``myFile``, write a couple of lines, save
#. Add and commit the newly created file ``git add myFile``, ``git commit -m "Created my file!"``.
#. Create and checkout a new branch ``git checkout -b newBranch``
#. Change the every line of the file, you can also add lines if you want. Save, add, commit
#. Checkout the main branch ``git checkout main``
#. Note the changes and additions are gone. Well, not really, they're just on the other branch.
#. Now change the first line of the file.
#. Merge ``newBranch`` into ``main``: ``git merge newBranch``.

You will now have a merge conflict. Read the message, and attempt to fix it. *hint* look at the state of ``myFile`` now. Where is the issue? Why couldn't git figure it out on its own? Did it manage to figure something out?

Fix the merge conflict as you see fit, then ``git add myFile`` and finally ``git commit`` (or ``git merge --continue``).

Pull requests, working with forks
++++++++++++++++++++++++++++++++++

This exercise needs to be done in a team.

a. Have one team member create a GitHub repository, and add some content. We will call this the upstream repository.
#. Other members should now fork the repository, and make a local clone.
#. Everyone should now make additions and modifications on their own side.
#. In order to include your changes in other peoples' forks, you need to do a pull request on GitHub. One team member should now create a pull request from their fork to upstream.
#. If there are no conflicts, GitHub will be able to merge. If there are conflicts, resolve them and merge.
#. Changes are now reflected in GitHub, in the upstream repository. The owner of upstream can use ``git pull`` to update their local version.
#. Changes in upstream are not reflected in any of the forks. Each team member should add a remote called "upstream": ``git remote add upstream <url for the upstream repo>``.
#. Run ``git fetch upstream``. What does this do?
#. We can now incorporate changes from upstream to our branch. There are a couple of options.

   1. ``git merge upstream/main``: will (usually) create a merge commit. If there are conflicts, you will be asked to resolve them.
   #. ``git merge --ff-only upstream/main``: will create a fast-forward merge. This will not work if there are merge conflicts. Use this if your fork has no additions compared to upstream.
   #. ``git rebase upstream/main``: you can also try this one out. Note that this rewrites history, so only use it if you know what you are doing. This is used in many projects to keep history linear.


Pulling, pushing, stashing
++++++++++++++++++++++++++++

Now we will have several people working on the same repository

a. Have one person add other people as collaborators in their GitHub repository. In Github, go to Settings -> Collaborators.
#. Have all collaborators clone the repository.
#. Now everyone has the same version. Have one person make a change, commit, and push.
#. Everyone else is now behind. Everyone can now get up to date using ``git pull``.
#. Have two people make conflicting changes, which we will refer to as A and B. Commit both A and B, but push A first. When B tries to push, you will be asked to pull first.
#. Since the changes are conflicting, pulling will fail. There are a few options here

   1. ``git pull --merge`` will create a merge commit. This makes commits A and B happen in parallel, with an additional merge commit to fix the conflict.
   #. ``git pull --rebase`` will rebase B on A. This makes commit B happen after commit A. Since there is a conflict, we will have to amend commit B.

#. Have a third person make a change C, but don't commit.  ``git pull`` will now fail, because there are uncommited changes. ``git stash`` will save all uncommited changes to a "stash", and return the repository to the state of the latest commit.
#. Once the changes are stashed, we can ``git pull`` without any issues. ``git stash pop`` will now apply the previously statshed uncommited changes ontop of the latest commit. If there are conflicts, you will have to resolve them.



Discarding work
++++++++++++++++
What are the differences between ``git reset``, ``git reset --hard``, ``git reset --soft``? What about ``git revert``? Try it out

a. Checkout a new branch called ``discarding``, and make a bunch of commits (3 or 4 should suffice).
#. Use ``git log`` to see your commits. Copy one of the commit hashes, and run ``git revert <hash>``. Check ``git log``.
#. Pick an old commit hash, and try ``git reset --soft <old commit>``, or ``git reset <old commit>`` (the only difference is that ``git reset`` does not stage files for commit).
   Try ``git log`` now. What happened? Did we lose any code?
#. We decided this was a bad idea. Let's undo this. The command ``git reflog`` shows *all* past operations, along with a hash.
   We can go back to the previous state by using ``git reset --hard HEAD@{1}``. Now hopefully ``git log`` is back to normal.
#. Take the same ``<old commit>`` as above, and do a ``git reset --hard <old commit>``. Try ``git log``. Did we lose anything?

Rewriting history means trouble (unless...)
++++++++++++++++++++++++++++++++++++++++++++
We will be simulating an unfortunate rewrite of history. This also must be done in a team.

a. Choose one repository as "upstream", and let other be forks.
#. Create a new branch called ``bad_branch`` in upstream, and commit a 3-4 incremental changes, push (e.g. add a file, commit, add a line, commit, add another line, commit, etc...)
#. In the forks, create new branches based on ``bad_branch``. Do this by adding a remote ``git remote add upstream <url here>``, fetching ``git fetch upstream``, and creating a branch ``git checkout -b <name here> upstream/bad_branch``.
#. In the forks, make an addition/change, commit and push ``git push origin``.
#. In the upstream fork, we will rewrite history. The easiest way is to change the most recent commit. Make a change to the file, ``git add <file>`` and then ``git commit --amend``.
#. (*bonus*) Change older commits. This is a little bit complicated, and conflicts will emerge. Using a GUI should make it easier, but we decribe the procedure for the terminal. Use ``git log`` to find the commit hash that you want to change, and then ``git rebase -i <commit hash>``. Read the file that gets output, and change one of the ``pick`` to ``edit``. If there are conflicts, solve them and ``git rebase --continue``.
#. In the upstream fork, push the changes to remote. ``git push`` will fail (why?), but you can ``git push --force``.
#. Try to make a pull request from one of the forks to upstream. This should fail catastrophically.
#. A possible fix wold be to rebase the forks to the new upstream. Try it out and see if you can fix it (``git rebase -i <commit hash>``).
