=====================================
Macaulay2 bootcamp
=====================================

| This page contains transcripts, exercises and examples from Macaulay2 bootcamp, held on Dec 13-14, 2022 at the MPI MIS, Leipzig, Germany.


.. toctree::
  :maxdepth: 2

  exercises.rst
  transcripts.rst

Project page created: 12/12/2022

Project contributors: Marc Härkönen, Eliana Duarte

Software used: Macaulay2 (v1.20)

.. System setup used: Dell XPS 13 7390 2-in-1 with Arch Linux (kernel 5.14.13), Processor 3,9 GHz Intel i7-1065G7, Memory 16 GB 4267 MHz LPDDR4, Graphics Intel Iris Plus Graphics G7.

Corresponding author of this page: Marc Härkönen, marc.harkonen@gmail.com