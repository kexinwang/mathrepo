-- This file contains code for computing the algebraic boundary of the oriented matroid strata for 2x2x2 games is used in Example 5.7 of our paper
-- This idea and the relationship between combinatorial types and the oriented matroid strata is discussed in Section 5 of our paper

-- This creates the ring of y variables which correspond to the coefficients on the inequalities of the correlated equilibria polytope
R = QQ[y111, y112, y121, y122, y211, y212, y221, y222, y311, y312, y321, y322]

A = {{y111, y112, y121, y122, 0, 0, 0, 0},
     {0, 0, 0, 0, -y111, -y112, -y121, -y122},
     {y211, y212, 0, 0, y221, y222, 0, 0},
     {0, 0, -y211, -y212, 0, 0, -y221, -y222},
     {y311, 0, y312, 0, y321, 0, y322, 0},
     {0, -y311, 0, -y312, 0, -y321, 0, -y322}};


A = matrix A;

-- This adds the rows corresponding to the identity matrix
A = A || map(R^8)

-- This creates the list of maximal minors. The signs of these minors encode the oriented matroid of A(Y)
maxMinors = delete(0_R, subsets(toList(0..13), 8) / (s -> det(A^s)))

-- Since a subset s is a basis if and only if the det(A_s) != 0, the algebraic boundary of the oriented matroid strata is given by the union of the varieties of the above minors
-- These minors need not be irreducible though. This means we may be able to describe the boundary with significantly fewer polynomials.
-- To do this we first compute the primary decomposition of the variety of each minors
minorIdeals = maxMinors / (f -> ideal(f))

-- We now compute the minimal primes of each ideal and remove the duplicates
-- The resulting list gives us the irreducible polynomials which define the algebraic boundary of the oriented matroid strata
irreducibles = minorIdeals / (f -> minimalPrimes f) // flatten // unique
