(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3698,        117]
NotebookOptionsPosition[      3261,        102]
NotebookOutlinePosition[      3653,        118]
CellTagsIndexPosition[      3610,        115]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"farey", "[", "0", "]"}], "=", 
   RowBox[{
    RowBox[{"-", "2"}], "+", 
    FractionBox["\[Alpha]", "\[Beta]"], "+", 
    FractionBox["\[Beta]", "\[Alpha]"], "-", "z"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"farey", "[", "1", "]"}], "=", 
   RowBox[{
    RowBox[{"-", "2"}], "+", 
    RowBox[{"\[Alpha]", " ", "\[Beta]"}], "+", 
    FractionBox["1", 
     RowBox[{"\[Alpha]", " ", "\[Beta]"}]], "+", "z"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"farey", "[", "2", "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"\[Alpha]", " ", "\[Beta]"}], "-", 
       FractionBox["\[Alpha]", "\[Beta]"], "-", 
       FractionBox["\[Beta]", "\[Alpha]"], "+", 
       FractionBox["1", 
        RowBox[{"\[Alpha]", " ", "\[Beta]"}]]}], ")"}], "z"}], "+", 
    RowBox[{"z", "^", "2"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"farey", "[", "n_", "]"}], "=", 
   RowBox[{
    RowBox[{"4", "-", 
     FractionBox["1", 
      RowBox[{"\[Alpha]", "^", "2"}]], "+", 
     RowBox[{"\[Alpha]", "^", "2"}], "+", 
     FractionBox["1", 
      RowBox[{"\[Beta]", "^", "2"}]], "+", 
     RowBox[{"\[Beta]", "^", "2"}], "-", 
     RowBox[{"farey", "[", 
      RowBox[{"n", "-", "3"}], "]"}], "-", 
     RowBox[{
      RowBox[{"farey", "[", 
       RowBox[{"n", "-", "2"}], "]"}], " ", 
      RowBox[{"farey", "[", 
       RowBox[{"n", "-", "1"}], "]"}]}]}], "/;", 
    RowBox[{"EvenQ", "[", "n", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"farey", "[", "n_", "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"2", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"\[Alpha]", " ", "\[Beta]"}], "+", 
        FractionBox["\[Alpha]", "\[Beta]"], "+", 
        FractionBox["\[Beta]", "\[Alpha]"], "+", 
        FractionBox["1", 
         RowBox[{"\[Alpha]", " ", "\[Beta]"}]]}], ")"}]}], "-", 
     RowBox[{"farey", "[", 
      RowBox[{"n", "-", "3"}], "]"}], "-", 
     RowBox[{
      RowBox[{"farey", "[", 
       RowBox[{"n", "-", "2"}], "]"}], " ", 
      RowBox[{"farey", "[", 
       RowBox[{"n", "-", "1"}], "]"}]}]}], "/;", 
    RowBox[{"OddQ", "[", "n", "]"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.857094115672739*^9, 3.857094123539653*^9}, {
  3.857094471840445*^9, 3.857094685311121*^9}, {3.857094870673099*^9, 
  3.857095015840568*^9}, {3.8574127416454782`*^9, 
  3.857412763207058*^9}},ExpressionUUID->"67c92868-950c-4b02-b0bb-\
ab197eae6c33"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.857094570631021*^9, 
  3.8570945717753057`*^9}},ExpressionUUID->"3f4eed95-54da-4aa9-8d08-\
2df174823f03"]
},
WindowSize->{1249, 740},
WindowMargins->{{139, Automatic}, {Automatic, 89}},
FrontEndVersion->"12.3 for Linux x86 (64-bit) (June 19, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"c003a659-274e-49a5-aa25-26ef4fca1170"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 2542, 75, 212, "Input",ExpressionUUID->"67c92868-950c-4b02-b0bb-ab197eae6c33"],
Cell[3103, 97, 154, 3, 30, "Input",ExpressionUUID->"3f4eed95-54da-4aa9-8d08-2df174823f03"]
}
]
*)

