=======================================================
Orders and Polytropes : Matrix Algebras from Valuations
=======================================================

| This page contains auxiliary files to the paper:
| Yassine El Maazouz, Marvin Anas Hahn, Gabriele Nebe, Mima Stanojkovski, Bernd Sturmfels: Orders and polytropes : matrix algebras from valuations
| In: Beiträge zur Algebra und Geometrie,  63 (2022) 3, p. 515-531
| DOI: `10.1007/s13366-021-00600-4 <https://dx.doi.org/10.1007/s13366-021-00600-4>`_ ARXIV: https://arxiv.org/abs/2107.00503 CODE: https://mathrepo.mis.mpg.de/OrdersPolytropes

   
Abstract:
We apply tropical geometry to study matrix algebras over a field with valuation. Using the shapes of min-max convexity, known as polytropes, we revisit the graduated orders introduced by Plesken and Zassenhaus. These are classified by the polytrope region. We advance the ideal theory of graduated orders by introducing their ideal class polytropes. This article emphasizes examples and computations. It offers first steps in the geometric combinatorics of endomorphism rings of configurations in affine buildings.


.. image:: 2.png
  :width: 400


.. image:: 3.png
  :width: 500

.. image:: 1.png
  :width: 700





This repository includes our code to compute the polytrope regions :math:`\mathcal{P}_d`  and truncated polytrope regions :math:`\mathcal{P}_d(M)` described in the paper (Section 4). We also provide code to compute the ideal class semigroup :math:`\mathcal{Q}_M` and its maximal subgroup :math:`\mathcal{G}_M` (Section 5).


Our code is written in `Julia <https://julialang.org/>`_ (v1.6.2) using the computer algebra system `Oscar <https://oscar.computeralgebra.de/>`_ (v0.5.2).

Please find the following jupyter notebook containing the code:

.. toctree::
  :maxdepth: 1
  :glob:

  code

You can also run our jupyter notebook online:

.. image:: ../binder_badge.svg
 :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mis.mpg.de%2Fkuhne%2Fbinderenvironments/OrdersPolytropes?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.mis.mpg.de%252Frok%252Fmathrepo%26urlpath%3Dtree%252Fmathrepo%252Fsource%252FOrdersPolytropes%252Fcode.ipynb%26branch%3Ddevelop





Project page created: 10/08/2021.

Project contributors:  Yassine El Maazouz, Marvin Anas Hahn, Gabriele Nebe, Mima Stanojkovski, Bernd Sturmfels

Corresponding author of this page: Yassine El Maazouz, yassine.el-maazouz@berkeley.edu

Software used: Julia (Version 1.6.2), Oscar (v0.5.2), GAP
