{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Nonlinear Algebra "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Numerical methods can also be used for computing the dimension of the vector spaces we present in the introduction by using Theorem 5.1. The following code in [julia](https://julialang.org/) provides an example for this by using the package [HomotopyContinuation.jl](https://www.juliahomotopycontinuation.org/). We compute the Euler characteristic of the very affine surface $X$ from Example 2.7, with $f$ as in Equation (2.8) as the number of solution to a system of rational functions in the variables $x,y$. The result in this case is $6$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\r",
      "\u001b[32mSolutions found: 6 \t Time: 0:00:02\u001b[39m\r\n",
      "\u001b[34m  tracked loops (queued):            36 (0)\u001b[39m\r\n",
      "\u001b[34m  solutions in current (last) loop:  0 (0)\u001b[39m\r\n",
      "\u001b[34m  generated loops (no change):       6 (5)\u001b[39m\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "MonodromyResult\n",
       "===============\n",
       "• return_code → :heuristic_stop\n",
       "• 6 solutions\n",
       "• 36 tracked loops\n",
       "• random_seed → 0x287750da"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "using HomotopyContinuation\n",
    "@var x y s ν[1:2]\n",
    "f = -x*y^2 + 2*x*y^3 + 3*x^2*y - x^2*y^3 - 2*x^3*y + 3*x^3*y^2\n",
    "L = s*log(f) + ν[1]*log(x) + ν[2]*log(y)\n",
    "F = System(differentiate(L,[x;y]), parameters = [s;ν])\n",
    "monodromy_solve(F)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following $\\texttt{julia}$ code is the one used for the experiments in Section 5 of the article. It consists of six functions, copied below with a few lines of documentation. One can also find it in Appendix B in the paper."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "integrate_loop (generic function with 1 method)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# One step of Euler’s method on dy/dx = ω(x)y, with stepsize Δx.\n",
    "function euler_step(x,y,Δx,ω)\n",
    "newy = (1+ω(x)*Δx)*y\n",
    "return (x+Δx,newy)\n",
    "end\n",
    "\n",
    "# One step of Newton iteration on y^k - prod(f(x).^(k*s))*x^(k*ν).\n",
    "function newton_step(y, x, f, s, ν, k)\n",
    "Fy = y^k - prod(f(x).^(k*s))*x^(k*ν)\n",
    "Δy = -Fy/(k*y^(k-1))\n",
    "return y + Δy\n",
    "end\n",
    "\n",
    "# Compute function values at N equidistant nodes on the line segment SxTx.\n",
    "# The initial condition is given by Sy.\n",
    "\n",
    "function track_line_segment(Sx,Sy,Tx,N,f,ω,s,ν,k)\n",
    "xx = zeros(ComplexF64,N)\n",
    "yy = zeros(ComplexF64,N)\n",
    "xx[1] = Sx\n",
    "yy[1] = Sy\n",
    "Δx = (Tx - Sx)/(N-1)\n",
    "for i = 2:N\n",
    "(xx[i], yi_tilde) = euler_step(xx[i-1],yy[i-1],Δx,ω)\n",
    "for j = 1:4\n",
    "yi_tilde = newton_step(yi_tilde, xx[i], f, s, ν, k)\n",
    "end\n",
    "yy[i] = copy(yi_tilde)\n",
    "end\n",
    "return xx, yy\n",
    "end\n",
    "\n",
    "\n",
    "# Numerical integration based on function values yy and interval length h.\n",
    "function integrate_trapezoidal(yy,h)\n",
    "return (yy[1]/2 + sum(yy[2:end-1]) + yy[end]/2)*h\n",
    "end\n",
    "# Compute the integral over the line segment AB with N discretization nodes.\n",
    "# The initial condition is given by phiAB_at_A.\n",
    "# The integral is computed for the cocycles in aabb.\n",
    "function integrate_line_segment(A,phiAB_at_A,B,N,f,ω,s,ν,k,aabb)\n",
    "IAB = []\n",
    "xxAB, yyAB = track_line_segment(A,phiAB_at_A,B,N,f,ω,s,ν,k)\n",
    "for ab in aabb\n",
    "a = ab[1]; b = ab[2];\n",
    "single_valued = [x^(b-1)*prod(f(x).^a) for x in xxAB]\n",
    "IAB = push!(IAB,integrate_trapezoidal(yyAB.*single_valued,(B-A)/(N-1)))\n",
    "end\n",
    "return IAB,yyAB\n",
    "end\n",
    "# Compute the integral over the twisted cycle defined by A, B, C,\n",
    "# and the initial condition phiAB_at_A.\n",
    "# The integral is computed for the cocycles in aabb.\n",
    "function integrate_loop(A,B,C,phiAB_at_A,N,f,ω,s,ν,k,aabb)\n",
    "IAB, yyAB = integrate_line_segment(A,phiAB_at_A,B,N,f,ω,s,ν,k,aabb)\n",
    "phiBC_at_B = yyAB[end]\n",
    "IBC, yyBC = integrate_line_segment(B,phiBC_at_B,C,N,f,ω,s,ν,k,aabb)\n",
    "phiCA_at_C = yyBC[end]\n",
    "ICA, yyCA = integrate_line_segment(C,phiCA_at_C,A,N,f,ω,s,ν,k,aabb)\n",
    "return IAB+IBC+ICA\n",
    "end\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 5.4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following lines illustrate how to use the code presented above in the case $f = (x-1, x-2),$ $s = (\\frac{1}{2}, \\frac{1}{2})$, and $\\nu = \\frac{1}{2}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Vector{ComplexF64}:\n",
       "   3.496076532931323 - 3.2862601528904634e-14im\n",
       "  0.6482432272682694 - 4.529709940470639e-14im\n",
       " -4.1443176753269615 - 7.549516567451064e-15im"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f = x -> [x-1; x-2]\n",
    "s = [1/2;1/2]; ν = 1/2; N = 1000; k = 2;\n",
    "ω = x -> s[1]/(x-1) + s[2]/(x-2) + ν/x\n",
    "cocycles = [[[-1;0],1], [[0,-1],1], [[0,0],0]]\n",
    "A1 = 1/2+im; B1 = 1/2-im; C1 = 3+0im\n",
    "phiA1B1_at_A1 = A1^ν*prod(f(A1).^s)\n",
    "I1 = integrate_loop(A1,B1,C1,phiA1B1_at_A1,N,f,ω,s,ν,k,cocycles)\n",
    "A2 = -1+0im; B2 = 3/2+im; C2 = 3/2-im\n",
    "phiA2B2_at_A2 = A2^ν*prod(f(A2).^s)\n",
    "I2 = integrate_loop(A2,B2,C2,phiA2B2_at_A2,N,f,ω,s,ν,k,cocycles)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.6.3",
   "language": "julia",
   "name": "julia-1.6"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
