#####################
Computing GKZ systems
#####################

This page contains the code we used to compute the GKZ system associated to the polynomial :math:`f = -c_1 xy^2 + c_2 xy^3 + c_3 x^2y - c_4 x^2y^3 - c_5 x^3y + c_6 x^3y^2.`  The matrix  :math:`A\in\mathbb{Z}^{3\times 6}` associated to the polynomial :math:`f` is 

.. math::   \begin{pmatrix}
            1 & 1 & 2 & 2 & 3 & 3\\
            2 & 3 & 1 & 3 & 1 & 2 \\
            1 & 1 & 1 & 1 & 1 & 1
            \end{pmatrix}.

Using the :math:`\verb|Macaulay2|` package `Dmodules <http://www2.macaulay2.com/Macaulay2/doc/Macaulay2-1.18/share/doc/Macaulay2/Dmodules/html/index.html>`_, one attains :math:`9` binomials generating the toric ideal :math:`I_A` and :math:`3` operators generating the ideal :math:`J_{A,\kappa}`: 

.. code-block:: macaulay2

                restart

                needsPackage "Dmodules"
                A = matrix{{1,1,1,1,1,1},
                           {1,1,2,2,3,3},
	                   {2,3,1,3,1,2}};
                # choose non resonant values for the vector b = (-\nu,s)^T 
                k = {1/2,1/3,1/5} 
 
                D = makeWA(QQ[x_1..x_6])

                G = gkz(A,k)

                holonomicRank G


Alternatively, one can compute exclusively the toric ideal :math:`I_A` using the package `Quasidegrees <http://www2.macaulay2.com/Macaulay2/doc/Macaulay2-1.18/share/doc/Macaulay2/Quasidegrees/html/index.html>`_: 

.. code-block:: macaulay2

                needsPackage "Quasidegrees"
                D6 = QQ[d_1..d_6]
                T = toricIdeal(A,D6)
                degree T

This last computation can also be done in :math:`\verb|Singular|` by running the following lines:

.. code-block:: singular

		LIB "toric.lib";
                ring s = 0,(d1,d2,d3,d4,d5,d6),dp; setring s;
                intmat Amat[3][6] = 1,1,2,2,3,3,2,3,1,3,1,2,1,1,1,1,1,1;
                ideal I = toric_ideal(Amat,"du"); I;