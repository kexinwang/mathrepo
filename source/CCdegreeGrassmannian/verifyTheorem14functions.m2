 defineRing = (d, n) -> (
    pvars = flatten flatten for i in 0..d list for r in subsets(d,i) list 
                        for c in subsets(d..n-1,i) list 
                        p_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1));
    qvars = flatten flatten for i in 0..1 list for r in subsets(d,i) list 
                        for c in subsets(d..n-1,i) list 
                        q_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1));
    QQ[s, t, x_{1, d+1}..x_{d, n}, pvars | qvars, MonomialOrder => Eliminate (d*(n-d)+2)]
)

diagProd = (M,j) -> (
    product for i in 0..j - 1 list M_(i,i)
)

getG = (d, n) -> (

    polyParametrization = (flatten flatten for i in 0..d list for r in subsets(d,i) list 
                      for c in subsets(d..n-1,i) list 
                      p_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1)) - s*det(submatrix(X, r, c - toList (#c:d)))) |
                      flatten flatten for i in 0..1 list for r in subsets(d,i) list 
                      for c in subsets(d..n-1,i) list 
                      q_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1)) - t*det(submatrix(X, r, c - toList (#c:d)));
    
    I1 = ideal(polyParametrization);
    G = ideal selectInSubring(1,gens gb (I1,DegreeLimit=>n))
)

getT = (d, n) -> (

    monomParametrization = (flatten flatten for i in 0..d list for r in subsets(d,i) list 
                      for c in subsets(d..n-1,i) list 
                      p_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1)) - s*diagProd(submatrix(X, r, c - toList (#c:d)), i)) |
                      flatten flatten for i in 0..1 list for r in subsets(d,i) list 
                      for c in subsets(d..n-1,i) list 
                      q_(sort toList (set(0..d-1) - set(r) + set(c)) + toList(d:1)) - t*diagProd(submatrix(X, r, c - toList (#c:d)), i);              

    I2  = ideal(monomParametrization);
    T = ideal selectInSubring(1,gens gb (I2,DegreeLimit=>n))
)

KhovanskiiCheck = (d, n) -> (
    
    R = defineRing(d, n);
    X = transpose (genericMatrix(R, x_{1, d+1}, n-d, d));
    G = getG(d, n); T = getT(d,n);
    << "CC degree = " << degree T;
    toString hilbertSeries (R/G) == toString hilbertSeries (R/T)

)

defineRingWithPoset = (d, n, poset) -> (
    var = flatten for tuple in poset list (
        if #drop(1..d, d-1) <= 1 then {p_tuple, q_tuple}
        else p_tuple 
        );
    QQ[s, t, x_{1, d+1}..x_{d, n}, var, MonomialOrder => Eliminate (d*(n-d)+2)]
)

KhovanskiiCheckWithPoset = (d, n, poset) -> (
    
    R = defineRingWithPoset(d, n, poset);
    X = transpose (genericMatrix(R, x_{1, d+1}, n-d, d));
    G = getG(d, n); T = getT(d,n);
    << "CC degree = " << degree T;
    toString hilbertSeries (R/G) == toString hilbertSeries (R/T)

)

SquarefreeCheck = (d, n, poset) -> (

    R = defineRingWithPoset(d, n, poset);
    X = transpose (genericMatrix(R, x_{1, d+1}, n-d, d));
    G = getG(d, n); T = getT(d, n);
    MG = ideal leadTerm G; MT = ideal leadTerm T;
    (radical MG == MG) and (radical MT == MT)
)
