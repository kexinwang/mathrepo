using HomotopyContinuation

@var L, x12, x13, x22, x23

H = [33     10    -89    -25    -42    -68;
     10      7     65    -96      7    -60;
    -89     65     12     50    -89     16;
    -25    -96     50    -60    -70     52;
    -42      7    -89    -70     34    -20;
    -68    -60     16     52    -20     -4] 


psi = [x22,x23,1,x12,x13,x12*x23 - x22*x13]
res = solve((H*psi)[1:5] - L*psi[1:5])




