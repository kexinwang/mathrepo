load("main.m2")

d=3; n=9;

integerToListOfDigits = n ->  (
    digits = {};
    while n > 0  do (
        digits = prepend(n % 10, digits);
        n = n // 10;
    );
    digits
);

poset39 = apply ({
239,   
389,
379, 289,
369, 789, 238,
359, 689, 279, 378,
349, 589, 679, 278, 368,
139, 489, 579, 358, 269, 678, 237, 
189, 479, 348, 569, 578, 268, 367,
179, 138, 469, 478, 259, 568, 357, 267,
169, 178, 459, 468, 347, 258, 567, 236, 
159, 168, 137, 249, 458, 467, 257, 356,
149, 158, 167, 248, 457, 346, 256,
129, 148, 157, 136, 247, 456, 235, 
128, 147, 156, 246, 345,
127, 146, 135, 245,
126, 145, 234,
134, 125, 
124, 
123
}, integerToListOfDigits);

R = defineRingWithPoset(d, n, poset39);
X = transpose (genericMatrix(R, x_{1, d+1}, n-d, d));

time G = getG(d, n); T = getT(d,n);
<< "CC degree = " << degree T;

MG = ideal leadTerm G; MT = ideal leadTerm T;
time (toString hilbertSeries (R/G) == toString hilbertSeries (R/T)) and (radical MG == MG) and (radical MT == MT)