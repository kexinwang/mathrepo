# computes ideals of neurovarieties of polynomial neural networks
# by Kaie Kubjas, Jiayi Li and Maximilian Wiesmann
# uses Julia version 1.9.1, Oscar version 0.12.1

using Oscar
using Combinatorics
using LinearAlgebra


multisets(n, d) = map(A -> [sum(A .== j) for j in 1:n], with_replacement_combinations(1:n, d))
monomial_exponents(i, d, n) = multisets(n, d)[i] 

# this function gives the "coeffcient" of a monomial mon (not necessarily
# containing all variables) in a polynomial p; the variables contained in 
# mon should start at pos with respect to all variables in the polynomial ring
function varcoeff(p, mon, pos)
    R = base_ring(ideal([p]))
    C = MPolyBuildCtx(R)
    l = length(mon)
    for (i,exp) in enumerate(exponents(p))
        if exp[pos:pos+l-1] == mon
            nonmon = vcat(exp[1:pos-1], fill(0, l), exp[pos+l:end])
            push_term!(C, coeff(p, exp), nonmon)
        end
    end
    return finish(C)
end


# computes an ideal of the neurovariety of a PNN with
# widths = (d_0,...,d_h) and exponentiation r
function neurovariety(widths, r)
    depth = length(widths) - 1
    wvars = String[]
    for i in 1:depth
        append!(wvars, ["w[$i,$j,$k]" for j in 1:(widths[i+1]) for k in 1:(widths[i])])
    end
    
    num_mons = binomial(widths[1]+r^(depth-1)-1, widths[1]-1)
    yvars = ["y[$i,$j]" for i in 1:widths[end] for j in 1:num_mons]

    R, vars = PolynomialRing(QQ, vcat(wvars, ["x$i" for i in 1:(widths[1])], yvars))

    w = vars[1:length(wvars)]
    x = vars[length(wvars)+1:length(wvars)+widths[1]]
    y = vars[length(wvars)+widths[1]+1:end]
    wvarsDict = Dict([t for t in tuple.(wvars, w)])
    yvarsDict = Dict([t for t in tuple.(yvars, y)])

    xVect = MatrixSpace(R, widths[1], 1)(x)
    for i in 1:depth
        W = MatrixSpace(R, widths[i+1], widths[i])()
        for (j,k) in Iterators.product(1:widths[i+1], 1:widths[i])
            W[j,k] = get(wvarsDict, "w[$i,$j,$k]", 0)
        end
        xVect = W*xVect
        if i < depth
            for j in 1:widths[i+1]
                xVect[j,1] = xVect[j,1]^r
            end
        end
    end
    
    equs = []
    for i in 1:widths[end]
        for j in 1:num_mons
            mon = monomial_exponents(j, r^(depth - 1), widths[1])
            pos = length(wvars) + 1
            c = varcoeff(xVect[i,1], mon, pos)
            push!(equs, c - get(yvarsDict, "y[$i,$j]", 0))
        end
    end

    I = ideal(equs)
    I = eliminate(I, w)

    S, newvars = graded_polynomial_ring(QQ, yvars)
    f = hom(R, S, vcat(Int.(zeros(length(w)+length(x))), newvars))
    return f(I)
end


widths = [2,2,3]
r = 2
I = neurovariety(widths, r)