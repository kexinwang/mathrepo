#![allow(unused_imports)]

use euclid::Point2D;
use ndarray::prelude::*;
use rayon::prelude::*;
use std::collections::HashMap;
use image::{Rgb, RgbImage};
use imageproc::drawing::draw_line_segment_mut;

fn create_normals(
    m: usize,
) -> Vec<(f64, f64)>
{
    let angles = Array::linspace(0., 2.0 * std::f64::consts::PI, m + 1);
    let mut normal: Vec<(f64, f64)> = Vec::new();
    for i in 0..m {
        normal.push((angles[i].cos(), angles[i].sin()));
    }
    normal
}

fn main() {

    let it: i32 = std::env::args().nth(1).unwrap_or("200".to_string()).parse::<i32>().unwrap();
    let m: usize = std::env::args().nth(2).unwrap_or("10".to_string()).parse::<usize>().unwrap();
    let euler: usize = std::env::args().nth(3).unwrap_or("10".to_string()).parse::<usize>().unwrap();
    let dt: f64 = std::env::args().nth(4).unwrap_or("0.0001".to_string()).parse::<f64>().unwrap();

    let normal: Vec<(f64, f64)>=create_normals(m);
    let mut ellipse: Vec<(f64, f64)>= vec![(1.,1.); m];
    let mut p: Vec<f64> = vec![1.; m];//vec![1.; m];
    let mut dp: Vec<(f64, f64)> = Vec::new();
    let mut ddp: Vec<(f64, f64)> = Vec::new();


    //Creates structure of the images
    let folder = "imgs";
    std::fs::create_dir_all(folder).unwrap();

    //Ellipse plotten
    for j in 0..m {
        /* Tangentiale Polar
        p[j] = (asqr * normal[j].0.powi(2) + bsqr * normal[j].1.powi(2)).sqrt();//0.4
        //p[j] = 0.4-0.1*normal[j].1.powi(8);//0.4
        ellipse[j] = (
            0.5 + p[j] * normal[j].0 - dp[j] * normal[j].1,
            0.5 + p[j] * normal[j].1 + dp[j] * normal[j].0,
        );*/

        //Polarcoords
        p[j] = 0.4;//-0.32*normal[j].1.powi(2);
        ellipse[j] = (
            0.5 + p[j] * normal[j].0,
            0.5 + p[j] * normal[j].1,
        );
    }
    for j in 0..m {
        dp.push(((ellipse[(j+1) % m].0 - ellipse[(j+m-1) % m].0)/2.,(ellipse[(j+1) % m].1 - ellipse[(j+m-1) % m].1)/2.));
        ddp.push((ellipse[(j+1) % m].0 + ellipse[(j+m-1) % m].0 - 2. * ellipse[j].0,ellipse[(j+1) % m].1 + ellipse[(j+m-1) % m].1 - 2. * ellipse[j].1));

        /* Tangentiale Polar
        ellipse[j] = (
            0.5 + p[j] * normal[j].0 - dp[j] * normal[j].1,
            0.5 + p[j] * normal[j].1 + dp[j] * normal[j].0,
        );*/
        /*Polarcoords
        ellipse[j] = (
            0.5 + p[j] * normal[j].0,
            0.5 + p[j] * normal[j].1,
        );*/
    }

    //Render Image
    render_to_imagef64(
        &(format!("{}/{:03}.png", folder, 0)),
        0.,
        &ellipse,
    );

    println!("First Frame complete");

    for i in 0..it {
        if i % 10 == 0 {
            println!("Iteration {}", i);
        }

        // for j in 0..points.len() {
        //     if points[j].position.x<2.*r || points[j].position.y<2.*r || points[j].position.x>1.-2.*r || points[j].position.y>1.-2.*r
        //     {
        //         points[j].value=point_valuef64(points[j].position.x,points[j].position.y)+4.*((i+1) as f64*DT);
        //     }
        // }

        for _k in 0..euler{
            for j in 0..m {

                // Tangentiale Polar
                //Zeit-Euler-Schema
                let norm=(dp[j].0.powi(2)+dp[j].1.powi(2)).sqrt();
                let curv =(dp[j].0*ddp[j].1-dp[j].1*ddp[j].0)/norm.powi(3);
                //println!("curv {} {}", j, curv);
                //p[j] = p[j] - dt / (euler as f64*(p[j] + ddp[j]));
                //p[j] = (p[j].powi(2) - dt *2.).sqrt();
                ellipse[j] = (
                    ellipse[j].0-dt/(euler as f64)*dp[j].1/norm*curv,//eigentlich dt/(euler as f64)* /norm*curv
                    ellipse[j].1+dt/(euler as f64)*dp[j].0/norm*curv,
                );
                /*Polarcoods
                let normalx=p[j]*normal[j].0+dp[j]*normal[j].1;
                let normaly=p[j]*normal[j].1-dp[j]*normal[j].0;
                let norm=(normalx.powi(2)+normaly.powi(2)).sqrt();
                let curv=((-2.*dp[j]*normal[j].1+normal[j].0*(ddp[j]-p[j])).powi(2)+(2.*dp[j]*normal[j].0+normal[j].1*(ddp[j]-p[j])).powi(2)).sqrt()/(norm.powi(3));
                p[j] = p[j] - dt *;
                ellipse[j] = (
                    0.5 + p[j] * normal[j].0,
                    0.5 + p[j] * normal[j].1,
                );*/
            }
            for j in 0..m {
                dp[j] = ((ellipse[(j+1) % m].0 - ellipse[(j+m-1) % m].0)/2.,(ellipse[(j+1) % m].1 - ellipse[(j+m-1) % m].1)/2.);
                ddp[j] = (ellipse[(j+1) % m].0 + ellipse[(j+m-1) % m].0 - 2. * ellipse[j].0,ellipse[(j+1) % m].1 + ellipse[(j+m-1) % m].1 - 2. * ellipse[j].1);
            } 
        }
        if i % 1 == 0 {
            let filename = format!("{}/{:04}.png", folder, (i/1+1));
            render_to_imagef64( &filename, (i+1) as f64*dt, &ellipse);
        }
    }

    println!("Finished");

    let output_video = "output.mp4";
    let ffmpeg_command = format!(
        "ffmpeg -framerate 2 -i {}/%04d.png -c:v libx264 -r 30 {}",
        folder, output_video
    );
    std::process::Command::new("sh")
        .arg("-c")
        .arg(&ffmpeg_command)
        .output()
        .expect("Failed to execute ffmpeg command");

    println!("Video saved as: {}", output_video);
}

pub fn render_to_imagef64(filename: &str, _t:f64, ellipse: &[(f64, f64)]) {
    let width = 800;
    let height = 800;
    //let mut img = RgbImage::new(width, height);
    let mut img=RgbImage::from_pixel(width, height, Rgb([255, 255, 255]));

    let color = Rgb([0, 0, 0]);//Rgb([255, 0, 0])
    //the last point is the first point to close the loop
    let mut x0 = (ellipse[ellipse.len() - 1].0.max(0.).min(1.) * width as f64) as u32;
    let mut y0 = (ellipse[ellipse.len() - 1].1.max(0.).min(1.) * height as f64) as u32;

    //draw lines between points
    for point in ellipse {
        let x = (point.0.max(0.).min(1.) * width as f64) as u32;
        let y = (point.1.max(0.).min(1.) * height as f64) as u32;
        draw_line_segment_mut(
            &mut img,
            (x0 as f32, y0 as f32),
            (x as f32, y as f32),
            color,
        );
        x0 = x;
        y0 = y;
    }

    img.save(filename).unwrap();
}