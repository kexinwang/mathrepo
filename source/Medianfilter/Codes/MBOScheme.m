%Anton Ullrich
%MCF - MBO scheme 
function MBOScheme
set(gcf,'Position',[100 100 800 800])
global W;
global X;
global Y;
[W,X,Y] = meshgrid(-1:.005:1);
Z = discretise(initalData(W,X,Y));
global h;
h=100;
v = VideoWriter('mcf-scheme.avi');
v.FrameRate=15;
open(v);
while any(Z(:)>0)
    Zdiff=convolve(Z);
    Z=discretise(Zdiff);
    if any(Z(:)>0)
        cla;
        p = patch(isosurface(W,X,Y,Zdiff,0.5));
        isonormals(W,X,Y,Zdiff,p)
        set(p,'FaceColor','red','EdgeColor','none');
        daspect([1 1 1])
        view(3); axis tight
        camlight 
        lighting gouraud
        view([-65,45]);
        xlim([-1,1]);
        ylim([-1,1]);
        zlim([-1,1]);
        frame = getframe(gcf);
        writeVideo(v,frame);
    end
end
close(v);
end

function Z=initalData(W,X,Y) %dumbbell
    Z=min(max(max(0.125-(X-0.65).^2-Y.^2-W.^2,0.125-(X+0.65).^2-Y.^2-W.^2),0.06-Y.^2-W.^2), 0.7-X.^2-Y.^2-W.^2)+0.45;
end

function Z=convolve(Zold) %Diffusion step
    global h;
    global X;
    global Y;
    global W;
    Z = abs(ifftn(fftshift(exp((-(X).^2-(Y).^2-(W).^2).*h/2).*fftshift(fftn(Zold)))));
end

function Z=discretise(Zold)%Thresholding step
    Z=ones(size(Zold));
    Z(Zold<0.5)=0;
end