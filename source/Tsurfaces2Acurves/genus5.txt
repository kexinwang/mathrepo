//Magma code for the experiments of the genus 5 JS surface in Section 3.3.3 of the arXiv version.

//Adjust the notation of the characteristics, by mapping 1->1/2. 
//It redefines the theta function with characteristic with the respect to this adjustment. 
Thetachar:=function(tau,char)
return Theta(Transpose(Matrix([[char[1][i]/2 : i in [1..5]] cat [char[2][i]/2: i in [1..5]]])),Transpose(Matrix([[0,0,0,0,0]])),tau);
end function;

//Complex field with 10 decimal digits of precision
C<I>:=ComplexField(10);
//The discrete Riemann matrix of level 7 in Table 7 of the arXiv version.
JSg5:=Matrix(C, 5, 5, [-0.184082280448567+0.964527357825949*I,0.000338075493623652+0.000310879694952452*I,1.96496720652709*10^-6-8.47902105234478*10^-7*I,0.00033807549365989+0.000310879694956109*I,-0.183404164493685-0.0348517306846398*I, 0.000338075493654674+0.000310879695121561*I,-0.1840822804486+0.964527357825969*I,0.000338075493482921+0.000310879694851483*I,1.96496731081912*10^-6-8.47901936949071*10^-7*I,-0.183404164493661-0.0348517306849424*I, 1.96496733567088*10^-6-8.47901789050823*10^-7*I,0.000338075493669542+0.000310879695129029*I,-0.184082280448643+0.964527357825918*I,0.000338075493631535+0.000310879694973129*I,-0.183404164493821-0.0348517306851707*I, 0.000338075493655019+0.000310879695121191*I,1.96496734630549*10^-6-8.4790178108778*10^-7*I,0.00033807549366611+0.000310879695129843*I,-0.184082280448587+0.964527357825968*I,-0.183404164493865-0.0348517306853252*I, -0.183404164493861-0.034851730685447*I,-0.183404164493837-0.0348517306853177*I,-0.183404164493548-0.0348517306849158*I,-0.183404164493797-0.0348517306850046*I,-0.733616657975974+3.86059307725676*I]);

//The three expressions of genus 5 (in JSg5) in Proposition 1.2. of the paper by Farkas, Grushevsky, Manni "An explicit solution to the weak Schottky problem". Denoted by R34, R35, R45. 
//This paper is cited as [FGM21] in the arXiv version. 
//The locus that is defined by these expressions contains the Schottky locus as an irreducible component. 
//Link of the paper http://content.algebraicgeometry.nl/2021-3/2021-3-009.pdf
R34:=0;R35:=0;R45:=0;
for eps1 in [0,1] do
for eps2 in [0,1] do
Characteristic34:=[[[0,0,0,eps1,eps2],[0,0,0,0,0]],
[[0,1,1,eps1,eps2],[1,0,0,0,0]],
[[1,0,0,eps1,eps2],[0,0,1,0,0]],
[[1,1,1,eps1,eps2],[1,0,1,0,0]],
[[0,1,0,eps1,eps2],[0,0,0,0,0]],
[[0,0,1,eps1,eps2],[1,0,0,0,0]],
[[1,1,0,eps1,eps2],[0,0,1,0,0]],
[[1,0,1,eps1,eps2],[1,0,1,0,0]],
[[0,0,0,eps1,eps2],[0,1,1,0,0]],
[[0,1,1,eps1,eps2],[1,1,1,0,0]],
[[1,0,0,eps1,eps2],[0,1,0,0,0]],
[[1,1,1,eps1,eps2],[1,1,0,0,0]]];


R34:=R34+Thetachar(JSg5,Characteristic34[1])*Thetachar(JSg5,Characteristic34[2])*Thetachar(JSg5,Characteristic34[3])*Thetachar(JSg5,Characteristic34[4])-
Thetachar(JSg5,Characteristic34[5])*Thetachar(JSg5,Characteristic34[6])*Thetachar(JSg5,Characteristic34[7])*Thetachar(JSg5,Characteristic34[8])+
Thetachar(JSg5,Characteristic34[9])*Thetachar(JSg5,Characteristic34[10])*Thetachar(JSg5,Characteristic34[11])*Thetachar(JSg5,Characteristic34[12]);

Characteristic35:=[];
for c in Characteristic34 do
c1:=c[1];c2:=c[2];
Append(~Characteristic35,[[c1[1],c1[2],c1[4],c1[3],c1[5]],[c2[1],c2[2],c2[4],c2[3],c2[5]]]);
end for;

R35:=R35+Thetachar(JSg5,Characteristic35[1])*Thetachar(JSg5,Characteristic35[2])*Thetachar(JSg5,Characteristic35[3])*Thetachar(JSg5,Characteristic35[4])-
Thetachar(JSg5,Characteristic35[5])*Thetachar(JSg5,Characteristic35[6])*Thetachar(JSg5,Characteristic35[7])*Thetachar(JSg5,Characteristic35[8])+
Thetachar(JSg5,Characteristic35[9])*Thetachar(JSg5,Characteristic35[10])*Thetachar(JSg5,Characteristic35[11])*Thetachar(JSg5,Characteristic35[12]);


Characteristic45:=[];
for c in Characteristic34 do
c1:=c[1];c2:=c[2];
Append(~Characteristic45,[[c1[1],c1[3],c1[2],c1[5],c1[4]],[c2[1],c2[3],c2[2],c2[5],c2[4]]]);
end for;

R45:=R45+Thetachar(JSg5,Characteristic45[1])*Thetachar(JSg5,Characteristic45[2])*Thetachar(JSg5,Characteristic45[3])*Thetachar(JSg5,Characteristic45[4])-
Thetachar(JSg5,Characteristic45[5])*Thetachar(JSg5,Characteristic45[6])*Thetachar(JSg5,Characteristic45[7])*Thetachar(JSg5,Characteristic45[8])+
Thetachar(JSg5,Characteristic45[9])*Thetachar(JSg5,Characteristic45[10])*Thetachar(JSg5,Characteristic45[11])*Thetachar(JSg5,Characteristic45[12]);

end for;
end for;

//Each of R34, R35, R45 is zero up to numerical round-off. 
//One may experiment further how the values behave by tuning the precision. 
R34;
//-2.188948606E-10 + 6.257163884E-11*I
R35;R45;
//2.351408651E-10 - 1.893776629E-10*I
//1.721435494E-10 - 2.233637164E-10*I