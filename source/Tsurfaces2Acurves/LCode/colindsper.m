%This function returns the indices of the variables used in the periodicity
%equation for side length "lam", level n, and period type "pertype" with
%index i. 
function inds = colindsper(lam, step, n, indices, pertype, i)
    formatSpec = "%d, %d";
    if pertype == "a"
        if mod(i, 2)== 0 
            key1 = "A1b";
        else key1 = "A1w";
        end
        key2 = sprintf(formatSpec, int64(lam*step*3^n), i);
        key3 = sprintf(formatSpec, 0, i);
        inds = [indices(key1), indices(key2), indices(key3)];
    elseif pertype == "diffa"
        if mod(i, 2)== 0 
            key1 = "A1b";
            key4 = "A2b";
        else key1 = "A1w";
            key4 = "A2w";
        end
        key3 = sprintf(formatSpec, 0, i);
        key5 = sprintf(formatSpec, step*3^n, i);
        inds = [indices(key1), indices(key4), indices(key5), indices(key3)];
    elseif pertype == "b"
        if mod(i, 2)== 0 
            key6 = "B1b";
        else
            key6 = "B1w";
        end
        key7 = sprintf(formatSpec, i,int64(lam * step * 3^n));
        key8 = sprintf(formatSpec, i,0);
        inds = [indices(key6), indices(key7), indices(key8)];
    else 
        if mod(i, 2)== 0 
            key6 = "B1b";
            key9 = "B2b";
        else 
            key6 = "B1w";
            key9 = "B2w";
        end
        key8 = sprintf(formatSpec, i,0);
        key10 = sprintf(formatSpec, i,step*3^n);
        inds = [indices(key6), indices(key9), indices(key10), indices(key8)];
    end
end