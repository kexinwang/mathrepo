==============================================================
Primary Decomposition with Differential Operators
==============================================================

| Here may you find the supplementary codes for the paper: 
| Yairon Cid-Ruiz and Bernd Sturmfels: Primary decomposition with differential operators
| In: International mathematics research notices, 2023 (2023) 14, p. 12119-12147
| DOI: `10.1093/imrn/rnac178 <https://dx.doi.org/10.1093/imrn/rnac178>`_ ARXIV: https://arxiv.org/abs/2101.03643 CODE: https://mathrepo.mis.mpg.de/PrimaryDecompositionWithDifferentialOperators

*Abstract of the paper*: We introduce differential primary decompositions for ideals in a commutative ring. Ideal membership is characterized by differential conditions. The minimal number of conditions needed is the arithmetic multiplicity. Minimal differential primary decompositions are unique up to change of bases. Our results generalize the construction of Noetherian operators for primary ideals in the analytic theory of Ehrenpreis-Palamodov, and they offer a concise method for representing affine schemes. The case of modules is also addressed. We implemented an algorithm in Macaulay2 that computes the minimal decomposition for an ideal in a polynomial ring.

In this repository you will find the implementation of Algorithm 5.4. This algorithm computes a minimal differential primary decomposition for a given ideal. 
We also provide an algorithm for the reverse process.

Examples of the performance of the algorithm and the complete code can be viewed here:

.. toctree::
   :maxdepth: 2

   Examples.rst
   Macaulay2Code.rst      

If you want to run the code yourself, you may download the following *Macaulay2* file:
:download:`noetherianOperatorsCode.m2 <noetherianOperatorsCode.m2>`  

All the examples contained in the paper and many more can be downloaded here:
:download:`Macaulay2 Examples <examplesDiffPrimDec.m2>`   


Project page created: 13/01/2021.

Software used: Macaulay2.

Project contributors: Yairon Cid-Ruiz and Bernd Sturmfels.

Corresponding author of this page: Yairon Cid-Ruiz, Yairon.CidRuiz@UGent.be.
