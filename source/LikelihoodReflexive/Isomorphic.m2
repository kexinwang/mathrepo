--- Isomorphism test for smooth reflexive polytopes in three dimensions

needsPackage "ReflexivePolytopesDB"
needsPackage "Polyhedra"
needsPackage "LatticePolytopes"

polytopes = kreuzerSkarkeDim3();

-- determine matrix A -- access to KS database
matrixA = i -> (
    P = polytopes_i;
    P = matrix P;
    P = convexHull P;
    L = latticePoints P;
    A = matrix{L} 
)

-- vertices of smooth polytopes determined using Julia/Oscar - access to polyDB
A1 = matrix{{1,1,0,1,1,0,0,-4},{1,0,1,-2,1,1,-3,1},{0,0,-1,1,1,1,1,-3}}
A2 = matrix{{1,1,-4,1,0,1},{1,1,1,-4,1,0},{-1,1,1,1,-1,-1}}
A3 = matrix{{0,0,1,1,1,1,-1,-1,-1,-1},{1,0,1,-1,0,1,1,-3,-1,1},{-1,-1,1,1,0,0,1,1,-1,-1}}
A4 = matrix{{0,0,1,1,1,1,0,0,-1,-1,-1,-1},{-2,1,1,0,-1,1,0,1,1,-1,-2,1},{1,1,0,0,1,1,-1,-1,-1,-1,0,0}}
A5 = matrix{{0,0,1,1,1,1,0,0,-2,-2},{1,0,1,-1,0,1,1,-2,-2,1},{-1,-1,1,1,0,0,1,1,-1,-1}}
A6 = matrix{{1,1,1,1,-2,0,-2,0},{0,-2,1,1,1,1,-2,0},{-1,1,1,-1,1,-1,1,-1}}
A7 = matrix{{1,1,1,1,-1,-1,-1,-1},{1,0,0,1,1,1,-2,-2},{0,-1,1,1,0,1,1,-3}}
A8 = matrix{{0,0,1,1,1,1,-1,-1,-1,-1},{-2,1,1,-1,0,1,0,1,1,-2},{1,1,1,1,0,0,-2,-2,0,0}}
A9 = matrix{{1,1,1,1,1,-1,-1,-1,-1,-1},{-1,0,1,1,-1,1,1,-1,0,-1},{0,-1,-1,1,1,1,-1,1,-1,0}}
A10 = matrix{{1,1,1,1,1,1,-1,-1,-1,-1,-1,-1},{-1,-1,0,1,1,0,1,1,0,0,-1,-1},{0,-1,-1,1,0,1,0,1,-1,1,-1,0}}
A11 = matrix{{1,-2,1,1,1,-1,-2,1},{1,1,1,-1,-1,-1,0,0},{-2,1,1,1,-1,1,1,-2}}
A12 = matrix{{1,1,1,1,-1,-1,-1,-1},{0,-2,1,1,1,1,-2,0},{-1,1,1,-1,1,-1,1,-1}}
A13 = matrix{{1,1,1,1,0,-2,0,-2},{0,-2,1,1,1,1,-2,0},{-1,1,1,-1,1,-1,1,-1}}
A14 = matrix{{1,1,1,1,-2,-2},{0,1,0,1,1,-3},{1,1,-2,-2,1,1}}
A15 = matrix{{1,1,-3,1,-1,1},{1,1,1,-3,1,-1},{-1,1,1,1,-1,-1}}
A16 = matrix{{1,1,1,1,-1,-1,-1,-1},{-1,-1,1,1,1,1,-1,-1},{-1,1,1,-1,1,-1,1,-1}}
A17 = matrix{{1,1,-2,1,-2,1},{-1,1,1,1,-1,-1},{1,1,1,-2,1,-2}}
A18 = matrix{{1,-3,1,1},{1,1,-3,1},{1,1,1,-3}}

-- determine matrix of lattice points regarding A1 - A18
ApolyDB = A -> (
    P = A;
    P = convexHull P;
    L = latticePoints P;
    A = matrix{L}
)

A1 = ApolyDB A1
A2 = ApolyDB A2
A3 = ApolyDB A3
A4 = ApolyDB A4
A5 = ApolyDB A5
A6 = ApolyDB A6
A7 = ApolyDB A7
A8 = ApolyDB A8
A9 = ApolyDB A9
A10 = ApolyDB A10
A11 = ApolyDB A11
A12 = ApolyDB A12
A13 = ApolyDB A13
A14 = ApolyDB A14
A15 = ApolyDB A15
A16 = ApolyDB A16
A17 = ApolyDB A17
A18 = ApolyDB A18

-- determine convex hull regarding A1 - A18
P1 = convexHull(A1)
P2 = convexHull(A2)
P3 = convexHull(A3)
P4 = convexHull(A4)
P5 = convexHull(A5)
P6 = convexHull(A6)
P7 = convexHull(A7)
P8 = convexHull(A8)
P9 = convexHull(A9)
P10 = convexHull(A10)
P11 = convexHull(A11)
P12 = convexHull(A12)
P13 = convexHull(A13)
P14 = convexHull(A14)
P15 = convexHull(A15)
P16 = convexHull(A16)
P17 = convexHull(A17)
P18 = convexHull(A18)

-- Example: P4 (polyDB) and polytope 2627 (KS database) are isomorphic
A = matrixA 2627
P = convexHull(A)
areIsomorphic(P4,P)

-- Search for smooth isomorphic pairs
for i from 480 to 500 do(
    A = matrixA i;
    P = convexHull(A);
    print(areIsomorphic(P1,P), i)
)