## access to the smooth reflexive polytopes in polyDB using Oscar

using Oscar
##
db = Polymake.Polydb.get_db()
collection = db["Polytopes.Lattice.SmoothReflexive"]
query = Dict("DIM"=>3)
res = Polymake.Polydb.find(collection, query)
Poly_list = [ Polyhedron(P) for P in res ]
##
vertices(Poly_list[1]) ## vertices of the first smooth reflexive polytope in polyDB
##

