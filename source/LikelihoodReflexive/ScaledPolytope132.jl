## Polytope 132 - ML degree 4 scaling
using HomotopyContinuation
##
@var x1 x2 x3 x4 u[1:6]
##
F = System(
    [
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6])*(1*x1*x2*x3*x4-1/108*x1*x2^2*x3*x4+1*x1*x2*x3*x4^2+1*x1*x3+1*x1*x4+1*x1*x2*x3^2*x4)-(u[1]+u[2]+u[3]+u[4]+u[5]+u[6])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6])*(1*x1*x2*x3*x4-2*1/108*x1*x2^2*x3*x4+1*x1*x2*x3*x4^2+1*x1*x2*x3^2*x4)-(u[1]+2*u[2]+u[3]+u[6])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6])*(1*x1*x2*x3*x4-1/108*x1*x2^2*x3*x4+1*x1*x2*x3*x4^2+1*x1*x3+2*1*x1*x2*x3^2*x4)-(u[1]+u[2]+u[3]+u[4]+2*u[6])
        (u[1]+u[2]+u[3]+u[4]+u[5]+u[6])*(1*x1*x2*x3*x4-1/108*x1*x2^2*x3*x4+2*1*x1*x2*x3*x4^2+1*x1*x4+1*x1*x2*x3^2*x4)-(u[1]+u[2]+2*u[3]+u[5]+u[6])
    ];
    parameters = u
)

## Solve generically:
u0 = rand(ComplexF64,6)
result_u0 = solve(F, target_parameters = u0)

## Solve for random parameter values
data = [rand((0:100000), 6)]

data_points = solve(
    F,
    solutions(result_u0),
    start_parameters =  u0,
    target_parameters = data,
)
##

