-- File to write the score equations for all 3D reflexive polytopes in a Julia file

needsPackage "ReflexivePolytopesDB"
needsPackage "Polyhedra"

polytopes = kreuzerSkarkeDim3();


-- determine matrix A with all-1-vector and nonnegative entries
matrixA = i -> (
    P = polytopes_i;
    P = matrix P;
    P = convexHull P;
    L = latticePoints P;
    L = matrix{L};
    n = numgens source L;
    d = numgens target L;
    seqn = 0..n-1;
    seqd = 0..d-1;
    listn = toList seqn;
    listd = toList seqd;
    listd = apply(listd, i -> min apply(listn, j -> L_(i,j)));
    T = matrix table(d,n,(i,j) -> -listd_i);
    L = L + T;
    E = transpose matrix vector(apply(listn, i -> 1));
    L = E||L
)


-- determine score equations
MLeqs = {coeffRing=>QQ} >> o -> (expr) -> (
    m = numgens source expr;
    R := (o.coeffRing)[vars {53..(52+(numgens target expr))},u_1..u_m];
    A := substitute(expr,R);
    U = u_1..u_m;
    U = toList U;
    U = transpose matrix {for i in U list i_R};
    N = sum for i from 1 to m list u_i;
    X := for i in 0..(m-1) list R_(entries expr_i);
    X = transpose matrix {X};
    system := ideal (A*((N * X) - U));
    return first entries gens system
)


-- write list of polynomial systems in a Julia file  

k = 0, l = 2; -- TO ADJUST: Consider polytopes k to l in one run. Choose k=l to consider a single polytope.

PolyList = for i from k to l when i < #polytopes list( MLeqs matrixA i)

polynomialjl = p -> (
    replace("_1","[1]",replace("_2","[2]",replace("_3","[3]",replace("_4","[4]",replace("_5","[5]",replace("_6","[6]",
    replace("_7","[7]",replace("_8","[8]",replace("_9","[9]",replace("_10","[10]",replace("_11","[11]",
    replace("_12","[12]",replace("_13","[13]",replace("_14","[14]",replace("_15","[15]",replace("_16","[16]",
    replace("_17","[17]",replace("_18","[18]",replace("_19","[19]",replace("_20","[20]",replace("_21","[21]",
    replace("_22","[22]",replace("_23","[23]",replace("_24","[24]",replace("_25","[25]",replace("_26","[26]",
    replace("_27","[27]",replace("_28","[28]",replace("_29","[29]",replace("_30","[30]",replace("_31","[31]",
    replace("_32","[32]",replace("_33","[33]",replace("_34","[34]",replace("_35","[35]",replace("_36","[36]",
    replace("_37","[37]",replace("_38","[38]",replace("_39","[39]",toExternalString p)))))))))))))))))))))))))))))))))))))))
)

writePolynomials = method(Options=>{})

writePolynomials (List, File) := o -> (P,f) -> (
    for i from k to l do (
    eqnCommas:=(#P#(i-k)-1):", ";
    f << "F"|i | "=" | concatenate mingle(apply(P#(i-k),e->polynomialjl e), eqnCommas)|"\n\n");
)

writePolynomials (List, String) :=o -> (P,filename) -> (
    f := openOut filename;
    writePolynomials(P,f);
    close f;
)

writePolynomials(PolyList, "filename.jl") -- TO ADJUST: Choose a name "filename" for the output file. 

