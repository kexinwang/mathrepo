==============================================================
Primary Ideals and Their Differential Equations
==============================================================

| Here may you find the supplementary codes for the paper: 
| Yairon Cid-Ruiz, Roser Homs, and Bernd Sturmfels: Primary ideals and their differential equations
| In: Foundations of computational mathematics, 21 (2021) 5, p. 1363-1399
| DOI: `10.1007/s10208-020-09485-6 <https://dx.doi.org/10.1007/s10208-020-09485-6>`_  ARXIV: https://arxiv.org/abs/2001.04700 CODE: https://mathrepo.mis.mpg.de/PrimaryIdealsandTheirDifferentialEquations

*Abstract of the paper*: An ideal in a polynomial ring encodes a system of linear partial differential equations with constant coefficients.
Primary decomposition organizes the solutions to the PDE.
This paper develops a novel structure theory for primary ideals in a polynomial ring.
We characterize primary ideals in terms of PDE, punctual Hilbert schemes, 
relative Weyl algebras, and the join construction.
Solving the PDE described by a primary ideal amounts to computing Noetherian operators in the sense of Ehrenpreis and Palamodov.
We develop new algorithms for this task, and
we present efficient implementations.

In this repository you will find the implementation of Algorithms 8.1 and 8.3 of our paper. Algorithm 8.1 computes the Noetherian operators of a primary ideal, hence it provides an integral representation of the solutions to the PDE described by the primary ideal. Algorithm 8.3 performs the reverse operation, namely computes the primary ideal from the Noetherian operators.

Examples of the performance of the algorithm and the complete code can be viewed here:

.. toctree::
   :maxdepth: 2

   Examples.rst
   Macaulay2Code.rst      

If you want to run the code yourself, you may download the following *Macaulay2* file:
:download:`noetherianOperatorsCode.m2 <noetherianOperatorsCode.m2>`  

All the examples contained in the paper and many more can be downloaded here:
:download:`Macaulay2 Examples <noetherianOperatorsExamples.m2>`   


Project page created: 13/01/2020.

Software used: Macaulay2 (Version 1.16)

Project contributors: Yairon Cid-Ruiz, Roser Homs and Bernd Sturmfels.

Code written by: Yairon Cid-Ruiz, last version 22/09/2020.

The examples in the paper were computed using Macaulay2, version 1.16, on a MacBook Pro with a 2,7 GHz Dual-Core Intel Core i5 processor and 8 GB 1867 MHz DDR3.

Corresponding author of this page: Roser Homs, homspons@mis.mpg.de.
