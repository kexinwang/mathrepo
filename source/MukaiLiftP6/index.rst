=======================================
Mukai lifting of self-dual points in ℙ⁶
=======================================

| This page contains auxiliary code, data and visualizations to the paper:
| Barbara Betti and Leonie Kayser: Mukai lifting of self-dual points in :math:`\mathbb{P}^6`
| ARXIV: https://arxiv.org/abs/2406.02734 CODE: https://mathrepo.mis.mpg.de/MukaiLiftP6 

.. contents:: Table of Contents
  :local:

Overview
--------

**Abstract.** A set of 2n points in :math:`\mathbb{P}^{n-1}` is self-dual if it is invariant under the Gale transform. Motivated by Mukai's work on canonical curves, Petrakiev showed that a general self-dual set of 14 points in :math:`\mathbb{P}^6` arises as the intersection of the Grassmannian :math:`\mathrm{Gr}(2,6)` in its Plücker embedding in :math:`\mathbb{P}^{14}` with a linear space of dimension 6. In this paper we focus on the inverse problem of recovering such a linear space associated to a general self-dual set of points. We use numerical homotopy continuation to approach the problem and implement an algorithm in Julia to solve it. Along the way we also implement the forward problem of slicing Grassmannians and use it to experimentally study the real solutions to this problem.

On this page you can find the code and data used to prove Theorem 5.1 and the implementation of numerical symmetric tensor decomposition using eigenvalue methods in Julia.
We also indicate how to verify and reproduce our computational claims.
Additionally we show some visualizations for the case example of 18 points in the plane and of the saturation gaps.

* Julia source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`
* Julia example notebook :download:`example.ipynb <example.ipynb>`

Slicing using homotopy continuation
-----------------------------------

The computational approach to slicing the Grassmannian :math:`{\rm Gr}(2,6) \subseteq \mathbb{P}^{14}` is detailed in section 3 of the paper.
The implementation can be found in the file ``Slicing.jl`` in the source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`.

Lifting using homotopy continuation
-----------------------------------

The computational approach to the Mukai lifting problem is detailed in section 4 of the paper.
The implementation can be found in the file ``MukaiLiftP6/src/Lifting.jl`` in the source code :download:`MukaiLiftSource.zip <MukaiLiftSource.zip>`, there is also an instructive notebook :download:`example.ipynb <example.ipynb>`. You can view the notebook in your browser on the following subpage:


.. toctree::
    :maxdepth: 1
    :glob:

    example.ipynb

------------------------------------------------------------------------------------------------------------------------------


| Project page created: 13/6/2024
| Project contributors: Barbara Betti, Leonie Kayser
| Corresponding author of this page: Barbara Betti, betti@mis.mpg.de

| Software used: Julia 1.9.3, OSCAR 1.0.3, HomotopyContinuation 2.9.3

| System used: MPI MiS compute server with 512 GB RAM using 2x 12-Core Intel Xeon E5-2680 v3 processor at 2.50 GHz

| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)
| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
| Last updated 17/6/2024
