==========
Problem 14
==========

.. compday_problem14:
   
Study the hypersimplex :math:`\Delta(6,3)`. Find its f-vector, volume,  toric ideal, Erhart polynomial.
List all coarsest and finest matroid subdivisions.

