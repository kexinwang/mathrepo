===============
Computation Day
===============

The following twenty challenges were posed in the very first *Computation Day*, July 17 2017, at the MPI MIS.

.. toctree::
   :maxdepth: 2

   problem1.rst
   problem2.rst
   problem3.rst
   problem4.rst
   problem5.rst
   problem6.rst
   problem7.rst
   problem8.rst
   problem9.rst
   problem10.rst
   problem11.rst
   problem12.rst
   problem13.rst
   problem14.rst
   problem15.rst
   problem16.rst
   problem17.rst
   problem18.rst
   problem19.rst
   problem20.rst
   open_problems.rst
   comments.rst

Software used: Magma, Mathematica, Matlab, Polymake, SageMath, GAP