========
Comments
========

Comments by Michael Joswig:
===========================

Given a so-called Wronski-system, i.e. :math:`d` polynomials in :math:`d` variables such that

  - all polynomials have the same Newton polytope
  - the Newton polytope has a regular triangulation whose dual graph is bipartite,   
    which implies that the vertices are :math:`d+1` colorable and the facet are 2 colorable
    (say black and white), and the coefficients are constant on the :math:`d+1` vertex colors.
    
Example
-------

Two polynomials of the form :math:`a(1+xy)+b(x+y^2)+c(y+x^2)`,
whose Newton polytope is a simplex of side length 2.

Then Soprunova and Sotile (2006) showed that the number of real solutions to that system
is at least the signature of that coloring 

.. math::
   
  \left(= | \# {\rm black\ facets} - \# {\rm white\ facets} | \right).
  
This is done by constructing a special map whose degree is the signature, 
which yields that many zeroes.

The goal is to find a algorithm to compute exactly those solutions, given a Wronski-system,
for the case :math:`d=2`. This can be regarded as a real homotoy algorithm.
