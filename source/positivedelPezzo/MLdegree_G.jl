#This code computes the ML degree of the Goepel parametrization
#How to obtain the correct system, inparticular the file matrix_for_G is explained in the file sytem_for_MLdegree_G.jl
using HomotopyContinuation
#We need 6 variables (7, but we dehomogenize with respect to the first), and 134 parameters (135, but as they have to sum to zero, the last one is determined by the first 134)
@var y[1:6] s[1:134];
d = vcat([1],y);
v = vcat(s,[-sum(s[i] for i in 1:134)]);
#We generate the matrix E7 that contains the coefficients for the 63 roots in the root system E7.
E7=[]
#we need these indexsets
ba37 = bases(uniform_matroid(3,7));
ba27 = bases(uniform_matroid(2,7));
ba67 = [bases(uniform_matroid(6,7))[8-i] for i in 1:7];
for v in ba27
    w = [0 for i in 1:7]
    w[v[1]]=1
    w[v[2]]=-1
    push!(E7,w)
end
for v in ba37
    w = [0 for i in 1:7]
    for i in 1:7 
        if i in v 
            w[i]=1
        end
    end
    push!(E7,w)
end
for v in ba67
    w = [0 for i in 1:7]
    for i in 1:7 
        if i in v 
            w[i]=1
        end
    end
    push!(E7,w)
end
#the Goeple variety arises from the reflection hyperplane arrangement of type E7. This has 63 hyperplanes in P^6. These are the hyperplanes in our log likelihood function. Thus, we get the coordinates depending on the variables d[1:6] as follows:
l=[log(transpose(E7[i])*d) for i in 1:length(E7)];
# In the next step we need to assign the 135 parameters in the correct way to each coordinate. This mirrors the monomial map from P^62 to P^134. How to obtain this, is explained in system_for_MLegree_G.jl
O=[]
inputfile ="matrix_for_G"
open(inputfile,"r") do file
    for b in eachline(inputfile)
        push!(O,eval(Meta.parse(b)));
    end
end
L=(sum(l[i]*(sum(O[i][j]*v[j] for j in 1:135)) for i in 1:63))
F = System(differentiate(L,y),parameters = s)
r=monodromy_solve(F)
c=certify(F, r) 