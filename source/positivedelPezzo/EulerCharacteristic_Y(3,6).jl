# We first construct the terms of the log-likelihood function for Y(3,6)
# Of the 6 points in P^2 we can fix a projective basis, so after scaling only 4 variables remain.
using Oscar
R, z = PolynomialRing(QQ,["a","b","c","d"]);
#we first add the nonzero minors, i.e. the non-constant Pluecker coordinates
M = matrix(R,[1 0 0 1 1 1; 0 1 0 1 z[1] z[2] ; 0 0 1 1 z[3] z[4]]);
P = minors(M,3)
T=[]
for p in P 
    if !is_constant(p)
        append!(T,[p])
    end
end
#now we add the conic condition
V = zero_matrix(R, 6,6)
for i in 1:6
    c=1
    for j in 1:3
        for k in j:3
        V[c,i] = M[j,i]*M[k,i]
        c = c+1
        end
    end
end
PV = minors(V,6)
for p in PV 
    if !is_constant(p)
        append!(T,[p])
    end
end
println(T)
#The output is: 
#Any[c, -a, -a + c, -c + 1, a - 1, d, -b, -b + d, -d + 1, b - 1, a*d - b*c, c - d, -a + b, a*d - a - b*c + b + c - d, -a*b*c + a*b*d + a*c*d - a*d - b*c*d + b*c]

#Now we change to HomotopyContinuation and copy the output of T into the vector F below
using HomotopyContinuation
@var a b c d  s[1:15]
F = [c, -a, -a + c, -c + 1, a - 1, d, -b, -b + d, -d + 1, b - 1, a*d - b*c, c - d, -a + b, a*d - a - b*c + b + c - d, -a*b*c + a*b*d + a*c*d - a*d - b*c*d + b*c]
S= sum(log(F[i])*s[i] for i in 1:15) #this is the log-likelihood function, the s[i] are the parameters
L = System(differentiate(S,[a,b,c,d]),parameters=s) 
r = monodromy_solve(L)
c = certify(L,r)
