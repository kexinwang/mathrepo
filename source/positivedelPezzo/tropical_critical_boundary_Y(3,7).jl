#This code computes the tropical critical points for the soft limit degeneration of Y(3,7) with respect to its boundary in X(3,7), by using degeneration with respect to only the 7 co-conic conditions.
#This code is adapted from the Tropical solutions to the scattering equations for X(3,7) from the project Likelihood degenerations by Agostini et al., which can be found here: https://mathrepo.mis.mpg.de/LikelihoodDegenerations/index.html

### A few functions we need 
function gradient(S,x)
    System(
        vec(differentiate(S, [x...])),
        variables = [x...],
        parameters = setdiff(variables(S), [x...]),
    )
end

function lagrange_equations(f,Vars)
    L = f
    ∇L = gradient(L,vec(Vars))
    return ∇L
end


##Function for the display of the collection of tropical critical points at the end
#how to use tally: input the list of all tropical critical points, it will collect the same points and record their multiplicities.
function tally(S) 
    D=Dict{Any,Int}()
    for s in S
        D[s]=get(D,s,0)+1
    end
    return D
end

#Function Rico: this computes the direction vector of the approximated line log(|p^_i(t')|) = log(|c_i|) +q^_i*log(t') (see Section 8 in Likelihood Degenerations)
# so the return is one coordinate of the tropical critical point
function Rico(v,target_params)
    l = [log(tp[1]) for tp ∈ target_params] #target_params are the t' we are sampling from (0,1]
        #v are the log(|p^_i(t')|)
    coeffmtx = [ones(length(l)) l]
    rhs = v
    x = coeffmtx\rhs #this solves the equation coeffmtx*x = rhs
        r=nothing
        ##Things may be so numerically unstable that x is not a number.
        ## This occurs if we tried to get too close to soft_limit=0
        ## If that's the case, take fewer samples toward soft_limit->0
        ## and try again.
        if string(x[2])!="NaN"
                r=round(Int,x[2])
        else
                r=Rico(v[1:length(v)-5],target_params[1:length(v)-5])
        end
    return r
end

#function to determine the valuation of a vector indexed by the non constant bases 
#(this function applies the Rico function from above)
# Takes a single soft_limit->0 sampling of a solution and computes the valuation.
# determine_valuation returns the tropical critical point that we obtain for one solution p^ (this is sollist)
# the output is one tropical critical point
#ATTENTION: This function is different from the one employed for the classical soft limit degeneration of Y(3,7), as the form of the interpolation matrix depends on the system the technique is applied to!
function determine_valuation(sollist,t_param_list)
        t_param_list=t_param_list[1:length(sollist)] #t_param_list are the t' we are sampling from (0,1]
        interpolmtx = fill(0.0,0,length(ba)+7)
        for sol ∈ sollist
                ##MM is the solution as a matrix
                MM = HomotopyContinuation.evaluate.(M,vec(F.variables) => sol[1:length(vec(F.variables))])
                MM = Matrix{ComplexF64}(MM) #equals the parametrization with substituted solutions instead of the variables
                ##we have to add the co-conic conditions
                PVeval=HomotopyContinuation.evaluate.(PV,vec(F.variables) => sol[1:length(vec(F.variables))])
                PVeval = Vector{ComplexF64}(PVeval)
                interpolmtx = vcat(interpolmtx,hcat(transpose([log(abs(det(MM[:,ii]))) for ii in ba]), transpose([log(abs(PVeval[ii])) for ii in 1:length(PV)])))
        end
        vals = [Rico(interpolmtx[:,i],t_param_list) for i = 1:length(ba)+7]
        return(vals)
end

###START OF COMPUTATIONS OF TROPICAL CRITICAL POINTS FOR Y(3,7)
# First we need the log likelihood function of Y(3,7)
using Oscar
ba = bases(uniform_matroid(3,7)); #this gives us the set of bases of the underlying Matroid 
using HomotopyContinuation
#We parametrize the 7 points by picking a projective basis and variables for the remaining 3
a = [Variable(:a,i) for i in 1:6];
M = Matrix([1 0 0 1 1 1 1; 0 1 0 1 a[1] a[2] a[3]; 0 0 1 1 a[4] a[5] a[6]]);
V = Matrix(undef,6,7);
for i in 1:7
    c=1
    for j in 1:3
        for k in j:3
        V[c,i] = M[j,i]*M[k,i]
        c = c+1
        end
    end
end
helpset = [[i for i in 1:7 if i != j] for j in 1:7];
V = Matrix{Expression}(V);
PV = [det(V[:,v]) for v in helpset]; #these are the 7 co-conic conditions
#we need the parameters for the log likelihood function
#for the Pluecker coordinates
s =[Variable(:s,ℓ...) for ℓ in ba];
VarDict=Dict{typeof(ba[1]),typeof(s[1])}();
for i in 1:length(ba)
    VarDict[ba[i]]=s[i]
end
#for the conic conditions
u = [Variable(:u,i) for i in 1:length(PV)];#new additional variables in the length of PV

S = sum([VarDict[I]*log(det(M[:,I])) for I in ba]) + sum(log(PV[i])*u[i] for i in 1:length(PV)); #log likelihood function for Y(3,7)
paramet = vcat(s,u); # we collect all the parameters
F = lagrange_equations(S,a) ;#this gives us the maximum likelihood equations, i.e. we differentiate w.r.t. a

(k,n) = size(M);#to simplify some of the notation

#the index sets are the basis of the uniform matroid, here ba as computed above by using OSCAR
indexSets = ba;

#we solve the original system (which later is used as the point where τ = 1)
# remove the target_solutions_count to verify the number 3600 as the number of total solutions, this additional option speeds the computation up
MonSolve = monodromy_solve(F,target_solutions_count = 3600);
Params = MonSolve.parameters;
Sols = solutions(MonSolve);
# Sols is a vector of length 3600 where each entry is a vector of length 6 giving the coordinates for a[i] in the matrix M 
# Evaluating the conic condition for any of these values should not give zero.
####HERE IS A TEST FOR THIS
##conic_Cond = []
##for i in 1:3600
##    push!(conic_Cond, [HomotopyContinuation.evaluate.(PV,vec(F.variables)=>Sols[i])])
##end
##boo = true
##for v in conic_Cond
##    for i in v
##       boo = boo && i != 0
##    end
##    end
##boo



## Now the degeneration part starts
#we degenerate with respect to the 7 co-conic conditions, i.e. only the parameter of the co-conic condition is multiplied with τ, then we let τ->0.
@var τ;
degen = vcat([1 for i in 1:35],[τ,τ,τ,τ,τ,τ,τ]); # this gives 1272+7*312+144(=6*24)
par = vcat([VarDict[I] for I in ba],[u[i] for i in 1:7]) ;
limit_eqs = subs(F.expressions,par=>[par[i]*degen[i] for i in 1:length(par)]);
soft_limit_system = System(limit_eqs,variables = F.variables, parameters = vcat(F.parameters,τ)); #the new limit system of which we need the critical points 

soft_start_params=vcat(Params,1); #the Parameters and Solutions of the solve of the original system F is used as the start point for t=1
target_params_in_t = [10.0^(-k) for k=2:0.1:6]; #sample list for the values of τ as τ->0.
soft_target_params=[vcat(Params,t) for t in target_params_in_t]; # we use the original starting parameters and add the different values for t, so we have a list of parameters to go through mirroring the limit process 
soft_limit_sys = soft_limit_system ;

#we create a list of solutions one for each value of τ in target_params_in_t
#this needs the solutions over the complex numbers computed above.
samples = HomotopyContinuation.solve(soft_limit_sys,Sols,
start_parameters = soft_start_params,
target_parameters = soft_target_params);

# as τ->0 not each value might give a solution, so we sort the bad ones out
samples_per_solution=[];
for i in 1:length(Sols)
	not_lost=true 
	t_num=0 #counter for the number of t' we are running through
	single_solution_sample=[]
	while not_lost==true && t_num<length(target_params_in_t)
		t_num+=1
		#single_t_result is all the solutions at time target_parameters_in_t[t_num]
		i_path_result = samples[t_num][1].path_results[i]
		if i_path_result.return_code == :success
			push!(single_solution_sample,i_path_result.solution)
		else
			not_lost=false
		end
	end
	push!(samples_per_solution,single_solution_sample)
end
(sample_list,t_param_list) = (samples_per_solution,target_params_in_t);
#The above tuple is a list S of lists, where each element s of S corresponds to a single solution, and the elements of s correspond to each successive soft_limit value of that path.



#Now we want to determine the tropical critical points, i.e. we need to approximate the log of the solutions along the degeneration of t by a line through (log t', log|p^_i(t')|), the slope is our tropical coordinate 
#The main work here is done by determine_valuation
#This function works for one solution at a time, doing all coordinates of it.
valuation_list = [];
for i in 1:length(sample_list)
    if Int(floor(i/1000)) == i/1000
        println("Progress: at valuation number ",i)
        flush(stdout)
    end
    s=sample_list[i]
    push!(valuation_list,determine_valuation(s,t_param_list))
end
#the dictionary sorting the tropical solutions and assigning the multiplicities
T = tally(valuation_list)

##how to read the output:  A tropical critical point in this setting will have 42 coordinates. The first part is a point in the Grassmannian, i.e. it has 35 coordinates in our case
# The 7 additional coordinates are given by the 7 co-conic conditions.
# The degeneration introduces a parameter t to some of the coordinates (here only the 7 corresponding to the co-conic conditions). 
# The tropical critical point now tells us, how each coordinate of the classical critical point depends on t.