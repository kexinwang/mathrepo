#This code computes the tropical critical points for the soft limit degeneration of Y(3,6).
#This code is adapted from the Tropical solutions to the scattering equations for X(3,7) from the project Likelihood degenerations by Agostini et al., which can be found here: https://mathrepo.mis.mpg.de/LikelihoodDegenerations/index.html

### A few functions we need 
function gradient(S,x)
    System(
        vec(differentiate(S, [x...])),
        variables = [x...],
        parameters = setdiff(variables(S), [x...]),
    )
end

function lagrange_equations(f,Vars) 
    L = f
    ∇L = gradient(L,vec(Vars))
    return ∇L
end

#how to use tally: input the list of all tropical critical points, it will collect the same points and record their multiplicities.
function tally(S) 
    D=Dict{Any,Int}()
    for s in S
        D[s]=get(D,s,0)+1
    end
    return D
end

#Function Rico: this computes the direction vector of the approximated line log(|p^_i(t')|) = log(|c_i|) +q^_i*log(t') (see Section 8 in "Likelihood Degenerations")
function Rico(v,target_params) 
    l = [log(tp[1]) for tp ∈ target_params] #target_params are the t' we are sampling from (0,1]
        #v are the log(|p^_i(t')|)
    coeffmtx = [ones(length(l)) l]
    rhs = v
    x = coeffmtx\rhs #this solves the equation coeffmtx*x = rhs
        r=nothing
        ##Things may be so numerically unstable that x is not a number.
        ## This occurs if we tried to get too close to the soft_limit=0
        ## If that's the case, take fewer samples toward soft_limit->0
        ## and try again.
        if string(x[2])!="NaN"
                r=round(Int,x[2])
        else
                r=Rico(v[1:length(v)-5],target_params[1:length(v)-5]) 
        end
    return r
end

#function to determine the valuation of a vector indexxed by the non constant bases 
#(this function applies the Rico function from above)
#ATTENTION: This function is different from the one employed for the boundary of Y(3,6) in X(3,6), as the form of the interpolation matrix depends on the system the technique is applied to!
function determine_valuation(sollist,t_param_list)
        #non_constant_bases = filter(x-> (x ⊆ collect(1:size(M)[1]+1)) == false,ba)#take out bases which are constant
        t_param_list=t_param_list[1:length(sollist)] #t_param_list are the t' we are sampling from (0,1]
        interpolmtx = fill(0.0,0,length(ba)+1)
        for sol ∈ sollist
                ##MM is the solution as a matrix
                MM = HomotopyContinuation.evaluate.(M,vec(F.variables) => sol[1:length(vec(F.variables))])
                MM = Matrix{ComplexF64}(MM) #equals the parametrization with substituted solutions instead of the variables
                ##we have to add the conic condition
                PVeval=HomotopyContinuation.evaluate.(PV,vec(F.variables) => sol[1:length(vec(F.variables))])
                PVeval = Vector{ComplexF64}(PVeval)
                interpolmtx = vcat(interpolmtx,hcat(transpose([log(abs(det(MM[:,ii]))) for ii in ba]), transpose([log(abs(PVeval[ii])) for ii in 1:length(PV)])))#det(MM[:,ii]) are exactly the p^_i(t')
        end
        vals = [Rico(interpolmtx[:,i],t_param_list) for i = 1:length(ba)+1]
        return(vals)
end

###START OF COMPUTATIONS OF TROPICAL CRITICAL POINTS FOR Y(3,6)
# First we need the log likelihood function of Y(3,6)
using Oscar
ba = bases(uniform_matroid(3,6));
using HomotopyContinuation
a = [Variable(:a,i) for i in 1:4];
M = Matrix([1 0 0 1 1 1; 0 1 0 1 a[1] a[2]; 0 0 1 1 a[3] a[4]]);
V = Matrix(undef,6,6);
for i in 1:6
    c=1
    for j in 1:3
        for k in j:3
        V[c,i] = M[j,i]*M[k,i]
        c = c+1
        end
    end
end
V = Matrix{Expression}(V);
PV = [det(V)] ;#this is the co-conic condition
s =[Variable(:s,ℓ...) for ℓ in ba];
VarDict=Dict{typeof(ba[1]),typeof(s[1])}();
for i in 1:length(ba)
    VarDict[ba[i]]=s[i]
end
u = [Variable(:u,i) for i in 1:length(PV)];#new additional variables in the length of PV
S = sum([VarDict[I]*log(det(M[:,I])) for I in ba]) + sum(log(PV[i])*u[i] for i in 1:length(PV)) ;#log likelihood function for Y(3,6), this is the template for the analogous computation for Y(3,7), which is hy we don't use hard coded length(PV)=1
paramet = vcat(s,u); #all the parameters in the log likelihood function
F = lagrange_equations(S,a);

(k,n) = size(M);

indexSets = ba;

#we degenerate with respect to the last particle (i.e. every term indexed by [ . . 6]) and the co-conic condition
#we first collect the parameters for the Pluecker coordinates indexed by [ . . 6] and then append the parameter for the co-conic condition u
last_particle_dependent = filter(ℓ -> maximum(ℓ)==n,indexSets);
lpd_parameters = vcat([VarDict[I] for I in last_particle_dependent],u);
@var τ # we will degenerate with respect to τ -> 0
soft_limit_eqs = subs(F.expressions,lpd_parameters=>τ*lpd_parameters); #change the parameters from s to s*t
soft_limit_system = System(soft_limit_eqs,variables = F.variables, parameters = vcat(F.parameters,τ));
MonSolve = monodromy_solve(F); # we solve the system first over the complex numbers
#collect parameters and solutions
Params = MonSolve.parameters ;
Sols = solutions(MonSolve);


## Now the degeneration part starts

soft_start_params=vcat(Params,1);
target_params_in_t = [10.0^(-k) for k=2:0.1:6]; #sample list for the values of τ as τ->0.
soft_target_params=[vcat(Params,t) for t in target_params_in_t];
soft_limit_sys = soft_limit_system ;

#we create a list of solutions one for each value of τ in target_params_in_t
#this needs the solutions over the complex numbers computed above.
samples = HomotopyContinuation.solve(soft_limit_sys,Sols,
start_parameters = soft_start_params,
target_parameters = soft_target_params) ;

# as τ->0 not each value might give a solution, so we sort the bad ones out
samples_per_solution=[];
for i in 1:length(Sols)
	not_lost=true 
	t_num=0 #counter for the number of t' we are running through
	single_solution_sample=[]
	while not_lost==true && t_num<length(target_params_in_t)
		t_num+=1
		#single_t_result is all the solutions at time target_parameters_in_t[t_num]
		i_path_result = samples[t_num][1].path_results[i]
		if i_path_result.return_code == :success
			push!(single_solution_sample,i_path_result.solution)
		else
			not_lost=false
		end
	end
	push!(samples_per_solution,single_solution_sample)
end
(sample_list,t_param_list) = (samples_per_solution,target_params_in_t);


#Take the sample list and use it to approximate the valuations using determine_valuation function from the start, then collect and tally the results.
valuation_list = [];
for i in 1:length(sample_list)
    if Int(floor(i/1000)) == i/1000
        println("Progress: at valuation number ",i)
        flush(stdout)
    end
    s=sample_list[i]
    push!(valuation_list,determine_valuation(s,t_param_list))
end
T = tally(valuation_list) #this is the final result of the tropical critical points and their multiplicities