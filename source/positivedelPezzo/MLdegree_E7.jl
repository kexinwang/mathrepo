## This file containes the code to compute the number of critical points of the log likelihood function corresponding to the hyperplane arrangement E7, or equivalently the ML degree of the very affine variety given by the hyperplane arrangement E7 in 7-dimensional projective space
# Set-up/How to use:  The coordinates in 7-dimensional projective space are d_1 to d_7.
    # - Decide which of the d_i you want to fix (you can set one of the d_i to 1 because of projectivity). The default in the code is d_1=1.
    # - The not-determined coordinates d_i  are collected in the multivariable x.
    # - By using the intermediate vector d, we can choose exactly which coordinate is set to one, by placing the 1 between the relevant variables x[i].
    # - The log likelihood function has 63 terms in the d[i] (given by the equations of the hyperplanes) with 63 parameters.
    # - These parameters have to sum to zero, so while we have 63 terms in the log likelihood function, we only have 62 parameters, collected in s[1:62], as the last one is the negative sum of all.
    # - We collect the 62 parameters and the negative sum of all in the vector v.
@var x[1:6] s[1:62]
d = vcat([1],x)
v = vcat(s,[-sum(s[i] for i in 1:62)])
# We build the log likelihood function by its different types of hyperplanes 
l1 = sum(sum(log(d[j]-d[i])*v[i-j+(j-1)*6] for i in j+1:7) for j in 1:2) + sum(sum(log(d[j]-d[i])*v[11+i-j+(j-3)*4] for i in j+1:7) for j in 3:4) + sum(sum(log(d[j]-d[i])*v[18+i-j+(j-5)*2] for i in j+1:7) for j in 5:6)
l2 = sum(sum(log(d[1]+d[j]+d[i])*v[21+i-j+(j-2)*5] for i in j+1:7) for j in 2:3) + sum(sum(log(d[1]+d[j]+d[i])*v[30+i-j+(j-4)*3] for i in j+1:7) for j in 4:5)  + sum(sum(log(d[1]+d[j]+d[i])*v[35+i-j+(j-6)*5] for i in j+1:7) for j in 6:6)
l3 = sum(sum(log(d[2]+d[j]+d[i])*v[36+i-j+(j-3)*4] for i in j+1:7) for j in 3:4) + sum(sum(log(d[2]+d[j]+d[i])*v[43+i-j+(j-5)*2] for i in j+1:7) for j in 5:6) 
l4 = sum(sum(log(d[3]+d[j]+d[i])*v[46+i-j+(j-4)*3] for i in j+1:7) for j in 4:5) + sum(sum(log(d[3]+d[j]+d[i])*v[51+i-j+(j-6)*5] for i in j+1:7) for j in 6:6) 
l5 = sum(sum(log(d[4]+d[j]+d[i])*v[52+i-j+(j-5)*2] for i in j+1:7) for j in 5:6) +log(d[5]+d[6]+d[7])*v[56]
l6 = sum(log((sum(d[i] for i in 1:7) )-d[j])*v[56+j] for j in 1:7)
L = l1+l2+l3+l4+l5+l6 # this is the finished log likelihood function for the hyperplane arrangment of type E7
F = System(differentiate(L,x),parameters = s) 
r=monodromy_solve(F)
c=certify(F, r)

