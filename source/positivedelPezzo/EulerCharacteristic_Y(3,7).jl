# We first construct the terms of the log-likelihood function for Y(3,7)
# Of the 7 points in P^2 we can fix a projective basis, so after scaling only 6 variables remain.
using Oscar
R, z = PolynomialRing(QQ,["a","b","c","d","e","f"]);
#we first add the nonzero minors, i.e. the non-constant Pluecker coordinates
M = matrix(R,[1 0 0 1 1 1 1; 0 1 0 1 z[1] z[2] z[3]; 0 0 1 1 z[4] z[5] z[6]]);
P = minors(M,3)
T=[]
for p in P 
    if !is_constant(p)
        append!(T,[p])
    end
end
#now we add the conic conditions
V = zero_matrix(R, 6,7)
for i in 1:7
    c=1
    for j in 1:3
        for k in j:3
        V[c,i] = M[j,i]*M[k,i]
        c = c+1
        end
    end
end
PV = minors(V,6)
for p in PV 
    if !is_constant(p)
        append!(T,[p])
    end
end
println(T)
#The output is: 
#Any[d, -a, -a + d, -d + 1, a - 1, e, -b, -b + e, -e + 1, b - 1, a*e - b*d, d - e, -a + b, a*e - a - b*d + b + d - e, f, -c, -c + f, -f + 1, c - 1, a*f - c*d, d - f, -a + c, a*f - a - c*d + c + d - f, b*f - c*e, e - f, -b + c, b*f - b - c*e + c + e - f, a*e - a*f - b*d + b*f + c*d - c*e, -a*b*d + a*b*e + a*d*e - a*e - b*d*e + b*d, -a*c*d + a*c*f + a*d*f - a*f - c*d*f + c*d, -b*c*e + b*c*f + b*e*f - b*f - c*e*f + c*e, -a*b*d*f + a*b*e*f + a*c*d*e - a*c*e*f - b*c*d*e + b*c*d*f, a*b*d*f^2 - a*b*d*f - a*b*e*f^2 + a*b*e*f - a*c*d*e^2 + a*c*d*e + a*c*e^2*f - a*c*e*f + a*d*e^2*f - a*d*e*f^2 - a*e^2*f + a*e*f^2 + b*c*d^2*e - b*c*d^2*f - b*c*d*e + b*c*d*f - b*d^2*e*f + b*d^2*f + b*d*e*f^2 - b*d*f^2 + c*d^2*e*f - c*d^2*e - c*d*e^2*f + c*d*e^2, -a^2*b*c*e + a^2*b*c*f + a^2*b*e*f - a^2*b*f - a^2*c*e*f + a^2*c*e + a*b^2*c*d - a*b^2*c*f - a*b^2*d*f + a*b^2*f - a*b*c^2*d + a*b*c^2*e + a*b*d*f - a*b*e*f + a*c^2*d*e - a*c^2*e - a*c*d*e + a*c*e*f + b^2*c*d*f - b^2*c*d - b*c^2*d*e + b*c^2*d + b*c*d*e - b*c*d*f, -a*b*d*f + a*b*d + a*b*e*f - a*b*e + a*c*d*e - a*c*d - a*c*e*f + a*c*f - a*d*e + a*d*f + a*e - a*f - b*c*d*e + b*c*d*f + b*c*e - b*c*f + b*d*e - b*d - b*e*f + b*f - c*d*f + c*d + c*e*f - c*e]

#Now we change to HomotopyContinuation and copy the output of T into the vector F below
using HomotopyContinuation
@var a b c d e f s[1:35]
F = [d, -a, -a + d, -d + 1, a - 1, e, -b, -b + e, -e + 1, b - 1, a*e - b*d, d - e, -a + b, a*e - a - b*d + b + d - e, f, -c, -c + f, -f + 1, c - 1, a*f - c*d, d - f, -a + c, a*f - a - c*d + c + d - f, b*f - c*e, e - f, -b + c, b*f - b - c*e + c + e - f, a*e - a*f - b*d + b*f + c*d - c*e, -a*b*d + a*b*e + a*d*e - a*e - b*d*e + b*d, -a*c*d + a*c*f + a*d*f - a*f - c*d*f + c*d, -b*c*e + b*c*f + b*e*f - b*f - c*e*f + c*e, -a*b*d*f + a*b*e*f + a*c*d*e - a*c*e*f - b*c*d*e + b*c*d*f, a*b*d*f^2 - a*b*d*f - a*b*e*f^2 + a*b*e*f - a*c*d*e^2 + a*c*d*e + a*c*e^2*f - a*c*e*f + a*d*e^2*f - a*d*e*f^2 - a*e^2*f + a*e*f^2 + b*c*d^2*e - b*c*d^2*f - b*c*d*e + b*c*d*f - b*d^2*e*f + b*d^2*f + b*d*e*f^2 - b*d*f^2 + c*d^2*e*f - c*d^2*e - c*d*e^2*f + c*d*e^2, -a^2*b*c*e + a^2*b*c*f + a^2*b*e*f - a^2*b*f - a^2*c*e*f + a^2*c*e + a*b^2*c*d - a*b^2*c*f - a*b^2*d*f + a*b^2*f - a*b*c^2*d + a*b*c^2*e + a*b*d*f - a*b*e*f + a*c^2*d*e - a*c^2*e - a*c*d*e + a*c*e*f + b^2*c*d*f - b^2*c*d - b*c^2*d*e + b*c^2*d + b*c*d*e - b*c*d*f, -a*b*d*f + a*b*d + a*b*e*f - a*b*e + a*c*d*e - a*c*d - a*c*e*f + a*c*f - a*d*e + a*d*f + a*e - a*f - b*c*d*e + b*c*d*f + b*c*e - b*c*f + b*d*e - b*d - b*e*f + b*f - c*d*f + c*d + c*e*f - c*e]
S = sum(log(F[i])*s[i] for i in 1:35)
L = System(differentiate(S,[a,b,c,d,e,f]),parameters = s)
r = monodromy_solve(L) 
c = certify(L,r)
