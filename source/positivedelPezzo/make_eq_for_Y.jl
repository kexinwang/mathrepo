#Here we construct the system needed to compute the ML degree of the Yoshida parametrization.
# The 40 coordinates in P^39 correspond to degree 6 monomials arising from products of the roots of E6
# These roots can be denoted in the bracket notation as below. This notation is introduced for example in "The Universal Kummer Threefold".
#Each entry in the Vector V contains the information to build one of the 40 degree 6 monomials.
using Oscar
V=[[[1,3,4],[2,3,4],[3,5,6],[4,5,6],[5,1,2],[6,1,2]],
[[1,5,6],[2,5,6],[5,3,4],[6,3,4],[3,1,2],[4,1,2]],
[[1,3,5],[2,3,5],[3,4,6],[5,4,6],[4,1,2],[6,1,2]],
[[1,4,6],[2,4,6],[4,3,5],[6,3,5],[3,1,2],[5,1,2]],
[[1,3,6],[2,3,6],[3,4,5],[6,4,5],[4,1,2],[5,1,2]],
[[1,4,5],[2,4,5],[4,3,6],[5,3,6],[3,1,2],[6,1,2]],
[[1,2,4],[3,2,4],[2,5,6],[4,5,6],[5,1,3],[6,1,3]],
[[1,5,6],[3,5,6],[5,2,4],[6,2,4],[2,1,3],[4,1,3]],
[[1,2,5],[3,2,5],[2,4,6],[5,4,6],[4,1,3],[6,1,3]],
[[1,4,6],[3,4,6],[4,2,5],[6,2,5],[2,1,3],[5,1,3]],
[[1,2,6],[3,2,6],[2,4,5],[6,4,5],[4,1,3],[5,1,3]],
[[1,4,5],[3,4,5],[4,2,6],[5,2,6],[2,1,3],[6,1,3]],
[[1,2,3],[4,2,3],[2,5,6],[3,5,6],[5,1,4],[6,1,4]],
[[1,5,6],[4,5,6],[5,2,3],[6,2,3],[2,1,4],[3,1,4]],
[[1,2,5],[4,2,5],[2,3,6],[5,3,6],[3,1,4],[6,1,4]],
[[1,3,6],[4,3,6],[3,2,5],[6,2,5],[2,1,4],[5,1,4]],
[[1,2,6],[4,2,6],[2,3,5],[6,3,5],[3,1,4],[5,1,4]],
[[1,3,5],[4,3,5],[3,2,6],[5,2,6],[2,1,4],[6,1,4]],
[[1,2,3],[5,2,3],[2,4,6],[3,4,6],[4,1,5],[6,1,5]],
[[1,4,6],[5,4,6],[4,2,3],[6,2,3],[2,1,5],[3,1,5]],
[[1,2,4],[5,2,4],[2,3,6],[4,3,6],[3,1,5],[6,1,5]],
[[1,3,6],[5,3,6],[3,2,4],[6,2,4],[2,1,5],[4,1,5]],
[[1,2,6],[5,2,6],[2,3,4],[6,3,4],[3,1,5],[4,1,5]],
[[1,3,4],[5,3,4],[3,2,6],[4,2,6],[2,1,5],[6,1,5]],
[[1,2,3],[6,2,3],[2,4,5],[3,4,5],[4,1,6],[5,1,6]],
[[1,4,5],[6,4,5],[4,2,3],[5,2,3],[2,1,6],[3,1,6]],
[[1,2,4],[6,2,4],[2,3,5],[4,3,5],[3,1,6],[5,1,6]],
[[1,3,5],[6,3,5],[3,2,4],[5,2,4],[2,1,6],[4,1,6]],
[[1,2,5],[6,2,5],[2,3,4],[5,3,4],[3,1,6],[4,1,6]],
[[1,3,4],[6,3,4],[3,2,5],[4,2,5],[2,1,6],[5,1,6]],
[[1,2,3],[4,5,6],[1,2,3,4,5,6]],
[[1,2,4],[3,5,6],[1,2,3,4,5,6]],
[[1,2,5],[3,4,6],[1,2,3,4,5,6]],
[[1,2,6],[3,4,5],[1,2,3,4,5,6]],
[[1,3,4],[2,5,6],[1,2,3,4,5,6]],
[[1,3,5],[2,4,6],[1,2,3,4,5,6]],
[[1,3,6],[2,4,5],[1,2,3,4,5,6]],
[[1,4,5],[2,3,6],[1,2,3,4,5,6]],
[[1,4,6],[2,3,5],[1,2,3,4,5,6]],
[[1,5,6],[2,3,4],[1,2,3,4,5,6]]];

#The following two functions build the correct monomial from an entry of V.

#This function creates the polynomial in 6 variables corresponding to a bracket in each of the entries of V
function make_polys(w::Vector{Int})
    if length(w) == 3
    f = (x[w[1]]-x[w[2]])*(x[w[1]]-x[w[3]])*(x[w[2]]-x[w[3]])*(x[w[1]]+x[w[2]]+x[w[3]])
    elseif length(w) ==6
        f=(sum(x[i] for i in 1:6))
    end
    return f
end

#the function below creates for each entry of V the full equation needed to cut out the Yoshida variety.
function make_eq(V::Vector)
    equ = []
    for r in V
        if length(r) == 3
            f = prod(make_polys(r[i]) for i in 1:length(r))
            append!(equ,[f])
        elseif length(r) == 6
            f_help = prod(make_polys(r[i]) for i in 1:length(r))
           # println(f_help)
            f=f_help//prod(prod(x[i]-x[j] for j in i+1:6) for i in 1:5)
            append!(equ,[f])
        end
    end
    return equ
end

#the aim is to factorise the resulting polynomials and observe how to tie them to the Mandestham invariants of E6

########
#To store the polynomials, run the above and then :
R, x = PolynomialRing(QQ,["a","b","c","d","e","f"])
f = make_eq(V);
outputfile = "polys_Y"
open(outputfile, "w" ) do file
for b in f
    println(file,b)
end
end

########
#To load the polynomials, restart new terminal with Oscar, and run
R, (a,b,c,d,e,f) = PolynomialRing(QQ,["a","b","c","d","e","f"])
inputfile ="polys_Y"
polyY=[]
open(inputfile,"r") do file
    for b in eachline(inputfile)
        push!(polyY,eval(Meta.parse(b)));
    end
end

##########
#Now we print and store the factorisation!
outputfile = "factorised_polys_Y"
open(outputfile, "w" ) do file
for b in polyY
    println(file,factor(b))
end
end

#IMPORTANT: Now we need to make an inbetween step outside of julia or oscar!
# Modify "factorized_polys_Y" such that each of the 36 linear expressions in there is substituted by x[i]. 
#This substitution has to be done in lexicographic ordering, i.e. a-b will be substituted by x[1], a-c by x[2], until e-f substituted by x[15], then start with the triples a+b+c by x[16], etc, until d+e+f substituted by x[35], and finish with a+b+c+d+e+f substituted by x[36].
#The file "factorised_polys_Yinx" that can be downloaded from the MathRepo page contains the substituted data.

#########
#Finally, we can take the factorisation and create the formal sum over the log(p_i) for the Yoshida variety with the forty Mandestham invariants a[i]
W, a = PolynomialRing(QQ, 40);
R, x = PolynomialRing(W, 36);
L=[] #this will become the list of factorised polys, where each factor is replaced by x[i]
inputfile ="factorised_polys_Yinx"
open(inputfile,"r") do file
    for b in eachline(inputfile)
        push!(L,eval(Meta.parse(b)));
    end
end
S = sum(L[i]*a[i] for i in 1:40) #this is formally the sum over the logarithm of the p_i for Y with the forty Mandestham invariants a[i]
#By way of defining the PolynomialRing this gets now sorted into coefficient of the hyperplanes x[i]=di-dj etc 
#Careful! The printed output will only contain variables of the name x. One needs to be very careful in parsing this into the two sets of variables a[1:40] and x[1:36] for further use.

#the following saves the polynomial S 
outputfile = "system_for_Y"
open(outputfile, "w" ) do file
    println(file,S)
end
#The file "system_for_Y" that can be downloaded from the MathRepo page has been modified so that the variables from the different polynomial rings have different names, and the polynomial can be read into julia and agrees with S as above.