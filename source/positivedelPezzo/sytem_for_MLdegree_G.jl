#This file computes the correct setup system for the computation of the ML degree of the Goepel parametrization.
using Oscar

#we need the following indexsets to identify the roots of E7
ba37 = bases(uniform_matroid(3,7));
ba27 = bases(uniform_matroid(2,7));
ba67 = [bases(uniform_matroid(6,7))[8-i] for i in 1:7];
ba26 = bases(uniform_matroid(2,6));

# The ordering of the variables in P^62 will then be identified as follows with specific sets of integers (which in the bracket notation give certain roots of E7)
#for i in 1:21 x[i] -> ba27[i]
#for i in 22:56 x[i+21] -> ba37[i]
#for i in 57:63 x[i+56] -> ba67[i]

#the following function takes a vector V of integers and two integers i and j and replaces the entry in V that coincides with i with the value j
#Example: replace([1,2,3],1,4) = [4,2,3].
function replace(V::Vector{Int},i::Int,j::Int)
    W=[0 for i in 1:length(V)]
    for k in 1:length(V)
        if V[k]==i
            W[k]=j
        else
            W[k]=V[k]
        end
    end
    return W
end

#The type of the polynomials is described in "The Universal Kummer Threefold" Section 6, which also explains the bracket notation.
#We first construct the polynomials denoted by g's in "The Universal Kummer Threefold". They are made of products of the variables x[57:63] (x[57]=sum(d[i]for i in 2:7)) and of disjoint triples of the x[1:21] together with three of the variables x[22:56] arising from the triples with the left out point from the first 6
#Afterwards we define the polynomials denoted by f's in "The Universal Kummer Threefold". They aer made out of products of the di+dj+dk which are the variables x[22:56]


#we first construct the 15 G's where the conic condition is on the points 1-6
G=[];
for w in ba26[1:5]
    for v in ba26[6:15]
        if w[2]!=2 && 2 in v && !(w[2] in v) #we want only 15 2-tuples of pairs, so we assume that if 3 is not in the first pair, it is in the second (possible due to reordering), and if it is in the first pair, then 2 is in the second pair
            append!(G,[[w,v]]) 
        elseif w[2]==2 && 3 in v && !(w[2] in v)
            append!(G,[[w,v]]) 
        end
    end
end
#now we need to add the third pair, which is the settheoretic complement 
for g in G
    y=[]
    for i in 1:6
        if !(i in vcat(g[1],g[2]))
            append!(y,[i])
        end
    end
    append!(g,[y])
end

#We construct the brackets of the first type. For this we need the vector G from above, which is the base for the construction.
#We start with those where 7 is not on the conic and go through the other points afterwards.
L=[] #list of the correct formulas for the G's
for g in G
    w=[[g[k][1],g[k][2]] for k in 1:length(g)];
        #we don't want to change G, so we make a new vector w, in which we substitute i for 7 in g 
    for h in g
        q = [h[1],h[2],7] #we still need to add the triple of pairs (g contains the triple of triples)
        append!(w,[q])
    end
    append!(w,[ba67[7]]) #finally we add the 6-tuple
	println(w)
    append!(L,[w])
end
L7c=deepcopy(L)

#for 6 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],6,7)
    end
    for k in 4:6
        if 6 in g[k] && 7 in g[k]
            g[k] = g[k]
        else
            g[k] =replace(g[k],7,6);
        end
    end
   g[7] = replace(g[7],6,7)
end
L6c=deepcopy(L)

#for 5 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],5,6)
    end
    for k in 4:6
        if 5 in g[k] && 6 in g[k]
            g[k] =g[k]
        else
            g[k] = replace(g[k],6,5);
        end
    end
    g[7]=replace(g[7],5,6);
end
L5c=deepcopy(L)

#for 4 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],4,5)
    end
    for k in 4:6
        if 4 in g[k] && 5 in g[k]
            g[k]=g[k]
        else
           g[k]=replace(g[k],5,4)
        end
    end
    g[7]=replace(g[7],4,5)
end
L4c=deepcopy(L)

#for 3 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],3,4)
    end
    for k in 4:6
        if 3 in g[k] && 4 in g[k]
            g[k]=g[k]
        else
            g[k]=replace(g[k],4,3)
        end
    end
    g[7]=replace(g[7],3,4)
end
L3c=deepcopy(L)

#for 2 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],2,3)
    end
    for k in 4:6
        if 2 in g[k] && 3 in g[k]
            g[k]=g[k]
        else
            g[k]=replace(g[k],3,2)
        end
    end
    g[7]=replace(g[7],2,3)
end
L2c=deepcopy(L)

#for 1 not on the conic
for g in L
    for k in 1:3
        g[k] = replace(g[k],1,2)
    end
    for k in 4:6
        if 1 in g[k] && 2 in g[k]
            g[k]=g[k]
        else
            g[k]=replace(g[k],2,1)
        end
    end
    g[7]=replace(g[7],1,2)
end
L1c = deepcopy(L)
#the whole set of the bracket conditions of this type:
L=[L1c;L2c;L3c;L4c;L5c;L6c;L7c]


#construct the set of 30 7-tuples of triples that are F 
F=[[[1, 2, 4], [1, 3, 7], [1, 5, 6], [2, 3, 5], [2, 6, 7], [3, 4, 6], [4, 5, 7]], [[1, 2, 4], [1, 3, 6], [1, 5, 7], [2, 3, 5], [2, 6, 7], [3, 4, 7], [4, 5, 6]],[[1, 2, 4], [1, 3, 7], [1, 5, 6], [2, 3, 6], [2, 5, 7], [3, 4, 5], [4, 6, 7]],[[1, 2, 4], [1, 3, 5], [1, 6, 7], [2, 3, 6], [2, 5, 7], [3, 4, 7], [4, 5, 6]],[[1, 2, 4], [1, 3, 6], [1, 5, 7], [2, 3, 7], [2, 5, 6], [3, 4, 5], [4, 6, 7]],[[1, 2, 4], [1, 3, 5], [1, 6, 7], [2, 3, 7], [2, 5, 6], [3, 4, 6], [4, 5, 7]],[[1, 2, 5],[1, 3, 7], [1, 4, 6], [2, 3, 4], [2, 6, 7], [3, 5, 6], [4, 5, 7]],[[1, 2, 5], [1, 3, 6], [1, 4, 7], [2, 3, 4], [2, 6, 7], [3, 5, 7], [4, 5, 6]],[[1, 2, 5], [1, 3, 7], [1, 4, 6], [2, 3, 6], [2, 4, 7], [3, 4, 5], [5, 6, 7]],[[1, 2, 5], [1, 3, 4], [1, 6, 7], [2, 3, 6], [2, 4, 7], [3, 5, 7], [4, 5, 6]],[[1, 2, 5], [1, 3, 6], [1, 4, 7], [2, 3, 7], [2, 4, 6], [3, 4, 5], [5, 6, 7]],[[1, 2, 5], [1, 3, 4], [1, 6, 7], [2, 3, 7], [2, 4, 6], [3, 5, 6], [4, 5, 7]],[[1, 2, 6], [1, 3, 7], [1, 4, 5], [2, 3, 4], [2, 5, 7], [3, 5, 6], [4, 6, 7]],[[1, 2, 6], [1, 3, 5], [1, 4, 7], [2, 3, 4], [2, 5, 7], [3, 6, 7], [4, 5, 6]],[[1, 2, 6], [1, 3, 7], [1, 4, 5], [2, 3, 5], [2, 4, 7], [3, 4, 6], [5, 6, 7]],[[1, 2, 6], [1, 3, 4], [1, 5, 7], [2, 3, 5], [2, 4, 7], [3, 6, 7], [4, 5, 6]],[[1, 2, 6], [1, 3, 5], [1, 4, 7], [2, 3, 7], [2, 4, 5], [3, 4, 6], [5, 6, 7]],[[1, 2, 6], [1, 3, 4], [1, 5, 7], [2, 3, 7], [2, 4, 5], [3, 5, 6], [4, 6, 7]],[[1, 2, 7], [1, 3, 6], [1, 4, 5], [2, 3, 4], [2, 5, 6], [3, 5, 7], [4, 6, 7]],[[1, 2, 7], [1, 3, 5], [1, 4, 6], [2, 3, 4], [2, 5, 6], [3, 6, 7], [4, 5, 7]],[[1, 2, 7], [1, 3, 6], [1, 4, 5], [2, 3, 5], [2, 4, 6], [3, 4, 7], [5, 6, 7]],[[1, 2, 7], [1, 3, 4], [1, 5, 6], [2, 3, 5], [2, 4, 6], [3, 6, 7], [4, 5, 7]],[[1, 2, 7], [1, 3, 5], [1, 4, 6], [2, 3, 6], [2, 4, 5], [3, 4, 7], [5, 6, 7]],[[1, 2, 7], [1, 3, 4], [1, 5, 6], [2, 3, 6], [2, 4, 5], [3, 5, 7], [4, 6, 7]],[[1, 2, 3], [1, 4, 7], [1, 5, 6], [2, 4, 5], [2, 6, 7], [3, 4, 6], [3, 5, 7]],[[1, 2, 3], [1, 4, 6], [1, 5, 7], [2, 4, 5], [2, 6, 7], [3, 4, 7], [3, 5, 6]],[[1, 2, 3], [1, 4, 7], [1, 5, 6], [2, 4, 6], [2, 5, 7], [3, 4, 5], [3, 6, 7]],[[1, 2, 3], [1, 4, 5], [1, 6, 7], [2, 4, 6], [2, 5, 7], [3, 4, 7], [3, 5, 6]],[[1, 2, 3], [1, 4, 6], [1, 5, 7], [2, 4, 7], [2, 5, 6], [3, 4, 5], [3, 6, 7]],[[1, 2, 3], [1, 4, 5], [1, 6, 7], [2, 4, 7], [2, 5, 6], [3, 4, 6], [3, 5, 7]]]

#the full system of the brackets
Sy=vcat(F,L);
#make a dictionary so that the order fits with how E7 is given (see comment at the top)
VarDict=Dict{Vector,Int}()
for i in 1:21
    VarDict[ba27[i]]=i
end
for i in 22:56
    VarDict[ba37[i-21]]=i
end
for i in 57:63
    VarDict[ba67[i-56]]=i
end

#we construct the matrix A, that contains the information of the monomial map from P^62 to P^134
A=zeros(QQ,63,135);
for i in 1:135
    for g in Sy[i]
        A[VarDict[g],i]=1
    end
end

#Some sanity checks with properties that A satisfies by "The Universal Kummer Threefold"
boo=true
for i in 1:63
    boo = boo && (sum(A[i,j] for j in 1:135) == 15)
end
for j in 1:135
    boo = boo && (sum(A[i,j] for i in 1:63) == 7)
end
boo

#save A
outputfile = "matrix_for_G"
open(outputfile, "w" ) do file
for i in 1:63
    println(file,A[i,:])
end
end
#to use the file, we need to manually open it and delete the leading entries "fmpq" before [
#This is already done for the file "matroix_for_G" that can be downloaded from the MathRepo page



