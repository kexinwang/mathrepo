## This file containes the code to compute the number of critical points of the log likelihood function corresponding to the hyperplane arrangement E6, or equivalently the ML degree of the very affine variety given by the hyperplane arrangement E6 in 5-dimensional projective space
# Set-up/How to use:  The coordinates in 5-dimensional projective space are d_1 to d_5.
    # - Decide which of the d_i you want to fix (you can set one of the d_i to 1 because of projectivity). The default in the code is d_1=1.
    # - The not-determined coordinates d_i  are collected in the multivariable x.
    # - By using the intermediate vector d, we can choose exactly which coordinate is set to one, by placing the 1 between the relevant variables x[i].
    # - The log likelihood function has 36 terms in the d[i] (given by the equations of the hyperplanes) with 36 parameters.
    # - These parameters have to sum to zero, so while we have 36 terms in the log likelihood function, we only have 35 parameters, collected in s[1:35], as the last one is the negative sum of all.
using HomotopyContinuation
@var x[1:5] s[1:35]
d = vcat([1],x)
L = log(d[1] - d[2])*s[1] + log(d[1] - d[3])*s[2] + log(d[1] - d[4])*s[3] + log(d[1] - d[5])*s[4] + log(d[1] - d[6])*s[5]  + log(d[2] - d[3])*s[6] + log(d[2] - d[4])*s[7] + log(d[2] - d[5])*s[8] + log(d[2] - d[6])*s[9] + log(d[3] - d[4])*s[10] + log(d[3] - d[5])*s[11] + log(d[3] - d[6])*s[12]   + log(d[4] - d[5])*s[13] + log(d[4] - d[6])*s[14] + log(d[5] - d[6])*s[15] + log(d[1] + d[2] + d[3])*s[16] + log(d[1] + d[2] + d[4])*s[17] + log(d[1] + d[2] + d[5])*s[18] + log(d[1] + d[2] + d[6])*s[19] + log(d[1] + d[3] + d[4])*s[20] + log(d[1] + d[3] + d[5])*s[21] + log(d[1] + d[3] + d[6])*s[22]  + log(d[1] + d[4] + d[5])*s[23] + log(d[1] + d[4] + d[6])*s[24] + log(d[1] + d[5] + d[6])*s[25] + log(d[2] + d[3] + d[4])*s[26] + log(d[2] + d[3] + d[5])*s[27] + log(d[2] + d[3] + d[6])*s[28] + log(d[2] + d[4] + d[5])*s[29] + log(d[2] + d[4] + d[6])*s[30]  + log(d[2] + d[5] + d[6])*s[31] + log(d[3] + d[4] + d[5])*s[32] + log(d[3] + d[4] + d[6])*s[33]   + log(d[3] + d[5] + d[6])*s[34]  + log(d[4] + d[5] + d[6])*s[35]  + log(d[1] + d[2] + d[3] + d[4] + d[5] + d[6])*(-sum(s[j] for j in 1:35))
F = System(differentiate(L,x),parameters = s)
r=monodromy_solve(F)
c=certify(F, r)