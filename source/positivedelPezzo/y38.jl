# Requires HomotopyContinuation.jl version 2.9.0 or higher
using HomotopyContinuation, Combinatorics, LinearAlgebra, Dates, Random
const HC = HomotopyContinuation

@var x[1:8]

M = [1 0 0 1 1 1 1 1; 0 1 0 1 x[1] x[2] x[3] x[4]; 0 0 1 1 x[5] x[6] x[7] x[8]]

n = size(M, 2)

S = collect(combinations(collect(1:n), 3))
nonconstant_minors = filter(s -> !ModelKit.is_number(s), [det(M[:, s]) for s in S])

V = zeros(Expression, 6, 8)
for i in 1:8
  c = 1
  for j in 1:3
    for k in j:3
      V[c, i] = M[j, i] * M[k, i]
      c = c + 1
    end
  end
end
conics = [det(V[:, s]) for s in combinations(collect(1:size(V, 2)), 6)]


@var s[1:length(nonconstant_minors)+length(conics)]

expressions = HomotopyContinuation.differentiate(sum(s .* log.([nonconstant_minors; conics])), x)
F = System(expressions; parameters=s)


p = HomotopyContinuation.read_parameters("y38_parameters.txt")
compute_time_limit = 18 * 60 * 60.0 # 18 hours
r = monodromy_solve(F, p; compile=:all, timeout=compute_time_limit)

dcs = distinct_certified_solutions(F, S, p; max_precision=512, threading=true, show_progress=true)

sols = solutions(dcs)
unix_time = floor(Int64, time())
HomotopyContinuation.write_solutions("y38_solutions_p_$(unix_time).txt", sols)

