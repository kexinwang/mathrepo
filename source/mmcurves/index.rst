===================================================
Maximal Mumford Curves from Planar Graphs
===================================================

| This page contains auxiliary files and code to the article:
| Mario Kummer, Bernd Sturmfels and Raluca Vlad: Maximal Mumford Curves from Planar Graphs
| ARXIV: `https://arxiv.org/abs/2404.11838 <https://arxiv.org/abs/2404.11838>`_



**Abstract:**
A curve of genus :math:`g` is maximal Mumford (MM) if it has :math:`g+1` ovals and :math:`g` tropical cycles. We construct full-dimensional families of MM curves in the Hilbert scheme of canonical curves. This rests on first-order deformations of graph curves whose graph is planar.


We implemented an algorithm that computes the tangent cone of first order deformations of a graph curve that lift to MM-curves.
We provide a complete list of these tangent cones for graph curves of small genus.
We also provide code computing the number of connected components of a real curve, which we used for numerical confirmations.


|


The MM-Cone
-----------

Let :math:`C_G \subset \mathbb{P}^{g-1}` be a graph curve whose dual graph :math:`G` is a planar trivalent graph of genus :math:`g` with :math:`2g-2` vertices.
For any edge pairing :math:`\rho` of :math:`G`, the tangent space to the Hilbert scheme at :math:`C_G` contains a full-dimensional open cone

.. math::
  \mathbb{R}^{g^2-1} \times \mathbb{R}_{>0}^{3g-3}

of first order deformations of :math:`C_G` that lift to smooth curves whose topology corresponds to :math:`\rho`.
We implemented an algorithm that inputs a graph :math:`G` and an edge pairing :math:`\rho`, and outputs a basis for the :math:`\mathbb{R}_{>0}^{3g-3}` factor of the corresponding tangent cone.
The `Macaulay2 <https://macaulay2.com>`_ code can be downloaded here: :download:`tangent-directions.m2 <tangent-directions.m2>`.


The *MM-cone* refers to this tangent cone when the edge pairing :math:`\rho` comes from the unique double cover of :math:`G` by :math:`g+1` cycles.
In this case, the first order deformations in our cone lift to MM-curves.



Example:
^^^^^^^^

Here, we explain an example of input and output from our algorithm.
Consider the following planar trivalent graph :math:`G` of genus :math:`4`, and its double cover by its :math:`5` *face cycles*.


.. image:: edge-pairing.png
   :width: 45%
   :align: right

.. image:: graph.png
   :width: 45%


The **input** is the list of edges of our graph :math:`G` and the edge pairing corresponding to the double cover above.

Per our paper, an *edge pairing* :math:`\rho` is defined as follows: for every edge :math:`e = \{i,j\}` with adjacent edges :math:`e_i, e_i'` and :math:`e_j, e_j'`, respectively, :math:`\rho(e)` is a partition of these four adjacent edges into either

.. math::
  \{e_i, e_j\} \sqcup \{e_i', e_j'\} \;\;\; \text{ or } \;\;\; \{e_i, e_j'\} \sqcup \{e_i', e_j\}.

In our code, such a partition :math:`\rho(e)` is encoded via either one of the pairs in it.
Thus, an edge pairing :math:`\rho` is encoded as a list of pairs :math:`\rho(e)`, where the order of the edges :math:`e` is the same as in the inputted list of edges of the graph.

The edge pairing associated to the double cover above is the one where each :math:`\rho(e)` contains edges belonging to the same face cycle of :math:`G`.


.. code-block:: macaulay2

    Edges = {{0,1}, {1,2}, {2,0}, {3,4}, {4,5}, {5,3}, {0,3}, {1,4}, {2,5}};

    EdgePairing = {{1,2}, {0,2}, {0,1}, {4,5}, {3,5}, {3,4}, {2,5}, {1,4}, {1,4}};


The **output** is:

- A list :math:`L` of generators of the ideal :math:`I \subset S:= \mathbb{Q}[x_0, x_1,\dots, x_{g-1}]` of the graph curve :math:`C_G`.

- A list of :math:`3g-3` tangent directions :math:`\eta_e`, where each :math:`\eta_e` deforms the node of :math:`C_G` corresponding to the edge :math:`e` according to the inputted edge pairing. Each tangent vector is thought of as an element of :math:`\operatorname{Hom}_S(I, S/I)_0` and is encoded as a list of size :math:`\#L`.

- The sum :math:`\eta` of all the tangent vectors :math:`\eta_e`, which is a vector in the interior of our MM-cone.


.. code-block:: text

    Generators of graph curve ideal:
    {x_0^2+x_0*x_1+x_0*x_2+x_0*x_3, x_1*x_2*x_3}

    Tangent vectors:
    matrix {{0, x_0*x_1^2},
            {0, x_0*x_2^2},
            {0, x_0*x_3^2},
            {0, -x_0*x_1^2-x_1^3-x_1^2*x_2-x_1^2*x_3},
            {0, -x_0*x_2^2-x_1*x_2^2-x_2^3-x_2^2*x_3},
            {0, -x_0*x_3^2-x_1*x_3^2-x_2*x_3^2-x_3^3},
            {-x_1*x_3, 0},
            {-x_1*x_2, 0},
            {-x_2*x_3, 0}}

    Sum of tangent vectors:
    {-x_1*x_2-x_1*x_3-x_2*x_3,
     -x_1^3-x_1^2*x_2-x_1*x_2^2-x_2^3-x_1^2*x_3-x_2^2*x_3-x_1*x_3^2-x_2*x_3^2-x_3^3}


The first order deformation of :math:`C_G` along :math:`\eta` lifts to an MM-curve.


.. image:: mm-curve.png
   :width: 45%
   :align: right

.. image:: graph-curve.png
   :width: 45%




How to determine the MM edge pairing?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Consider any planar trivalent :math:`3`-connected graph :math:`G` of genus :math:`g`.
The graph :math:`G` admits a unique double cover by :math:`g+1` cycles via its face cycles (including the outer rim).

As above, the MM-cone corresponds to the edge pairing :math:`\rho` where each pair :math:`\rho(e)` belongs to the same face cycle.
Thus, the edge pairing that gives the MM-cone can be read easily from any planar embedding of our graph :math:`G`.

The function ``tutte_lifting`` in `polymake <https://polymake.org/doku.php/start>`_ provides such a planar embedding when :math:`G` contains a :math:`3`-cycle.
Since our graphs are always trivalent, one can introduce a :math:`3`-cycle by replacing a vertex by a :math:`3`-cycle.

.. code-block:: perl

    $edges = [[0,1], [1,2], [2,0], [3,4], [4,5], [5,3], [0,3], [1,4], [2,5]];

    $G = graph_from_edges($edges);

    $T = tutte_lifting($G);

    print $T->VISUAL;



|


Data for Low Genus
------------------

There are :math:`1`, :math:`2`, :math:`4`, :math:`14`, and :math:`57` trivalent :math:`3`-connected graphs of genus :math:`3`, :math:`4`, :math:`5`, :math:`6`, and :math:`7`, respectively.
Out of these graphs, :math:`1`, :math:`1`, :math:`2`, :math:`5`, and :math:`14`, respectively, are planar.
We ran our code and computed the MM-cones for all these planar graphs.
The input/output data can be downloaded here: :download:`mm-cones-low-genus.txt <mm-cones-low-genus.txt>`.


|


Numerical Confirmations
-----------------------

We used numerical methods to confirm that our lifts have the expected number of connected components.
We used code written by Jonathan Hauenstein that computes the number of connected components of a complete intersection of three hypersurfaces in :math:`\mathbb{RP}^4`.
The code runs in `MATLAB <https://www.mathworks.com/products/matlab.html>`_ and calls `Bertini <https://bertini.nd.edu>`_.
It can be downloaded here: :download:`connectivity-computation.zip <connectivity-computation.zip>`.


For another numerical approach to computing the connectivity of a smooth algebraic curve, using gradient flows, see the article
`Smooth connectivity in real algebraic varieties <https://arxiv.org/abs/2405.18578>`_ by Cummings, Hauenstein, Hong, and Smyth.


Example:
^^^^^^^^

Continuing the previous example, we check numerically that the smooth genus :math:`4` lift :math:`V(I + t * \eta) \subset \mathbb{P}^3` has :math:`5` connected components, hence is maximal, for small :math:`t`.

For **input**, set:

- the parameter :math:`t` in the file :math:`\text{runTest.m}`, and

- the three equations cutting out the curve and their total derivatives in the files :math:`\text{input_slice}` and :math:`\text{input_crit}`. (In our example, the third equation simply cuts out a hyperplane :math:`\mathbb{P}^3 \subset \mathbb{P}^4`.)

.. code-block:: matlab

    f1 = w * (w + x + y + z) + t * (- x * y - x * z - y * z);

    f2 =  x * y * z
        + t * (- x^3 - x^2 * y - x * y^2 - y^3 - x^2 * z - y^2 * z - x * z^2 - y * z^2 - z^3);

    f3 = v;


    g1 =  l1 * (2*w + x + y + z) + l2 * (w - t * (y + z))
        + l3 * (w - t * (x + z)) + l4 * (w - t * (x + y));

    g2 =  l2 * (y*z - t * (3*x^2 + y^2 + z^2 + 2*x*y + 2*x*z))
        + l3 * (x*z - t * (x^2 + 3*y^2 + z^2 + 2*x*y + 2*y*z))
        + l4 * (x*y - t * (x^2 + y^2 + 3*z^2 + 2*x*z + 2*y*z));

    g3 = l0;


To run the code, follow these steps in MATLAB:

- Set ``ParameterHomotopy`` to :math:`1` in the file :math:`\text{input_slice}`, and run

::

  system('bertini input_slice');


- Copy the file :math:`\text{start_parameters}` to :math:`\text{start_parameters_slice}` and the file :math:`\text{finite_solutions}` to :math:`\text{start_slice}`.


- Set ``ParameterHomotopy`` back to :math:`2`.


- Repeat these three steps for :math:`\text{slice}` replaced by :math:`\text{crit}`.


Finally, running ``runTest`` **outputs** the number of connected components of our real curve:

.. code-block:: text

    Number of connected components: 5


|


| **Credits**
| Project page created: 22/04/2024
| Software used: Bertini (Version 1.6), Macaulay2 (Version 1.22), MATLAB (Version R2023b), Polymake (Version 4.11)
| System setup used: MacBook Pro with macOS Sonoma 14.4.1, Apple M1 Processor, Memory 16 GB
| Corresponding author of this page: Raluca Vlad, raluca_vlad@brown.edu
| Project contributors: Mario Kummer, Bernd Sturmfels and Raluca Vlad
| Other code contributors: Jonathan Hauenstein
| License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)
| License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)

