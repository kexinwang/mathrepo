=======================================================
The Gaussian entropy map in valued fields
=======================================================

| This page contains auxiliary files to the paper:
| El Maazouz, Yassine : The Gaussian entropy map in valued fields
| In: Algebraic statistics, 13 (2022) 1, p. 1-18
| DOI: `10.2140/astat.2022.13.1 <https://msp.org/astat/2022/13-1/p01.xhtml>`_ ARXIV: https://arxiv.org/abs/2101.00767 CODE: https://mathrepo.mis.mpg.de/GaussianEntropyMap


Abstract:
We exhibit the analog of the entropy map for multivariate Gaussian distributions on local fields. As in the real case, the image of this map lies in the supermodular cone and it determines the distribution of the push forward under the valuation map. In general, this map can be defined for non-archimedian valued fields whose valuation group is an additive subgroup of the real line, and it remains supermodular. We also explicitly compute the image of this map in dimension 3.


.. image:: 1.png
  :width: 150


.. image:: 2.png
  :width: 150


.. image:: 3.png
  :width: 150


.. image:: 4.png
  :width: 150


.. image:: 5.png
  :width: 150





This repository includes our code to compute the entropy map (Section 3) when the field :math:`K` is the field of Puiseux series in one variable with real coefficients. It also includes Polymake code to compute the image of the entropy map in dimension 3 (Section 5).

Our code is written in `Julia <https://julialang.org/>`_ (v1.6.2) (using the computer algebra system `Oscar <https://oscar.computeralgebra.de/>`_ (v0.5.2)) and in `Polymake <https://polymake.org/doku.php>`_ (v4.3)


The following jupyter notebook contains code to compute entropy:

.. toctree::
  :maxdepth: 1
  :glob: 

  code



We also provide polymake code to visualize the image of the entropy map in :math:`d=3` :

.. toctree::
   :maxdepth: 2

   polymakeCode.rst













Project page created: 17/01/2022.

Project contributors:  Yassine El Maazouz.

Corresponding author of this page: Yassine El Maazouz, yassine.el-maazouz@berkeley.edu

Software used: Julia (Version 1.6.2), Oscar (v0.5.2), Polymake(v4.3).
