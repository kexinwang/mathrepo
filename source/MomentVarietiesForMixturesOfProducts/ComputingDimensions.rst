####################
Computing Dimensions
####################

This page contains the code we used to compute dimensions of :math:`\sigma_r(\mathcal{M}_{n,d})` and :math:`\sigma_r(\mathcal{M}_{n,\lambda})`. We implemented three different approaches to computing these dimesnions: by computing the Jacobian matrix directly, by using the Terracini lemma, and via the :math:`\verb|Macaulay2|` package "NumericalImplicitization".

The below code uses the first approach for :math:`\sigma_r(\mathcal{M}_{n,d})`.

.. code-block:: macaulay2
	
	restart
	--for the variety (r,n,d)
	r=2; n=5; d=2; --these parameters will be changed depending on moment variety
	las=compositions(n, d); len=length(las); L={}; 
	for i from 0 to len-1 do L=append(L,m_(las_i)); R=QQ[L]

	--define parameters:
	S=QQ[toList(x_(1,1,1) ..< y_(r+1,n+1,d+1))]

	--create parametrization for the secant variety:
	M=apply(toList({0..length(L)-1}_0), i->0);
	for k from 1 to r do
	{Mx={}; for i from 0 to len-1 do (prod=1; for j from 0 to n-1 do 
    	(if las_i_j!=0 then prod=prod*x_(k,j+1,las_i_j);); Mx=append(Mx,prod););
	M=M+Mx;}

	--form the Jacobian: 
	nums={}; nums=apply(length(generators(S)), i -> random(1,100));
	toNums=map(QQ,S,nums);
	A=toNums(jacobian(ideal(M)));
	
	--rank of this Jacobian matrix will be one more than the projective dimension of the variety:
	rank A 

	--if terminates, the below will give the implicit description of the moment variety:
	f=map(S,R,M); 
	I=ker f; 


For the variety :math:`\sigma_r(\mathcal{M}_{n,\lambda})`, replace the first 5 lines of the code above with:

.. code-block:: macaulay2

	restart 
	--for the variety (r,n,lambda)
	r=2; n=5; d=2; --vary as needed
	la={1,1,0,0,0}; --vary the partition as needed
	las=toList(set(permutations(la))); len=length(las); L={}; 
	for i from 0 to len-1 do L=append(L,m_(las_i)); R=QQ[L]; 


For large values of :math:`r`, it is more efficient to use the Terracini lemma to compute the Jacobian. The below code implements this approach.

.. code-block:: macaulay2

	r=2; n=5; d=4;
	las=compositions(n, d); len=length(las); L={}; 
	for i from 0 to len-1 do L=append(L,m_(las_i)); R=QQ[L]

	--first, assume r=1:
	S=QQ[toList(x_(1,1) ..< y_(n+1,d+1))]
	M={}; for i from 0 to len-1 do (prod=1; for j from 0 to n-1 do 
    	(if las_i_j!=0 then prod=prod*x_(j+1,las_i_j);); 
	M=append(M,prod);)

	nums={}; nums=apply(length(generators(S)), i -> random(1,100));
	toNums=map(QQ,S,nums); J=jacobian(ideal(M));
	A=toNums(J);
	rank A

	--now adjust r and compute the true Jacobian:
	for k from 1 to r-1 do
	{
	 nums={}; nums=apply(length(generators(S)), i -> random(1,100));
	 toNums=map(QQ,S,nums);
	 A1=toNums(J); 
	 A=A||A1;
	}

	--rank of this matrix will be one more than the projective dimension of the variety:
	rank A 


Finally, the below code implements the "NumericalImplicitization" approach, along with some examples.

:download:`dimensions_numerical.m2 <dimensions_numerical.m2>`
		  