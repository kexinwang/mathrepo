#################
Computing Degrees
#################

This page contains degree computations for :math:`\sigma_2(\mathcal{M}_{5,3})`, :math:`\sigma_2(\mathcal{M}_{4,4})`, :math:`\sigma_2(\mathcal{M}_{6,(1\;1\;1)})`, :math:`\sigma_3(\mathcal{M}_{6,(1\;1\;1)})`.


| The secant variety :math:`\sigma_2(\mathcal{M}_{5,3})` has degree 3225, computed numerically in :math:`\verb|Julia|`: :download:`code <(2,5,3)-degree-computuation.jl>`.
| The secant variety :math:`\sigma_2(\mathcal{M}_{4,4})` has degree 8650, computed numerically in :math:`\verb|Julia|`: :download:`code <(2,4,4)-degree-computation.jl>`.
| The secant variety :math:`\sigma_3(\mathcal{M}_{2,(1\;1\;1)})` has degree 465, computed numerically in :math:`\verb|Julia|`: :download:`code <(2,6,(1,1,1))-numerical-degree-computation.jl>` and symbolically  in :math:`\verb|Macaulay2|`: :download:`code <(2,6,(1,1,1))-symbolic-degree-computaton.m2>`.
| The secant variety :math:`\sigma_3(\mathcal{M}_{3,(1\;1\;1)})` has degree 80, computed numerically in :math:`\verb|Julia|`: :download:`code <(3,6,(1,1,1))-numerical-degree-computation.jl>`.