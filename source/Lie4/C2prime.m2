R = QQ[f1,f2,f3,f4,f5,k1,k2,k3,k4,k5,k6,m, a121,a122,a123,a124,
   a131,a132,a133,a134,a141,a142,a143,a144,a231,a232,a233,a234,
   a241,a242,a243,a244,a341,a342,a343,a344, MonomialOrder => Eliminate 12];
   
I = ideal(a121-f1*f4*k5-f2*f5*k5+f1*k1+f2*k3+f3*k5, a122+f4*k5-k1, a132+f4*k6-k2, 
a142+f4^2*k5+f4*f5*k6+f4*k4-f5*k2, a232+f1*f4*k6-f2*f4*k5-f1*k2+f2*k1+f4*m, 
a242+f1*f4^2*k5+f1*f4*f5*k6+f1*f4*k4-f1*f5*k2-f3*f4*k5+f4*f5*m+f3*k1,
a342+f2*f4^2*k5+f2*f4*f5*k6+f2*f4*k4-f2*f5*k2-f3*f4*k6-f4^2*m+f3*k2,
a123+f5*k5-k3,  a133+f5*k6-k4, a143+f4*f5*k5+f5^2*k6-f4*k3+f5*k1,
a131-f1*f4*k6-f2*f5*k6+f1*k2+f2*k4+f3*k6, a233+f1*f5*k6-f2*f5*k5-f1*k4+f2*k3+f5*m,
a243+f1*f4*f5*k5+f1*f5^2*k6-f1*f4*k3+f1*f5*k1-f3*f5*k5+f5^2*m+f3*k3,
a343+f2*f4*f5*k5+f2*f5^2*k6-f2*f4*k3+f2*f5*k1-f3*f5*k6-f4*f5*m+f3*k4,
a144-f4*k5-f5*k6-k1-k4, a244-f1*f4*k5-f1*f5*k6-f1*k1-f1*k4+f3*k5-f5*m,	
a234-f1*k6+f2*k5-m, a344-f2*f4*k5-f2*f5*k6-f2*k1-f2*k4+f3*k6+f4*m,
a141-f1*f4^2*k5-f1*f4*f5*k6-f2*f4*f5*k5-f2*f5^2*k6 -f1*f4*k4+f1*f5*k2+f2*f4*k3
        -f2*f5*k1+f3*f4*k5+f3*f5*k6+f3*k1+f3*k4,
a231-f1^2*f4*k6+f1*f2*f4*k5-f1*f2*f5*k6+f2^2*f5*k5+f1^2*k2-f1*f2*k1
    +f1*f2*k4+f1*f3*k6-f1*f4*m-f2^2*k3-f2*f3*k5-f2*f5*m+f3*m, a124-k5, a134-k6,
a241-f1^2*f4^2*k5-f1^2*f4*f5*k6-f1*f2*f4*f5*k5-f1*f2*f5^2*k6-f1^2*f4*k4+f1^2*f5*k2
    +f1*f2*f4*k3-f1*f2*f5*k1+2*f1*f3*f4*k5+f1*f3*f5*k6-f1*f4*f5*m+f2*f3*f5*k5
    -f2*f5^2*m+f1*f3*k4-f2*f3*k3-f3^2*k5+f3*f5*m,
a341-f1*f2*f4^2*k5-f1*f2*f4*f5*k6-f2^2*f4*f5*k5-f2^2*f5^2*k6-f1*f2*f4*k4
    +f1*f2*f5*k2+f1*f3*f4*k6+f1*f4^2*m+f2^2*f4*k3-f2^2*f5*k1+f2*f3*f4*k5
    +2*f2*f3*f5*k6+f2*f4*f5*m-f1*f3*k2+f2*f3*k1-f3^2*k6-f3*f4*m);

time C2 = ideal selectInSubring( 1, gens gb(I) );
betti mingens C2
codim C2, degree C2
