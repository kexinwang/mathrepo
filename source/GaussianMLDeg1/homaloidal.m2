-- A symmetric matrix in Sym_2(C^m) can be flattened and represented as a vector in C^N
-- N = binom(m+1,2)
-- We need to keep track of how to go back and forth between matrix and vector representations 
-- This is done in the function CreateSym2Ring

CreateSym2Ring = (Size) -> (
-- Input: The size, denoted by m, of the matrices in Sym_2(C^m)
-- Output: Sets the values of the global variables m, R, inds, ordering:
-- m is the size of the matrices
-- R = C[Sym_2(C^m)] is the polynomial ring of Sym_2(C^m)
-- inds is list such that inds_i = (a,b) means that i'th entry of a vector in C^N corresponds to the (a,b) of a matrix in Sym_2(C^m)
-- ordering is a matrix such that ordering_(a-1,b-1) = i means that the (a,b) entry of matrix in Sym_2(C^m) corresponds to the i'th entry of a vector in C^N
   m=Size;
   inds = flatten apply(m, i -> apply(i+1, j -> (j+1,i+1)));
   ordering = matrix (apply(m, i-> apply(m, j -> position(inds, ind -> ind == (min(i,j)+1,max(i,j)+1)))));
   R = QQ[apply(inds, I -> s_I)];
   use R;
   )

toVector = (mat) -> (
-- Input: Symmetric matrix of size m
-- Output: The corresponding vector in C^N
   matrix {apply(inds, ind -> mat_(ind#0 -1, ind#1 - 1 ))}
   )

toMatrix = (vec) -> (
-- Input: vector in C^N
-- Output: the corresponding symmetric matrix
    v := flatten entries vec;
    matrix (apply(m, i-> apply(m, j -> v_(ordering_(i,j)))))
    )

sym2gradient= (func) -> (
-- Input: A rational function (an element of frac R)
-- Output: the gradient of func in Sym_2(C^m) with the scaling of 1/2 discussed in equation (10) of  Section 5.1
    f := sub(func, frac R);
    p := numerator f; gradp := diff(vars R, p);
    q := denominator f; gradq := diff(vars R, q);
    toMatrix (sub((1/q^2), frac R)*sub((q*gradp - p*gradq), frac R)*(diagonalMatrix apply(inds, ind -> 
	    if ind_0 == ind_1 then 1 else 1/2 ))) --quotient rule for f=p/q, scaled by 1/(2-delta(i,j))
    )

verifyHomaloidal=(Phi) -> (
-- Input: An element of frac C[Sym_2(C^m)], a candidate solution to the homaloidal PDE
-- Output: Boolean specifying if the input Phi is a solution to the homaloidal PDE 
Psi := -1/(Phi)*(sym2gradient Phi);
det(Psi) == Phi

)

varietyAssociatedTo=(Phi) -> (
-- Input: An element of frac C[Sym_2(C^m)], a solution to the homaloidal PDE
-- Output: The ideal of the variety X associated to Phi
--Psi := -1/(Phi)*(sym2gradient Phi);
--ker (map(R,R, toVector sub(-denominator(Phi)^2*Phi*Psi, R))) 
ker (map(R,QQ[apply(inds, I -> k_I)], toVector sub(-denominator(Phi)^2*(sym2gradient Phi), R)))
)



--Define a function for taking subdeterminants of S
-- Input: index set, a subset of {0,..., m-1}
-- Output: The corresponding subdeterminant of S
detS = (l) -> det(S_l^l)

detS = (l) -> (
    lplus := apply(l, a -> a-1);
    det(S_lplus^lplus)
)


printMLE=(Phi) -> (
-- Input: An element of frac C[Sym_2(C^m)], a solution to the homaloidal PDE
-- Output: The MLE map as a list
Psi := -1/(Phi)*(sym2gradient Phi);
netList apply(#inds, k -> (((inds#k#0 , inds#k#1 ) ), factor (Psi)_(inds#k#0 -1, inds#k#1 -1) ))
)

